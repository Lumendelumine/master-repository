name="MEIOU and Taxes RMG-Compatibility Module v2.0"
path="mod/MEIOUandTaxesRMG"
dependencies=
{
	"MEIOU and Taxes 1.23"
}
tags=
{
	"Map" 
	"MEIOU and Taxes"
}
supported_version = "1.15"
picture="MEIOUandTaxesRMG.jpg"
