country_decisions = {

	form_qing_dynasty = { # Slightly changed from the original form manchu dynasty decision
		major = yes
		potential = {
			tag = MCH
			NOT = { exists = QNG }
			NOT = { has_country_flag = qing_dynasty_formed }
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 5
				}
			}
		}
		allow = {
			owns = 707
			owns = 713
			owns = 2248
			owns = 2249
			is_at_war = no
			is_subject = no
		}
		effect = {
			ming_china = { add_claim = QNG } # Mod
			random_owned_province = { add_base_tax = 1 }
			change_variable = { which = "centralization_decentralization" value = -1 }
			set_capital = 2249
			add_prestige = 10 # Mod
			change_tag = QNG
			if = {
				limit = {
					technology_group = steppestech
				}
				change_technology_group = chinese
				change_unit_type = chinese
			}
			change_government = despotic_monarchy_1
			set_country_flag = qing_dynasty_formed
			country_event = { id = prov_admin.1 days = 1 }
		}
		ai_will_do = {
			factor = 1
		}
	}

	QNG_middle_kingdom = {  # added by Pax Sinica
		major = yes
		potential = {
			tag = QNG
			has_country_flag = qing_dynasty_formed
			NOT = { has_country_flag = qing_got_mandate_of_heaven }
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 10
				}
			}
			NOT = { capital = 708 } # In order to avoid to fire in the late state date
		}
		allow = {
			owns = 708		# Beijing
			owns = 701		# Kaifeng
			owns = 2492		# Shandong
			owns = 2468		# Taiyuan
			owns = 2252		# Xi'an
			is_at_war = no
			is_subject = no
		}
		effect = {
			ming_china = { add_claim = QNG }
			random_owned_province = { add_base_tax = 1 }
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 3650
			}
			set_capital = 708 # Beijing
			add_prestige = 10
			set_country_flag = qing_got_mandate_of_heaven
			country_event = { id = prov_admin.1 days = 1 }
		}
		ai_will_do = {
			factor = 1
		}
	}	

} #End of country decisions
