government = native_council
mercantilism = 0.1
primary_culture = abenaki
religion = totemism
technology_group = north_american
capital = 986

1000.1.1   = {
	set_variable = { which = "centralization_decentralization" value = 5 }
	set_global_flag = native_nations_enabled
}

1356.1.10  = {
	monarch = {
		name = "Native Council"
		adm = 3
		dip = 3
		mil = 3
		regent = yes
	}
}
