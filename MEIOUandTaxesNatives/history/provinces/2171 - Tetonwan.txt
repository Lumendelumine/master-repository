# No previous file for Tetonwan

culture = lakota
religion = totemism
capital = "Tetonwan"
trade_goods = crops
hre = no discovered_by = north_american
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 3
native_hostileness = 4

1710.1.1 = {
	owner = CHY
	controller = CHY
	add_core = CHY
	is_city = yes
	trade_goods = fur
	culture = cheyenne
} #Horses cause waves of migration on the Great Plains
1738.1.1 = { discovered_by = FRA }# Pierre Gaultier de Varennes, no real settlements until the 1800's
1760.1.1  = {
	owner = SIO
	controller = SIO
	add_core = SIO
	culture = dakota
	is_city = yes
} #Horses allow plain tribes to expand
