# No previous file for Sapotaweyak

culture = anishinabe
religion = totemism
capital = "Sapotaweyak"
trade_goods = crops
hre = no discovered_by = north_american
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 5

1650.1.1 = {
	owner = WCR
	controller = WCR
	add_core = WCR 
	trade_goods = fur
	is_city = yes
} #Before the Beaver Wars
