# No previous file for Gespogoitnag

add_core = MIK
owner = MIK
controller = MIK
is_city = yes
culture = miqmaq
religion = totemism
capital = "Gespogoitnag"
trade_goods = crops
hre = no discovered_by = north_american
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 5

1497.6.24 = {
	discovered_by = ENG
} # John Cabbot
1534.1.1  = {
	discovered_by = FRA
} # Jacques Cartier
1605.1.1   = {
	owner = FRA
	controller = FRA		
	citysize = 200
	capital = "Port Royal"
	religion = catholic
	culture = francien
	trade_goods = fur
	base_tax = 2
	manpower = 2
	fort_14th = yes
} 
1625.1.1   = {
	add_core = FRA
	is_city = yes
}
1713.4.11  = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = FRA
	capital = "Fort Louisbourg"
}
1749.1.1   = {
	capital = "Halifax"
	fort_14th = no
	fort_15th = yes
} # Chebucto Bay
1750.1.1   = {
	add_core = QUE
	culture = canadian
}
1755.8.11  = {
	add_core = CAN
	remove_core = QUE
	religion = protestant #anglican
	culture = american
} # Great upheaval
