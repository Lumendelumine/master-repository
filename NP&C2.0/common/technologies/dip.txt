monarch_power = DIP


technology = {
    # Tech 0
    year = 1300

	range = 100
	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02
}

technology = {
    # Tech 1
    year = 1310

	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = galley
}

technology = {
    # Tech 2
    year = 1320

	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 3
    year = 1330

	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	merchants = yes
}

technology = {
    # Tech 4
    year = 1340

	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = cog
}

technology = {
    # Tech 5
    year = 1350

	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = warcog

	dock = yes
}

technology = {
    # Tech 6
    year = 1360

	range = 5
	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 7
    year = 1370

	range = 5
	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	mill = yes
	enable = holk
}

technology = {
    # Tech 8
    year = 1380

	range = 5
	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 9
    year = 1390

	range = 5
	trade_range = 10

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 10
    year = 1400

	range = 5
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	marketplace = yes

	enable = carrack
}

technology = {
    # Tech 11
    year = 1410

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 12
    year = 1420

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
	
	paved_road = yes
	enable = caravel
}

technology = {
    # Tech 13
    year = 1430

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	drydock = yes
}

technology = {
    # Tech 14
    year = 1440

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 15
    year = 1450

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	# diplomatic_upkeep = 1

	customs_house = yes
}

technology = {
    # Tech 16
    year = 1460

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 17
    year = 1470

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = caravela_redonda
}

technology = {
    # Tech 18
    year = 1480

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	toll_road = yes
	shipyard = yes
}

technology = {
    # Tech 19
    year = 1490

	range = 10
	trade_range = 20

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	merchant_guild = yes
}

technology = {
    # Tech 20
    year = 1500

	range = 10
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	refinery = yes

	wharf = yes
}

technology = {
    # Tech 21
    year = 1510

	range = 10
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	tradecompany = yes

	enable = flute
}

technology = {
    # Tech 22
    year = 1520

	range = 10
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 23
    year = 1530

	range = 10
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	grand_shipyard = yes
}

technology = {
    # Tech 24
    year = 1540

	range = 15
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = barque
}

technology = {
    # Tech 25
    year = 1550

	range = 15
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	# diplomatic_upkeep = 1
}

technology = {
    # Tech 26
    year = 1560

	range = 15
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	post_office = yes
}

technology = {
    # Tech 27
    year = 1570

	range = 20
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = galleon
	enable = galleass
}

technology = {
    # Tech 28
    year = 1580

	range = 20
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	road_network = yes
	admiralty = yes
}

technology = {
    # Tech 29
    year = 1590

	range = 20
	trade_range = 30

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	naval_arsenal = yes
}

technology = {
    # Tech 30
    year = 1600

	range = 25
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = early_frigate
}

technology = {
    # Tech 31
    year = 1610

	range = 25
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = merchantman
}

technology = {
    # Tech 32
    year = 1620

	range = 25
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = wargalleon
}

technology = {
    # Tech 33
    year = 1630

	range = 25
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01
}

technology = {
    # Tech 34
    year = 1640

	range = 30
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	enable = frigate
}

technology = {
    # Tech 35
    year = 1650

	range = 30
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.02

	trade_efficiency = 0.01

	allow_client_states = yes

	# diplomatic_upkeep = 1
}

technology = {
    # Tech 36
    year = 1660

	range = 35
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
	
	enable = chebeck
}

technology = {
    # Tech 37
    year = 1670

	range = 35
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
	
	enable = twodecker
}

technology = {
    # Tech 38
    year = 1680

	range = 35
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01

}

technology = {
    # Tech 39
    year = 1690

	range = 35
	trade_range = 40

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01

	reduced_naval_attrition = yes
}

technology = {
    # Tech 40
    year = 1700

	range = 40
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
	
	enable = heavy_frigate
}

technology = {
    # Tech 41
    year = 1710

	range = 40
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
}

technology = {
    # Tech 42
    year = 1720

	range = 40
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
	
	enable = eastindiaman
}

technology = {
    # Tech 43
    year = 1730

	range = 40
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
}

technology = {
    # Tech 44
    year = 1740

	range = 50
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01
	
	enable = threedecker
}

technology = {
    # Tech 45
    year = 1750

	range = 50
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.03

	trade_efficiency = 0.01

	# diplomatic_upkeep = 1
}

technology = {
    # Tech 46
    year = 1760

	range = 50
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 47
    year = 1770

	range = 50
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 48
    year = 1780

	range = 50
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
	
	naval_base = yes
}

technology = {
    # Tech 49
    year = 1790

	range = 50
	trade_range = 50

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
	
	road_with_tree_lines = yes
}

technology = {
    # Tech 50
    year = 1800

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 51
    year = 1810

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 52
    year = 1820

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 53
    year = 1830

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 54
    year = 1840

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01
}

technology = {
    # Tech 55
    year = 1850

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.04

	trade_efficiency = 0.01

	# diplomatic_upkeep = 1
	
	railway = yes
}

technology = {
    # Tech 56
    year = 1860
	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.05

	trade_efficiency = 0.01
}

technology = {
    # Tech 57
    year = 1870

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.05

	trade_efficiency = 0.01
}

technology = {
    # Tech 58
    year = 1880

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.05

	trade_efficiency = 0.01
}

technology = {
    # Tech 59
    year = 1890

	range = 50
	trade_range = 60

	naval_morale = 0.1
	naval_maintenance = 0.05

	trade_efficiency = 0.01
}

technology = {
    # Tech 60
    year = 1900

	range = 35
	trade_range = 70

	naval_morale = 0.1
	naval_maintenance = 0.05

	trade_efficiency = 0.01
}
