# Do not change tags in here without changing every other reference to them.
# If adding new technology, make sure they are uniquely named.

# Armies get a insufficient support penalty when the ratio cav / (inf+cav) is _higher_ than "cav_to_inf_ratio". This applies for the "unit_type" of a country.

# Groups must be defined BEFORE tables.


groups = {
	nomad_group = {
		modifier = 0.75
		start_level = 3
		cav_to_inf_ratio = 1.0
	}
	
	western = {
		modifier = 0.00
		start_level = 4
		cav_to_inf_ratio = 0.4
		trade_company = yes
	}
	
	turkishtech = {
		modifier = 0.05
		start_level = 6
		cav_to_inf_ratio = 0.6
		trade_company = yes
	}
	
	eastern = {
		modifier = 0.05
		start_level = 4
		cav_to_inf_ratio = 0.5
		trade_company = yes
	}
	
	muslim = {
		modifier = 0.10
		start_level = 6
		cav_to_inf_ratio = 0.7
		trade_company = yes
	}
	
	chinese = {
		modifier = 0.15
		start_level = 6
		cav_to_inf_ratio = 0.5
	}
	
	indian = {
		modifier = 0.25
		start_level = 5
		cav_to_inf_ratio = 0.5
	}
	
	austranesian = {
		modifier = 0.50
		start_level = 4
		cav_to_inf_ratio = 0.1
	}
	
	steppestech = {
		modifier = 0.50
		start_level = 5
		cav_to_inf_ratio = 0.8
		power = -2
	}
	
	soudantech = {
		modifier = 0.50
		start_level = 3
		cav_to_inf_ratio = 0.4
	}
	
	sub_saharan = {
		modifier = 0.50
		start_level = 3
		cav_to_inf_ratio = 0.0
		power = -2
	}
	
	north_american = {
		modifier = 2.5
		start_level = 2
		cav_to_inf_ratio = 0.5
	}
	mesoamerican = {
		modifier = 2.0
		start_level = 2
		cav_to_inf_ratio = 0.5
	}
	south_american = {
		modifier = 2.0
		start_level = 2
		cav_to_inf_ratio = 0.5
	}	
	
	mongol_tech = {
		modifier = 0.00
		start_level = 2
		cav_to_inf_ratio = 0.9
	}
	
	hawaii_tech = {
		modifier = 0.75
		start_level = 1
		cav_to_inf_ratio = 0.0
	}
}

tables = {
	adm_tech = "technologies/adm.txt"
	dip_tech = "technologies/dip.txt"
	mil_tech = "technologies/mil.txt"
}
