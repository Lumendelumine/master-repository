name="M&T Hundred Years' War DLC Support v1.0"
path="mod/MEIOUandTaxes_hyw_unit_DLC_support"
picture="MEIOUandTaxesHYW.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
