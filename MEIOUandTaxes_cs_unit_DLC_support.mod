name="M&T Common Sense Content Pack DLC Support v1.0"
path="mod/MEIOUandTaxes_cs_unit_DLC_support"
picture="MEIOUandTaxesCS.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
