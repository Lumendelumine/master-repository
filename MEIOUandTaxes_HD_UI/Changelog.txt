##############################
# MEIOU and Taxes HD_UI V0.3 #
# credits to :  Aristos      #
#               Impi07       #
#               zebez        #
#               ibnarabi     #
##############################
Mod is optimized for resolution FULL HD 1920x1080
Lower resolution could break your interface.

---- 08.10.2013
[*]- Adaption for MEIOU&Taxes
[*]- Initial Version
[*]- Several Bugfixes
---- 13.10.2013
[*]- Updated to V0.2
[*]- Added new font Segoe13 etc
--- 7.11.2013
[*]- Updated to V0.3
[*]- Now Works with EUIV 1.31
--- 23.3.2014
[*]- Adapted to 10 Idea Groups
