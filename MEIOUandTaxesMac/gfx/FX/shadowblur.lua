Samplers = 
{
	BlurSample = {
		Index = 0;
		MagFilter = "Linear";
		MipFilter = "Linear";
		AddressU = "Clamp";
		AddressV = "Clamp";
	}
}
AddSamplers()

Includes = {

}

BlendState =
{
	BlendEnable = false;
	AlphaTest = false;
	SourceBlend = "SRC_ALPHA";
	DestBlend = "INV_SRC_ALPHA";
}
Defines = { } -- Comma separated defines ie. "USE_SIMPLE_LIGHTS", "GUI"

DeclareVertex( [[
struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float2 vTexCoord  : TEXCOORD0;
};
]] )

DeclareVertex( [[
struct VS_OUTPUT
{
    float4  vPosition : POSITION;
    float2  vTexCoord : TEXCOORD0;
};
]] )

DeclareShared( [[
CONSTANT_BUFFER( 0, 0 )
{
	float4  	vGaussianOffsetsWeights[2];
};
]] )

ShadowBlurHorizontal = {
	VertexShader = "VertexShader";
	PixelShader = "HorizontalPixelShader";
	ShaderModel = 3;
}

ShadowBlurVertical = {
	VertexShader = "VertexShader";
	PixelShader = "VerticalPixelShader";
	ShaderModel = 3;
}

DeclareShader( "VertexShader", [[
VS_OUTPUT main(const VS_INPUT In )
{
    VS_OUTPUT Out;
    Out.vPosition  	= In.vPosition;
    Out.vTexCoord  	= In.vTexCoord;

    return Out;
}

]] )

DeclareShader( "HorizontalPixelShader", [[
float4 main( VS_OUTPUT In ) : COLOR
{
	// Accumulated color
	float fAccum = 0.0f;

	float vOffset[3];
	float vWeight[3];
	vOffset[0] = vGaussianOffsetsWeights[0].x;
	vOffset[1] = vGaussianOffsetsWeights[0].y;
	vOffset[2] = vGaussianOffsetsWeights[0].z;	
	vWeight[0] = vGaussianOffsetsWeights[0].w;
	vWeight[1] = vGaussianOffsetsWeights[1].x;
	vWeight[2] = vGaussianOffsetsWeights[1].y;		
	
	fAccum = tex2D( BlurSample, In.vTexCoord ).r * vWeight[0];
	for (int i=1; i<3; i++) {
		fAccum += tex2D( BlurSample, ( In.vTexCoord+float2(vOffset[i], 0.0) ) ).r * vWeight[i];
		fAccum += tex2D( BlurSample, ( In.vTexCoord-float2(vOffset[i], 0.0) ) ).r * vWeight[i];
	}	

	return float4( fAccum, fAccum, fAccum, 1.0f );	
}
]] )

DeclareShader( "VerticalPixelShader", [[
float4 main( VS_OUTPUT In ) : COLOR
{
	// Accumulated color
	float fAccum = 0.0f;

	float vOffset[3];
	float vWeight[3];
	vOffset[0] = vGaussianOffsetsWeights[0].x;
	vOffset[1] = vGaussianOffsetsWeights[0].y;
	vOffset[2] = vGaussianOffsetsWeights[0].z;	
	vWeight[0] = vGaussianOffsetsWeights[0].w;
	vWeight[1] = vGaussianOffsetsWeights[1].x;
	vWeight[2] = vGaussianOffsetsWeights[1].y;		
	
	fAccum = tex2D( BlurSample, In.vTexCoord ).r * vWeight[0];
	for (int i=1; i<3; i++) {
		fAccum += tex2D( BlurSample, ( In.vTexCoord+float2(0.0, vOffset[i]) ) ).r * vWeight[i];
		fAccum += tex2D( BlurSample, ( In.vTexCoord-float2(0.0, vOffset[i]) ) ).r * vWeight[i];
	}	
	
	return float4( fAccum, fAccum, fAccum, 1.0f );	
}
]] )
