Samplers = 
{
	SimpleTexture = {
		Index = 0;
		MagFilter = "Linear";
		MipFilter = "Linear";
		MinFilter = "Point";
		AddressU = "Wrap";
		AddressV = "Wrap";
	}
}
AddSamplers()

Includes = {
--	"standardfuncsgfx.fxh"
}

BlendState =
{
	BlendEnable = true;
	AlphaTest = false;
	SourceBlend = "SRC_ALPHA";
	DestBlend = "INV_SRC_ALPHA";
}
Defines = { } -- Comma separated defines ie. "USE_SIMPLE_LIGHTS", "GUI"

DeclareVertex( [[
struct VS_INPUT
{
    float4 vPosition  	: POSITION;
    float2 vTexCoord  	: TEXCOORD0;
	float4 vFillColor 	: COLOR;
	float4 vBorderColor : TEXCOORD1;
};
]] )

DeclareVertex( [[
struct VS_OUTPUT
{
    float4  vPosition 	: POSITION;
    float2  vTexCoord 	: TEXCOORD0;
	float4 vFillColor 	: COLOR;
	float4 vBorderColor : TEXCOORD1;
};
]] )

DeclareVertex( [[
struct VS_FLAG_INPUT
{
    float4 vPosition  	: POSITION;
    float2 vTexCoord  	: TEXCOORD0;
	float4 vFillColor 	: COLOR;
	float4 vBorderColor : TEXCOORD1;
    float2 vTexCoordFlag : TEXCOORD2;
};
]] )

DeclareVertex( [[
struct VS_FLAG_OUTPUT
{
    float4  vPosition 	: POSITION;
    float2  vTexCoord 	: TEXCOORD0;
	float4 vFillColor 	: COLOR;
	float4 vBorderColor : TEXCOORD1;
    float2 vTexCoordFlag : TEXCOORD2;
};
]] )

DeclareShared( [[
CONSTANT_BUFFER( 0, 0 )
{
	float4x4 	Matrix;//			: register( c0 );
	float4		ModColor;
};

CONSTANT_BUFFER( 1, 16 )
{
	float4		FieldColor;
	float		FieldCutoff;
};

/*
	In glsl:
	
	uniform float4 UNIFORM0[ 4 ];
	#define Matrix mat4( UNIFORM[0], UNIFORM[1], UNIFORM[2], UNIFORM[3] )
	
	In hlsl
	
	float4x4 Matrix : register( c0 );
*/

]] )

Text = {
	VertexShader = "VertexShaderText";
	PixelShader = "PixelShaderText";
	ShaderModel = 3;
}

FlagText = {
	VertexShader = "VertexShaderFlagText";
	PixelShader = "PixelShaderFlagText";
	ShaderModel = 3;
}

Text3D = {
	VertexShader = "VertexShaderText3D";
	PixelShader = "PixelShaderText";
	ShaderModel = 3;
}

DeclareShader( "VertexShaderText3D", [[
VS_OUTPUT main(const VS_INPUT v )
{
    VS_OUTPUT Out;
    Out.vPosition  	= mul( Matrix, v.vPosition );
	
    Out.vTexCoord  	= v.vTexCoord;
	
	Out.vFillColor	= v.vFillColor * ModColor;
	Out.vBorderColor = float4( ( v.vBorderColor * ModColor ).rgb, v.vBorderColor.a );
	
    return Out;
}

]] )


DeclareShader( "VertexShaderText", [[
VS_OUTPUT main(const VS_INPUT v )
{
    VS_OUTPUT Out;
    Out.vPosition  	= mul( Matrix, v.vPosition );
	
    Out.vTexCoord  	= v.vTexCoord;
	
	Out.vFillColor	= v.vFillColor * ModColor;
	Out.vBorderColor = float4( ( v.vBorderColor * ModColor ).rgb, v.vBorderColor.a );
	
    return Out;
}

]] )

DeclareShader( "VertexShaderFlagText", [[
VS_FLAG_OUTPUT main(const VS_FLAG_INPUT v )
{
    VS_FLAG_OUTPUT Out;
    Out.vPosition  	= mul( Matrix, v.vPosition );
	
    Out.vTexCoord  	= v.vTexCoord;
	Out.vTexCoordFlag  	= v.vTexCoordFlag;
	
	Out.vFillColor	= v.vFillColor * ModColor;
	Out.vBorderColor = float4( ( v.vBorderColor * ModColor ).rgb, v.vBorderColor.a );
	
    return Out;
}

]] )

DeclareShader( "PixelShaderText", [[
float4 main( VS_OUTPUT v ) : COLOR
{
    float4 TexColor = tex2D( SimpleTexture, v.vTexCoord );
	
	float4 OutColor = TexColor * v.vFillColor;
	
	//TODO: Replace branching if performance is low or if just have time over.
	if( TexColor.a < v.vBorderColor.a * 0.75 )
	{
		OutColor = float4( v.vBorderColor.rgb, v.vFillColor.a ) * TexColor;
	}
	else if( TexColor.a < v.vBorderColor.a )
	{
		float3 MixColor = lerp( v.vBorderColor.rgb, v.vFillColor.rgb,  TexColor.a );
		OutColor = float4( MixColor, v.vFillColor.a ) * TexColor;
	}
	
    return OutColor;
}
]] )

DeclareShader( "PixelShaderFlagText", [[
float4 main( VS_FLAG_OUTPUT v ) : COLOR
{
	//return float4(1,1,1,1);
    
	float4 TexColor = tex2D( SimpleTexture, v.vTexCoord );
	
	if( v.vTexCoordFlag.x > FieldCutoff )
		TexColor = FieldColor;
	
	float4 OutColor = TexColor * v.vFillColor;
	
	//TODO: Replace branching if performance is low or if just have time over.
	if( TexColor.a < v.vBorderColor.a * 0.75 )
	{
		OutColor = float4( v.vBorderColor.rgb, v.vFillColor.a ) * TexColor;
	}
	else if( TexColor.a < v.vBorderColor.a )
	{
		float3 MixColor = lerp( v.vBorderColor.rgb, v.vFillColor.rgb,  TexColor.a );
		OutColor = float4( MixColor, v.vFillColor.a ) * TexColor;
	}
	
    return OutColor;
}
]] )
