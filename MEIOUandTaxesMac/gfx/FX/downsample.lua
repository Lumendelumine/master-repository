Samplers = 
{
	MainScene = {
		Index = 0;
		MagFilter = "Point";
		MinFilter = "Point";
		MipFilter = "None";
		AddressU = "Clamp";
		AddressV = "Clamp";
	}
}

AddSamplers()

Includes = {
	"posteffect_base.fxh",
	"constants.fxh",	
	"standardfuncsgfx.fxh"
}

downsample = {
	VertexShader = "VertexShader";
	PixelShader = "PixelShader";
	ShaderModel = 3;
}

BlendState = {
	BlendEnable = false;
	AlphaTest = false;
}

DeclareVertex( [[

struct VS_OUTPUT_DOWNSAMPLE
{
    float4 position			: POSITION;
	float2 uv				: TEXCOORD0;
};

]] )

DeclareVertex( [[

struct VS_INPUT
{
    float2 position			: POSITION;
};
]] )

DeclareShader( "VertexShader", [[

VS_OUTPUT_DOWNSAMPLE main( const VS_INPUT VertexIn )
{
	VS_OUTPUT_DOWNSAMPLE VertexOut;
	VertexOut.position = float4( VertexIn.position, 0.0f, 1.0f );
	
	VertexOut.uv = ( VertexIn.position + 1.0f ) * 0.5f;
	VertexOut.uv.y = 1.0f - VertexOut.uv.y;
	VertexOut.uv.xy += 0.5f / vBloomSize;
	
	VertexOut.position.xy += float2( -0.5f / vBloomSize.x, 0.5f / vBloomSize.y );

	return VertexOut;
}

]] )

DeclareShader( "PixelShader", [[

float4 main( VS_OUTPUT_DOWNSAMPLE Input ) : COLOR
{
	return tex2D( MainScene, Input.uv );
}

]] )
