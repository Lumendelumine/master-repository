Samplers = {
	DiffuseMap = {
		Index = 0;
		MinFilter = "Linear";
		MagFilter = "Linear";
		MipFilter = "Linear";
		AddressU = "Wrap";
		AddressV = "Wrap";
	}	
}

AddSamplers()

BlendState = {
	BlendEnable = true;
	AlphaTest = false;

	SourceBlend = "SRC_ALPHA";
	DestBlend = "INV_SRC_ALPHA";
}

BlendStatePreAlphaBlend = {
	BlendEnable = true;
	AlphaTest = false;

	SourceBlend = "ONE";
	DestBlend = "INV_SRC_ALPHA";
}

BlendStateAdditive = {
	BlendEnable = true;
	AlphaTest = false;

	SourceBlend = "SRC_ALPHA";
	DestBlend = "ONE";
}


Includes = {
	"constants.fxh",
	"standardfuncsgfx.fxh"
}

Defines = { }

DeclareVertex( [[
struct VS_INPUT_SIMPLEPARTICLE
{
	float2 vUV0			: TEXCOORD0;
	float4 vPosSize		: TEXCOORD1;
	float4 vVelRot		: TEXCOORD2;
	float4 vTile		: TEXCOORD3;
	float4 vColor		: COLOR;
};

]] )

DeclareVertex( [[

struct VS_OUTPUT_SIMPLEPARTICLE
{
    float4 vPosition	: POSITION;
	float2 vUV0			: TEXCOORD0;
	float4 vColor		: COLOR;
};

]] )

DeclareShared( [[

CONSTANT_BUFFER( 1, 32 )
{
	float4x4 ProjectionMatrix;
}

CONSTANT_BUFFER( 2, 36 )
{
	float Scale;
}

CONSTANT_BUFFER( 3, 40 )
{
	float4	HalfPixelWH_RowsCols;
	float	vLocalTime;
}

]] )


ParticleAlphaBlend = {
	VertexShader = "VertexSimpleParticle";
	PixelShader = "PixelSimpleParticle";
	ShaderModel = 3;
}

ParticlePreAlphaBlend = {
	VertexShader = "VertexSimpleParticle";
	PixelShader = "PixelSimpleParticle";
	ShaderModel = 3;
	BlendState = "BlendStatePreAlphaBlend";
}

ParticleAdditive = {
	VertexShader = "VertexSimpleParticle";
	PixelShader = "PixelSimpleParticle";
	ShaderModel = 3;
	BlendState = "BlendStateAdditive";
}

DeclareShader( "VertexSimpleParticle", [[

VS_OUTPUT_SIMPLEPARTICLE main( const VS_INPUT_SIMPLEPARTICLE v )
{
  	VS_OUTPUT_SIMPLEPARTICLE Out;
	
	Out.vPosition = mul( ViewProjectionMatrix, float4( v.vPosSize.xyz, 1.0 ) );
	
	float2 offset = ( v.vUV0 - 0.5f ) * v.vPosSize.w * Scale;
	float2 vSinCos;
	sincos( v.vVelRot.w * ( 3.14159265359f / 180.0f ), vSinCos.x, vSinCos.y );
	offset = float2( 
		offset.x * vSinCos.y - offset.y * vSinCos.x, 
		offset.x * vSinCos.x + offset.y * vSinCos.y );

	Out.vPosition.xy += offset * float2( ProjectionMatrix[0][0], ProjectionMatrix[1][1] );

	Out.vColor = v.vColor;
	
	float2 tmpUV = float2( v.vUV0.x, 1.0f - v.vUV0.y );
	Out.vUV0 = HalfPixelWH_RowsCols.xy + ( v.vTile.xy + tmpUV ) / HalfPixelWH_RowsCols.zw - HalfPixelWH_RowsCols.xy * 2.0f * tmpUV;
	return Out;
}

]] )


DeclareShader( "PixelSimpleParticle", [[
	
float4 main( VS_OUTPUT_SIMPLEPARTICLE In ) : COLOR
{
	float4 vColor = tex2D( DiffuseMap, In.vUV0 ) * In.vColor;
	return vColor;
}
	
]] )
