
Includes = {
--	"standardfuncsgfx.fxh"
}

Defines = { } -- Comma separated defines ie. "USE_SIMPLE_LIGHTS", "GUI"

DeclareVertex( [[
struct VS_INPUT
{
    float2 vPosition  : POSITION;
 	float4 vColor	  : COLOR;
};
]] )

DeclareVertex( [[
struct VS_OUTPUT
{
    float4  vPosition : POSITION;
	float4  vColor	  : TEXCOORD1;
};
]] )

DeclareShared( [[
CONSTANT_BUFFER( 0, 0 )
{
	float4x4 Matrix;//			: register( c0 );
};
]] )

Color = {
	VertexShader = "VertexColor";
	PixelShader = "PixelColor";
	ShaderModel = 3;
}

DeclareShader( "VertexColor", [[
VS_OUTPUT main(const VS_INPUT v )
{
    VS_OUTPUT Out;
	float4 vPosition = float4( v.vPosition.x, v.vPosition.y, 0, 1 );
    Out.vPosition  	= mul( Matrix, vPosition );	
	Out.vColor		= v.vColor;
    return Out;
}

]] )

DeclareShader( "PixelColor", [[
float4 main( VS_OUTPUT v ) : COLOR
{
    return v.vColor;
}
]] )
