BlendState =
{
	BlendEnable = true;
	AlphaTest = false;
	SourceBlend = "SRC_ALPHA";
	DestBlend = "INV_SRC_ALPHA";
}

DeclareVertex( [[
struct VS_INPUT
{
    float4 vPosition	: POSITION;
    float4 vColor		: COLOR;
};
]] )

DeclareVertex( [[
struct VS_OUTPUT
{
    float4  vPosition : POSITION;
	float4  vColor	  : TEXCOORD0;
};
]] )

DeclareShared( [[
CONSTANT_BUFFER( 0, 0 )
{
	float4x4 Matrix;//			: register( c0 );
};
]] )

DebugLines = {
	VertexShader = "VertexShaderDebugLines";
	PixelShader = "PixelShaderDebugLines";
	ShaderModel = 3;
}

DeclareShader( "VertexShaderDebugLines", [[
VS_OUTPUT main(const VS_INPUT In )
{
    VS_OUTPUT Out;
    Out.vPosition  	= mul( Matrix, In.vPosition );
	Out.vColor		= In.vColor;
    return Out;
}

]] )

DeclareShader( "PixelShaderDebugLines", [[
float4 main( VS_OUTPUT In ) : COLOR
{
    return In.vColor;
}
]] )
