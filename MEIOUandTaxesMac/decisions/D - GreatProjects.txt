country_decisions = {

	construct_kiel_canal = {
		major = yes
		potential = {
			OR = {
				ai = no
				AND = {
					treasury = 50000
					owns = 1252
				}
			}
			has_discovered = 1252
			1252 = {
				range = ROOT
				NOT = { has_great_project = kiel_canal }
				NOT = { has_construction = canal }
			}
			adm_tech = 40
		}
		allow = {
			owns = 1252
			adm_tech = 45
			treasury = 20000
		}
		effect = {
			1252 = {	# Holstein
				add_great_project = kiel_canal
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	construct_suez_canal = {
		major = yes
		potential = {
			OR = {
				ai = no
				AND = {
					treasury = 50000
					owns = 359
				}
			}
			has_discovered = 359
			359 = {
				range = ROOT
				NOT = { has_great_project = suez_canal }
				NOT = { has_construction = canal }
			}
			adm_tech = 40
		}
		allow = {
			owns = 359
			adm_tech = 45
			treasury = 20000
		}
		effect = {
			add_treasury = -20000
			359 = {	# Dumyat
				add_great_project = suez_canal
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	construct_panama_canal = {
		major = yes
		potential = {
			is_random_new_world = no
			OR = {
				ai = no
				AND = {
					treasury = 50000
					owns = 835
				}
			}
			has_discovered = 835
			835 = {
				range = ROOT
				NOT = { has_great_project = panama_canal }
				NOT = { has_construction = canal }
			}
			adm_tech = 45
		}
		allow = {
			owns = 835
			adm_tech = 50
			treasury = 20000
		}
		effect = {
			add_treasury = -20000
			835 = {
				add_great_project = panama_canal
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	construct_forbidden_city_kaifeng = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 701
			}
			has_discovered = 701
			701 = {
				range = ROOT
				NOT = { has_province_modifier = forbidden_city_kaifeng }
			}
			708 = {
				NOT = { has_province_modifier = forbidden_city_beijing }
			}
			2121 = {
				NOT = { has_province_modifier = forbidden_city_guangzhou }
			}
			2150 = {
				NOT = { has_province_modifier = forbidden_city_nanjing }
			}
			2252 = {
				NOT = { has_province_modifier = forbidden_city_xian }
			}
		}
		allow = {
			capital = 701
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			701 = {
				add_permanent_province_modifier = { name = forbidden_city_kaifeng duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_beijing = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 708
			}
			has_discovered = 708
			701 = {
				NOT = { has_province_modifier = forbidden_city_kaifeng }
			}
			708 = {
				range = ROOT
				NOT = { has_province_modifier = forbidden_city_beijing }
			}
			2121 = {
				NOT = { has_province_modifier = forbidden_city_guangzhou }
			}
			2150 = {
				NOT = { has_province_modifier = forbidden_city_nanjing }
			}
			2252 = {
				NOT = { has_province_modifier = forbidden_city_xian }
			}
		}
		allow = {
			capital = 708
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			708 = {
				add_permanent_province_modifier = { name = forbidden_city_beijing duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_guangzhou = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 2121
			}
			has_discovered = 2121
			701 = {
				NOT = { has_province_modifier = forbidden_city_kaifeng }
			}
			708 = {
				NOT = { has_province_modifier = forbidden_city_beijing }
			}
			2121 = {
				range = ROOT
				NOT = { has_province_modifier = forbidden_city_guangzhou }
			}
			2150 = {
				NOT = { has_province_modifier = forbidden_city_nanjing }
			}
			2252 = {
				NOT = { has_province_modifier = forbidden_city_xian }
			}
		}
		allow = {
			capital = 2121
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			2121 = {
				add_permanent_province_modifier = { name = forbidden_city_guangzhou duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_nanjing = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 2150
			}
			has_discovered = 2150
			701 = {
				NOT = { has_province_modifier = forbidden_city_kaifeng }
			}
			708 = {
				NOT = { has_province_modifier = forbidden_city_beijing }
			}
			2121 = {
				NOT = { has_province_modifier = forbidden_city_guangzhou }
			}
			2150 = {
				range = ROOT
				NOT = { has_province_modifier = forbidden_city_nanjing }
			}
			2252 = {
				NOT = { has_province_modifier = forbidden_city_xian }
			}
		}
		allow = {
			capital = 2150
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			2150 = {
				add_permanent_province_modifier = { name = forbidden_city_nanjing duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_forbidden_city_xian = {
		major = yes
		potential = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
			OR = {
				ai = no
				owns = 2252
			}
			has_discovered = 2252
			701 = {
				NOT = { has_province_modifier = forbidden_city_kaifeng }
			}
			708 = {
				NOT = { has_province_modifier = forbidden_city_beijing }
			}
			2121 = {
				NOT = { has_province_modifier = forbidden_city_guangzhou }
			}
			2150 = {
				NOT = { has_province_modifier = forbidden_city_nanjing }
			}
			2252 = {
				range = ROOT
				NOT = { has_province_modifier = forbidden_city_xian }
			}
		}
		allow = {
			capital = 2252
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 3
			years_of_income = 5.0
			adm_power = 50
		}
		effect = {
			add_years_of_income = -5.0
			add_adm_power = -50
			2252 = {
				add_permanent_province_modifier = { name = forbidden_city_xian duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 6.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}

	construct_hagia_sophia_minarets = {
		major = yes
		potential = {
			religion = sunni
			any_owned_province = { region = eastern_balkans }
			OR = {
				ai = no
				owns = 1402
			}
			has_discovered = 1402
			1402 = {
				range = ROOT
				NOT = { has_province_modifier = hagia_sophia_minarets }
			}
		}
		allow = {
			capital = 1402
			is_at_war = no
			OR = {
				statesman = 3
				adm = 3
			}
			stability = 1
			treasury = 200
			adm_power = 50
		}
		effect = {
			add_treasury = -100
			add_adm_power = -50
			1402 = {
				add_permanent_province_modifier = { name = hagia_sophia_minarets duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { years_of_income = 1.0 }
					NOT = { adm_power = 100 }
				}
			}
		}
	}
	construct_kremlin_moscow = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 295
			}
			has_discovered = 295
			295 = {
				range = ROOT
				NOT = { has_great_project = kremlin_moscow }
				NOT = { has_province_modifier = kremlin_moscow }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 295
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			295 = {
				add_permanent_province_modifier = { name = kremlin_moscow duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_astrakhan = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 1293
			}
			has_discovered = 1293
			1293 = {
				range = ROOT
				NOT = { has_great_project = kremlin_astrakhan }
				NOT = { has_province_modifier = kremlin_astrakhan }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 1293
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			1293 = {
				add_permanent_province_modifier = { name = kremlin_astrakhan duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_novgorod = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 309
			}
			has_discovered = 309
			309 = {
				range = ROOT
				NOT = { has_great_project = kremlin_novgorod }
				NOT = { has_province_modifier = kremlin_novgorod }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 309
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			309 = {
				add_permanent_province_modifier = { name = kremlin_novgorod duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_kazan = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 1307
			}
			has_discovered = 1307
			1307 = {
				range = ROOT
				NOT = { has_great_project = kremlin_kazan }
				NOT = { has_province_modifier = kremlin_kazan }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 1307
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			1307 = {
				add_permanent_province_modifier = { name = kremlin_kazan duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_nizhny = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 1292
			}
			has_discovered = 1292
			1292 = {
				range = ROOT
				NOT = { has_great_project = kremlin_nizhny }
				NOT = { has_province_modifier = kremlin_nizhny }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 1292
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			1292 = {
				add_permanent_province_modifier = { name = kremlin_nizhny duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_smolensk = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 293
			}
			has_discovered = 293
			293 = {
				range = ROOT
				NOT = { has_great_project = kremlin_smolensk }
				NOT = { has_province_modifier = kremlin_smolensk }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 293
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			293 = {
				add_permanent_province_modifier = { name = kremlin_smolensk duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	construct_kremlin_pskov = {
		major = yes
		potential = {
			culture_group = east_slavic
			OR = {
				ai = no
				owns = 274
			}
			has_discovered = 274
			274 = {
				range = ROOT
				NOT = { has_great_project = kremlin_pskov }
				NOT = { has_province_modifier = kremlin_pskov }
				NOT = { has_construction = canal }
			}
		}
		allow = {
			capital = 274
			is_at_war = no
			OR = {
				fortification_expert = 2
				army_organiser = 2
				mil = 3
			}
			stability = 1
			treasury = 200
			mil_power = 50
		}
		effect = {
			add_treasury = -100
			add_mil_power = -50
			274 = {
				add_permanent_province_modifier = { name = kremlin_pskov duration = -1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 1000
	}

	cancel_kiel_canal = {
		major = yes
		potential = {
			ai = no
			owns = 1252
			1252 = {
				has_construction = canal
			}
		}
		allow = {
			1252 = {
				has_construction = canal
			}
		}
		effect = {
			1252 = {	# Holstein
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	cancel_suez_canal = {
		major = yes
		potential = {
			ai = no
			owns = 359
			359 = {
				has_construction = canal
			}
		}
		allow = {
			359 = {
				has_construction = canal
			}
		}
		effect = {
			359 = {	# Diamientia
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	cancel_panama_canal = {
		major = yes
		potential = {
			ai = no
			owns = 835
			835 = {
				has_construction = canal
			}
		}
		allow = {
			835 = {
				has_construction = canal
			}
		}
		effect = {
			835 = {
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	cancel_great_projects = {
		major = yes
		potential = {
			ai = no
			any_province = {
				has_construction = canal
			}
			NOT = { has_country_flag = cancel_great_projects }
		}
		allow = {
			any_province = {
				has_construction = canal
			}
		}
		effect = {
			set_country_flag = cancel_great_projects
			every_province = {
				limit = { has_construction = canal }
				cancel_construction = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
		
}
