name="M&T El Dorado Content DLC Support v1.0"
path="mod/MEIOUandTaxes_meamup_unit_DLC_support"
picture="MEIOUandTaxesEDC.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
