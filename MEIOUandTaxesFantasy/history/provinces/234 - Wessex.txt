#234 - Wessex

owner = ENG
controller = ENG
culture = english
religion = catholic
hre = no
base_tax = 7
base_production = 7
trade_goods = livestock
base_manpower = 3
is_city = yes
capital = "Southampton"
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1356.1.1  = {
	add_core = ENG
#	add_core = ESS
}
1422.10.21 = {
	owner = UEF
	controller = UEF
	add_core = UEF
	remove_core = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
	add_core = ENG
	remove_core = UEF
}
1453.1.1  = { unrest = 5 } #Start of the War of the Roses
1459.1.1  = { unrest = 7 } #Increasing Disorder & Piracy Along South Coast
1461.3.1  = { controller = REB } #Increasing Yorkist Support in the South
1461.6.1  = { unrest = 2 controller = ENG } #Coronation of Edward IV
1467.1.1  = { unrest = 5 } #Rivalry between Edward IV & Warwick
1471.1.1  = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4  = { unrest = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26 = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = { unrest = 0 } #Battle of Bosworth Field & the End of the War of the Roses
 # First know drydock constructed at Portsmouth
 #Estimated
 #Completed of Second Dock at Portsmouth
1525.1.1  = { fort_14th = yes }
1540.1.1  = { religion = protestant } #anglican
1600.1.1  = {   } #Manufactory, constable Estimated
1642.8.22 = { controller = REB } #Start of First English Civil War
1646.5.5  = { controller = ENG } #End of First English Civil War
 #Estimated
1700.1.1  = { capital = "Portsmouth" }
1707.5.12 = {
	owner = GBR
		controller = GBR
		add_core = GBR
	    	remove_core = ENG
	    }
1750.1.1  = {  } #Tax Assessor Estimated
 #Estimated
