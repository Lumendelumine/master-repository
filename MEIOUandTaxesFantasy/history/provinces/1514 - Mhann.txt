# 1514 - Isle of Mann

owner = NOR
controller = NOR
culture = norse_gaelic
religion = catholic
hre = no
base_tax = 1
base_production = 1
trade_goods = iron
base_manpower = 1
is_city = yes
capital = "Balley Chashtal"
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1266.1.1  = {
	owner = SCO
	controller = SCO
	add_core = SCO
	add_core = ENG
}
1333.8.9 = {
	owner = ENG
	controller = ENG
}	
1399.10.19 = {
	remove_core = SCO
}
1422.10.21 = {
	owner = UEF
	controller = UEF
	add_core = UEF
	remove_core = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
	add_core = ENG
	remove_core = UEF
}
1560.8.1  = { religion = reformed }
1643.1.1  = { unrest = 4 }
1644.1.1  = { unrest = 0 }
1651.10.1 = {
	controller = REB
}
1652.1.1  = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
 #workshop, Tax Assessor Estimated
