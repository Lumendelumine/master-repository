# 131 - Hrvatska

owner = CRO
controller = CRO
culture = croatian
religion = catholic
capital = "Gradec"
base_tax = 4
base_production = 4
base_manpower = 5
is_city = yes
trade_goods = wheat
hre = no
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1   = {
	add_core = CRO
	add_core = HUN
}
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_core = HUN
}	
1526.8.30  = {
	owner = RHU
	controller = RHU
	add_core = RHU
	add_core = HAB
}
1573.1.20  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Peasant uprising against Habsburg rule & the feudal system
1573.2.9   = { revolt = {  } controller = RHU } # Decisively defeated
1671.1.1   = { unrest = 5 } # Conspiracy against Habsburg rule uncovered
1671.5.1   = { unrest = 0  } # Estimated, the conspirators are executed
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
	remove_core = RHU
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1784.1.1   = { unrest = 7 } # Reforms to introduce German as the official language
1789.1.1   = { unrest = 0 } # Most of the unpopular reforms were cancelled
