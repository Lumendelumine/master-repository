# 1268 - Krajina

owner = CRO
controller = CRO
culture = croatian
religion = catholic
capital = "Bihac"
trade_goods = hemp
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
fort_14th = yes
add_core = HUN
add_core = CRO
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1385.1.1   = {
	owner = BOS
	controller = BOS
	add_core = BOS
}
1394.1.1   = {
	owner = CRO
	controller = CRO
	remove_core = BOS
}
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_core = HUN
}
1526.8.30  = {
	owner = RHU
	controller = RHU
	add_core = RHU
	add_core = HAB
}
1541.1.1   = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = RHU
} # The Bihac fort would become the westernmost fort taken by the Ottoman army
1560.1.1   = {
	
}
1650.1.1   = {
	culture = bosnian
	religion = sunni
}
1671.1.1   = {
	unrest = 5
} # Conspiracy against Habsburg rule uncovered
1671.5.1   = {
	unrest = 0
	
} # Estimated, the conspirators are executed
1700.1.1   = {
	unrest = 7
}
1813.1.1   = {
	unrest = 3
} # Croatian national revival (Hrvatski narodni preporod)
