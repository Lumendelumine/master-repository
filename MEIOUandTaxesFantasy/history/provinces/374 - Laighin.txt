#374 - Leinster

owner = ENG
controller = ENG
culture = irish
religion = catholic
capital = "Loch Garman"
trade_goods = wheat
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern
hre = no


1356.1.1  = {
	add_core = ENG
}
1422.10.21 = {
	owner = UEF
	controller = UEF
	add_core = UEF
	remove_core = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
	add_core = ENG
	remove_core = UEF
}
1536.1.1  = {
	owner = ENG
	controller = ENG
   	add_core = ENG
} #Succession of the Protestant James, 1st Duke of Ormond
1642.1.1  = { controller = REB } #Estimated
1642.6.7  = { owner = IRE controller = IRE } #Confederation of Kilkenny
1650.3.28 = { controller = ENG } #Capture of Kilkenny
1652.4.1  = { owner = ENG } #End of the Irish Confederates
1655.1.1  = { fort_14th = yes }
 #Estimated
1689.3.12 = { controller = REB } #James II Lands in Ireland
1690.8.1  = { controller = ENG } #Estimated
 #marketplace Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
