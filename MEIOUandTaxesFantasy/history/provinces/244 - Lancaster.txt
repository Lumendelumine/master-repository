# 244 - Lancashire

owner = ENG
controller = ENG
culture = northern_e
religion = catholic
hre = no
base_tax = 6
base_production = 6
trade_goods = wool
base_manpower = 2
is_city = yes
capital = "Lancaster"
add_core = ENG
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1422.10.21 = {
	owner = UEF
	controller = UEF
	add_core = UEF
	remove_core = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
	add_core = ENG
	remove_core = UEF
}
1453.1.1  = { unrest = 5 } #Start of the War of the Roses
1461.6.1  = { unrest = 2 } #Coronation of Edward IV
1467.1.1  = { unrest = 5 } #Rivalry between Edward IV & Warwick
1471.1.1  = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4  = { unrest = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26 = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = { unrest = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1600.1.1  = {  fort_14th = yes } #Estimated
1644.1.1  = { controller = REB } #Estimated
1645.1.1  = { controller = ENG }
1700.1.1  = { capital = "Liverpool" } #Includes Manchester; Tax Assessor Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
   	remove_core = ENG
}
