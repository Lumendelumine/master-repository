# Vidin given to his son by the Tsar of Bulgaria
alliance = {
	first = BUL
	second = VID
	start_date = 1356.1.1
	end_date = 1371.2.17
}
alliance = {
	first = TAR
	second = VID
	start_date = 1371.2.17
	end_date = 1393.7.17
}

royal_marriage = {
	first = HUN 
	second = POL
	start_date = 1356.1.1
	end_date = 1382.9.11
}

royal_marriage = {
	first = HUN
	second = SER 
	start_date = 1356.1.1		# Gameplay reasons
	end_date = 1453.1.1
}

royal_marriage = {
	first = NAP
	second = HUN 
	start_date = 1310.8.20		# Crowning of the first Anjou Hungarian King
	end_date = 1386.2.24		# Death of the last one
}

# Duchies of Silesia
vassal = {
	first = BOH
	second = LGZ
	start_date = 1356.1.1
	end_date = 1620.11.7
}
vassal = {
	first = HAB
	second = LGZ
	start_date = 1620.11.8
	end_date = 1675.11.21
}
vassal = {
	first = BOH
	second = OPO
	start_date = 1356.1.1
	end_date = 1620.11.7
}
vassal = {
	first = HAB
	second = OPO
	start_date = 1620.11.8
	end_date = 1666.1.1
}
vassal = {
	first = BOH
	second = TES
	start_date = 1356.1.1
	end_date = 1620.11.7
}
vassal = {
	first = HAB
	second = TES
	start_date = 1620.11.8
	end_date = 1675.11.21
}
vassal = {
	first = BOH
	second = WRO
	start_date = 1356.1.1
	end_date = 1620.11.7
}
vassal = {
	first = HAB
	second = WRO
	start_date = 1620.11.8
	end_date = 1675.11.21
}

# Union under Sigismond von Luxemburg
union = {
	first = BOH
	second = HUN
	start_date = 1419.8.16
	end_date = 1437.12.9
}

# Serbia-Zeta
vassal = {
	first = SER
	second = MON
	start_date = 1356.1.1
	end_date = 1391.1.1
}
alliance = {
	first = SER
	second = MON
	start_date = 1356.1.1
	end_date = 1391.1.1
}

# Poland-Hungary Union
union = {
	first = HUN
	second = POL
	start_date = 1370.11.5 # Death of the last Piast
	end_date = 1382.9.10 # Death of the last Anjou king
}

# Bosnia-Serbia
union = {
	first = BOS
	second = SER
	start_date = 1458.1.1
	end_date = 1459.6.20
}
alliance = {
	first = BOS
	second = SER
	start_date = 1458.1.1
	end_date = 1459.6.20
}

# Croatia
# Bohemia-Hungary
union = {
	first = HUN
	second = CRO
	start_date = 1102.1.1
	end_date = 1526.8.29
}

# Hungary-Transylvania
#FB in MEIOU Transylvania does nor exist until 1526.8.30
#union = {
#	first = HUN
#	second = TRA
#	start_date = 1315.1.1
#	end_date = 1526.8.29
#}
#alliance = {
#	first = HUN
#	second = TRA
#	start_date = 1315.1.1
#	end_date = 1526.8.29
#}

# Bohemia-Hungary
union = {
	first = HUN
	second = BOH
	start_date = 1490.1.1
	end_date = 1526.8.29
}

# Austria-Hungary
union = {
	first = HAB
	second = RHU
	start_date = 1526.8.29
	end_date = 1685.11.11
}
alliance = {
	first = HAB
	second = RHU
	start_date = 1526.8.29
	end_date = 1685.11.11
}

# Austria-Transylvania
vassal = {
	first = HAB
	second = TRA
	start_date = 1570.8.16
	end_date = 1599.1.1
}
alliance = {
	first = HAB
	second = TRA
	start_date = 1570.8.16
	end_date = 1599.1.1
}

union = {
	first = HAB
	second = BOH
	start_date = 1526.8.29
	end_date = 1918.11.11
}
alliance = {
	first = HAB
	second = BOH
	start_date = 1526.8.29
	end_date = 1918.11.11
}

# Austria and Aragon (League of Cambrai)
alliance = {
	first = HAB
	second = ARA
	start_date = 1508.12.10
	end_date = 1516.8.13
}

# Habsburg, Spain and Bavaria
alliance = {
	first = HAB
	second = SPA
	start_date = 1516.1.23
	end_date = 1700.11.1
}

# Habsburg, Spain and Bavaria
alliance = {
	first = HAB
	second = BAV
	start_date = 1516.1.23
	end_date = 1700.11.1
}

# Bohemia and Saxony (Thirty Years War)
alliance = {
	first = BOH
	second = SAX
	start_date = 1635.10.16
	end_date = 1646.4.14
}

# The Quadruple Alliance
alliance = {
	first = HAB
	second = NED
	start_date = 1674.1.1
	end_date = 1678.8.10
}

# The Quadruple Alliance
alliance = {
	first = HAB
	second = SPA
	start_date = 1674.1.1
	end_date = 1678.9.19
}

# The Quadruple Alliance
alliance = {
	first = HAB
	second = BRA
	start_date = 1674.1.1
	end_date = 1678.9.19
}

# Austria and PLC (Holy League of 1684)
alliance = {
	first = HAB
	second = PLC
	start_date = 1684.3.5
	end_date = 1699.1.26
}

# Austria and Venice (Holy League of 1684)
alliance = {
	first = HAB
	second = VEN
	start_date = 1684.3.5
	end_date = 1699.1.26
}

# The Grand alliance
alliance = {
	first = HAB
	second = BAV
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = BRA
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = PAL
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = POR
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = SAX
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = SPA
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = SWE
	start_date = 1689.7.9
	end_date = 1700.1.1
}

# The Grand alliance
alliance = {
	first = HAB
	second = NED
	start_date = 1689.5.12
	end_date = 1713.4.11
}

# The Grand alliance
alliance = {
	first = HAB
	second = ENG
	start_date = 1689.5.12
	end_date = 1707.5.1
}

# Austria-Savoy
alliance = {
	first = HAB
	second = SAV
	start_date = 1690.1.1
	end_date = 1696.8.29
}

# Austria-Savoy
alliance = {
	first = HAB
	second = SAV
	start_date = 1703.7.1
	end_date = 1713.4.11
}

# Austria and Venice
alliance = {
	first = HAB
	second = VEN
	start_date = 1716.4.1
	end_date = 1718.7.21
}

# Austria and Russia
alliance = {
	first = HAB
	second = RUS
	start_date = 1726.8.6
	end_date = 1739.8.21
}

# Austrian-Tuscany
alliance = {
	first = HAB
	second = TUS
	start_date = 1736.2.12
	end_date = 1765.8.18
}

# Austrian and Savoy-Piedmont-Sardinia
alliance = {
	first = HAB
	second = SAR
	start_date = 1742.2.1
	end_date = 1749.1.1
}

# Austria and Great Britain (Treaty of Worms)
alliance = {
	first = HAB
	second = GBR
	start_date = 1743.9.2
	end_date = 1748.10.18
}

# Second Quadruple Alliance
alliance = {
	first = HAB
	second = GBR
	start_date = 1745.1.1
	end_date = 1748.10.18
}

# Second Quadruple Alliance
alliance = {
	first = HAB
	second = NED
	start_date = 1745.1.1
	end_date = 1748.10.18
}

# Second Quadruple Alliance
alliance = {
	first = HAB
	second = SAX
	start_date = 1745.1.1
	end_date = 1745.12.25
}

# Austria and Russia
alliance = {
	first = HAB
	second = RUS
	start_date = 1746.5.22
	end_date = 1762.5.5
}

# Austria and Russia 
alliance = {
	first = HAB
	second = RUS
	start_date = 1781.5.1
	end_date = 1792.1.1
}

# The First Coalition - Declaration of Pillnitz
alliance = {
	first = HAB
	second = PRU
	start_date = 1791.8.27
	end_date = 1795.4.5
}

# The First Coalition
alliance = {
	first = HAB
	second = NED
	start_date = 1793.2.1
	end_date = 1795.1.19
}

# The First Coalition
alliance = {
	first = HAB
	second = GBR
	start_date = 1793.2.21
	end_date = 1797.10.17
}

# The First Coalition
alliance = {
	first = HAB
	second = SPA
	start_date = 1793.3.7
	end_date = 1795.7.22
}

# The First Coalition, France invades Savoie
alliance = {
	first = HAB
	second = SPI
	start_date = 1792.9.1
	end_date = 1797.10.17
}

# Matthias Corvinus of Hungary and Beatrice of Naples
royal_marriage = {
	first = HUN
	second = SIC
	start_date = 1476.1.1
	end_date = 1490.4.6
}

# John Z�polya I and Isabella Jagiello
royal_marriage = {
	first = HUN
	second = POL
	start_date = 1539.1.1
	end_date = 1556.1.1
}

# Friedrich III ~ Eleanor (daughter of King Duarte I of Portugal)
royal_marriage = {
	first = HAB
	second = POR
	start_date = 1453.1.1
	end_date = 1467.9.3
}

# Maximilian I ~ Marie (daughter of Duke Charles of Burgundy)
royal_marriage = {
	first = HAB
	second = BUR
	start_date = 1477.8.20
	end_date = 1483.3.27
}

# Maximilian I ~ Bianca Maria (daughter of Duke Galeazzo Maria of Milan)
royal_marriage = {
	first = HAB
	second = MLO
	start_date = 1494.1.1
	end_date = 1504.1.30	#FB was 1510.1.1
}

# Philipp I ~ Juana (daughter of King Fernando II of Spain)
royal_marriage = {
	first = HAB
	second = SPA
	start_date = 1496.8.21
	end_date = 1506.9.25
}

# Karl V ~ Isabel (daughter of King Manuel I of Portugal)
royal_marriage = {
	first = HAB
	second = POR
	start_date = 1526.3.11
	end_date = 1539.5.1
}

# King Felipe II of Spain (son of Karl V) ~ Isabel (daughter of King Jo�o III of Portugal)
royal_marriage = {
	first = HAB
	second = POR
	start_date = 1543.11.12
	end_date = 1545.7.12
}

# Ferdinand I ~ Anna (daughter of King Vladislav II of Bohemia)
royal_marriage = {
	first = HAB
	second = BOH
	start_date = 1521.1.1
	end_date = 1547.1.21
}

# Maximilian II ~ Maria (daughter of Emperor Karl V)
royal_marriage = {
	first = HAB
	second = SPA
	start_date = 1548.1.1
	end_date = 1576.10.12
}

# Ferdinand II ~ Maria-Anna (daughter of Duke Wilhelm V of Bavaria)
royal_marriage = {
	first = HAB
	second = BAV
	start_date = 1600.1.1
	end_date = 1616.1.1
}

# Ferdinand II ~ Eleanora (daughter of Duke Vincenzo I of Mantua)
royal_marriage = {
	first = HAB
	second = MAN
	start_date = 1622.1.1
	end_date = 1637.2.15
}

# Ferdinand III ~ Maria Anna (daughter of King Felipe III of Spain)
royal_marriage = {
	first = HAB
	second = SPA
	start_date = 1631.1.1
	end_date = 1646.1.1
}

# Leopold I ~ Margaret Teresa (daughter of King Felipe IV of Spain)
royal_marriage = {
	first = HAB
	second = SPA
	start_date = 1666.1.1
	end_date = 1673.1.1
}

# Joseph I ~ Wilhelmine Augusta (daughter of Duke Johann Friedrich of Brunswick-L�neburg)
royal_marriage = {
	first = HAB
	second = LUN
	start_date = 1699.2.24
	end_date = 1711.4.17
}

# Karl VI ~ Elisabeth Christine (daughter of Duke Ludwig Rudolf of Brunswick-Wolfenb�ttel)
royal_marriage = {
	first = HAB
	second = BRU
	start_date = 1708.1.1
	end_date = 1740.10.20
}

# Maria Theresa and Francis I
royal_marriage = {
	first = HAB
	second = TUS
	start_date = 1736.2.12
	end_date = 1765.8.18
}

# Joseph II ~ Maria Isabella (daughter of Duke Filipo of Parma)
royal_marriage = {
	first = HAB
	second = PAR
	start_date = 1760.10.6
	end_date = 1763.11.27
}

# Joseph II ~ Maria Josepha (daughter of Emperor Karl Albrecht)
royal_marriage = {
	first = HAB
	second = BAV
	start_date = 1765.1.23
	end_date = 1767.5.28
}

# Leopold II ~ Maria Luisa (daughter of King Carlos III of Spain)
royal_marriage = {
	first = HAB
	second = SPA
	start_date = 1765.8.5
	end_date = 1792.3.1
}

# Franz II ~ Elisabeth Wilhelmine (daughter of Duke Friedrich II of W�rttemberg)
royal_marriage = {
	first = HAB
	second = WUR
	start_date = 1788.1.6
	end_date = 1790.2.18
}

# Franz II ~ Maria Teresa (daughter of King Ferdinando I of Two Sicilies)
royal_marriage = {
	first = HAB
	second = SIC
	start_date = 1788.1.6
	end_date = 1790.2.18
}
