TUR_ideas = {
	start = {
		discipline = 0.05
		core_creation = -0.25
	}

	bonus = {
		land_forcelimit_modifier = 0.33
	}

	trigger = {
		OR = {
			tag = OTT
			tag = TUR
			tag = TUY
		}
	}
	free = yes		#will be added at load.

	ottoman_tolerance = {
 		tolerance_heathen = 2
		religious_unity = 0.33
	}
	ghazi = {
		manpower_recovery_speed = 0.05
	}
	timariot_system = {
		cavalry_power = 0.15
	}
	autonmous_pashas = {
		war_exhaustion_cost = -0.33
	}
	lawcode_of_suleiman_01 = {
		global_tax_modifier = 0.15
	}
	tulip_period = {
		global_trade_income_modifier = 0.10
	}
	imperial_school_of_naval_engineering = {
		global_ship_cost = -0.1
	}
}