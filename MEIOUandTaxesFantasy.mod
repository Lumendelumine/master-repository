name="MEIOU and Taxes Fantasy Tags v2.0"
path="mod/MEIOUandTaxesFantasy"
dependencies=
{
	"MEIOU and Taxes 1.23"
}
picture="MEIOUandTaxesFantasy.jpg"
supported_version = "1.15"

replace_path = "common/scripted_triggers"
replace_path = "common/scripted_effects"
tags={
	"MEIOU and Taxes"
}