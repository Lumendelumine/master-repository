name="M&T French and English Colonial Unit DLC Support v2.0"
path="mod/MEIOUandTaxes_cupb_unit_DLC_support"
picture="MEIOUandTaxesFIW.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
