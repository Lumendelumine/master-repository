name="M&T Cossacks Content DLC Support v1.0"
path="mod/MEIOUandTaxes_cossacks_content_DLC_support"
picture="MEIOUandTaxesCC.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
