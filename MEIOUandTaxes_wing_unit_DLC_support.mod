name="M&T Winged Hussars DLC Support v2.0"
path="mod/MEIOUandTaxes_wing_unit_DLC_support"
picture="MEIOUandTaxesWH.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
