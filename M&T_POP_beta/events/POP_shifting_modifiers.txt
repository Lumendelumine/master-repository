namespace = POP_shifting_modifiers

country_event = {
    id = POP_shifting_modifiers.001
    title = "POP_shifting_modifiers.001.t"
    desc = "POP_shifting_modifiers.001.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
 
    hidden = no 
 
    trigger = {
        owns = 118
        NOT = { has_country_modifier = modifiers_shifted }
    }
    
    mean_time_to_happen = {
        days = 1
    }
 
    immediate = {
        add_country_modifier = {
			name = modifiers_shifted
			duration = 4000
		}
	}
 
    option = {
        name = "POP_shifting_modifiers.001.a"
        ai_chance = { factor = 100 }
		
		every_country = {
			limit = {
				any_owned_province = {
					has_province_modifier = preeminent_region_city
				}
			}
			every_owned_province = {
				limit = {
					has_province_modifier = preeminent_region_city 
				}
				every_province = {
					limit = {
						region = PREV 
						NOT = { has_province_modifier = preeminent_region_city }
					}
					if = {
						limit = {
							check_variable = { which = urban_population which = PREV } 
						}
						add_permanent_province_modifier = {
							name = preeminent_region_city
							duration = -1
						}
						PREV = {
							if = {
								limit = {
									has_province_modifier = preeminent_region_city
								}
								remove_province_modifier = preeminent_region_city
							}
						}
					}
				}
			}
		}
		country_event = { 
			id = POP_shifting_modifiers.002
		}
	}
}

country_event = {
    id = POP_shifting_modifiers.002
    title = "POP_shifting_modifiers.002.t"
    desc = "POP_shifting_modifiers.002.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
 
    immediate = {
        add_country_modifier = {
			name = modifiers_shifted
			duration = 4000
		}
	}
 
    option = {
        name = "POP_shifting_modifiers.002.a"
        ai_chance = { factor = 100 }		
		
		every_country = {
			limit = {
				any_owned_province = {
					has_province_modifier = preeminent_sub_continent_city
				}
			}
			every_owned_province = {
				limit = {
					has_province_modifier = preeminent_sub_continent_city 
				}
				every_province = {
					limit = {
						superregion = PREV 
						NOT = { has_province_modifier = preeminent_sub_continent_city }
					}
					if = {
						limit = {
							check_variable = { which = urban_population which = PREV } 
						}
						add_permanent_province_modifier = {
							name = preeminent_sub_continent_city
							duration = -1
						}
						PREV = {
							if = {
								limit = {
									has_province_modifier = preeminent_sub_continent_city
								}
								remove_province_modifier = preeminent_sub_continent_city
							}
						}
					}
				}
			}
		}
		country_event = { 
				id = POP_shifting_modifiers.003
		}
	}
}

country_event = {
    id = POP_shifting_modifiers.003
    title = "POP_shifting_modifiers.003.t"
    desc = "POP_shifting_modifiers.003.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
 
    immediate = {
        add_country_modifier = {
			name = modifiers_shifted
			duration = 4000
		}
	}
 
    option = {
        name = "POP_shifting_modifiers.003.a"
        ai_chance = { factor = 100 }
		
		every_country = {
			limit = {
				any_owned_province = {
					has_province_modifier = preeminent_continent_city
				}
			}
			every_owned_province = {
				limit = {
					has_province_modifier = preeminent_continent_city 
				}
				every_province = {
					limit = {
						continent = PREV 
						NOT = { has_province_modifier = preeminent_continent_city }
					}
					if = {
						limit = {
							check_variable = { which = urban_population which = PREV } 
						}
						add_permanent_province_modifier = {
							name = preeminent_continent_city
							duration = -1
						}
						PREV = {
							if = {
								limit = {
									has_province_modifier = preeminent_continent_city
								}
								remove_province_modifier = preeminent_continent_city
							}
						}
					}
				}
			}
		}
	}
}

country_event = {
    id = POP_shifting_modifiers.004
    title = "POP_shifting_modifiers.004.t"
    desc = "POP_shifting_modifiers.004.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
 
    trigger = {
        owns = 118
        NOT = { has_country_modifier = modifiers_shifted }
    }
    
    mean_time_to_happen = {
        days = 10000
    }
 
    option = {
        name = "POP_shifting_modifiers.004.a"
        ai_chance = { factor = 100 }
		
		random_owned_province = {
			limit = {
				province_id = 223 
			}
			add_base_production = 5
			change_variable = { which = urban_population value = 5 }
		}
	}
}

country_event = { 
    id = POP_shifting_modifiers.005
    title = "POP_shifting_modifiers.005.t"
    desc = "POP_shifting_modifiers.005.d"
    picture = CITY_DEVELOPMENT_AU_eventPicture
	is_triggered_only = yes
    hidden = no 
 
    trigger = {
        owns = 118
        NOT = { has_country_modifier = modifiers_shifted }
    }
    
    mean_time_to_happen = {
        days = 10000
    }
 
    option = {
        name = "POP_shifting_modifiers.005.a"
        ai_chance = { factor = 100 }
		
		every_province = {
			limit = {
				province_id = 2311
			}
			add_base_production = 5
			change_variable = { which = urban_population value = 5 }
		}
	}
}

