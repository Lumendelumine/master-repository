# 2622 - Constanz

culture = schwabisch
owner = FUR
controller = FUR
capital = "Fürstenberg"
religion = catholic
trade_goods = beer
base_tax = 6
base_production = 1
base_manpower = 0
is_city = yes
fort_14th = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = FUR
}
1453.1.1 = {
	
}
1500.1.1 = { road_network = yes }
1670.1.1  = {
	
}
1700.1.1  = {
	
}
1733.1.1  = {
	unrest = 2
	
	base_tax = 5
base_production = 5
} # Karl Alexander becomes Duke of Würtemberg. He is a catholic with a jewish  advisor, which is very much resented by the protestant nobility.
1737.3.12 = {
	unrest = 0
	
} # Death of the Duke, execution of the advisor after a set up process.
1806.7.12 = {
	hre = no
	owner = BAD
	controller = BAD
	add_core = BAD
	remove_core = FUR
} # The Holy Roman Empire is dissolved
