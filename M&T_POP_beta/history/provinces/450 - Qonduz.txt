# 450 - Qonduz

owner = KAB
controller = KAB
culture = uzbehk			#FB was: "pashtun", was causing issues with Khivan revolt and the area is mixed Tajik/Pashtun
religion = sunni
capital = "Qonduz"
trade_goods = wool
hre = no
base_tax = 16
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = KAB
	add_core = TIM
}
1369.1.1  = {
	owner = TIM
	controller = TIM
}
1444.1.1  = {
	owner = KTD
	controller = KTD
	remove_core = TIM
	remove_core = KAB
	add_core = DUR
} # Shaybanids break free from the Timurids
1457.7.19 = { controller = TIM }
1457.8.30 = {
	owner = TIM
	add_core = TIM
	remove_core = KTD
}
1502.1.1 = {
	owner = KTD
	controller = KTD
	add_core = KTD
	remove_core = TIM
	add_core = DUR
} # The end of the Timurids in Samarkand
1504.6.1  = {
	controller = TIM
	owner = TIM
	add_core = TIM
	remove_core = KTD
} #Conquered by Babur
1512.1.1 = {
	controller = BUK
	owner = BUK
	add_core = BUK
	remove_core = TIM
}
1515.1.1 = { training_fields = yes }
#1526.4.21 = {
#	owner = MUG
#	controller = MUG
#	add_core = MUG
#	remove_core = TIM
#} # Battle of Panipat
1566.6.1  = { revolt = { }
	owner = KAB
	controller = KAB
}	#Independent of Mughals for a long while
1585.1.1  = {
	controller = MUG
}	# Man Singh occupies Kabulistan after death of Mirza Hakim
1585.2.1  = {
	owner = MUG
} # Annexed into empire again
1672.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels }
} # Widespread tribal uprisings
1675.1.1  = {
	controller = MUG
	revolt = { }
} # End of uprisings
1677.1.1  = { discovered_by = FRA }
1689.1.1 = {
	controller = REB ##
} # Uzbeks
1690.1.1 = {
	discovered_by = ENG
	owner = SHY
	controller = SHY
} # Uzbeks
1707.5.12 = { discovered_by = GBR }
1740.1.1  = {
	controller = PER
}
1741.1.1  = {
	owner = PER
} # Captured by Persia, Nadir Shah
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
