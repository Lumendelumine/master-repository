# 378 - Tripoli

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "Tarabulus Al-Sham"
trade_goods = cotton
hre = no
base_tax = 12
base_production = 1
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1200.1.1 = { road_network = yes }
1250.1.1 = { harbour_infrastructure_1 = yes }

1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1500.1.1 = { fort_14th = yes }
1516.1.1   = { add_core = TUR }
1516.8.28  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1750.1.1 = {  }
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
