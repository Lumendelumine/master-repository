#203 - Bresse

owner = SAV
controller = SAV
add_core = SAV
culture = arpitan
religion = catholic
capital = "Bourc"
base_tax = 13
base_production = 0
base_manpower = 1
is_city = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1349.1.1   = { add_core = FRA } # Dauphin� is sold to Philippe VI
1467.6.15  = { add_core = BUR } # Charles the Bold ascends to the throne and lays claims
1477.1.5   = { remove_core = BUR } # Charles the Bold dies
1536.4.1   = {
	controller = FRA
	add_core = FRA
} 
1538.6.17   = {
	controller = SAV
}
1559.1.1   = {
	remove_core = FRA
} 
1601.1.1  = {
	owner = FRA
	controller = FRA
} 
1625.1.1   = { fort_14th = no fort_15th = yes }
1632.1.1   = { unrest = 3 }
1634.1.1   = { unrest = 0 }
1641.1.1   = { unrest = 3 }
1644.1.1   = { unrest = 0 }
1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1650.3.1   = { revolt = { type = noble_rebels size = 2 } controller = REB unrest = 3 } # Fronde rebels take control
1651.4.1   = { revolt = { } controller = FRA unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = { unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1680.1.1   = { fort_15th = no fort_16th = yes }

1715.1.1   = { fort_16th = no fort_17th = yes }
