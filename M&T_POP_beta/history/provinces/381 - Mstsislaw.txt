# 381 - Mstsislaw

owner = SMO
controller = SMO     
culture = ruthenian
religion = orthodox 
hre = no
base_tax = 8
base_production = 0
trade_goods = wheat
base_manpower = 0
is_city = yes
capital = "Mstsislaw"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

#1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = LIT
	add_core = SMO
	add_core = PLT
}
1377.1.1   = {
	owner = LIT
	controller = LIT
}
1523.8.16 = { mill = yes }
1567.1.1  = { fort_14th = yes }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1772.8.5  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = LIT
	remove_core = PLC
	culture = byelorussian
} # First partition of Poland
1779.1.1  = {  } # Almost entirely rebuilt.
 # Became a large trade center.
1812.6.28  = { controller = FRA } # Occupied by French troops
1812.12.10 = { controller = RUS }
