# 4 - Dalarna
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE
culture = swedish
religion = catholic
capital = "Falun"
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = copper
discovered_by = eastern
discovered_by = western
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = stora_kopparberget_modifier
		duration = -1
	}
}
1515.1.1 = { training_fields = yes }
1527.6.1  = { religion = protestant}
1529.12.17 = { merchant_guild = yes }
1544.1.1  = { fort_14th = yes } #Västerås Castle
 #controller of trade route from the Swedish Mine Region
 #a lot of weapon factories in this region
 #Västmanlandsregemente
1625.1.1  = { fort_14th = no fort_15th = yes } 
 #minor court belonging to Svea Hovrätt
1670.1.1  = {  } #Due to the "Staplepolicy act"
 #Due to the support of manufactories
1755.1.1  = { fort_15th = no fort_16th = yes }
