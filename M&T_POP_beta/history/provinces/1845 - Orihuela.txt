# 1845 - Orihuela

owner = ARA
controller = ARA
culture = valencian
religion = catholic 
hre = no
base_tax = 6
base_production = 1
trade_goods = marble
base_manpower = 0
is_city = yes
fort_14th = yes
capital = "Orihuela"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = ARA
}
1510.1.1 = { temple = yes }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1566.1.1   = {
	culture = andalucian
}
1713.4.11  = {
	remove_core = CAS
}
1808.6.6   = {
	controller = REB
}
1811.1.1   = {
	controller = SPA
}
1812.10.1  = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
