# 101 - Zena

owner = GEN
controller = GEN
culture = ligurian
religion = catholic
capital = "Zena"
base_tax = 2
base_production = 10
base_manpower = 1
is_city = yes
trade_goods = naval_supplies

fort_14th = yes
shipyard = yes
extra_cost = 20


discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes


1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1119.1.1 = { bailiff = yes }
1200.1.1 = { urban_infrastructure_1 = yes corporation_guild = yes merchant_guild = yes harbour_infrastructure_3 = yes } }
1250.1.1 = { temple = yes }
1252.1.1  = {  }

1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_permanent_province_modifier = { name = ma_ligure duration = -1 }
	add_core = GEN
	add_permanent_province_modifier = {
		name = "county_of_torriglia"
		duration = -1
	}
}
1471.1.1 = { medieval_university = yes }
1477.3.20 = {
	controller = REB
 }
1477.4.28 = {
	controller = GEN # MLO
}
1478.7.7  = {
	controller = REB
}
#1481.1.1  = {
#	university = yes
#} #University of Genoa est. 1481
1488.1.6  = {
	controller = GEN # MLO
}
1488.8.7  = {
	controller = REB
}
1488.9.13 = {
	controller = GEN # MLO
}
1499.1.1  = {
	owner = FRA
	controller = FRA
	add_core = FRA
}
1500.1.1  = {
	
}
1507.4.10 = {
	controller = REB
}
1507.4.27 = {
	controller = FRA
}
1512.1.1  = {
	controller = REB
}
1513.1.1  = {
	controller = FRA
}
1522.1.1  = {
	owner = GEN
	controller = GEN
}
1527.1.1  = {
	owner = FRA
	controller = FRA
}
1528.1.1  = {
	owner = GEN
	controller = GEN
	remove_core = FRA
} #Andrea Doria
1618.1.1  =  { hre = no }
1805.6.10 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1814.4.11 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = FRA
} # Incorporated into the kingdom of Sardinia
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
