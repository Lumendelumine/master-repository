# 2390 - Slavonija

owner = HUN
controller = HUN
culture = croatian
religion = catholic
capital = "Osijek"
base_tax = 7
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = wine

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
1356.1.1 = {
	owner = CRO
	controller = CRO
	add_core = HUN
	add_core = CRO
}
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_core = HUN
}	
1526.8.30  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = HAB
}
1671.1.1   = {
	unrest = 5
} # Conspiracy against Habsburg rule uncovered
1671.5.1   = {
	unrest = 0
} # Estimated, the conspirators are executed
1699.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
	unrest = 0
} # Austrian occupation
1700.1.1   = {
	unrest = 7
}
