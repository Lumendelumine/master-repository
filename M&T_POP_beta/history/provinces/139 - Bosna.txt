# 139 - Bosna

owner = BOS
controller = BOS
culture = serbian
religion = catholic # with large gnostic minotiry
capital = "Bobovac"
trade_goods = silver
hre = no
base_tax = 10
base_production = 1
base_manpower = 1
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1119.1.1 = { bailiff = yes }
1356.1.1   = {
	add_core = BOS
	add_core = CRO
}
1394.1.1   = {
	remove_core = CRO
}
1444.1.1 = {
	add_core = TUR
}
1463.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The Ottoman province of Bosnia
1515.2.1 = { training_fields = yes }
1593.1.1  = {
	unrest = 3
} # Fighting began in northwestern Bosnia, sparked Habsburg-Ottoman conflict
1600.1.1  = {
	
}
1606.1.1  = {
	unrest = 0
} # Temporarty peace
1650.1.1  = {
	culture = bosnian
	religion = sunni
}
1670.1.1  = {
	
}
1683.1.1  = {
	unrest = 6
} # Heavy fighting & destruction in western Bosnia
1699.1.1  = {
	unrest = 0
} # Flood of Muslim refugees from Slavonia & Ottoman Hungary 
1700.1.1  = {
	unrest = 7
}
1716.12.9 = {
	controller = HAB
} # Occupied by Habsburg
1718.7.21 = {
	controller = TUR
} # Estimated
1730.1.1  = {
	
}
1737.7.1  = {
	controller = HAB
} # Occupied by Habsburg again
1739.9.18 = {
	controller = TUR
} # Treaty of Belgrade, Habsburg gave up its claim to the territory
1788.2.9  = {
	controller = HAB
} # Habsburg invasion
1791.8.4  = {
	controller = TUR
} # Peace settlement
