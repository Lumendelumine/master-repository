# No previous file for Fejer

owner = HUN
controller = HUN  
culture = hungarian
religion = catholic
capital = "Szekesfehervar"
trade_goods = wheat
hre = no
base_tax = 9
base_production = 0
base_manpower = 0
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = HUN
}
1523.8.16 = { mill = yes }
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1530.1.1 = { add_claim = TUR }
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
