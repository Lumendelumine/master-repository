# No previous file for Gostynin

owner = MAZ
controller = MAZ
add_core = MAZ
add_core = POL
religion = catholic
culture = polish
capital = "Rawa"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = wheat

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech
hre = no

#1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1462.2.28 = {
	owner = POL
	controller = POL
	add_core = POL
	remove_core = MAZ
}

1523.8.16 = { mill = yes }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1575.1.1   = {  fort_14th = yes }
1588.1.1   = { controller = REB } # Civil war, Polish succession
1589.1.1   = { controller = PLC } # Coronation of Sigismund III
1596.1.1   = {  } #Polish capital moved
1656.7.28  = { controller = SWE } # Battle of Warsaw, against Sweden & Brandenburg
1660.1.1   = { controller = PLC } # End of Northern war
1700.1.1   = {  }
1702.5.1   = { controller = SWE } # Occupied again
1706.9.24  = { controller = PLC } # Karl XII defeated in the battle of Poltava
1733.1.1   = { controller = REB } # The war of Polish succession
1735.1.1   = { controller = PLC  }
 # Kolegium Pijar�w founded
1768.2.28  = { unrest = 8 } # Uprisings against the Polish king & Russia
1772.8.18  = { unrest = 0 } # Uprisings suppressed by Russian troops
1793.1.23 = {
	controller = PRU
	owner = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Second partition
1794.3.24  = { unrest = 5 } # Kosciuszko uprising
1794.11.5  = { unrest = 0 } # Kosciuszko is captured
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1806.11.3  = { controller = REB } # Polish uprising instigated by Napoleon
1807.7.9   = {
	owner = POL
	controller = POL
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = { controller = RUS }
1814.4.11  = { controller = POL }
1815.6.9   = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
