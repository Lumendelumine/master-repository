#361 - Al Qahira

owner = MAM
controller = MAM
culture = egyptian
religion = sunni
capital = "Al Qahira"
trade_goods = carpet
hre = no
base_tax = 14
base_production = 35
base_manpower = 2
is_city = yes
small_university = yes

discovered_by = ALW
discovered_by = MKU
discovered_by = CIR
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = preeminent_continent_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = preeminent_region_city
		duration = -1
	}
}
1119.1.1 = { bailiff = yes }
1200.1.1 = { urban_infrastructure_2 = yes corporation_guild = yes merchant_guild = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = MAM
	add_core = EGY
}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1520.11.10 = { controller = REB } # Mameluk Uprising under Canbirdi Ghazeli
1521.1.1   = { controller = TUR }
1530.1.1   = {
	#owner = MAM
	#controller = MAM
	add_core = MAM
	#remove_core = TUR
}
1586.1.1   = { unrest = 2 } # Beginning of oppositions
1589.1.1   = { unrest = 4 }
1598.1.1   = { unrest = 6 }

1601.1.1   = { unrest = 8 }
1604.1.1   = { unrest = 10 }
1609.1.1   = { controller = REB } # Attempt to stop the troops from extracting payment
1610.2.5   = { controller = TUR  } # The leaders are executed
1623.1.1   = { unrest = 5 }
1624.1.1   = { unrest = 2 }
1631.1.1   = { unrest = 4 }
1632.1.1   = { unrest = 0 }
1695.1.1   = { unrest = 3 } # Demonstrations against the Mamelukes
1700.1.1   = {  unrest = 0 }
1724.1.1   = { unrest = 3 } # Power struggle

1770.1.1   = { controller = REB } # Ali Bey's Rebellion
1771.1.1   = { controller = TUR }
1789.10.22 = { unrest = 6 } # Introduction of house tax, strained relations 
1796.1.1   = { controller = REB } # Revolts against the Ottomans
1798.8.10  = {
	revolt = {}
	controller = FRA
} # Occupied by the French
1802.10.1  = {
	controller = TUR
	unrest = 8
} # Turkish rule is restored but a few troublesome years follow
1805.1.1 = {
	unrest = 0
	owner = EGY
	controller = EGY
}
1811.6.1   = {
	owner = TUR
	controller = TUR
} # Order is restored
