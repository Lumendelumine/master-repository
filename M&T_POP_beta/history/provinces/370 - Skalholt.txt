# West Iceland
# MEIOU - Gigau

owner = ICE
controller = ICE
add_core = ICE
culture = norse
religion = catholic
hre = no
base_tax = 4
base_production = 0
trade_goods = fish # ivory
base_manpower = 0
is_city = yes
capital = "Reykjavik"
discovered_by = western

1088.1.1 = { dock = yes }
1262.1.1   = {
	owner = NOR
	controller = NOR
	add_core = NOR
	remove_core = ICE
}
 # Established during the 12th century
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = NOR
}
1550.1.1 = { religion = protestant }
1752.1.1 = { trade_goods = wool } # Wool becomes more important.
 # Receives trade rights
1814.5.17 = { owner = DEN controller = DEN add_core = DEN remove_core = DAN }
