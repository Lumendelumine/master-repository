# 1400 - Basarabia

owner = MOL
controller = MOL  
culture = moldovian
religion =  orthodox
capital = "Orheiu"

trade_goods = wine
hre = no
base_tax = 9
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = MOL

}
#1498.1.1  = {
#	add_core = TUR
#} # Bayezid II forces Stephen the Great to accept Ottoman suzereignty.
1660.1.1  = {
	
}
1812.5.28 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = TUR
} # Treaty of Bucarest ending the Russo-Turkidh War
