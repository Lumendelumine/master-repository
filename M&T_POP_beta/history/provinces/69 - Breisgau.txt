#69 - Breisgau
# Freiburg

owner = FRE
controller = FRE
culture = alemanisch
religion = catholic
capital = "Freiburg" 
base_tax = 7
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = silver

discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = FRE
	add_core = HAB 
}
1368.1.1 = { owner = HAB controller = HAB remove_core = FRE }
1379.1.1   = {
	controller = TIR
	owner = TIR
	add_core = TIR
}
1457.1.1 = { medieval_university = yes }
1490.1.1   = {
	controller = HAB
	owner = HAB
	add_core = HAB
	remove_core = TIR
}

 # Freiburger M�nster 
 #Freiburg has one of Germany's oldest Universities (founded 1457)
1500.1.1 = { road_network = yes }
1510.1.1  = { fort_14th = yes } #Breisach is one of the most important Fortresses on the upper Rhine
1524.4.1  = { unrest = 8 } # Peasant Revolts - 18.000 peasants take Freiburg (May)
1524.12.1 = { unrest = 0 }



1600.1.1  = {  }
1615.1.1  = {  }
1646.1.1  = { fort_14th = yes }
1797.10.17 = {	owner = MOD
		controller = MOD
		add_core = MOD
	     	remove_core = HAB
	     } # Treaty of Campo Formio
1805.12.26 = {	owner = BAD
		controller = BAD
		add_core = BAD
		remove_core = MOD
	     } # Treaty of Pressburg
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
