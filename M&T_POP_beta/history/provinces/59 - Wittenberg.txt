#59 - Wittenberg

owner = SAX
controller = SAX
culture = high_saxon
religion = catholic
capital = "Wittenberg"
trade_goods = copper
hre = yes
base_tax = 8
base_production = 1
base_manpower = 0
is_city = yes
add_core = SAX
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = { road_network = yes }
1530.1.1  = { religion = protestant }
1589.1.1  = { fort_14th = yes } # Current structure of Festung Königstein is built

 # First European porcelain is manufactured
1790.8.1  = { unrest = 5 } # Peasant revolt
1791.1.1  = { unrest = 0 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} #Ceded to Prussia
