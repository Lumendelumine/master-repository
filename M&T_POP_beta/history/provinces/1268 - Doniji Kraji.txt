# 1268 - Doniji Kraji

owner = CRO
controller = CRO
culture = croatian
religion = catholic
capital = "Banja Luka"
trade_goods = lumber
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

#1385.1.1   = {
1356.1.1   = {
	owner = BOS
	controller = BOS
	add_core = BOS
	add_core = CRO
}
1394.1.1   = {
	owner = CRO
	controller = CRO
	remove_core = BOS
}
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_core = HUN
}	
#1463.1.1  = {
#	owner = TUR
#	controller = TUR
#	add_core = TUR
#} # The Ottoman province of Bosnia
1526.8.30  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = HAB
}
1540.1.1   = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The Bihac fort would become the westernmost fort taken by the Ottoman army
1560.1.1   = {
	
}
1650.1.1   = {
	culture = bosnian
	religion = sunni
}
1671.1.1   = {
	unrest = 5
} # Conspiracy against Habsburg rule uncovered
1671.5.1   = {
	unrest = 0
	
} # Estimated, the conspirators are executed
1700.1.1   = {
	unrest = 7
}
1813.1.1   = {
	unrest = 3
} # Croatian national revival (Hrvatski narodni preporod)
