# 190 Bourbon - Principal cities: Moulin Vichy

owner = BOU
controller = BOU 
add_core = BOU
add_core = FRA
culture = auvergnat
religion = catholic
capital = "Moulins"
base_tax = 12
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = iron
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1500.1.1 = { temple = yes }
1527.1.1   = {
	owner = DAL
	controller = DAL
	add_core = DAL
	remove_core = BOU
}
1530.1.1   = { fort_14th = yes } # Important metal & arms industry
1573.9.1   = { unrest = 5 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { unrest = 0 } # Charles IX dies, situation cools a bit	
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1589.8.2   = { owner = FRA controller = FRA } # Charles IV of Bourbon dies, Bourbon added to France
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism

1630.1.1   = { fort_14th = no fort_15th = yes }
1639.1.1   = { unrest = 3 }
1641.1.1   = { unrest = 0 }

1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1650.3.1   = { controller = REB unrest = 3 } # Fronde rebels take control
1651.4.1   = { controller = FRA unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = { unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.

1700.1.1   = { fort_15th = no fort_16th = yes }
