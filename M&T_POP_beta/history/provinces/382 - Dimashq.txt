# 382 - Suriya
# MEIOU-GG - Turko-Mongol mod

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "Dimashq"
trade_goods = steel
hre = no
base_tax = 14
base_production = 7
base_manpower = 1
is_city = yes
fort_14th = yes
small_university = yes
estate = estate_nobles
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = preeminent_continent_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = preeminent_region_city
		duration = -1
	}
}
1119.1.1 = { bailiff = yes }
1133.1.1 = { urban_infrastructure_2 = yes corporation_guild = yes }
1200.1.1 = { merchant_guild = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1516.1.1   = { add_core = TUR }
1516.11.8  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops

1520.1.1 = { unrest = 4 } # Revolt against Ottoman rule
1521.1.1 = { unrest = 0 }
1600.1.1 = { }
1720.1.1 = {  }
1771.1.1 = { unrest = 4 } # Ali Bey gained control of Syria, reconstituting the Mamluk state
1772.1.1 = { unrest = 0 }
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
