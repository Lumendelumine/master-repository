# 3378 - Heidelberg

capital = "Heidelberg"
culture = eastfranconian
religion = catholic
trade_goods = wine
owner = PAL
base_tax = 8
base_production = 1
base_manpower = 1
is_city = yes
fort_14th = yes
 # The Kaiserdom in Speyer is on of the oldest and greatest in the HRE, built over the 11th and 12th century
controller = PAL
add_core = PAL
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim




 # The Reichskammergericht (1495-1806) is the highest court in the HRE situated in Worms and after 1527 Speyer

1200.1.1 = { road_network = yes }
1386.1.1 = { small_university = yes }
1495.1.1 = { courthouse = yes }
1500.1.1 = { road_network = yes }
1546.4.19  = { religion = protestant } # #Friedrich II converts the coutnry to protestant

1620.1.1   = { fort_14th = yes  }
1689.1.1   = { fort_14th = no fort_15th = yes }
1689.8.1   = { fort_15th = no fort_16th = no controller = FRA } # French troops burn, pillage and destroy in the succession wars.
1697.4.19  = { controller = PAL } # Peace of Rijswijk strengthens the catholics
1700.1.1   = { fort_14th = yes }
1750.1.1   = {  }
1777.12.30 = {
	owner = BAV
	controller = BAV
	add_core = BAV
} # Karl Theodor becomes elector of both the Pawesternate and Bavaria after Maximillian III's death
1796.8.7   = {
	owner = FRA
	controller = FRA
	add_core = FRA
}
1803.1.1   = {
	owner = BAD
	controller = BAD
	add_core = BAD
	remove_core = FUR
	remove_core = FRA
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
