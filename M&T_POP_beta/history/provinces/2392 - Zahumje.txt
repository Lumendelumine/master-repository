# 2392 - Zahumje

owner = HUM
controller = HUM
culture = croatian
religion = gnostic
capital = "Mostar"
trade_goods = wool
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = HUM
}
1373.1.1   = {
	owner = BOS
	controller = BOS
	add_core = BOS
}
1441.1.1 = { 
	owner = HUM
	controller = HUM
	add_core = HUM
	remove_core = BOS
}
1444.1.1 = {
	add_core = TUR
}
1482.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = HUM
} # The Ottoman province of Hezergovina
1550.1.1   = {
	
}
1650.1.1   = {
	culture = bosnian
	religion = sunni
}
1700.1.1   = {
	
	unrest = 7
}
