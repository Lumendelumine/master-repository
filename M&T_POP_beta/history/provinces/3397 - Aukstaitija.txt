# No previous file for Aukstaitija

owner = LIT
controller = LIT
capital = "Kaunas"
culture = lithuanian
religion = baltic_pagan_reformed
trade_goods = livestock
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1356.1.1   = {
	add_core = LIT
}
1386.1.1   = {
	religion = catholic
}
1389.1.1   = { owner = TEU controller = TEU add_core = TEU }
1411.1.1   = { owner = LIT controller = LIT remove_core = TEU }

1500.1.1   = {   }
1543.1.1   = { unrest = 3 } # Counter reformation
1569.7.1   = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1579.1.1   = { unrest = 0 }

1791.6.1   = { unrest = 3} # 3rd May constitution raised opposition
1794.3.24  = { unrest = 6 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 }
1795.10.24  = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Most of Lithuania became part of the Russian empire
