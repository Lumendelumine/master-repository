# 1 - Uppland
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE
culture = swedish
religion = catholic
capital = "Stockholm"
trade_goods = iron
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
fort_14th = yes
hre = no
 #"Stockholms borgarskap"

discovered_by = eastern
discovered_by = western
discovered_by = muslim


1000.1.1   = {
	add_permanent_province_modifier = {
		name = preeminent_region_city
		duration = -1
	}
}
1119.1.1 = { bailiff = yes }
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
1356.1.1   = {
	owner = RSW
	controller = RSW
	add_core = RSW
	add_core = SWE
}
1360.1.1   = {
	owner = SWE
	controller = SWE
	remove_core = RSW
}
1477.1.1  = {
	medieval_university = yes
}
1515.1.1 = { training_fields = yes }
1522.2.15 = { dock = yes shipyard = yes }
1527.6.1 = {
	religion = protestant
	reformation_center = protestant
	
}
1529.12.17 = { merchant_guild = yes }
1530.1.1 = { weapons = yes }
1537.1.1   = { fort_14th = yes } #Gripsholms Castle
1598.8.12  = { controller = PLC } #Sigismund tries to reconquer his crown
1598.12.15 = { controller = SWE } #Duke Karl get it back
1621.1.1   = { fort_14th = no fort_15th = yes } # Key forts defending roads to Stockholm
1680.1.1   = { fort_15th = no fort_16th = yes } # Added forts giving coastal defense of region
1730.1.1   = { fort_16th = no fort_17th = yes } # estimated date
