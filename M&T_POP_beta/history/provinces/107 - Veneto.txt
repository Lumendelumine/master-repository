# 107 - V�neto

owner = VEN		
controller = VEN
culture = venetian 
religion = catholic 
capital = "Travixo" 
base_tax = 9
base_production = 1
base_manpower = 0
is_city = yes       
trade_goods = wheat
fort_14th = yes	
small_university = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1133.1.1 = { mill = yes urban_infrastructure_1 = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = VEN
	add_claim = VER
 } 
1381.1.1   = {
	owner = HAB
	controller = HAB
}
1384.1.1   = {
	owner = VEN
	controller = VEN
} 
1509.6.1   = { controller = FRA } # Venice collapses
1512.1.1   = { controller = VEN } # Brescia revolts
1512.2.18  = { controller = FRA } # Sack of Brescia
1513.3.23  = { controller = VEN }
1618.1.1  =  { hre = no }

1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = VEN
} # Treaty of Campo Formio
1805.3.17  = {
	owner = ITE
	controller = ITE
	add_core = ITE
} # Merged with the Cisalpine Republic
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1866.1.1 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
