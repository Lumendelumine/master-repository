# 152 - Zagorje

owner = HUN
controller = HUN
culture = croatian
religion = catholic
capital = "Varazdin"
trade_goods = hemp
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	owner = CRO
	controller = CRO
	add_core = HUN
	add_core = CRO
}
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_core = HUN
}	
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Battle of Moh�cs
#1540.1.1  = {
#	owner = TUR
#	controller = TUR
#	add_core = TUR
#}
#1604.1.1  = { controller = REB } # The nobility in Royal Hungary rebelled
#1606.1.1  = { controller = TUR }

1687.9.29 = { controller = HAB } # Occupied by the Habsburg Empire
1699.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
}
1721.1.1  = { fort_14th = yes }
