# 1394 - Pales�sye

owner = LIT
controller = LIT
culture = ruthenian
religion = orthodox
capital = "Turav"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = LIT
}
1523.8.16 = { mill = yes }

1569.1.1   = { fort_14th = yes }
1569.7.1   = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1793.1.23  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = PLC
	culture = byelorussian
} # First partition
