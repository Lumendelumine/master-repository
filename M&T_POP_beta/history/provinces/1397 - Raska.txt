# 1397 - Raska

owner = SER
controller = SER
culture = serbian
religion = orthodox
capital = "Ras"
trade_goods = wool
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
add_core = SER
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = { add_local_autonomy = 15 }
1444.1.1 = {
	add_core = TUR
}
1455.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_local_autonomy = -15
}
1515.2.1 = { training_fields = yes }
1688.11.7 = { unrest = 7 } # Serb rebellion under D. Brankovic

1750.1.1  = { fort_14th = yes }
1806.1.8  = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} # The first Serbian uprising
1813.1.1  = {
	revolt = { }
	controller = TUR
}
1815.4.23 = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} # The second Serbian uprising
1817.1.1  = {
	revolt = { }
	controller = TUR
}
