# 2311 - Sevilla + Utrera + Cazalla de la Sierra + Alcal� de Guadaira + Guadalcanal + Constantina

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = andalucian # culture = western_andalucian
religion = catholic
hre = no
base_tax = 5
base_production = 8
trade_goods = silk
base_manpower = 1
capital = "Sevilla"
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech


1000.1.1 = {
	add_permanent_province_modifier = {
		name = guadalquivir_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	
}
1200.1.1 = { marketplace = yes  }
1350.1.1 = { urban_infrastructure_1 = yes harbour_infrastructure_2 = yes workshop = yes }
1356.1.1   = {
	set_province_flag = spanish_name
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_sevilla"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
}
### 1503.1.1  = {  } # The "Casa de la Contrataci�n" is established in Sevilla as the monarchy tries to control American trade through that port.

1506.1.1 = { temple = yes }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1520.1.1   = {
	
	
}

1610.1.12  = {  } # Decree for the expulsion of the morisques in Andaluc�a, which is speedily and uneventfully performed
1713.4.11  = { remove_core = CAS }
1730.1.1   = {  }
1775.1.1   = {  }
1808.6.6   = { controller = REB }
1813.12.11 = { controller = SPA }
