# 162 - Maramures

owner = HUN
controller = HUN
culture = ruthenian
religion = orthodox
capital = "Sighet"

trade_goods = copper
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes
hre = no
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1   = {
	add_core = HUN
}
1515.2.1 = { training_fields = yes }
1526.8.30  = {
	owner = TRA
	controller = TRA
	add_core = HAB
	add_core = TRA
}
#1570.8.16  = { owner = TRA controller = TRA } # Abdication of Janos II as king of Hungary, becomes prince of Transylvania
1711.5.1   = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
