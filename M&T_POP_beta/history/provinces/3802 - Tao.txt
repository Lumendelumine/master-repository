# No previous file for Tao

owner = GEO
controller = GEO
culture = owm_armenian
religion = coptic
capital = "Artanuji"
trade_goods = wool
hre = no
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
add_core = GEO
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1  = {
	add_core = GEO
}
1444.1.1  = {
        add_core = SMT
}
1466.1.1  = {
	owner = SMT
	controller = SMT
	add_core = SMT
	remove_core = GEO
}
1530.1.1 = { add_core = TUR } #Ruled by local Turkish Beys
1550.1.1 = { fort_14th = yes }
1551.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1600.1.1 = {
	religion = sunni
}
1658.1.1 = { controller = REB } # Revolt of Abaza Hasan Pasha
1659.1.1 = { controller = TUR }