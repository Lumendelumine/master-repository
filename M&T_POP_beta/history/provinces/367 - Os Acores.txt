# 367 - The Azores

culture = portugese
religion = catholic
capital = "Ponta Delgada"
trade_goods = fish	#could be wheat
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 0
native_ferocity = 0
native_hostileness = 0

1427.1.1  = {
	discovered_by = POR
	owner = POR
	controller = POR
	citysize = 100
	is_city = yes
}
1452.1.1  = {
	add_core = POR
	citysize = 500
}
1500.1.1 = { citysize = 1240 }
1550.1.1 = { citysize = 1800 }
1600.1.1 = { citysize = 2790 }
1650.1.1 = { citysize = 3560 }
1700.1.1 = { citysize = 4100 }
1750.1.1 = { citysize = 4800 }
1800.1.1 = { citysize = 5100 }
