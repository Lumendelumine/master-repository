# 2749 - Candia

owner = VEN
controller = VEN
culture = greek
religion = orthodox
capital = "Candia"
trade_goods = sugar
hre = no
base_tax = 1
base_production = 1
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { harbour_infrastructure_2 = yes }
1100.1.1 = { marketplace = yes }

1356.1.1 = {
	add_core = VEN
	add_core = CRT
	add_core = BYZ
	set_province_flag = greek_name
}
1444.1.1 = {
	remove_core = BYZ
	add_core = CRT
}
1453.5.30 = {
	remove_core = BYZ
}
1470.1.1  = { controller = REB } # Cretan rebellion against Venetian rule
1471.1.1  = { controller = VEN } # Estimated

1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0 }

1580.1.1  = { fort_14th = yes } # Rethymnon
1669.9.26 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = VEN
	change_province_name = "Kandiye"
	rename_capital = "Kandiye"
}
1692.1.1  = { controller = REB } # Insurrection
1693.1.1  = { controller = TUR } # Estimated
1750.1.1  = { add_core = GRE }
1770.3.9  = { controller = REB } # Revolutionary government
1770.6.1  = { controller = TUR }
