# comte de charolais comte de chalons comte de macon
# chalons charolle macon cluny

owner = AMG
controller = AMG 
culture = burgundian
religion = catholic
capital = "Charolle"
base_tax = 12
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = wine
 #  the Abbey of Cluny
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BUR
	add_core = FRA
	add_core = AMG
}
1391.7.25 = {
	owner = BUR
	controller = BUR
	remove_core = AMG
} # Sold to Philippe le Hardi
1444.1.1 = { remove_core = FRA }
1477.1.5 = { add_core = FRA }
1482.3.27 = {
	owner = FRA
	controller = FRA
	add_core = HAB
} # Charles the Bold dies and transfers Bourgogne to France
1493.1.1  = {
	owner = HAB
	controller = HAB
}
1507.1.1  = { controller = FRA } 
1509.1.1  = { controller = HAB }
1515.1.1 = { training_fields = yes }
1521.1.1  = { controller = FRA } 
1526.1.1  = { controller = HAB }
1530.1.1  = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = HAB
}
1536.1.1  = { controller = FRA } 
1544.1.1  = { controller = HAB }
1551.1.1  = { controller = FRA } 
1556.1.14 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1635.1.1  = { controller = FRA }
1668.1.1  = { controller = SPA }
1674.1.1  = { controller = FRA }
1678.1.1  = { controller = SPA }
1684.1.1  = {
	owner = FRA
	controller = FRA
	remove_core = SPA
}
1685.1.1  = { fort_14th = yes }
1690.1.1  = {  }
1750.1.1  = {  }
