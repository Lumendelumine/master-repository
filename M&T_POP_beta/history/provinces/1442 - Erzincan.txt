# 1442 - Erzincan

owner = ERE
controller = ERE
culture = ge_armenian
religion = coptic
capital = "Erzincan"
trade_goods = wine
hre = no
base_tax = 6
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1   = {
	add_core = ERZ
	add_core = ERE
}
1380.1.1   = {
	owner = ERZ
	controller = ERZ
	remove_core = ERE
}
1381.1.1  = {
	owner = OTT
	controller = OTT
	add_core = OTT
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1400.1.1  = {
	owner = TIM
	controller = TIM
	add_core = TIM
} # Timur takes control of Sivas
1405.1.1 = {
	owner = AKK		#1402
	controller = AKK
	add_core = AKK
} # The Ak Koyunlu
1408.1.1  = {
	owner = TUR
	controller = TUR
	remove_core = TIM
} # The Ottomans restore control
1444.1.1 = {
	remove_core = AKK
	remove_core = ERE
}

1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = ERE } ###
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1615.1.1  = { base_tax = 3
base_production = 3 } #The Decentralizing Effect of the Provincial System
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1640.1.1  = {   } # One of the most important port-cities during the 17th-19th centuries
1740.1.1  = {  }
