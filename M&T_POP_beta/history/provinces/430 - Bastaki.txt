# 430 - Bastaki

owner = ISF
controller = ISF
culture = khuzi
religion = sunni
capital = "Bastak"
trade_goods = coffee
hre = no
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = indian
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = ISF
	add_core = ORM
	add_core = MUZ
	controller = MUZ
}
1357.1.1   = {
	owner = MUZ
}
1393.5.22  = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = MUZ
}
1444.1.1 = {
	controller = ISF
	owner = ISF
	remove_core = TIM
	add_core = AKK
}
1447.1.1   = {
	controller = QAR
}
1447.3.11  = {
	owner = QAR
	add_core = QAR
	remove_core = TIM
} # Fars and surroundings to Qara Quyunlu
1469.11.1  = {
	controller = AKK
}
1470.1.1   = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1490.1.1   = {
	controller = REB
	revolt = { type = noble_rebels }
} # Civil War
1494.1.1   = {
	controller = AKK
	revolt = { }
} # Civil War
1497.1.1   = {
#	controller = MSS
#	owner = MSS
#	add_core = MSS
	religion = shiite
}
1507.1.1   = {
	controller = SAM
}
1508.2.1   = {
	owner = SAM
}
1512.1.1   = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	remove_core = AKK
} # Safawids "form persia"

1677.1.1  = { discovered_by = FRA }
