# 214 - Zaragoza

owner = ARA		#Alfons V of Aragon
controller = ARA
add_core = ARA
culture = aragonese
religion = catholic
capital = "Zaragoza" 
base_tax = 6
base_production = 2
base_manpower = 0
is_city = yes
trade_goods = leather
 
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1350.1.1 = { urban_infrastructure_1 = yes workshop = yes }
1474.1.1 = { medieval_university = yes }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
 # The university of Zaragoza becomes one of the most important universities in Spain, competing with Salamanca

1591.9.24  = { controller = REB } # Foral riots in Zaragoza  
1591.12.24 = { controller = SPA } # The army is dispatched to quell the riots
1705.1.1   = {  }
1705.6.29  = { controller = HAB } # Aragon joins the Austrian side in the War of Spanish Succession
1707.5.26  = { controller = SPA } # Aragon falls to the Borbonic troops
1710.6.13  = { controller = HAB } # Archduke Carlos takes Zaragoza, paving again the way to Madrid
1710.11.1  = { controller = SPA } # Felipe V reorganises his army and is able to force the retreat of the Archduke's army
1713.7.13  = { remove_core = ARA }
1808.6.6   = { controller = REB }
1810.1.1   = { controller = SPA }
1812.7.26  = { controller = REB }
1813.12.11 = { controller = SPA }
