# 203 - Liyones

owner = BOU
controller = BOU
capital = "Feurs"
culture = arpitan
religion = catholic
base_tax = 17
base_production = 0
base_manpower = 1
is_city = yes
trade_goods = silk

 # La Primatiale Saint-Jean-Baptiste
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = FRA
	add_core = BOU
}
1467.6.15  = { add_core = BUR } # Charles the Bold ascends to the throne and lays claims
1477.1.5   = { remove_core = BUR } # Charles the Bold dies
 # Important arms industry
1527.1.1   = {
	owner = DAL
	controller = DAL
	add_core = DAL
	remove_core = BOU
}
1530.1.1   = { fort_14th = yes } # Important metal & arms industry
1550.1.1   = {
	
	culture = francien
}
1573.9.1   = { unrest = 5 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { unrest = 0 } # Charles IX dies, situation cools a bit

1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1589.8.2   = { owner = FRA controller = FRA } # Charles IV of Bourbon dies, Bourbon added to France
 # Henri IV's quest to eliminate corruption and establish state control
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism

1625.1.1   = { fort_14th = no fort_15th = yes }
1632.1.1   = { unrest = 3 }
1634.1.1   = { unrest = 0 }
1641.1.1   = { unrest = 3 }
1644.1.1   = { unrest = 0 }
1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1650.3.1   = { revolt = { type = noble_rebels size = 2 } controller = REB unrest = 3 } # Fronde rebels take control
1651.4.1   = { revolt = { } controller = FRA unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = { unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1680.1.1   = { fort_15th = no fort_16th = yes }

