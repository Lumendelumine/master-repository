# 3384 - Nafplion

owner = ACH
controller = ACH
culture = greek
religion = orthodox
capital = "Nafplion"
trade_goods = wine
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}

1088.1.1 = { harbour_infrastructure_2 = yes }
1356.1.1  = {
	add_core = ACH
	add_core = MOE
	add_core = BYZ
}
1388.1.1  = {
	owner = VEN
	controller = VEN
	add_core = VEN
} # Sold to Venice
1453.5.29 = {
	remove_core = BYZ
}
1540.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # Treaty with the Turkish Empire
1685.1.1  = {
	owner = VEN
	controller=  VEN
} # Venetian recovery
1715.1.1  = {
	owner = TUR
	controller = TUR
} # 
1750.1.1  = { add_core = GRE }
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	owner = GRE
	controller = GRE
}
1832.5.7  = {
	remove_core = TUR
} # Treaty of Constantinople
