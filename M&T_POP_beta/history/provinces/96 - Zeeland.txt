# 96 Zeeland - Principal cities: Middelburg, Vlissingen & Zierikzee

owner = HOL
controller = HOL
add_core = HOL 
culture = dutch
religion = catholic
capital = "Middelburg"
base_tax = 3
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = salt

discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }
1433.1.1   = { owner = BUR controller = BUR add_core = BUR }
1477.1.5   = { unrest = 10 } # death of Charles the Bold
1477.8.18  = { unrest = 0 } # Personal Union with HAS (marriage of Mary of Burgondy & Maximmilian of Hasburg)
1482.3.27  = { owner = HAB controller = HAB add_core = HAB remove_core = BUR } # Mary of burgondy dies, Lowlands to Austria
1500.1.1 = { road_network = yes }
1522.2.15 = { shipyard = yes }
1530.1.1  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1549.11.4  = { add_core = NED remove_core = HOL }
1550.1.1   = { add_core = NED }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }
1559.5.12  = { unrest = 3 } # New bishoprics in the Lowlands create an outrage
1565.1.1   = { unrest = 5 } # Letters of Segovia, Philip I orders the harsh persecution of Calvinists
1566.4.5   = { unrest = 3 } # 'Eedverbond der Edelen', Margaretha of Parma promises leniency
1567.9.10  = { unrest = 4 } # Counts of Egmont & Hoorne arrested
1568.6.5   = { unrest = 6 } # Counts of Egmont & Hoorne beheaded
1569.1.1   = { unrest = 12 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = { unrest = 20 fort_14th = yes } # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1572.1.1   = { religion = reformed }
1572.4.1   = { controller = REB } # Dutch rebels take Den Briel
1579.1.23  = { owner = NED controller = NED remove_core = SPA unrest = 0 } # Union of Utrecht

1640.1.1   = {  } # Vlissingen is a significant port
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1670.1.1   = {  }


1730.1.1   = {  }
1810.7.10  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1813.11.30 = {
	owner = NED
	controller = NED
	remove_core = FRA
} # William returns to the Netherlands
