# 385 - Makka

owner = HEJ
controller = HEJ
culture = hejazi
religion = sunni
capital = "Makka"
trade_goods = coffee
hre = no
base_tax = 15
base_production = 2
base_manpower = 1
is_city = yes

discovered_by = ADA
discovered_by = MKU
discovered_by = KIL
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	owner = MAM
	controller = MAM
	add_core = HEJ
	add_core = MAM
}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
#1530.1.1   = {
#	owner = HEJ
#	controller = HEJ
#	add_core = HEJ
#	remove_core = TUR
#}

1750.1.1 = {  }
1770.1.1 = {
	owner = MAM
	controller = MAM
	remove_core = TUR
} # Ali Bey gained control of the Hijaz, reconstituting the Mamluk state
1772.1.1 = {
	owner = HEJ
	controller = HEJ
}
1802.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
} # Incorporated into the First Saudi State
1811.1.1 = {
	add_core = HEJ
} # Intervention of Mehmet Ali on behalf of the Sultan
1812.6.1 = {
	owner = TUR
	controller = TUR
	remove_core = NAJ
}
