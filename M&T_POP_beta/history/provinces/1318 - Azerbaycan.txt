# 1318 - Azerbaycan
# MEIOU-GG - Turko-Mongol mod

owner = CHU
controller = CHU
culture = azerbadjani
religion = sunni
capital = "Ardabil"
trade_goods = wool
hre = no
base_tax = 5
base_production = 2
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
}
1356.1.1 = {
	add_core = CHU
	add_core = BLU
	add_core = QAR
	unrest = 4
}
1357.1.1   = {
	owner = BLU
	controller = BLU
	remove_core = CHU
}
1360.1.1   = {
	owner = JAI
	controller = JAI
	remove_core = BLU
}
1375.1.1   = {
	owner = QAR
	controller = QAR
} # Independance secured
1444.1.1 = {
	remove_core = JAI
	add_core = AKK
}	
1467.1.1 = {
	controller = AKK
}
1470.1.1 = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1493.1.1  = {
	owner = SAM
	controller = SAM
	add_core = SAM
	remove_core = AKK
} #The Safawid Order
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
} # The Safavids took over
1515.1.1 = { training_fields = yes }
1578.9.9  = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR } # Peace of Istanbul
1607.1.1   = { controller = PER } # Persian reconquest
1612.11.20 = { owner = PER remove_core = TUR } # Part of Persia
1722.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1730.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
1747.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1760.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
