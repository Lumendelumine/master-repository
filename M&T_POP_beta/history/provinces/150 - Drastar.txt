# 150 - Drastar

owner = BUL
controller = BUL
culture = bulgarian
religion = orthodox
capital = "Drastar"
trade_goods = lumber
hre = no
base_tax = 9
base_production = 1
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = BUL
}
1371.2.17 = {
	owner = TAR
	controller = TAR
	add_core = TAR
	remove_core = BUL
}
1388.1.1 = {
	owner = OTT
	controller = OTT
	add_core = OTT
	add_core = BUL
	remove_core = TAR
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1413.1.1  = {
	owner = BYZ
	controller = BYZ
	add_local_autonomy = 15
}
1444.1.1  = {
	owner = TUR
	controller = TUR
	add_local_autonomy = -15
}
1773.1.1  = {
	controller = RUS
} # Russo-Turkish War of 1768�1774
1774.7.21 = {
	controller = TUR
} # Treaty of Ku�uk Kainarji
