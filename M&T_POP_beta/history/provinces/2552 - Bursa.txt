# 2552 - Bursa

owner = OTT
controller = OTT
culture = turkish #Capital of Ottomans
religion = sunni #Capital of Ottomans
capital = "Bursa"
trade_goods = olive
hre = no
base_tax = 7
base_production = 4
base_manpower = 1
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1000.1.1   = {
	add_permanent_province_modifier = {
		name = preeminent_region_city
		duration = -1
	}
}
1111.1.1 = { urban_infrastructure_1 = yes }
1119.1.1 = { bailiff = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BYZ
	add_core = OTT
	set_province_flag = turkish_name
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1399.1.1 = { temple = yes }
1453.5.29 = {
	remove_core = BYZ
} # Fall of Constantinople
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR  }
1515.1.1 = { training_fields = yes }
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR  } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1720.1.1  = {  }
