# 2726 - Shushtar

owner = JAI
controller = JAI
culture = lurish
religion = sunni #Dei Gratia
capital = "Dezful"
trade_goods = wheat
hre = no
base_tax = 8
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = western
discovered_by = eastern
discovered_by = turkishtech
discovered_by = steppestech

1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = JAI
}
1387.1.1  = {
	controller = TIM
	owner = TIM
}
1408.1.1 = {
	controller = QAR
}
1409.1.1 = {
	owner = QAR
}
1441.1.1  = {
	add_core = MSS
	controller = MSS
	owner = MSS
	revolt = {}
} # Musha'sha'iyyah takes control of Hoveizeh
1444.1.1 = {
	remove_core = JAI
}	
1444.1.1 = {
	remove_core = QAR
}
1447.3.12  = {
	controller = MSS
	owner = MSS
	revolt = {}
} # Musha'sha'iyyah consolidates control of province
1508.1.1  = {
	controller = SAM
}
1508.2.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	remove_core = QAR
	remove_core = AKK
	remove_core = JAI
	religion = shiite
} # Safawids "form persia"

1719.1.1  = { unrest = 5 } # Rebellion
1720.1.1  = { unrest = 0 }
1747.1.1  = { unrest = 3 } # Shah Nadir is killed, local khanates emerged
1748.1.1  = { unrest = 4 } # The empire began to decline
1750.1.1  = {  }
1779.1.1  = { unrest = 0 } # With the Qajar dynasty the situation stabilized
