# 36 - Eestimaa

owner = LVO 
controller = LVO 
culture = estonian
religion = catholic
capital = "K�rdla"
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish

discovered_by = western
discovered_by = eastern
discovered_by = steppestech
hre = no

1088.1.1 = { dock = yes }
1346.1.1  = { 

	add_core = LVO 
	add_core = EST
	
}
1356.1.1   = {
	add_local_autonomy = 25 # supression of St. George's Night Uprising
}

1542.1.1  = { religion = protestant} #Unknown date
1561.11.18 = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = LVO
} # # Wilno Pact
1710.9.15 = { controller = RUS } #The Great Nordic War-Captured Reval
1721.8.30 = {
	owner = RUS
	add_core = RUS
	remove_core = SWE
} #The Peace of Nystad
#1814.5.17 = {
#	add_core = DEN
#	remove_core = DAN
#}
