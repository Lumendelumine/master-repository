# 1212 - Siwa

culture = bedouin
religion = sunni
capital = "Siwa"
trade_goods = wool
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 4.5
native_hostileness = 9
discovered_by = muslim
discovered_by = soudantech

1819.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	is_city = yes
}
