# 2219 - Urmia
# MEIOU-GG - Turko-Mongol mod

owner = CHU
controller = CHU
culture = azerbadjani
religion = sunni #Dei Gratia
capital = "Urmia"
trade_goods = carpet
hre = no
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_core = CHU
	add_core = BLU
	unrest = 4
	add_core = QAR
}
1357.1.1   = {
	owner = BLU
	controller = BLU
	remove_core = CHU
}
1360.1.1   = {
	owner = JAI
	controller = JAI
	remove_core = BLU
}
1386.1.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1406.1.1 = {
	controller = QAR
}
1409.1.1  = {
	owner = QAR
	remove_core = TIM
} # Fars and surroundings to Qara Quyunlu
1444.1.1 = {
	remove_core = JAI
	add_core = AKK
}	
1458.9.1   = { revolt = { type = pretender_rebels size = 2 } controller = REB } # Civil war in Qara Quyunlu
1458.12.1  = { revolt = {} controller = QAR }
1467.1.1 = {
	controller = AKK
}
1470.1.1 = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1501.1.1  = {
	controller = SAM
}
1508.1.1  = {
	owner = SAM
}
1512.1.1   = {
	owner = PER
	controller = PER
	add_core = PER
	religion = shiite
	remove_core = SAM
	remove_core = AKK
} # Safawids "form persia"
1515.1.1 = { training_fields = yes }
1534.7.1   = { controller = TUR } # Wartime occupation
1536.1.1   = { controller = PER } # End of Ottoman-Safavid war
1585.9.22  = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR } # Peace of Istanbul
1603.9.26  = { controller = PER } # Persian reconquest
1612.11.20 = { owner = PER }
1720.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1722.1.1  = { controller = TUR revolt = { } } # Ottoman conquest
1727.1.1  = {
	controller = PER
	
} # Afghans Ousted
1747.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1760.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
