#109 - Mantua
 
owner = MAN
controller = MAN
add_core = MAN
culture = emilian
religion = catholic 
hre = yes 
base_tax = 7
base_production = 2
trade_goods = linen       
base_manpower = 0
is_city = yes
fort_14th = yes 
capital = "M�ntua"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

  # temple of San Pietro, 1540 
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1380.1.1   = {
	add_permanent_province_modifier = {
		name = "county_of_rolo"
		duration = -1
	}
}
1618.1.1   = { hre = no }
1708.6.30  = { controller = HAB owner = HAB add_core = HAB } # Annexed to Austria
1796.6.4   = { controller = FRA } # Besieged by the French
1796.7.31  = { controller = HAB } # The French are driven off by Austrian and Russian forces
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
	remove_core = HAB
} # Transpadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
# 1805.3.17 - Kingdom of Italy 
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {   #??
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = HAB
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
