# 159 - Dobrogea

owner = DOB
controller = DOB
add_core = DOB
culture = bulgarian
religion = orthodox
capital = "Karvuna"
base_tax = 3
base_production = 0
base_manpower = 0
is_city = yes
trade_goods = fish
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1388.1.1  = {
	owner = WAL
	controller = WAL
	add_core = WAL
}
1419.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	#remove_core = WAL
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1519.1.1 = { bailiff = yes }
1550.1.1  = { fort_14th = yes }
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }
1688.1.1  = { unrest = 6 } # Rebellion against Ottoman rule, centered on Chiprovtzi
1689.1.1  = { unrest = 0 } # Brutally suppressed by Janissaries
1793.1.1  = { unrest = 5 } # Pasvanoglu  Rebellion, centered at Vidin
1798.1.1  = { unrest = 0 }
