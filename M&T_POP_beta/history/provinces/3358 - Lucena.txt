#3358 - Lucena + Baena + Montilla + Cabra + La Rambla + Priego + Aguilar + Castro del R�o + Puente Genil 

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = andalucian # culture = western_andalucian
religion = catholic
hre = no
base_tax = 9
base_production = 1
trade_goods = olive
base_manpower = 1
is_city = yes
capital = "Lucena"
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1356.1.1   = {
	set_province_flag = spanish_name
	add_core = ENR
	add_core = CAS
	add_permanent_province_modifier = {
		name = "lordship_of_cordoba"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
}
1516.1.23  = {
	owner = SPA
	controller = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1610.1.12  = {  } # Decree for the expulsion of the morisques in Andaluc�a, unlike Valencia, this was performed uneventfully.
1713.4.11  = { remove_core = CAS }
1808.6.6   = { controller = REB }
1811.1.1   = { controller = SPA }
1812.10.1  = { controller = REB }
1813.12.11 = { controller = SPA }
