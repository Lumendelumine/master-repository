# Vyborg
# MEIOU - Gigau

owner = SWE
controller = SWE
culture = ingrian
religion = catholic
hre = no
base_tax = 1
base_production = 0
trade_goods = fur
base_manpower = 0
is_city = yes
fort_14th = yes
capital = "Viipuri"
discovered_by = western
discovered_by = eastern

# 1088.1.1 = { dock = yes }
1356.1.1   = {
	owner = RSW
	controller = RSW
	add_core = RSW
	add_core = SWE
}
1360.1.1   = {
	owner = SWE
	controller = SWE
	remove_core = RSW
	#add_core = NOV
}
1478.1.14  = {
	#remove_core = NOV
	#add_core = MOS
}
1522.2.15 = { dock = yes shipyard = yes }
1547.1.1  = {
	#add_core = RUS
	#remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia

1617.2.17  = { religion = protestant}
1710.9.9   = { controller = RUS } #The Great Nordic War-Captured Keksholm
1721.8.30  = {
	owner = RUS
	remove_core = SWE
} #The Peace of Nystad
1812.1.1 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
	capital = "Viipuri"
}
