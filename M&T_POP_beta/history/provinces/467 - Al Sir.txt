# 467 - Al sir
# Bani Yas

owner = BYA
controller = BYA
culture = bahrani
religion = sunni
capital = "Abu Dabi"
trade_goods = coffee
hre = no
base_tax = 1
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1356.1.1 = {
	add_core = BYA
}
1480.1.1 = { discovered_by = TUR }
1507.9.1 = { discovered_by = POR } # Alfonso d'Albuquerque
1818.9.9 = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The end of the Saudi State
