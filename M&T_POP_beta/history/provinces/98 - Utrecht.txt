#98 - Utrecht

owner = UTR
controller = UTR 
add_core = UTR
culture = dutch
religion = catholic
capital = "Uterech" #Utrecht
base_tax = 2
base_production = 1
base_manpower = 0
is_city = yes
trade_goods = linen
discovered_by = eastern
discovered_by = western
discovered_by = muslim

hre = yes

1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }

1500.1.1 = { road_network = yes }
1515.1.5   = { add_core = HAB } # Charles V ascends to the throne
 # Dom of Utrecht finished
1522.2.15 = { shipyard = yes }
1527.1.1   = { fort_14th = yes }
1528.6.21  = { owner = HAB controller = HAB add_core = HAB  } # Charles V annexes Utrecht
1530.1.1  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1549.11.4  = { add_core = NED  }
1550.1.1   = { add_core = NED }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }

1559.5.12  = { unrest = 3 } # New bishoprics in the Lowlands create an outrage
1565.1.1   = { unrest = 5 } # Letters of Segovia, Philip I orders the harsh persecution of Calvinists
1566.4.5   = { unrest = 3 } # 'Eedverbond der Edelen', Margaretha of Parma promises leniency
1567.9.10  = { unrest = 4 } # Counts of Egmont & Hoorne arrested
1568.6.5   = { unrest = 6 } # Counts of Egmont & Hoorne beheaded
1569.1.1   = { unrest = 12 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = { unrest = 20 } # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1572.1.1   = { religion = reformed  }
1572.6.10  = { controller = REB } # Rebels rise up in large parts of the Netherlands
1579.1.23  = { owner = NED controller = NED remove_core = SPA unrest = 0 } # Union of Utrecht
 # Professionalisation of the Army


1636.3.26  = { early_modern_university = yes } # University of Utrecht founded
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1650.1.1   = { fort_14th = no fort_15th = yes }
1672.5.25  = { controller = FRA } # France blitzes through the Lowlands in the Franco-Dutch War and even takes Utrecht!
1678.8.10  = { controller = NED } # Peace of Nijmegen (France-Netherlands)
1681.1.1   = { base_tax = 10
base_production = 10 } # Protestants expelled from France
1705.1.1   = {  }
1725.1.1   = { fort_15th = no fort_16th = yes }
1775.1.1   = { fort_16th = no fort_17th = yes }
1785.12.1  = { unrest = 7 } # Unrest in Utrecht
1786.1.1   = { controller = REB unrest = 3 } # 'The Patriots' take control of Utrecht
1786.9.13  = { controller = NED unrest = 0 } # With the help of 20,000 Prussians, the Dutch government regains control
1810.7.10  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1813.11.30 = {
	owner = NED
	controller = NED
	remove_core = FRA
} # William returns to the Netherlands
