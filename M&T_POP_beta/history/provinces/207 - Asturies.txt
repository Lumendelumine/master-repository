# 207 - Asturies

owner = CAS		#Juan II of Castille

controller = CAS
add_core = CAS
add_core = LEO
culture = asturian
religion = catholic
hre = no
base_tax = 14
base_production = 1
trade_goods = cheese 
base_manpower = 1
is_city = yes
capital = "Uvi�u"

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1  = {
	owner = ENR	
	controller = ENR
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_asturias"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1475.6.2   = { controller = POR }
1476.3.2   = { controller = CAS }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla

1528.1.1 = { temple = yes }
1608.9.21  = { early_modern_university = yes } # University of Oviedo opens its doors
1713.4.11  = { remove_core = LEO }
 # Audiencia de Asturias
1808.5.6   = { controller = REB }
1809.1.1   = { controller = SPA }
1812.10.1  = { controller = REB }
1813.12.11 = { controller = SPA }
