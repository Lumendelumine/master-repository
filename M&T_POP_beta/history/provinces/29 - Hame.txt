# H�mee
# MEIOU - Gigau

owner = SWE
controller = SWE
culture = finnish
religion = catholic
hre = no
base_tax = 2
base_production = 0
trade_goods = fur
base_manpower = 0
is_city = yes
fort_14th = yes #Tavastehus
capital = "H�meenlinna"
discovered_by = western
discovered_by = eastern

1356.1.1   = {
	owner = RSW
	controller = RSW
	add_core = RSW
	add_core = SWE
}
1360.1.1   = {
	owner = SWE
	controller = SWE
	remove_core = RSW
}
1527.6.1   = { religion = protestant}

1598.8.1   = { controller = PLC } #Sigismund tries to reconquer his crown
1599.7.15  = { controller = SWE } #Duke Karl get it back
1600.1.1   = { trade_goods = lumber } #Estimated Date
1713.10.6  = { controller = RUS } #The Great Nordic War-Captured by Apraksin
1721.8.30  = { controller = SWE } #The Peace of Nystad
1742.10.30 = { controller = RUS } #The War of the Hats-Estimated date
1743.8.7   = { controller = SWE } #The Peace of �bo
1808.3.1   = { controller = RUS } # Overran by Russian troops
1809.3.29  = {
	owner = RUS
	add_core = RUS
	remove_core = SWE
} # Treaty of Fredrikshamn
1809.9.17 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
}
