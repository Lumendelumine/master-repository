# 1405 - Ardeal / Bihor

owner = HUN
controller = HUN  
culture = transylvanian
religion = orthodox
capital = "Kolozsvár"
trade_goods = salt
hre = no
base_tax = 7
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1   = {
	add_core = TRA
	add_core = HUN
}
1506.1.1  = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Szekely rebellion
1507.1.1  = { revolt = { } controller = HUN } # Estimated
1514.4.1  = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Peasant rebellion against Hungary's magnates
1515.1.1  = { revolt = { } controller = HUN } # Estimated
1515.2.1 = { training_fields = yes }

1526.8.30  = {
	owner = TRA
	controller = TRA
	add_core = HAB
}
1540.1.1  = { religion = reformed }
1570.8.16  = {
	owner = TRA
	controller = TRA
} # Abdication of Janos II as king of Hungary, becomes prince of Transylvania
1599.1.1   = {
	owner = WAL
	controller = WAL
	add_core = WAL
}
1601.1.1   = {
	owner = TRA
	controller = TRA
	remove_core = WAL
} # Michael is assasinated

1695.1.1 = {
	remove_core = TUR
}
1711.5.1   = {
	owner = HAB 
	controller = HAB
	add_core = HAB
} # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
1744.1.1   = { controller = REB } # Visarion Sarai stir up an orthodox rebellion against the Uniate church
1745.1.1   = { controller = HAB } # Estimated, Visarion Sarai is arrested and executed
1755.1.1   = {  }
1784.1.1   = { controller = REB } # Peasant rebellion against the nobles
1785.1.1   = { controller = HAB } # The leaders are arrested and executed
