# 1450 - Srem

owner = HUN
controller = HUN
culture = hungarian
religion = catholic
capital = "Zimony"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 0
base_manpower = 0
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
#	owner = CRO
#	controller = CRO
	add_core = HUN
#	add_core = CRO
	add_core = SER
}
1444.1.1 = {
	remove_core = SER
}
1523.8.16 = { mill = yes }
1526.8.30  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = HAB
}
1550.1.1  = {
	citysize = 10080
	capital = "Sremska Mitrovica"
}
1595.1.1  = {
	fort_14th = yes
} # Estimated
1688.11.7 = {
	unrest = 7
} # Serb rebellion under D. Brankovic
1699.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
	unrest = 0
} # Austrian occupation
1737.1.1   = {
	culture = serbian
	religion = orthodox
}
1807.1.1  = {
	unrest = 7
} # Tican's Rebellion
