# 1433 - Amsciauscyna

owner = KIE
controller = KIE
add_core = KIE
culture = ruthenian
religion = orthodox
hre = no
base_tax = 15
base_production = 0
trade_goods = wheat
base_manpower = 1
is_city = yes
capital = "Bratslav"
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = LIT
}
1362.12.25 = {
	owner = LIT
	controller = LIT
} # Aftermath of the Battle of Blue Waters
1480.1.1  = {
	fort_14th = yes
}
1523.8.16 = { mill = yes }
1569.3.5 = {
	owner = POL
	controller = POL
	add_core = POL
	remove_core = LIT
}# Annexed to the crown of poland before Union of Lublin
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
	remove_core = LIT
} # Union of Lublin
1649.2.1   = {
	owner = ZAZ
	controller = ZAZ
	add_core = ZAZ
}
1654.1.1  = {
	add_core = RUS
} # Treaty of Pereyaslav
1667.2.9  = {
	owner = PLC
	controller = PLC
	add_core = PLC
	remove_core = ZAZ
} # Truce of Andrusovo
1793.1.23 = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Second partition of Poland
1794.3.24  = { unrest = 5 } # Kosciuszko uprising, minimize the Russian influence
1794.11.16 = { unrest = 0 }
1795.1.1   = {
	remove_core = PLC
}
