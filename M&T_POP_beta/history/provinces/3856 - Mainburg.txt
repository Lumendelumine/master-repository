# No previous file for Mainburg

owner = BAY
controller = BAY
add_core = BAY
culture = bavarian
religion = catholic
base_tax = 6
base_production = 0
trade_goods = beer
base_manpower = 0
is_city = yes
capital = "Mainburg"
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1340.12.20 = { owner = BAV controller = BAV add_core = BAV remove_core = BAY }
1349.1.1   = { owner = BAY controller = BAY add_core = BAY remove_core = BAV }
1363.1.13  = { owner = BAX controller = BAX add_core = BAX remove_core = BAY }
1392.1.1   = { owner = BAY controller = BAY add_core = BAY remove_core = BAX }
1500.1.1 = { road_network = yes }
1505.7.30  = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = BAY
} # Diet of Cologne
1704.8.13  = {
	owner = HAB
	controller = HAB
} # Bavaria is conquered by the Emperor, is however 10 years later restored for the balance of power
1714.9.7   = { owner = BAV controller = BAV }
1742.2.1   = {
	controller = HAB
} # Austrian troops take Munich and Bavaria
1745.1.1   = {
	controller = BAV
} # Against Peace and their vote in the Emperor election Bavaria is returned to the Wittelsbachs
1777.1.1   = {
	unrest = 3
} # Bavarian line of the Wittlelsbach family dies out and is replaced by the Pwesternate branch of the family. The people in Munich hate the new duke.
1779.1.1   = { unrest = 0 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
