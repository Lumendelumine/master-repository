#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
# Rural buildings
################################################

#Rural Infrastructure Efficiency 1
#Rural Infrastructure Efficiency 2
#Rural Infrastructure Efficiency 3
#Rural Infrastructure Efficiency 4
#Rural Infrastructure Efficiency 5
#Rural Infrastructure Efficiency 6
#Rural Infrastructure Efficiency 7
#Rural Infrastructure Efficiency 8
#Rural Infrastructure Efficiency 9
#Rural Infrastructure Efficiency 10

#Rural Arable lands 1
#Rural Arable lands 2
#Rural Arable lands 3
#Rural Arable lands 4
#Rural Arable lands 5
#Rural Arable lands 6
#Rural Arable lands 7
#Rural Arable lands 8
#Rural Arable lands 9
#Rural Arable lands 10

################################################
# Arable Lands
################################################

arable_lands_1 = {
	cost = 100
	time = 24
	
	make_obsolete = arable_lands_0
	
	trigger = {
		
	}
		
	modifier = {
	
	}
}

arable_lands_2 = {
	cost = 250
	time = 24
	
	make_obsolete = arable_lands_1
	
	trigger = {
		
	}
	
	modifier = {
	
	}
}

arable_lands_3 = {
	cost = 350
	time = 36
	
	make_obsolete = arable_lands_2
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_4 = {
	cost = 500
	time = 36
	
	make_obsolete = arable_lands_3
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_5 = {
	cost = 1000
	time = 48
	
	make_obsolete = arable_lands_4
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_6 = {
	cost = 1000
	time = 48
	
	make_obsolete = arable_lands_5
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_7 = {
	cost = 1000
	time = 48
	
	make_obsolete = arable_lands_6
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_8 = {
	cost = 1000
	time = 48
	
	make_obsolete = arable_lands_7
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_9 = {
	cost = 1000
	time = 48
	
	make_obsolete = arable_lands_8
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

arable_lands_10 = {
	cost = 1000
	time = 48
	
	make_obsolete = arable_lands_9
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}


################################################
# Rural efficiency
################################################

rural_infrastructure_1 = {
	cost = 100
	time = 24
	
	make_obsolete = rural_infrastructure_0
	
	trigger = {
		
	}
		
	modifier = {
		
	}
}

rural_infrastructure_2 = {
	cost = 250
	time = 24
	
	make_obsolete = rural_infrastructure_1
	
	trigger = {
		
	}
	
	modifier = {
	
	}
}

rural_infrastructure_3 = {
	cost = 350
	time = 36
	
	make_obsolete = rural_infrastructure_2
	
	trigger = {
		
	}
	
	modifier = {
	
	}
}

rural_infrastructure_4 = {
	cost = 500
	time = 36
	
	make_obsolete = rural_infrastructure_3
	
	trigger = {
		
	}
	
	modifier = {
	
	}
}

rural_infrastructure_5 = {
	cost = 1000
	time = 48
	
	make_obsolete = rural_infrastructure_4
	
	trigger = {
		
	}
	
	modifier = {
		
	
	}
}

rural_infrastructure_6 = {
	cost = 1000
	time = 48
	
	make_obsolete = rural_infrastructure_5
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

rural_infrastructure_7 = {
	cost = 1000
	time = 48
	
	make_obsolete = rural_infrastructure_6
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

rural_infrastructure_8 = {
	cost = 1000
	time = 48
	
	make_obsolete = rural_infrastructure_7
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

rural_infrastructure_9 = {
	cost = 1000
	time = 48
	
	make_obsolete = rural_infrastructure_8
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

rural_infrastructure_10 = {
	cost = 1000
	time = 48
	
	make_obsolete = rural_infrastructure_9
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

################################################
# Mines
################################################

mines_1 = {
	cost = 150
	time = 48
	
	trigger = {
		
	}
	
	modifier = {
		
				
	}
}

mines_2 = {
	cost = 500
	time = 48
	
	make_obsolete = mines_1
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

mines_3 = {
	cost = 1000
	time = 48
	
	make_obsolete = mines_2
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

mines_4 = {
	cost = 1500
	time = 48
	
	make_obsolete = mines_3
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

mines_5 = {
	cost = 2000
	time = 48
	
	make_obsolete = mines_4
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

################################################
# Farm_Estates
################################################

farm_estate_1 = {
	cost = 75
	time = 48
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

farm_estate_2 = {
	cost = 150
	time = 48
	
	make_obsolete = farm_estate_1
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

farm_estate_3 = {
	cost = 300
	time = 48
	
	make_obsolete = farm_estate_2
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

farm_estate_4 = {
	cost = 600
	time = 48
	
	make_obsolete = farm_estate_3
	
	trigger = {
		
	}
	
	modifier = {
		

	}
}

farm_estate_5 = {
	cost = 1000
	time = 48
	
	make_obsolete = farm_estate_4
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

################################################
# Plantations
################################################

plantation_1 = {
	cost = 150
	time = 48
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

plantation_2 = {
	cost = 300
	time = 48
	
	make_obsolete = plantation_1
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

plantation_3 = {
	cost = 600
	time = 48
	
	make_obsolete = plantation_2
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

plantation_4 = {
	cost = 1000
	time = 48
	
	make_obsolete = plantation_3
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

plantation_5 = {
	cost = 1500
	time = 48
	
	make_obsolete = plantation_4
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

################################################
# Gathering
################################################

gathering_1 = {
	cost = 150
	time = 48
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

gathering_2 = {
	cost = 300
	time = 48
	
	make_obsolete = gathering_1
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

gathering_3 = {
	cost = 600
	time = 48
	
	make_obsolete = gathering_2
	
	trigger = {
		
	}
	
	modifier = {
	
	}
}

gathering_4 = {
	cost = 1000
	time = 48
	
	make_obsolete = gathering_3
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}

gathering_5 = {
	cost = 1500
	time = 48
	
	make_obsolete = gathering_4
	
	trigger = {
		
	}
	
	modifier = {
		
	}
}
