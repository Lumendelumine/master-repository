
NDefines.NCountry.ALLOW_ZERO_BASE_VALUES = 1						-- Affects base tax, base manpower, and base production
NDefines.NCountry.CULTURAL_UNION_MIN_RANK = 6						-- Minimum rank to get cultural union effect with Common Sense.
NDefines.NCountry.MIN_DEV_FOR_FREE_CITY = 3							-- Capital must have at least this much development to be a free city
NDefines.NCountry.MIN_DEVELOPMENT_FOR_GOV_RANK_2 = 99999			-- Need at least this much development to upgrade to government rank 2
NDefines.NCountry.MIN_DEVELOPMENT_FOR_GOV_RANK_3 = 99999			-- Need at least this much development to upgrade to government rank 3
NDefines.NCountry.ADDITIONAL_MIN_DEVELOPMENT_FOR_GOV_RANK_X = 99999	-- ADDITIONAL development needed to upgrade to government rank above 3
NDefines.NCountry.HRE_RANK = 6										-- Emperor of the HRE is displayed as this rank (for ruler title only)
NDefines.NCountry.HRE_MAX_RANK = 6									-- for members
NDefines.NCountry.HRE_MAX_RANK_ELECTOR = 6							-- for electors	
NDefines.NCountry.SUBJECT_MAX_RANK = 5								-- max government rank of subjects
NDefines.NCountry.MAX_GOV_RANK = 6									-- Max possible is 10
NDefines.NCountry.EXPLORE_COAST_EVENT_CHANCE = 3					-- Chance (in %) of getting an event when exploring coasts
NDefines.NCountry.MIN_TECH_FOR_CIRCUMNAVIGATE = 20					-- Must have this level of dip tech to circumnavigate the globe
NDefines.NCountry.CIRCUMNAVIGATION_PROVINCE_1 = 2046				-- Provinces for circumnavigation (coast of gambia)
NDefines.NCountry.CIRCUMNAVIGATION_PROVINCE_2 = 1641				-- Provinces for circumnavigation (magellan strait)
NDefines.NCountry.CIRCUMNAVIGATION_PROVINCE_3 = 1799				-- Provinces for circumnavigation (hawaii sea)
NDefines.NCountry.CIRCUMNAVIGATION_PROVINCE_4 = 1992				-- Provinces for circumnavigation (sulu sea)
NDefines.NCountry.CIRCUMNAVIGATION_PROVINCE_5 = 2024				-- Provinces for circumnavigation (horn of africa)
NDefines.NCountry.CIRCUMNAVIGATION_PROVINCE_6 = 2038				-- Provinces for circumnavigation (cape of good hope)
NDefines.NCountry.MIN_DEV_FOR_GREAT_POWER = 300						-- Countries with less dev than this cannot be considered Great Powers
NDefines.NCountry.MAX_ACTIVE_POLICIES = 7							-- how many active policies at once.
NDefines.NCountry.WESTERNISATION_THRESHOLD = 9						-- techs behind to be allowed to westernize.
--NDefines.NCountry.WESTERN_POWER_TICK = 0							-- max power to transfer every month.
--NDefines.NCountry.WESTERN_POWER_TICK_MIN = 0						-- min power to transfer every month.
--NDefines.NCountry.WESTERN_NEEDED_BASE_POWER = 50000					-- needed for full westernisation						# Inimicus
--NDefines.NCountry.WESTERN_NEEDED_MAX_POWER = 50000					-- needed for full westernisation						# Inimicus - May 2014
--NDefines.NCountry.WESTERN_POWER_TICK_REDUCTION_FACTOR = 15			-- reduce max power transfer by 1 for each multiplication of this in total monthly income.
NDefines.NCountry.PIETY_PERCENTAGE_AT_NEW_RULER = 1.00				-- percentage of piety kept at new ruler. #DEI GRATIA
NDefines.NCountry.PIETY_INCREASE_AT_GOOD_WAR = 0.00					-- #DEI GRATIA
NDefines.NCountry.PIETY_DECREASE_AT_BAD_WAR = 0.00					-- #DEI GRATIA
NDefines.NCountry.NAT_FOCUS_YEARS_RANK = 2							-- how many years are removed from nat focus cooldown per gov rank above 1
NDefines.NCountry.CULTURE_LOSS_THRESHOLD = 0.00 					-- _CDEF_CULTURE_LOSS_THRESHOLD = 10
NDefines.NCountry.CULTURE_GAIN_THRESHOLD = 0.50 					-- _CDEF_CULTURE_GAIN_THRESHOLD = 10
NDefines.NCountry.NAT_FOCUS_YEARS_RANK = 3							-- how many years are removed from nat focus cooldown per gov rank above 1
NDefines.NCountry.POWER_MAX = 1400									-- how much power can be stored at maximum.
NDefines.NCountry.FREE_IDEA_GROUP_COST  = 7							-- modifier on cheapness of "free" idea group
NDefines.NCountry.IDEA_TO_TECH = -0.0075							-- percentage on tech reduction per idea.
NDefines.NCountry.TECH_TIME_COST = 0.4								-- tech grow with 20% cost over time.
NDefines.NCountry.OVEREXTENSION_FACTOR = 0.5
NDefines.NCountry.PS_ASSAULT = 1
NDefines.NCountry.PS_DEMAND_NON_WARGOAL_PROVINCE = 25
NDefines.NCountry.PS_DEMAND_NON_WARGOAL_PEACE = 3
NDefines.NCountry.PS_DEMAND_NON_WARGOAL_PEACE_PRIMITIVES = 0
NDefines.NCountry.PS_MAKE_PROVINCE_CORE = 5							-- vanilla = 10, M&T 1.21 = 2
NDefines.NCountry.PS_REDUCE_INFLATION = 100
NDefines.NCountry.PS_MOVE_TRADE_PORT = 300
NDefines.NCountry.PS_CHANGE_CULTURE = 5000							-- no more magical culture change
NDefines.NCountry.PS_CHANGE_CULTURE_OVERSEAS_RELIGION_MOD = -1.0	-- Modifier how much cheaper it is to change culture in overseas province if same religion
NDefines.NCountry.PS_HARSH_TREATMENT_COST = 100
NDefines.NCountry.PS_HARSH_TREATMENT_REDUCE = 25
NDefines.NCountry.PS_GARRISON_SORTIES = 5
NDefines.NCountry.PS_REDUCE_WAREXHAUSTION = 250						---DO INCREASED
NDefines.NCountry.PS_FACTION_BOOST = 50
NDefines.NCountry.PS_RAISE_TARIFFS = 25
NDefines.NCountry.PS_LOWER_TARIFFS = 75
NDefines.NCountry.PS_IMPROVE_PROVINCE_BASE = 9999					---DO SMASHED THE BUTTON
NDefines.NCountry.PS_MOVE_CAPITAL = 400								---Move capital, DO ADDED
NDefines.NCountry.CORE_COLONY = 0.1									-- Multiplied with base tax colonized by country or overseas, 1.21 = 0.1
NDefines.NCountry.CORE_SAME_REGION = 1.0							-- Multiplied with base tax for colonial nations, 1.21 = 0.25
NDefines.NCountry.CORE_SAME_CONTINENT = 1.0						-- Multiplied with base tax for colonial nations, 1.21 = 0.75
NDefines.NCountry.CORE_HAD_CLAIM = 0.25								-- Impacts MODIFIER_CORE_CREATION, 1.21 = 0.25
NDefines.NCountry.CORE_HAD_PERMANENT_CLAIM = 0.75					-- Impacts MODIFIER_CORE_CREATION
NDefines.NCountry.WAREXHAUSTION_REDUCTION = 2
NDefines.NCountry.RECENT_UPRISING_IN_MONTHS = 60	
NDefines.NCountry.UNREST_REVOLT_FACTOR = 0.6						-- How much does each point of unrest contribute to chance of revolt uprising increasing (base)
NDefines.NCountry.UPRISING_INCREASE = 5 							-- Number of percent that the progress increases
NDefines.NCountry.BREAK_VASSAL_PRESTIGE_PENALTY = -100.0			-- Prestige penalty when break vassalisation
NDefines.NCountry.PROVINCE_DISCOVERY_PRESTIGE = 1.0					-- Prestige change when discovered province
NDefines.NCountry.CLAIM_LOSE = 50									-- how many years until a claim is lost.
NDefines.NCountry.CORE_LOSE_PRIMARY_CULTURE_TAG = -1				-- how many years until a core is lost for the primary tag of a country.
NDefines.NCountry.YEARS_OF_NATIONALISM = 50 						-- _CDEF_YEARS_OF_NATIONALISM_; Years of Nationalism, 1.21 = 20
NDefines.NCountry.REGULAR_COLONY_GROWTH = 30
NDefines.NCountry.MONTHS_TO_CORE_MAXIMUM = 120						-- 1.21 = 600
NDefines.NCountry.MONTHS_TO_CORE = 120								-- How many months it will take to core a province, 1.21 = 600
NDefines.NCountry.MONTHS_TO_CHANGE_CULTURE = 120					-- How many months it will take to change culture in a province per basetax.
NDefines.NCountry.REVOLT_SIZE_BASE = 1
NDefines.NCountry.REVOLT_TECH_IMPACT = 0.05 						-- % each tech increases size of rebels by this percent.
NDefines.NCountry.REVOLT_TECH_MORALE = 0.0075
NDefines.NCountry.REVOLT_SIZE_DEVELOPMENT_MULTIPLIER = 0.1
NDefines.NCountry.REBEL_ARTILLERY_INCREASE_LEVEL_1_TECH = 25		-- Tech level at which REBEL_ARTILLERY_INCREASE_LEVEL_1_SIZE is applied
NDefines.NCountry.REBEL_ARTILLERY_INCREASE_LEVEL_2_TECH = 30		-- Tech level at which REBEL_ARTILLERY_INCREASE_LEVEL_2_SIZE is applied
NDefines.NCountry.LIBERTY_DESIRE_RELATIVE_POWER = 50				-- Liberty desire from relative power to liege
NDefines.NCountry.LIBERTY_DESIRE_MARCH = -25						-- Liberty desire from being a March
NDefines.NCountry.LIBERTY_DESIRE_DAIMYO = 50						-- Liberty desire from being a Daimyo
NDefines.NCountry.LIBERTY_DESIRE_DIPLO_TECH = 5					-- Liberty desire per point of diplo tech more than overlord
NDefines.NCountry.LIBERTY_DESIRE_HISTORICAL_FRIEND = -25			-- Liberty desire from being historical friends
NDefines.NCountry.LIBERTY_DESIRE_HISTORICAL_RIVAL = 25				-- Liberty desire from being historical rivals
NDefines.NCountry.LIBERTY_DESIRE_TRUST = -0.1						-- Liberty desire from trust
NDefines.NCountry.LIBERTY_DESIRE_POSITIVE_OPINION = -0.1			-- Liberty desire from positive opinion
NDefines.NCountry.LIBERTY_DESIRE_NEGATIVE_OPINION = 0.35			-- Liberty desire from negative opinion
NDefines.NCountry.LIBERTY_DESIRE_ANNEXATION = 15					-- Liberty desire from being annexed
NDefines.NCountry.LIBERTY_DESIRE_LARGE_VASSAL = 15					-- Liberty desire from vassal having more than LARGE_VASSAL_LIMIT base tax
NDefines.NCountry.LIBERTY_DESIRE_GREAT_POWER_VASSAL = 30			-- Liberty desire from vassal having more than GREAT_POWER_VASSAL_LIMIT base tax
NDefines.NCountry.LARGE_VASSAL_LIMIT = 300							-- Above what development is a vassal considered a large vassal
NDefines.NCountry.GREAT_POWER_VASSAL_LIMIT = 600 					-- Above what development is a vassal considered a great power vassal
NDefines.NCountry.MAX_CROWN_COLONIES = 2							-- How many province a country can hold in a colonial region before creating a colonial nation
NDefines.NCountry.SEIZE_TERRITORY_LIBERTY_MULTIPLIER = 1
NDefines.NCountry.PROMOTE_INVESTMENTS_INFLATION = 0.05
NDefines.NCountry.OVERSEAS_DISTANCE = 300							-- Provinces beyond this distance to capital are distant overseas
NDefines.NCountry.PROTECTORATE_TECH_THRESHOLD = 0.3
NDefines.NCountry.ENFORCE_RELIGION_LIBERTY = 99
NDefines.NCountry.ENFORCE_RELIGION_LIBERTY_THRESHOLD = -1
NDefines.NCountry.ENFORCE_CULTURE_LIBERTY = 99
NDefines.NCountry.ENFORCE_CULTURE_LIBERTY_THRESHOLD = -1
NDefines.NCountry.RELATIVE_ON_THRONE_LIBERTY = 35
NDefines.NCountry.CULTURAL_UNION_MIN_DEV = 1000						-- Minimum development to get cultural union effect without Common Sense, or if CULTURAL_UNION_MIN_RANK is set to negative value.
