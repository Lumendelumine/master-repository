#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
### DO_OVERHAUL MODIFIED ###

# Transportation Buildings
################################################
#Road Network
#Paved Road Network
#Road and Rail Network

#Post System

################################################
# Tier 1, 15th Century Buildings
################################################
post_system = {
	cost = 100
	time = 12
	
	modifier = {
		local_tax_modifier = 0.05
		local_production_efficiency = 0.05
		local_spy_defence = 0.10
		local_unrest = -1
		province_trade_power_value = 0.10
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_tax = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_tax = 5 }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			has_port = yes
		}		
		modifier = {
			factor = 3.0
			OR = {
				has_province_modifier = center_of_trade_modifier
				has_province_modifier = the_staple_port
				has_province_modifier = free_shipping_through_the_sound
				has_province_modifier = bosphorous_sound_toll
				has_province_modifier = sound_toll
				has_province_modifier = port_to_the_new_world
				has_province_modifier = neva_estuary_modifier
				has_province_modifier = daugava_estuary_modifier
				has_province_modifier = neman_estuary_modifier
				has_province_modifier = vistula_estuary_modifier
				has_province_modifier = oder_estuary_modifier
				has_province_modifier = elbe_estuary_modifier
				has_province_modifier = weser_estuary_modifier
				has_province_modifier = ems_estuary_modifier
				has_province_modifier = rhine_estuary_modifier
				has_province_modifier = thames_estuary_modifier
				has_province_modifier = rhone_estuary_modifier
				has_province_modifier = gironde_estuary_modifier
				has_province_modifier = loire_estuary_modifier
				has_province_modifier = seine_estuary_modifier
				has_province_modifier = ebro_estuary_modifier
				has_province_modifier = douro_estuary_modifier
				has_province_modifier = tagus_estuary_modifier
				has_province_modifier = guadiana_estuary_modifier
				has_province_modifier = po_estuary_modifier
				has_province_modifier = danube_estuary_modifier
				has_province_modifier = dnestr_estuary_modifier 
				has_province_modifier = dnieper_estuary_modifier
				has_province_modifier = volga_estuary_modifier
				has_province_modifier = don_estuary_modifier 
				has_province_modifier = yangtze_estuary_modifier
				has_province_modifier = huang_he_estuary_modifier
				has_province_modifier = ganges_estuary_modifier
				has_province_modifier = indus_estuary_modifier
				has_province_modifier = euphrates_estuary_modifier
				has_province_modifier = nile_estuary_modifier
				has_province_modifier = gambia_estuary_modifier
				has_province_modifier = pearl_estuary_modifier
				has_province_modifier = parana_estuary_modifier
				has_province_modifier = mekong_estuary_modifier
				has_province_modifier = mississippi_estuary_modifier
				has_province_modifier = rio_grande_estuary_modifier 
				has_province_modifier = niger_estuary_modifier
			}
		}
	}
}

road_network = {
	cost = 100
	time = 24
	
	modifier = {
		local_tax_modifier = 0.10
		tax_income = -0.5
		local_movement_speed = 0.5
		province_trade_power_value = 0.5
		supply_limit = 1
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_tax = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_tax = 5 }
		}
		modifier = {
			factor = 1.1
			is_overseas = no
		}			
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			OR = {
				has_province_modifier = center_of_trade_modifier
				has_province_modifier = the_staple_port
				has_province_modifier = free_shipping_through_the_sound
				has_province_modifier = bosphorous_sound_toll
				has_province_modifier = sound_toll
				has_province_modifier = port_to_the_new_world
				has_province_modifier = neva_estuary_modifier
				has_province_modifier = daugava_estuary_modifier
				has_province_modifier = neman_estuary_modifier
				has_province_modifier = vistula_estuary_modifier
				has_province_modifier = oder_estuary_modifier
				has_province_modifier = elbe_estuary_modifier
				has_province_modifier = weser_estuary_modifier
				has_province_modifier = ems_estuary_modifier
				has_province_modifier = rhine_estuary_modifier
				has_province_modifier = thames_estuary_modifier
				has_province_modifier = rhone_estuary_modifier
				has_province_modifier = gironde_estuary_modifier
				has_province_modifier = loire_estuary_modifier
				has_province_modifier = seine_estuary_modifier
				has_province_modifier = ebro_estuary_modifier
				has_province_modifier = douro_estuary_modifier
				has_province_modifier = tagus_estuary_modifier
				has_province_modifier = guadiana_estuary_modifier
				has_province_modifier = po_estuary_modifier
				has_province_modifier = danube_estuary_modifier
				has_province_modifier = dnestr_estuary_modifier 
				has_province_modifier = dnieper_estuary_modifier
				has_province_modifier = volga_estuary_modifier
				has_province_modifier = don_estuary_modifier 
				has_province_modifier = yangtze_estuary_modifier
				has_province_modifier = huang_he_estuary_modifier
				has_province_modifier = ganges_estuary_modifier
				has_province_modifier = indus_estuary_modifier
				has_province_modifier = euphrates_estuary_modifier
				has_province_modifier = nile_estuary_modifier
				has_province_modifier = gambia_estuary_modifier
				has_province_modifier = pearl_estuary_modifier
				has_province_modifier = parana_estuary_modifier
				has_province_modifier = mekong_estuary_modifier
				has_province_modifier = mississippi_estuary_modifier
				has_province_modifier = rio_grande_estuary_modifier 
				has_province_modifier = niger_estuary_modifier
			}
		}	
		modifier = {
			factor = 0.5
			is_overseas = yes
		}
		modifier = {
			factor = 5.0
			AND = {
				check_variable = { 
						which = "ProvincialAdministration"  
						value = 47 
				}
				NOT = {
					check_variable = { 
							which = "ProvincialAdministration"  
							value = 50 
					}
				}
			}
		}	
	}
}

################################################
# Tier 3, 17th Century Buildings
################################################

paved_road_network = {
	cost = 300
	time = 24
	
	make_obsolete = road_network
	
	modifier = {
		local_tax_modifier = 0.25
		tax_income = -2
		local_movement_speed = 1
		province_trade_power_value = 1
		supply_limit = 2
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_tax = 5 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_tax = 10 }
		}
		modifier = {
			factor = 1.1
			is_overseas = no
		}			
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			OR = {
				has_province_modifier = center_of_trade_modifier
				has_province_modifier = the_staple_port
				has_province_modifier = free_shipping_through_the_sound
				has_province_modifier = bosphorous_sound_toll
				has_province_modifier = sound_toll
				has_province_modifier = port_to_the_new_world
				has_province_modifier = neva_estuary_modifier
				has_province_modifier = daugava_estuary_modifier
				has_province_modifier = neman_estuary_modifier
				has_province_modifier = vistula_estuary_modifier
				has_province_modifier = oder_estuary_modifier
				has_province_modifier = elbe_estuary_modifier
				has_province_modifier = weser_estuary_modifier
				has_province_modifier = ems_estuary_modifier
				has_province_modifier = rhine_estuary_modifier
				has_province_modifier = thames_estuary_modifier
				has_province_modifier = rhone_estuary_modifier
				has_province_modifier = gironde_estuary_modifier
				has_province_modifier = loire_estuary_modifier
				has_province_modifier = seine_estuary_modifier
				has_province_modifier = ebro_estuary_modifier
				has_province_modifier = douro_estuary_modifier
				has_province_modifier = tagus_estuary_modifier
				has_province_modifier = guadiana_estuary_modifier
				has_province_modifier = po_estuary_modifier
				has_province_modifier = danube_estuary_modifier
				has_province_modifier = dnestr_estuary_modifier 
				has_province_modifier = dnieper_estuary_modifier
				has_province_modifier = volga_estuary_modifier
				has_province_modifier = don_estuary_modifier 
				has_province_modifier = yangtze_estuary_modifier
				has_province_modifier = huang_he_estuary_modifier
				has_province_modifier = ganges_estuary_modifier
				has_province_modifier = indus_estuary_modifier
				has_province_modifier = euphrates_estuary_modifier
				has_province_modifier = nile_estuary_modifier
				has_province_modifier = gambia_estuary_modifier
				has_province_modifier = pearl_estuary_modifier
				has_province_modifier = parana_estuary_modifier
				has_province_modifier = mekong_estuary_modifier
				has_province_modifier = mississippi_estuary_modifier
				has_province_modifier = rio_grande_estuary_modifier 
				has_province_modifier = niger_estuary_modifier
			}
		}	
		modifier = {
			factor = 0.5
			is_overseas = yes
		}	
		modifier = {
			factor = 5.0
			AND = {
				check_variable = { 
						which = "ProvincialAdministration"  
						value = 57 
				}
				NOT = {
					check_variable = { 
							which = "ProvincialAdministration"  
							value = 60 
					}
				}
			}
		}
	}
}

toll_road_network = {
	cost = 300
	time = 24
	
	make_obsolete = paved_road_network
	
	modifier = {
		local_tax_modifier = 0.25
		tax_income = -2
		local_movement_speed = 1
		province_trade_power_value = 1
		supply_limit = 2
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_tax = 5 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_tax = 10 }
		}
		modifier = {
			factor = 1.1
			is_overseas = no
		}			
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			OR = {
				has_province_modifier = center_of_trade_modifier
				has_province_modifier = the_staple_port
				has_province_modifier = free_shipping_through_the_sound
				has_province_modifier = bosphorous_sound_toll
				has_province_modifier = sound_toll
				has_province_modifier = port_to_the_new_world
				has_province_modifier = neva_estuary_modifier
				has_province_modifier = daugava_estuary_modifier
				has_province_modifier = neman_estuary_modifier
				has_province_modifier = vistula_estuary_modifier
				has_province_modifier = oder_estuary_modifier
				has_province_modifier = elbe_estuary_modifier
				has_province_modifier = weser_estuary_modifier
				has_province_modifier = ems_estuary_modifier
				has_province_modifier = rhine_estuary_modifier
				has_province_modifier = thames_estuary_modifier
				has_province_modifier = rhone_estuary_modifier
				has_province_modifier = gironde_estuary_modifier
				has_province_modifier = loire_estuary_modifier
				has_province_modifier = seine_estuary_modifier
				has_province_modifier = ebro_estuary_modifier
				has_province_modifier = douro_estuary_modifier
				has_province_modifier = tagus_estuary_modifier
				has_province_modifier = guadiana_estuary_modifier
				has_province_modifier = po_estuary_modifier
				has_province_modifier = danube_estuary_modifier
				has_province_modifier = dnestr_estuary_modifier 
				has_province_modifier = dnieper_estuary_modifier
				has_province_modifier = volga_estuary_modifier
				has_province_modifier = don_estuary_modifier 
				has_province_modifier = yangtze_estuary_modifier
				has_province_modifier = huang_he_estuary_modifier
				has_province_modifier = ganges_estuary_modifier
				has_province_modifier = indus_estuary_modifier
				has_province_modifier = euphrates_estuary_modifier
				has_province_modifier = nile_estuary_modifier
				has_province_modifier = gambia_estuary_modifier
				has_province_modifier = pearl_estuary_modifier
				has_province_modifier = parana_estuary_modifier
				has_province_modifier = mekong_estuary_modifier
				has_province_modifier = mississippi_estuary_modifier
				has_province_modifier = rio_grande_estuary_modifier 
				has_province_modifier = niger_estuary_modifier
			}
		}	
		modifier = {
			factor = 0.5
			is_overseas = yes
		}	
		modifier = {
			factor = 5.0
			AND = {
				check_variable = { 
						which = "ProvincialAdministration"  
						value = 57 
				}
				NOT = {
					check_variable = { 
							which = "ProvincialAdministration"  
							value = 60 
					}
				}
			}
		}
	}
}

################################################
# Tier 5, 19th Century Buildings
################################################

road_and_rail_network = {
	cost = 500
	time = 36
	
	make_obsolete = toll_road_network
	
	modifier = {
		local_tax_modifier = 0.5
		tax_income = -8
		local_movement_speed = 2
		province_trade_power_value = 1.5
		supply_limit = 3
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 1.1
			is_overseas = no
		}			
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0
			NOT = { base_tax = 12 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_tax = 24 }
		}
		modifier = {
			factor = 1.5
			OR = {
				has_province_modifier = center_of_trade_modifier
				has_province_modifier = the_staple_port
				has_province_modifier = free_shipping_through_the_sound
				has_province_modifier = bosphorous_sound_toll
				has_province_modifier = sound_toll
				has_province_modifier = port_to_the_new_world
				has_province_modifier = neva_estuary_modifier
				has_province_modifier = daugava_estuary_modifier
				has_province_modifier = neman_estuary_modifier
				has_province_modifier = vistula_estuary_modifier
				has_province_modifier = oder_estuary_modifier
				has_province_modifier = elbe_estuary_modifier
				has_province_modifier = weser_estuary_modifier
				has_province_modifier = ems_estuary_modifier
				has_province_modifier = rhine_estuary_modifier
				has_province_modifier = thames_estuary_modifier
				has_province_modifier = rhone_estuary_modifier
				has_province_modifier = gironde_estuary_modifier
				has_province_modifier = loire_estuary_modifier
				has_province_modifier = seine_estuary_modifier
				has_province_modifier = ebro_estuary_modifier
				has_province_modifier = douro_estuary_modifier
				has_province_modifier = tagus_estuary_modifier
				has_province_modifier = guadiana_estuary_modifier
				has_province_modifier = po_estuary_modifier
				has_province_modifier = danube_estuary_modifier
				has_province_modifier = dnestr_estuary_modifier 
				has_province_modifier = dnieper_estuary_modifier
				has_province_modifier = volga_estuary_modifier
				has_province_modifier = don_estuary_modifier 
				has_province_modifier = yangtze_estuary_modifier
				has_province_modifier = huang_he_estuary_modifier
				has_province_modifier = ganges_estuary_modifier
				has_province_modifier = indus_estuary_modifier
				has_province_modifier = euphrates_estuary_modifier
				has_province_modifier = nile_estuary_modifier
				has_province_modifier = gambia_estuary_modifier
				has_province_modifier = pearl_estuary_modifier
				has_province_modifier = parana_estuary_modifier
				has_province_modifier = mekong_estuary_modifier
				has_province_modifier = mississippi_estuary_modifier
				has_province_modifier = rio_grande_estuary_modifier 
				has_province_modifier = niger_estuary_modifier
			}
		}	
		modifier = {
			factor = 0.5
			is_overseas = yes
		}
		modifier = {
			factor = 5.0
			AND = {
				check_variable = { 
						which = "ProvincialAdministration"  
						value = 67 
				}
				NOT = {
					check_variable = { 
							which = "ProvincialAdministration"  
							value = 70 
					}
				}
			}
		}	
	}
}