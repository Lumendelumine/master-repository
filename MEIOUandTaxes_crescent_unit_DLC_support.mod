name="M&T Horsemen of the Crescent DLC Support v2.0"
path="mod/MEIOUandTaxes_crescent_unit_DLC_support"
picture="MEIOUandTaxesHoC.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
