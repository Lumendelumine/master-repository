name="M&T Purple Phoenix DLC Support v1.0"
path="mod/MEIOUandTaxes_pp_unit_DLC_support"
picture="MEIOUandTaxesPP.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
