name="M&T Iberian Colonial Unit DLC Support v2.0"
path="mod/MEIOUandTaxes_cuba_unit_DLC_support"
picture="MEIOUandTaxesICU.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
