name="M&T Res Publica DLC Support v1.0"
path="mod/MEIOUandTaxes_rep_unit_DLC_support"
picture="MEIOUandTaxesRP.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
