country_decisions = {
	
	#benign_neglect = {
	#	potential = {
	#		NOT = { has_country_modifier = beneficial_neglect }
	#		technology_group = western
	#		innovativeness_ideas = 3
	#	}
	#	allow = {
	#		NOT = { overextension_percentage = 0.02 }
	#		full_idea_group = innovativeness_ideas
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "beneficial_neglect"
	#			duration = -1
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	#colonial_expansion = {
	#	potential = {
	#		NOT = { has_country_modifier = restrictions_for_colonies }
	#		NOT = { has_country_modifier = colonial_expansions }
	#		technology_group = western
	#		OR = {
	#			has_idea = colonial_ventures
	#			has_idea = land_of_opportunity
	#		}
	#	}
	#	allow = {
	#		OR = { ADM = 3 advisor = lord_proprietor }
	#		full_idea_group = exploration_ideas
	#		num_of_colonial_subjects = 1
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "colonial_expansions"
	#			duration = -1
	#		}		
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	house_of_trade = {
		potential = {
			NOT = { has_country_modifier = the_house_of_trade }
			technology_group = western
			dip_tech = 22
		}
		allow = {
			OR = { 
				full_idea_group = trade_ideas
				full_idea_group = expansion_ideas
			}
			OR = { 
				num_of_protectorates = 2
				num_of_colonial_subjects = 3
			}
			OR = { trade_efficiency = 0.5 advisor = trader }
		}
		effect = {
			add_mercantilism = 0.05
			add_country_modifier = {
				name = "the_house_of_trade"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	colonial_restrictions = {
		potential = {
			NOT = { has_country_modifier = colonial_expansions }
			NOT = { has_country_modifier = restrictions_for_colonies }
			exploration_ideas = 3
			NOT = { economic_ideas = 3 }
		}
		allow =  {
			OR = { ADM = 4 advisor = alderman }
			num_of_colonial_subjects = 3
			full_idea_group = exploration_ideas
		}
		effect = {
			add_country_modifier = {
				name = "restrictions_for_colonies"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	open_christian_age_of_exploration = {
		potential = {
			NOT = { has_country_flag = age_of_exploration }
			religion_group = christian
			num_of_ports = 1
			always = no
		}
		allow = {
			OR = {
				full_idea_group = naval_ideas
				ai = yes
			}
			OR = {
				AND = {
					OR = { tag = POR tag = GBR tag = NED }
					dip_tech = 14
				}
				AND = {
					culture_group = iberian
					NOT = { iberia_superregion = { owner = { religion_group = muslim } } }
					dip_tech = 14
				}
				AND = {
					OR = { tag = ENG  tag = FRA }
					NOT = { has_global_flag = hundred_year_war }
					dip_tech = 14
				}
					dip_tech = 20
			}
			num_of_ports = 2
			is_at_war = no
			
		}
		effect = {
			set_country_flag = age_of_exploration
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	open_other_age_of_exploration = {
		potential = {
			NOT = { has_country_flag = age_of_exploration }
			NOT = { religion_group = christian }
			num_of_ports = 1
			always = no
		}
		allow = {
			full_idea_group = naval_ideas
			dip_tech = 20
			num_of_ports = 2
			is_at_war = no
		}
		effect = {
			set_country_flag = age_of_exploration
		}
		ai_will_do = {
			factor = 0
		}
		ai_importance = 0
	}
	
	open_early_age_of_exploration = {
		potential = {
			religion_group = christian
			num_of_ports = 1
			owns = 230
			OR = {
				tag = POR
				culture_group = iberian
			}
			NOT = { num_of_explorers = 1 }
			NOT = { has_idea_group = exploration_ideas }
			NOT = { has_country_flag = early_exploration }
		}
		allow = {
			OR = {
				full_idea_group = naval_ideas
				ai = yes
			}
			OR = {
				AND = {
					tag = POR
					dip_tech = 12
				}
				AND = {
					culture_group = iberian
					dip_tech = 14
				}
			}
			num_of_ports = 2
		}
		effect = {
			add_country_modifier = {
				name = "early_exploration"
				duration = -1
			}
			add_prestige = 1
			set_country_flag = early_exploration
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

}
