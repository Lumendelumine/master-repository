country_decisions = {

	evangelical_union = {
		potential = {
			OR = { 
				religion = reformed
				religion = protestant
				}
			OR = { 
				AND = { 
					religion = reformed
					num_of_religion = { religion = protestant value = 0.25 }
					}
				AND = { 
					religion = protestant
					num_of_religion = { religion = reformed value = 0.25 }
					}
				}
			tech_1750_trigger = yes
			NOT = { has_country_flag = evangelical_union }
			is_religion_enabled = protestant
			is_religion_enabled = reformed
			}
		allow = { 
			is_at_war = no
			stability = 1
			DIP = 4
			}
		effect = {
			hidden_effect = { set_country_flag = evangelical_union }
			add_stability = -1
			if = { 
				limit = { 
					dominant_religion = protestant 
					NOT = { religion = protestant }
					}
				change_religion = protestant
				}
			if = { 
				limit = { 
					dominant_religion = reformed
					NOT = { religion = reformed }
					}
				change_religion = reformed
				}
			if = {
				limit = { religion = protestant }
				custom_tooltip = convert_half_protestant
				hidden_effect = {
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 4 } }
							check_variable = { which = "reformed" value = 2 }
							}
						change_variable = { which = "reformed" value = -1 }
						change_variable = { which = "protestant" value = 1 }
						}
					}
				}
			hidden_effect = {
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 6 } }
							check_variable = { which = "reformed" value = 4 }
							}
						change_variable = { which = "reformed" value = -2 }
						change_variable = { which = "protestant" value = 2 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 8 } }
							check_variable = { which = "reformed" value = 6 }
							}
						change_variable = { which = "reformed" value = -3 }
						change_variable = { which = "protestant" value = 3 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 10 } }
							check_variable = { which = "reformed" value = 8 }
							}
						change_variable = { which = "reformed" value = -4 }
						change_variable = { which = "protestant" value = 4 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 12 } }
							check_variable = { which = "reformed" value = 10 }
							}
						change_variable = { which = "reformed" value = -5 }
						change_variable = { which = "protestant" value = 5 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 14 } }
							check_variable = { which = "reformed" value = 12 }
							}
						change_variable = { which = "reformed" value = -6 }
						change_variable = { which = "protestant" value = 6 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 16 } }
							check_variable = { which = "reformed" value = 14 }
							}
						change_variable = { which = "reformed" value = -7 }
						change_variable = { which = "protestant" value = 7 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 18 } }
							check_variable = { which = "reformed" value = 16 }
							}
						change_variable = { which = "reformed" value = -8 }
						change_variable = { which = "protestant" value = 8 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "reformed" value = 20 } }
							check_variable = { which = "reformed" value = 18 }
							}
						change_variable = { which = "reformed" value = -9 }
						change_variable = { which = "protestant" value = 9 }
						}
					}
				if = {
					limit = { religion = protestant }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							check_variable = { which = "reformed" value = 20 }
							}
						change_variable = { which = "reformed" value = -10 }
						change_variable = { which = "protestant" value = 10 }
						}
					}
				}
			if = {
				limit = { religion = reformed }
				custom_tooltip = convert_half_reformed
				hidden_effect = {
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 4 } }
							check_variable = { which = "protestant" value = 2 }
							}
						change_variable = { which = "protestant" value = -1 }
						change_variable = { which = "reformed" value = 1 }
						}
					}
				}
			hidden_effect = {
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 6 } }
							check_variable = { which = "protestant" value = 4 }
							}
						change_variable = { which = "protestant" value = -2 }
						change_variable = { which = "reformed" value = 2 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 8 } }
							check_variable = { which = "protestant" value = 6 }
							}
						change_variable = { which = "protestant" value = -3 }
						change_variable = { which = "reformed" value = 3 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 10 } }
							check_variable = { which = "protestant" value = 8 }
							}
						change_variable = { which = "protestant" value = -4 }
						change_variable = { which = "reformed" value = 4 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 12 } }
							check_variable = { which = "protestant" value = 10 }
							}
						change_variable = { which = "protestant" value = -5 }
						change_variable = { which = "reformed" value = 5 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 14 } }
							check_variable = { which = "protestant" value = 12 }
							}
						change_variable = { which = "protestant" value = -6 }
						change_variable = { which = "reformed" value = 6 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 16 } }
							check_variable = { which = "protestant" value = 14 }
							}
						change_variable = { which = "protestant" value = -7 }
						change_variable = { which = "reformed" value = 7 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 18 } }
							check_variable = { which = "protestant" value = 16 }
							}
						change_variable = { which = "protestant" value = -8 }
						change_variable = { which = "reformed" value = 8 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							NOT = { check_variable = { which = "protestant" value = 20 } }
							check_variable = { which = "protestant" value = 18 }
							}
						change_variable = { which = "protestant" value = -9 }
						change_variable = { which = "reformed" value = 9 }
						}
					}
				if = {
					limit = { religion = reformed }
					every_owned_province = { 
						limit = { 
							culture_group = ROOT 
							check_variable = { which = "protestant" value = 20 }
							}
						change_variable = { which = "protestant" value = -10 }
						change_variable = { which = "reformed" value = 10 }
						}
					}
				}
			}
		ai_will_do = {
			factor = 1
			}
		}
	#book_of_common_prayer = { }
	
	witchcraft_laws = {
		potential = { 
			OR = { 
				religion = catholic
				religion = protestant
				religion = reformed
				religion = gnostic
				religion = hussite
				}
			OR = { 
				any_owned_province = { has_province_modifier = witch_hunt }
				has_country_flag = witch_hunts_discouraged
				tech_1700_trigger = yes
				NOT = { piety = -0.20 }
				}
			NOT = { has_country_flag = witchcraft_laws }
			}
		allow = { 
			ADM = 2
			}
		effect = { 
			hidden_effect = { set_country_flag = witchcraft_laws }
			custom_tooltip = prevents_witch_hunts
			add_piety = -0.05
			}
		ai_will_do = {
			factor = 1
			modifier = {
				NOT = { any_owned_province = { has_province_modifier = witch_hunt } }
				NOT = { has_country_flag = witch_hunts_discouraged }
				factor = 0
				}
			}
		}

	divorce_spouse = {
		potential = { 
			OR = { 
				religion = protestant
				AND = { 
					religion = catholic
					has_country_flag = national_church
					}
				}
			government = monarchy
			has_regency = no
			has_heir = no
			ruler_age = 25
			OR = { 
				NOT = { has_country_flag = divorced_ruler }
				had_country_flag = { flag = divorced_ruler days = 1000 }
				}
			NOT = { any_country = { reverse_has_opinion_modifier = { who = ROOT modifier = marriage_broken } } }
			}
		allow = { 
			months_of_ruling = 24
			num_of_royal_marriages = 1
			OR = {
				NOT = { has_country_modifier = limited_divorce_aspect }
				adm_power = 50
				}
			}
		effect = { 
			if = {
				limit = { has_country_modifier = limited_divorce_aspect }
				add_adm_power = -50
				}
			add_prestige = -2.5
			add_legitimacy = -2.5
			hidden_effect = { set_country_flag = divorced_ruler }
			random = { 
				chance = 25
				define_heir = { claim = 90 dynasty = ROOT }
				add_legitimacy = 5
				}
			random_country = { 
				limit = { 
					marriage_with = ROOT
					religion = ROOT
					OR = { 
						ai = no
						is_rival = ROOT
						is_emperor = yes
						culture_group = ROOT
						}
					}
				country_event = { id = dg_catholic.087 days = 30 }
				}
			random_country = { 
				limit = { 
					NOT = { 
						any_country = { 
							marriage_with = ROOT
							religion = ROOT
							OR = { 
								ai = no
								is_rival = ROOT
								is_emperor = yes
								culture_group = ROOT
								}
							}
						}
					marriage_with = ROOT
					}
				reverse_add_opinion = { who = ROOT modifier = marriage_broken }
				country_event = { id = dg_catholic.087 days = 30 }
				}
			}
		ai_will_do = { 
			factor = 1
			}
		}
}