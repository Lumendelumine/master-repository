country_decisions = {

	make_thraki_capital = { #Edirne is the renamed Adrianopolis, e.g. Hevros province (development 24), not Thraki (development 10)
		major = yes
		potential = {
			primary_culture = turkish
			any_owned_province = { area = thrace_area }
			NOT = { capital = 3876 }
			NOT = { capital = 1402 }
			NOT = { has_country_flag = capital_to_thraki }
		}
		allow = {
			owns = 3876
			controls = 3876 #Hevros
			adm = 3
			owns = 2238 #Gallipoli, neighboring and owned by Ottomans in 1356
#			owns = 155 #Serres, owned by Serbia - never seems to happen
			owns = 317 #Bergama in Anatolia, owned by Ottomans in 1356
			owns = 1446 #Izmit in Anatolia, owned by Ottomans in 1356
			is_at_war = no
		}
		effect = {
			set_country_flag = capital_to_thraki
			add_adm_power = -25
			capital_scope = {
				if = {
					limit = {
						NOT = { has_building = town_hall }
						OR = {
							num_free_building_slots = 1
							has_building = constable
							}
						}
					remove_building = constable
					add_building = town_hall
					}
				}
			add_stability = 1
			set_capital = 3876
			add_core = 3876
			3876 = {
				change_culture = turkish 
				change_religion = sunni
				change_province_name = "Trakya"
				rename_capital = "Edirne"
				add_base_tax = 3
				add_base_manpower = 2
				add_nationalism = -10
				if = {
					limit = { 
						NOT = { has_building = fort_14th }
						NOT = { has_building = fort_15th }
						NOT = { has_building = fort_16th }
						NOT = { has_building = fort_17th }
						NOT = { has_building = fort_18th }
						num_free_building_slots = 1
						}
					add_building = fort_14th
					}
				every_neighbor_province = {
					limit = { owned_by = ROOT is_core = ROOT }
					add_local_autonomy = -25
					hidden_effect = { change_variable = { which = converted_heathens value = 4 } }
					}
				every_neighbor_province = {
					limit = { owned_by = ROOT NOT = { is_core = ROOT } }
					add_core = ROOT
					add_local_autonomy = -10
					hidden_effect = { change_variable = { which = converted_heathens value = 2 } }
					}
				every_neighbor_province = {
					limit = { NOT = { owned_by = ROOT } NOT = { is_core = ROOT } }
					add_claim = ROOT
					}
				}
			hidden_effect = { REB = { country_event = { id = prov_admin.1 days = 1 } } }
			}
		ai_will_do = {
			factor = 1
		}
	}

	make_constantinople_capital = {
		major = yes
		potential = {
			primary_culture = turkish
			owns = 1402
			NOT = { capital = 1402 }
			NOT = { has_country_flag = relocated_capital_constantinople }
		}
		allow = {
			adm = 3
			1402 = {
				controlled_by = ROOT
				NOT = { any_neighbor_province = { NOT = { owned_by = ROOT } } }
				}
			}
		effect = {
			add_adm_power = -25
			capital_scope = {
				if = {
					limit = {
						NOT = { has_building = town_hall }
						OR = {
							num_free_building_slots = 1
							has_building = constable
							}
						}
					remove_building = constable
					add_building = town_hall
					}
				}
			set_country_flag = relocated_capital_constantinople
			add_stability = 1
			set_capital = 1402
			add_core = 1402
			1402 = {
				add_nationalism = -10
				change_culture = turkish 
				change_religion = sunni
				change_province_name = "Kostantiniyye"
				rename_capital = "Kostantiniyye"
				add_base_tax = 6
				add_base_manpower = 5
				if = {
					limit = { 
						NOT = { has_building = fort_14th }
						NOT = { has_building = fort_15th }
						NOT = { has_building = fort_16th }
						NOT = { has_building = fort_17th }
						NOT = { has_building = fort_18th }
						num_free_building_slots = 1
						}
					add_building = fort_14th
					}
				every_neighbor_province = {
					limit = { owned_by = ROOT is_core = ROOT }
					add_local_autonomy = -25
					change_culture = turkish
					hidden_effect = { change_variable = { which = converted_heathens value = 4 } }
					}
				every_neighbor_province = {
					limit = { owned_by = ROOT NOT = { is_core = ROOT } }
					add_core = ROOT
					add_local_autonomy = -10
					hidden_effect = { change_variable = { which = converted_heathens value = 2 } }
					}
				}
			if = {
				limit = {
					check_variable = { which = "centralization_decentralization" value = -4 }
				}
				change_variable = { which = "centralization_decentralization" value = -1 }
			}
			hidden_effect = { REB = { country_event = { id = prov_admin.1 days = 1 } } }
		}
		ai_will_do = {
			factor = 1
		}
	}

	provincial_system = {
		potential = {
			primary_culture = turkish
			NOT = { has_country_modifier = the_provincial_system }
		}
		allow = {
			adm_tech = 17
			or = { adm = 4 advisor = sheriff advisor = alderman }
			adm_power = 50
		}
		effect = {
			add_country_modifier = {
				name = "the_provincial_system"
				duration = -1
			}
			add_adm_power = -50
			set_country_flag = the_provincial_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { mil_power = 100 }
			}
		}
	}

	claim_on_rum = {
		potential = {
			primary_culture = turkish
			num_of_owned_provinces_with = {
				value = 3
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			NOT = { owns = 1402 }
			NOT = { 1402 = { is_claim = ROOT } }
		}
		allow = {
			num_of_owned_provinces_with = {
				value = 7
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			is_at_war = no
			dip_power = 150
		}
		effect = {
			BYZ = {
				every_owned_province = {
					add_claim = ROOT
				}
			}
			add_dip_power = -100
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	claim_the_sultanate_of_rum = {
		potential = {
			primary_culture = turkish
			NOT = { has_country_flag = rum_sultanate_claimed }
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				area = thrace_area
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			set_country_flag = rum_sultanate_claimed
			if = {
				limit = { NOT = { government_rank = 5 } }
				set_government_rank = 5
				}
			add_ruler_modifier = { name = rum_sultanate_claimed }
			add_dip_power = 100
			add_mil_power = 100
			add_adm_power = 100
			add_prestige = 25
			add_legitimacy = 10
			every_province = {
				limit = {
					OR = {
						region = coastal_anatolia_region
						region = inland_anatolia_region
						region = greece_region
						area = thrace_area
					}
					NOT = { is_core = ROOT }
					NOT = { is_claim = ROOT }
					NOT = { owned_by = ROOT }
					}
				add_claim = ROOT
				}
			clr_country_flag = rising_nation
			}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { total_development = 500 }
				}
			modifier = {
				factor = 0
				average_autonomy = 40
				}
		}
	}
	
	disband_the_janissaries = {
		potential = {
			OR = {
				has_country_modifier = tur_janissary_1
				has_country_modifier = tur_janissary_2
				has_country_modifier = tur_janissary_3
				has_country_modifier = tur_janissary_4
			}
		}
		allow = {
			has_disaster = janissary_decadence
			
		}
		effect = {
			remove_country_modifier = tur_janissary_1
			remove_country_modifier = tur_janissary_2
			remove_country_modifier = tur_janissary_3
			remove_country_modifier = tur_janissary_4
			end_disaster = janissary_decadence
		}
		ai_will_do = {
			factor = 1
		}
	}

	abrogate_beylik_of_karasi = {
		potential = {
			any_owned_province = {
				has_province_modifier = beylik_of_karasi
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			OR = {
				NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
				NOT = { religion_group = muslim }
			}
			mil_power = 150
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				region = greece_region
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = beylik_of_karasi
				}
				remove_province_modifier = beylik_of_karasi
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 100
	}

	abrogate_beylik_of_saruhan = {
		potential = {
			any_owned_province = {
				has_province_modifier = beylik_of_saruhan
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			OR = {
				NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
				NOT = { religion_group = muslim }
			}
			mil_power = 150
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				region = greece_region
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = beylik_of_saruhan
				}
				remove_province_modifier = beylik_of_saruhan
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 100
	}

	abrogate_beylik_of_isfendiyar = {
		potential = {
			any_owned_province = {
				has_province_modifier = beylik_of_isfendiyar
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			OR = {
				NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
				NOT = { religion_group = muslim }
			}
			mil_power = 150
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				region = greece_region
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = beylik_of_isfendiyar
				}
				remove_province_modifier = beylik_of_isfendiyar
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 100
	}

	abrogate_beylik_of_artuqids = {
		potential = {
			any_owned_province = {
				has_province_modifier = beylik_of_artuqids
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			OR = {
				NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
				NOT = { religion_group = muslim }
			}
			mil_power = 150
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				region = greece_region
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = beylik_of_artuqids
				}
				remove_province_modifier = beylik_of_artuqids
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 100
	}

	abrogate_beylik_of_ankara = {
		potential = {
			any_owned_province = {
				has_province_modifier = beylik_of_ankara
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			OR = {
				NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
				NOT = { religion_group = muslim }
			}
			mil_power = 150
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				region = greece_region
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = beylik_of_ankara
				}
				remove_province_modifier = beylik_of_ankara
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 100
	}

	abrogate_ahis_fraternity = {
		potential = {
			any_owned_province = {
				has_province_modifier = ahis_fraternity
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
		}
		allow = {
			OR = {
				NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
				NOT = { religion_group = muslim }
			}
			mil_power = 150
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					region = coastal_anatolia_region
					region = inland_anatolia_region
				}
			}
			num_of_owned_provinces_with = {
				value = 3
				region = greece_region
			}
			owns = 1402
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = ahis_fraternity
				}
				remove_province_modifier = ahis_fraternity
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 100
	}

}
