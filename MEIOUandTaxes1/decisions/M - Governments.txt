country_decisions = {
	
	imperial_administration = {
		potential = {
			NOT = { has_country_modifier = "imperial_admin" }
			government = monarchy
			num_of_cities = 30
			is_subject = no
			prestige = 10
		}
		allow =  {
			stability = 1
			num_of_cities = 45
			is_at_war = no
			adm_power = 250
			prestige = 20
		}
		effect = {
			add_country_modifier = {
				name = "imperial_admin"
				duration = -1
			}
			add_adm_power = -250
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 200	#FB
	}

	# Support for venetian_republic
	plutocratic_administration = {
		potential = {
			NOT = { 
				OR = {
					government = merchant_republic 
					government = venetian_republic
				}
			}
			OR = {
				technology_group = western
				technology_group = eastern
				technology_group = turkishtech
				technology_group = high_turkishtech
				technology_group = muslim
			}
			OR = {
				government = noble_republic
				government = administrative_republic
				government = republican_dictatorship
				government = oligarchic_republic
				tag = FRL
				tag = GEN
				tag = VEN
				tag = HSA
			}
		}
		allow =  {
			stability = 1
			full_idea_group = trade_ideas

			is_at_war = no 
			is_subject = no
			num_of_ports = 1
		}
		effect = {
			change_government = merchant_republic
			set_government_rank = 4
			add_stability = -2
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				num_of_cities = 3
			}
		}
	}

	formalize_separation_of_powers = {
		potential = {
			government = constitutional_monarchy
			NOT = {
				has_country_modifier = separation_of_powers
			}
		}
		allow = {
			stability = 2
			advisor = statesman
			is_at_war = no
		}
		effect = {
			add_country_modifier = {
				name = "separation_of_powers"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	go_cult_of_reason = {
		potential = {
			government = revolutionary_republic
			NOT = {
				has_country_modifier = cult_of_reason
			}
		}
		allow = {
			NOT = { stability = -1 }
		}
		effect = {
			add_country_modifier = {
				name = "cult_of_reason"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_altaic_despotic_monarchy = {
		potential = {
			government = altaic_monarchy
		}
		allow = {
			adm_tech = 5			# From technology\ADM.txt
			stability = 1
			adm_power = 100			# From common\defines.lua
			NOT = { culture_group = altaic }
			NOT = { culture_group = tartar_group }
			OR = {
				adm = 5
				mil = 5
			}
		}
		effect = {
			add_adm_power = -100
			change_government = despotic_monarchy
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_despotic_monarchy = {
		potential = {
			OR = {
				government = feudal_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 5			# From technology\ADM.txt
			stability = 1
			adm_power = 100			# From common\defines.lua
			OR = {
				has_country_modifier = "arbitrary"
				has_country_modifier = "cruel"
				has_country_modifier = "flamboyant_schemer"
				has_country_modifier = "energetic"
				has_country_modifier = "selfish"
				adm = 5
				mil = 5
			}
		}
		effect = {
			add_adm_power = -100
			change_government = despotic_monarchy
		}
		ai_will_do = {
			factor = 0		# You never want to willingly go back to despotic
		}
	}
	
	changegov_feudal_monarchy = {
		potential = {
			OR = {
				government = despotic_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 5			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			# Tier 1
			change_government = feudal_monarchy
		}
		ai_will_do = {
			factor = 0		# You never want to willingly go back to feudal
		}
	}
	
	changegov_administrative_monarchy = {
		potential = {
			adm_tech = 9			# 50 years away
			OR = {
				government = despotic_monarchy
				government = feudal_monarchy
				government = constitutional_monarchy
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 22			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_monarchy
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0		# Don't go backwards from constitutional
				government = constitutional_monarchy
			}
		}
	}

	changegov_absolute_monarchy = {
		potential = {
			adm_tech = 28			# 50 years away
			OR = {
				government = despotic_monarchy
				government = feudal_monarchy
				government = administrative_monarchy
				government = enlightened_despotism
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 33			# From technology\ADM.txt
			stability = 0
			adm = 3			
			OR = {
				adm_power = 100			# From common\defines.lua
				AND = {
					government = administrative_monarchy
					adm_power = 250
				}
			}
			NOT = { check_variable = { which = "centralization_decentralization" value = -2 } }
		}
		effect = {
			# Higher cost for administrative, not a natural change.
			if = {
				limit = {
					government = administrative_monarchy
				}
				add_adm_power = -250
			}
			
			# Standard cost for rest
			if = {
				limit = {
					NOT = { government = administrative_monarchy }
				}
				add_adm_power = -100
			}
			change_government = absolute_monarchy
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0		# Don't go backwards from e.despotism
				government = enlightened_despotism
			}
		}
	}
	
	changegov_constitutional_monarchy = {
		potential = {
			adm_tech = 33			# 50 years away
			OR = {
				government = administrative_monarchy
				government = english_monarchy
			}
		}
		allow = {
			adm_tech = 39			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = constitutional_monarchy
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				check_variable = { which = "centralization_decentralization" value = 0 }
			}
		}
	}
	
	changegov_enlightened_despotism = {
		potential = {
			adm_tech = 41			# 50 years away
			government = absolute_monarchy
		}
		allow = {
			adm_tech = 46			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = enlightened_despotism
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_religious_order = {
		potential = {
			government = theocratic_government
		}
		allow = {
			adm_tech = 7			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = monastic_order_government
		}
		ai_will_do = {	
			factor = 0
		}
	}
	
	changegov_theocracy = {
		potential = {
			government = monastic_order_government
		}
		allow = {
			adm_tech = 7			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = theocratic_government
		}
		ai_will_do = {	
			factor = 0
		}
	}
	
	changegov_medieval_monarchy = {
		potential = {
			government = medieval_monarchy
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = 4 } }
			adm_tech = 5			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = feudal_monarchy
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	changegov_administrative_republic = {
		potential = {
			OR = {
				government = oligarchic_republic
				government = noble_republic
				government = merchant_republic
			}
			adm_tech = 12
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
			adm_tech = 16			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_republic
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0		
				government = merchant_republic
			}
		}
	}
	
	changegov_noble_republic = {
		potential = {
			OR = { 
				government = oligarchic_republic
				government = merchant_republic
			}
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
			adm_tech = 7			# From technology\ADM.txt
			stability = 1
			adm_power = 100			# From common\defines.lua
			faction_in_power = mr_aristocrats
		}
		effect = {
			add_adm_power = -100
			add_stability = -1
			change_government = noble_republic
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_oligarchic_republic = {
		potential = {
			government = noble_republic
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = 4 } }
			adm_tech = 7			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_republic
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_republican_dictatorship = {
		potential = {
			adm_tech = 22	
			government = republic
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			adm_tech = 35			# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_republic
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	changegov_constitutional_republic = {
		potential = {
			adm_tech = 30
			government = republic
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			adm_tech = 45  		# From technology\ADM.txt
			stability = 0
			adm_power = 100			# From common\defines.lua
		}
		effect = {
			add_adm_power = -100
			change_government = administrative_republic
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
