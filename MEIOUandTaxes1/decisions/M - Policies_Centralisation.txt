country_decisions = {
	
	centralise_the_state_1 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			check_variable = { which = "centralization_decentralization" value = 5 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_2 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 5 } }
			check_variable = { which = "centralization_decentralization" value = 4 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_3 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 4 } }
			check_variable = { which = "centralization_decentralization" value = 3 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_4 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
			check_variable = { which = "centralization_decentralization" value = 2 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_5 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 2 } }
			check_variable = { which = "centralization_decentralization" value = 1 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_6 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
			check_variable = { which = "centralization_decentralization" value = 0 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_7 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
			check_variable = { which = "centralization_decentralization" value = -1 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_8 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			check_variable = { which = "centralization_decentralization" value = -2 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
			remove_country_modifier = millet_system
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_9 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -2 } }
			check_variable = { which = "centralization_decentralization" value = -3 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_10 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -3 } }
			check_variable = { which = "centralization_decentralization" value = -4 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			set_country_flag = "centralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}
	
	cancel_centralisation = {
		potential = {
			has_country_flag = "centralisation_efforts"
		}
		allow = {
			has_country_flag = "centralisation_efforts"
		}
		effect = {
			clr_country_flag = "centralisation_efforts"
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_1 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -4 } }
			check_variable = { which = "centralization_decentralization" value = -5 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_2 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -3 } }
			check_variable = { which = "centralization_decentralization" value = -4 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_3 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -2 } }
			check_variable = { which = "centralization_decentralization" value = -3 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_4 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			check_variable = { which = "centralization_decentralization" value = -2 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_5 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
			check_variable = { which = "centralization_decentralization" value = -1 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_6 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
			check_variable = { which = "centralization_decentralization" value = 0 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_7 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 2 } }
			check_variable = { which = "centralization_decentralization" value = 1 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_8 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
			check_variable = { which = "centralization_decentralization" value = 2 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_9 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 4 } }
			check_variable = { which = "centralization_decentralization" value = 3 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_10 = {
		potential = {
			NOT = { has_country_flag = "centralisation_efforts" }
			NOT = { has_country_flag = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 5 } }
			check_variable = { which = "centralization_decentralization" value = 4 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			OR = { AND = { ai = no adm_power = 150 } AND = { ai = yes adm_power = 50 } }
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			set_country_flag = "decentralisation_efforts"
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	cancel_decentralisation = {
		potential = {
			has_country_flag = "decentralisation_efforts"
		}
		allow = {
			has_country_flag = "decentralisation_efforts"
		}
		effect = {
			clr_country_flag = "decentralisation_efforts"
		}
		ai_will_do = {
			factor = 0
		}
	}

}
