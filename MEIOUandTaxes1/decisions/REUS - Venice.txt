country_decisions = {

	confirm_thalassocracy = {
		potential = {
			num_of_total_ports = 12
			#OR = { 
			#	tag = VEN 
			#	tag = GEN 
			#	tag = HSA 
			#	tag = FRL
			#}
			OR = { 
				government = merchant_republic
				government = venetian_republic
			}
			NOT = { 
				has_country_modifier = thalassocracy
			}
		}
		allow = {
			overseas_provinces_percentage = 0.5
			OR = {
				full_idea_group = naval_ideas
				full_idea_group = merchant_marine_ideas
			}
		}
		effect = {
			add_country_modifier = {
				name = "thalassocracy"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	promote_printing_industry = {
		potential = {
			has_global_flag = printing_press
			NOT = { has_country_modifier = printing_industry }
			dip_tech = 10
		}
		allow = {
			adm_power = 50
			is_at_war = no
			capital_scope = { has_province_flag = printing_press }
		}
		effect = {
			add_adm_power = -50
			add_country_modifier = {
				name = "printing_industry"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	enact_stato_da_mar = {
		potential = {
			OR = { 
				government = merchant_republic
				government = venetian_republic
			}
			num_of_total_ports = 12
			NOT = { has_country_modifier = stato_da_mar }
		}
		allow = {
			adm_power = 50
			num_of_total_ports = 18
			OR = {
				trade_income_percentage = 0.33
				monthly_income = 150
			}
		}
		effect = {
			add_adm_power = -50
			add_country_modifier = {
				name = "stato_da_mar"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

}
