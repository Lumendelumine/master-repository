country_decisions = {

	# Form Japan
	form_japan = { #Shogunate
		major = yes
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 5
			NOT = { government_rank = 6 }
		}
		allow = {
			is_subject = no
			NOT = {
				any_country = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 5 }
				}
			}
			owns = 2283
		}
		effect = {
			set_government_rank = 6
			if = {
				limit = { NOT = { tag = JAP } }
				inherit = JAP
				change_tag = JAP
			}
			JAP = {
				set_capital = 2283
			}
			add_legitimacy = 100
			add_adm_power = 100
			add_dip_power = 100
			add_mil_power = 100
			form_new_country_effect = yes
			hidden_effect = { REB = { country_event = { id = prov_admin.1 days = 1 } } }
		}
		ai_will_do = {
			factor = 1
		}
	}	

	# Unite Japan
	end_shogunate = {
		major = yes
		potential = {
			tag = JAP
			government_rank = 6
			NOT = { has_country_flag = shogunate_ended }
		}
		allow = {
			is_subject = no
			is_at_war = no
			NOT = {
				any_country = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					government_rank = 5
					NOT = { government_rank = 6 }
				}
			}
			OR = {
				check_variable = { which = "Demesne_in_Shogunate" value = 35 }
				check_variable = { which = "Cores_on_Shogunate" value = 25 }
			}
			owns = 2283
		}
		effect = {
			japan_region = { limit = { owned_by = ROOT } add_core = JAP }
			japan_region = { limit = { NOT = { owned_by = ROOT } } add_claim = JAP }
			increase_centralisation = yes
			set_country_flag = shogunate_ended
			hidden_effect = { REB = { country_event = { id = prov_admin.1 days = 1 } } }
		}
		ai_will_do = {
			factor = 1
		}
	}

	unite_japan_shogun = {
		major = yes
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 5
			NOT = { government_rank = 6 }
			num_of_vassals = 1
			any_country = {
				vassal_of = ROOT
				OR = {
					government = early_medieval_japanese_gov
					government = late_medieval_japanese_gov
					government = early_modern_japanese_gov
				}
				NOT = { government_rank = 5 }
			}
		}
		allow = {
			is_subject = no
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 15
				region = japan_region
				is_core = ROOT
			}
			OR = {
				owns = 2283
				JAP = {
					owns = 2283
				}
			}
		}
		effect = {
			japan_region = { limit = { owned_by = ROOT } add_core = JAP }
			japan_region = { limit = { NOT = { owned_by = ROOT } } add_claim = JAP }
			set_government_rank = 6
			every_country = {
				limit = {
					vassal_of = ROOT
					NOT = {
						tag = JAP
					}
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 5 }
					ai = yes
				}
				country_event = { id = japan.6 days = 1 }
			}
			if = {
				limit = { NOT = { tag = JAP } }
				inherit = JAP
				change_tag = JAP
			}
			JAP = {
				set_capital = 2283
			}
			form_new_country_effect = yes
			hidden_effect = { REB = { country_event = { id = prov_admin.1 days = 1 } } }
		}
		ai_will_do = {
			factor = 1
		}
	}

	unite_japan_daimyo = {
		major = yes
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }
			NOT = {
				any_country = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					government_rank = 5
					NOT = { government_rank = 6 }
				}
			}
		}
		allow = {
			is_subject = no
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 30
				region = japan_region
				is_core = ROOT
			}
		}
		effect = {
			japan_region = { limit = { owned_by = ROOT } add_core = JAP }
			japan_region = { limit = { NOT = { owned_by = ROOT } } add_claim = JAP }
			if = {
				limit = {
					exists = JAP
				}
				inherit = JAP
			}
			change_tag = JAP
			JAP = {
				set_capital = 2283
			}
			set_government_rank = 6
			form_new_country_effect = yes
			hidden_effect = { REB = { country_event = { id = prov_admin.1 days = 1 } } }
		}
		ai_will_do = {
			factor = 1
		}
	}

	# Sword Hunt
	sword_hunt = {
		potential = {
			always = no
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }
			NOT = {
				has_country_modifier = sword_hunter
				has_country_flag = sword_hunt
			}
		}
		allow = {
			stability = 3
			legitimacy = 50
			prestige = 50
		}
		effect = {
			add_country_modifier = {
				name = sword_hunter
				duration = 365
			}
			set_country_flag = sword_hunt
			every_country = {
				limit = { 
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 5 }
					NOT = {
						has_country_modifier = sword_hunter
					}
				}
				add_opinion = { who = ROOT modifier = opinion_sword_hunt }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = {
					any_country = {
						OR = {
							government = early_medieval_japanese_gov
							government = late_medieval_japanese_gov
							government = early_modern_japanese_gov
						}
						OR = {
							NOT = { has_opinion = { who = ROOT value = 0 } }
							has_opinion = { who = ROOT value = 170 }
						}
					}
				}
			}
		}
	}
	
	# Sakoku
	enforce_sakoku_law = {
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 5
			NOT = { government_rank = 6 }
			NOT = { has_country_modifier = sakoku_law }
			NOT = { has_country_flag = sakoku_law }
			has_country_modifier = "western_trade"
		}
		allow = {
			stability = 3
			num_of_cities = 14
			legitimacy = 75
			adm_power = 200
			mil_power = 200
			dip_power = 200
		}
		effect = {
			every_country = {
				limit = { culture_group = japanese }
				remove_country_modifier = "western_trade"
				add_country_modifier = {
					name = "sakoku_law"
					duration = -1
				}
				set_country_flag = sakoku_law
			}
			set_global_flag = japan_is_closed
			add_adm_power = -200
			add_dip_power = -200
			add_mil_power = -200
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				has_country_modifier = western_trade
				NOT = {
					any_owned_province = {
						religion = catholic
						has_missionary = no
					}
				}
			}
		}
	}

	send_western_embassy_mission = {
		potential = {
			always = no
			NOT = {
				has_global_flag = japan_is_closed
				has_country_modifier = western_embassy_mission
			}
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }
			has_global_flag = japan_has_encountered_the_west
			any_known_country = {
				technology_group = western
			}
		}
	
		allow = {
			treasury = 200
			dip_power = 50
		}
	
		effect = {
			add_country_modifier = {
				name = western_embassy_mission
				duration = 3650
			}
			add_treasury = -200
			add_dip_power = -50
		}
	}
	
	hire_ninjas = {
		potential = {
			always = no
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = ninjas }
			advisor = spymaster
		}
	
		allow = {
			treasury = 100
		}
	
		effect = {
			add_country_modifier = {
				name = ninjas
				duration = 1825
			}
			add_treasury = -100
		}
	}

	reform_japanese_government = {
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 6
			NOT = {
				any_country = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 6 }
				}
			}
			NOT = { has_country_modifier = "reformer_emperor" }
		}
		allow = {
			stability = 2
			innovativeness_ideas = 4
		}
		effect = {
			add_stability = -6
			change_government = feudal_monarchy
			set_government_rank = 6
			add_country_modifier = {
				name = "reformer_emperor"
				duration = 7300
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	grab_shogunate = {
		major = yes
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }
			NOT = {
				any_country = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 6 }
					government_rank = 5
				}
			}
		}
		allow = {
			is_subject = no
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 20
				region = japan_region
				is_core = ROOT
			}
		}
		effect = {
			set_government_rank = 5
		}
		ai_will_do = {
			factor = 1
		}
	}

	take_over_shogunate = {
		major = yes
		potential = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			any_country = {
				OR = {
					government = early_medieval_japanese_gov
					government = late_medieval_japanese_gov
					government = early_modern_japanese_gov
				}
				government_rank = 5
				NOT = { government_rank = 6 }
				NOT = {
					num_of_owned_provinces_with = {
						value = 6
						region = japan_region
					}
				}
			}
		}
		allow = {
			OR = {
				is_subject = no
				overlord = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					government_rank = 5
					NOT = { government_rank = 6 }
				}
			}
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 20
				region = japan_region
				is_core = ROOT
			}
		}
		effect = {
			every_country = {
				limit = {
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 6 }
					government_rank = 5
					NOT = {
						num_of_owned_provinces_with = {
							value = 6
							region = japan_region
						}
					}
				}
				country_event = {
					id = japan.3
					days = 5
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

}
