#40 - Carabinier Cavalry

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 6
defensive_morale = 4
offensive_fire = 5
defensive_fire = 3
offensive_shock = 4
defensive_shock = 4

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { region = central_africa_region }
		NOT = { region = kongo_region }
		NOT = { region = south_africa_region }
		NOT = { region = east_africa_region }
		}
	}
