#7 - Western Free Company Infantry

type = infantry
unit_type = western
maneuver = 1

offensive_morale = 2
defensive_morale = 0
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 4

trigger = { 
	OR = {
		government = republic
		any_owned_province = { region = italy_region }
		}
	NOT = { culture_group = latin } #Genoese Balestrieri
	}

