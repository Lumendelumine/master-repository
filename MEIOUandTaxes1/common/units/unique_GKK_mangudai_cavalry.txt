#6 - Heavy Swarm Cavalry - Mangudai Cavalry

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 1
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4 #BONUS
defensive_shock = 3

trigger = {
	culture_group = altaic
	}