#4 - Cog - Troop Transport
type = transport

hull_size = 14 #140 tonnage
base_cannons = 4 #4 guns
sail_speed = 4 #4-5 knots in a good wind

sprite_level = 1

trigger = { 
	OR = {
		technology_group = chinese
		technology_group = austranesian
		}
	}
	