# Tumandar Cavalry (16)

unit_type = indian
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 4
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 4

trigger = { religion_group = muslim }
