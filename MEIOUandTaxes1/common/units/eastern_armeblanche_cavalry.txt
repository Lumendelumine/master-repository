#Arme Blanche Cavalry (40)

type = cavalry
unit_type = eastern
maneuver = 2

offensive_morale = 8
defensive_morale = 4
offensive_fire = 1
defensive_fire = 1
offensive_shock = 7
defensive_shock = 5
