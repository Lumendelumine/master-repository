#4 - Cog - Morskiye Koch
type = transport

hull_size = 14 #140 tonnage
base_cannons = 4 #4 guns
sail_speed = 4 #4-5 knots in a good wind
trade_power = 0.5

sprite_level = 1

trigger = { 
	technology_group = eastern
	}
	