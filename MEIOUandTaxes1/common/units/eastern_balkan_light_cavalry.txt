#Medium Cavalry
#tech level 13

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 3
