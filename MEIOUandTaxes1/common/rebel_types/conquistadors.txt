spanish_conquistadors = { #The Conquistadors acted without state oversight in many cases, will help fell the Inca and the Aztecs quickly.

	color = { 179 100 100 }
	area = nation
	government = any
	defection = none
	independence = none
	defect_delay = 24
	gfx_type = westerngfx
	
	resilient = yes
	reinforcing = yes
	general = yes
	smart = yes
	unit_transfer = yes				# Units remain after enforcing demands
	will_relocate = yes				# Units will relocate if they have sieged all provinces nearby and has to move over sea.
	
	artillery = 0.1
	infantry = 0.6
	cavalry = 0.3
	morale = 1.25 

	# Possible handle actions
	handle_action_negotiate = no
	handle_action_stability = yes
	handle_action_build_core = yes
	handle_action_send_missionary = no
	
	# The rebel type with the highest modifier for this province gets picked
	spawn_chance = {
		factor = 1
		modifier = {
			OR = {
				is_core = SPA
				is_core = CAS
				}
			factor = 3
			}
		modifier = {
			owner = {
				OR = {
					technology_group = mesoamerican
					technology_group = south_american
				}
			}	
			factor = 4
			}
		modifier = {
			OR = {
				region = mexico_region
				region = central_america_region
				region = peru_region
			}
			factor = 2
			}
		modifier = {
			owner = {
				technology_group = western
			}	
			factor = 0
		}
		modifier = {	
			is_year = 1700
			factor = 0
		}
		modifier = {
			NOT = {
				any_neighbor_province = {
					OR = {
						owned_by = SPA
						owned_by = CAS
						owner = {
							is_subject_of = SPA
						}
						owner = {
							is_subject_of = CAS
						}
					}
				}
			}
			factor = 0
		}
		modifier = {
			NOT = {
				OR = {
					range = SPA
					range = CAS
				}
			}
			factor = 0
		}
		modifier = {
			NOT = {
				OR = {
					exists = SPA
					exists = CAS
				}
			}
			factor = 0
		}
		modifier = {
			has_province_flag = taken_by_spanish_conquistadors
			factor = 0
		}
		modifier = {
			NOT = { OR = { continent = south_america continent = north_america } }
			factor = 0
		}
		modifier = { 
			has_province_modifier = nobles_organizing
			factor = 1.4
		}
	}

	# This is checked for EACH province in the Area of Operations
	movement_evaluation = {
		factor = 1
		modifier = {
			factor = 2
			is_capital = yes
			}
		modifier = {
			factor = 0.1
			units_in_province = 1
		}
		modifier = {
			factor = 0.001
			controlled_by = REB
		}
		modifier = {
			factor = 1.5
			unrest = 2
		}
		modifier = {
			factor = 1.5
			unrest = 4
		}
		modifier = {
			factor = 1.5
			unrest = 6
		}
	}
 
 	# Province Scope
 	siege_won_trigger = {
	}
	siege_won_effect = {
		if = {
			limit = { NOT = { has_province_flag = taken_by_spanish_conquistadors } }
			if = {
				limit = { exists = CAS }
				CAS = { add_treasury = 10 }
				}
			if = {
				limit = { 
					NOT = { exists = CAS }
					exists = SPA
					}
				SPA = { add_treasury = 10 }
				}
			}
		set_province_flag = taken_by_spanish_conquistadors
		}
	
	# Country scope
	can_negotiate_trigger = {
		any_owned_province = { controlled_by = spanish_conquistadors }
	}
	
	# Country scope
	can_enforce_trigger = {
		always = yes
	}
	
	# Localisation for their demands
	demands_description = "spanish_conquistadors_demand"
	
	# Country scope
	demands_enforced_effect = {
		if = { 
			limit = { 
				exists = CAS
				NOT = { tag = CAS }
				}
			every_owned_province = { 
				limit = { controlled_by = spanish_conquistadors }
				cede_province = CAS
				add_core = CAS
				hidden_effect = { CAS = { add_treasury = 25 } }
				}
			}
		if = { 
			limit = { 
				NOT = { exists = CAS }
				exists = SPA
				NOT = { tag = SPA }
				}
			every_owned_province = { 
				limit = { controlled_by = spanish_conquistadors }
				cede_province = SPA
				add_core = SPA
				hidden_effect = { SPA = { add_treasury = 25 } }
				}
			}
		}		
}