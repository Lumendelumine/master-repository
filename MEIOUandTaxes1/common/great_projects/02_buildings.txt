forbidden_city_kaifeng = {
	ambient_object = forbidden_city_kaifeng
	province = 701
	time = 120
	
	modifier = {
		stability_cost_modifier = -0.1
		prestige = 1
		legitimacy = 0.1
	}
}

forbidden_city_beijing = {
	ambient_object = forbidden_city_beijing
	province = 708
	time = 120
	
	modifier = {
		stability_cost_modifier = -0.1
		prestige = 1
		legitimacy = 0.1
	}
}

forbidden_city_nanjing = {
	ambient_object = forbidden_city_nanjing
	province = 2150
	time = 120
	
	modifier = {
		stability_cost_modifier = -0.1
		prestige = 1
		legitimacy = 0.1
	}
}

forbidden_city_xian = {
	ambient_object = forbidden_city_xian
	province = 2252
	time = 120
	
	modifier = {
		stability_cost_modifier = -0.1
		prestige = 1
		legitimacy = 0.1
	}
}

forbidden_city_guangzhou = {
	ambient_object = forbidden_city_guangzhou
	province = 2121
	time = 120
	
	modifier = {
		stability_cost_modifier = -0.1
		prestige = 1
		legitimacy = 0.1
	}
}

hagia_sophia_minarets = {
	ambient_object = nammi2_hagia_sophia_minarets_entity
	province = 1402
	time = 120
	
	modifier = {
		stability_cost_modifier = -0.1
		prestige = 1
		legitimacy = 0.1
	}
}
