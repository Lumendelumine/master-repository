# Do not change tags in here without changing every other reference to them.
# Do not change tags in here without changing every other reference to them.
# If adding new groups or ideas, make sure they are unique.

# ai_will do does not actually determine weighting, but AI will not pick an idea with ai_will_do = 0
# ROOT = country picking the idea
# groups set to colonial=yes will be higher prioritized by the AI when spending power

#Index
#ADM ideas (13)						#DIP ideas (13)						#MIL ideas (12)
#Administrative						#Aristocracy 						#Grand Army 
#Asceticism							#Culture 							#Grand Fleet
#Ceremony							#Democracy 							#Leadership 
#Economy							#Diplomacy 							#Logistics 
#Empire								#Eminence 							#Mercenary 
#Engineering						#Espionage 							#Naval 
#Expansion							#Exploration 						#Naval Leadership
#Global Empire						#Free Trade 						#Naval Quality
#Humanism							#Influence 							#Professional Army
#Innovative							#Mercantilism 						#Quality
#Popular Religion					#Merchant Marine 					#Quantity
#Religious Ideas (only ai)			#Plutocracy 						#Standing Army
#Scholasticism						#Trade
#State Religion (theology)

administrative_ideas = {
	category = ADM
	
	bonus = {
		inflation_reduction = 0.05
	}
	
	adaptability = {
		core_creation = -0.15
	}
	debt_and_loans = {
		interest = -3.00
	}
	national_bank = {
		inflation_action_cost = -0.25
	}
	martial_administration = {
		land_maintenance_modifier = -0.05
		naval_maintenance_modifier = -0.05
	}
	centralization = {
		global_autonomy = -0.06
	}
	civil_servants = {
		adm_tech_cost_modifier = -0.10
	}
	corvee = {
		development_cost = -0.05
	}
		
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1.5
			inflation = 5
		}
		modifier = {
			factor = 1.5
			inflation = 10
		}	
	}
}

aristocracy_ideas = {
	category = DIP

	trigger = {
		OR = {
			government = monarchy
			government = noble_republic
			government = theocratic_government
			government = monastic_order_government
			government = papal_government
		}
		NOT = { has_idea_group = plutocracy_ideas }
	}

	bonus = {
		free_leader_pool = 1
	}
	
	noble_knights = {
		cavalry_cost = -0.15
		cavalry_power = 0.10
	}
	local_nobility = {
		enemy_core_creation = 0.20
		vassal_income = 0.10
	}
	serfdom = {
		global_autonomy = -0.06
	}
	noble_officers = {
		leader_land_shock = 1
	}
	international_nobility = {
		diplomats = 1
	}
	noble_resilience = {
		war_exhaustion_cost = -0.20
	}
	military_traditions = {
		mil_tech_cost_modifier  = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

asceticism_ideas = { #DG
	category = ADM
	
	trigger = {
		OR = {
			religion = catholic
			religion = coptic
			religion = orthodox
			religion = gnostic
			religion = hinduism
			religion = vajrayana
			religion = mahayana
			religion = buddhism
			religion = jain
			}
		}
	
	bonus = {
		diplomatic_reputation = 1
	}
	
	devotion = { 
		tolerance_own = 1
	}
	charitable_work = { 
		global_unrest = -1
	}
	fraternities = { 
		production_efficiency = 0.10
	}
	missionary_work = { 
		missionaries = 1
	}
	religious_settlements = { 
		prestige = 1
	}
	monastic_diversity = {
		global_missionary_strength = 0.02
	}
	interfaith_dialogue = {
		tolerance_heretic = 2
	}
	
	ai_will_do = {
		factor = 0.7
		modifier = {
			piety = 0.60
			factor = 1.4
			}
		modifier = {
			NOT = { piety = -0.20 }
			government = monarchy
			}
		modifier = {
			NOT = { piety = -0.60 }
			factor = 0
			}
		modifier = {
			NOT = { piety = 0.20 }
			government = republic
			factor = 0
			}
		modifier = { 
			NOT = { religious_unity = 50 }
			factor = 1.4
			}
		modifier = { 
			NOT = { religious_unity = 80 }
			factor = 1.4
			}
		modifier = { 
			NOT = { religious_unity = 60 }
			factor = 1.4
			}
		modifier = {
			government = republic
			factor = 0.5
			}
		modifier = {
			government = theocracy
			factor = 2
			}
		}
}

ceremony_ideas = { #DG
	category = ADM
	
	trigger = {
		NOT = { 
			OR = {
				religion = catholic
				religion = coptic
				religion = orthodox
				religion = gnostic
				religion = hinduism
				religion = vajrayana
				religion = mahayana
				religion = buddhism
				religion = jain
				}
			}
		}
	
	bonus = {
		prestige = 1
	}
	
	holy_warriors = {
		manpower_recovery_speed = 0.05
		infantry_cost = -0.10
	}
	augury = {
		land_morale = 0.10		
	}
	monuments = {
		prestige_decay = -0.01
		global_autonomy = -0.05
	}
	sacrifices = {
		global_unrest = -1
	}
	grand_ceremony = {
		legitimacy = 0.50
		republican_tradition = 0.0025
		devotion = 1
	}
	prophecy = {
		tolerance_own = 1
	}
	divine_intercession = {
		stability_cost_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 0.7
		modifier = {
			NOT = { piety = -0.20 }
			government = monarchy
			}
		modifier = {
			NOT = { piety = -0.60 }
			factor = 0
			}
		modifier = {
			NOT = { piety = 0.20 }
			government = republic
			factor = 0
			}
		modifier = {
			piety = 0.60
			factor = 1.4
			}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.7
			}
		modifier = {
			government = republic
			factor = 0.5
			}
		modifier = {
			government = theocracy
			factor = 2
			}
	}
}

culture_ideas = {
	category = DIP

	bonus = {
		technology_cost = -0.05
	}
	
	patron_of_art  = {
		prestige_decay = -0.01
	}
	mecenas = { 
		prestige = 1
	}
	luxury_tax  = { 
		global_tax_modifier = 0.10
	}
	prestigious_court  = { 
		advisor_cost = -0.20
	}
	educated_ruler  = { 
		legitimacy = 0.25
		republican_tradition = 0.0025
		devotion = 0.25
	}
	great_court  = { 
		advisor_pool = 1
	}
	scientific_encouragement  = { 
		idea_cost = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

democracy_ideas = {
	category = DIP

	trigger = {
		OR = { 
			has_idea = dynamic_court 
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
		NOT = { government = absolute_monarchy }
		NOT = { government = despotic_monarchy }
		adm_tech = 20
	}
	
	bonus = {
		years_of_nationalism = -15
	}

	freedom_fighters = {
		infantry_power = 0.10
	}
	regulated_taxes = {
		global_tax_modifier = 0.10
	}	
	bill_of_rights = { 
		global_unrest = -2
	}
	democratic_production = {
		production_efficiency = 0.10
	}
	popular_militias = {
		manpower_recovery_speed = 0.10
	}
	a_leader_for_liberty = {
		leader_land_fire = 1
	}
	right_of_vote  = { 
		legitimacy = 0.25
		republican_tradition = 0.01
		devotion = 0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

diplomatic_ideas = {
	category = DIP
	
	bonus = {
		reduced_stab_impacts = yes
		diplomatic_annexation_cost = -0.25
	}

	foreign_embassies = {
		diplomats = 1
	}
	cabinet = {
		diplomatic_upkeep = 2
	}
	war_cabinet = {
		war_exhaustion_cost = -0.25
	}
	benign_diplomats = {
		improve_relation_modifier = 0.25
	}
	postal_service = {
		envoy_travel_time = -0.25
	}		
	flexible_negotiation = {
		unjustified_demands = -0.25
		ae_impact = -0.1
	}
	diplomatic_corps = {
		dip_tech_cost_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

economic_ideas = {
	category = ADM

	bonus = {
		global_trade_goods_size_modifier = 0.10
	}	
	
	bureaucracy = {
		global_tax_modifier = 0.10
	}
	national_fairs = {
		global_trade_power = 0.10
	}
	trade_councils = {
		trade_efficiency = 0.10
	}
	secure_trade_routes = {
		global_prov_trade_power_modifier = 0.10
	}
	burgher_rights = {
		build_cost = -0.05
	}
	smithian_economics = {
		production_efficiency = 0.10
 	}
	real_economy = {
		adm_tech_cost_modifier = -0.05
	}
	
	ai_will_do = {
		factor = 1		
	}
}

eminence_ideas = {
	category = DIP
	
	trigger = {
		OR = {
			has_idea = benign_diplomats
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
	}
	
	bonus = {
		province_warscore_cost = -0.20
	}

	additional_diplomats = {
		diplomats = 1
	}
	additional_relations = { 
		diplomatic_upkeep = 2
	}
	friendly_relations = {
		relations_decay_of_me = 0.30
	}
	fair_cause = {
		ae_impact = -0.10
	}
	experienced_diplomats  = {
		diplomatic_reputation = 3
	}		
	imperial_ambitions_emi = {
		imperial_authority = 0.10
		prestige = 0.5
	}
	diplomatic_studies = {
		dip_tech_cost_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

empire_ideas = {
	category = ADM

	trigger = {
		OR = {
			has_idea = martial_administration
			OR = { 
				has_idea = noble_officers
				has_idea = benign_diplomats
			}
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}	
	}
	
	bonus = {
		core_creation = -0.15
	}
	
	magistrates = {
		development_cost = -0.05		#DO Modified
	}	
	ministry_of_war = {
		war_exhaustion_cost = -0.25
	}
	liege_and_vassal = {
		vassal_income = 0.15
	}
	imperial_governors = {
		global_autonomy = -0.06
	}
	i_am_the_emperor = {
		imperial_authority = 0.10
		prestige = 0.5
	}
	military_fealty = {
		vassal_forcelimit_bonus = 0.25
	}	
	greatness = {
		stability_cost_modifier = -0.15
	}	
	
	ai_will_do = {
		factor = 1
	}
}

engineering_ideas = {
	category = ADM

	trigger = {
		OR = {
			has_idea = secure_trade_routes
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
	}
	
	bonus = {
		hostile_attrition = 1
	}	
	
	organised_construction = {
		build_cost = -0.15
	}
	unified_construction_plans = {
		development_cost = -0.075
	}
	multi-role_artillery = {
		siege_ability = 0.20
	}
	engineer_advisor = {
		leader_siege = 1
	}
	new_fortification_techniques = {
		defensiveness = 0.2
	}
	improved_gunpowder = {
		artillery_power = 0.10
		artillery_cost = -0.10
 	}
	architecture_studies = {
		adm_tech_cost_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 1		
	}
}

spy_ideas = {
	category = DIP

	bonus = {
		rebel_support_efficiency = 0.50
	}
	
	claim_fabrication = { 
		fabricate_claims_time = -0.33
	}
	vetting = {
		global_spy_defence = 0.25
	}
	rumourmongering = {
		may_sabotage_reputation = yes
	}
	efficient_spies = {
		spy_offence = 0.25
		diplomats = 1
	}
	shady_recruitment = {
		discovered_relations_impact = -0.33
		may_study_technology = yes
	}
	destabilising_efforts = {
		may_sow_discontent = yes
		may_agitate_for_liberty = yes
	}
	espionage  = {
		may_infiltrate_administration = yes
	}
	
	ai_will_do = {
		factor = 1
		
		# AI generally should not pick this
		modifier = {
			factor = 0.1
			always = yes
		}
	}
}

expansion_ideas = {
	category = ADM
	important = yes

	trigger = {
		OR = {
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
			AND = {
				has_idea = martial_administration
				ai = no
			}
			AND = {
				has_idea_group = administrative_ideas
				ai = yes
			}
		}
	}
	
	bonus = {
 		cb_on_primitives = yes
	}

	additional_colonists = {
		colonists = 1
	}
	native_scouts = {
		auto_explore_adjacent_to_colony = yes
	}
	land_of_opportunity = {
		global_colonial_growth = 20
	}
	private_colony_charters = {
		colonist_placement_chance = 0.05
	}
	free_colonies = {
		colonists = 1
	}
	tough_colonists = { 
		land_attrition = -0.15
	}	
	improved_communications = {
		envoy_travel_time = -0.25
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
		}
	}
}

exploration_ideas = {
	category = DIP
	important = yes

	trigger = {
		num_of_ports = 1
		OR = {
			religion_group = christian
			ai = no
		}
		OR = {
			has_idea = sheltered_ports
			ai = yes
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
		OR = {
			AND = {
				OR = {
					tag = FRA
					tag = ENG
					tag = POR
					tag = GBR
					tag = NED
					culture_group = iberian
				}
				dip_tech = 14
				adm_tech = 16
				ai = yes
			}
			AND = {
				OR = {
					tag = ENG
					tag = FRA
				}
				NOT = { has_global_flag = hundred_year_war }
				dip_tech = 14
				adm_tech = 16
			}
			AND = {
				OR = {
					tag = POR
					tag = GBR
					tag = NED
				}
				dip_tech = 14
				adm_tech = 16
			}
			AND = {
				culture_group = iberian
				any_province = {
					iberia_region_trigger = yes
					NOT = { owner = { religion_group = muslim } }
				}
				dip_tech = 14
				adm_tech = 16
			}
			dip_tech = 20
		}
	}

	bonus = {
		cb_on_overseas = yes
	}
	
	quest_for_the_new_world	= {
		may_explore = yes
	}
	colonial_ventures = {
		colonists = 1
	}
	overseas_exploration = {
		range = 0.50
		auto_explore_adjacent_to_colony = yes
	}	
	faster_colonists = { 
		global_colonial_growth = 10
	}
	exploration_navies  = {
		global_ship_recruit_speed = -0.10
	}
	naval_expeditions = { 
		naval_attrition = -0.25
	}
	global_empire = {
		naval_forcelimit_modifier = 0.25
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { is_colonial_nation = yes }
		}		
		modifier = {
			factor = 0.1
			NOT = { num_of_ports = 5 }
		}				
		modifier = {
			factor = 0.1
			NOT = { technology_group = western }
		}			
		modifier = {
			factor = 0.1
			capital_scope = {
				NOT = {
					region = france_region
					region = provence_region
					region = languedoc_region
					region = brittany_region
					region = high_countries_region
					region = aragon_region
					region = castille_region
					region = leon_region
					region = andalucia_region
					region = portugal_region
					region = england_region
					region = scotland_region
					region = ireland_region
					region = belgii_region
					region = low_countries_region
				}
			}
		}
		modifier = {
			factor = 2.0
			num_of_ports = 10		
		}			
		modifier = {
			factor = 2.0
			num_of_ports = 15		
		}	
		modifier = {
			factor = 2.0
			num_of_ports = 20	
		}		
	}
}

free_trade_ideas = {
	category = DIP
	
	trigger = {
		OR = { 
			has_idea = national_trade_policy
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
		NOT = { has_idea_group = mercantilism_ideas }
	}
	
	bonus = {
		trade_efficiency = 0.05
	}
	
	aggressive_traders = {
		justify_trade_conflict_time = -0.25
	}
	overseas_merchants = {
		merchants = 1
	}
	web_of_trade_contracts  = { 
		trade_range_modifier = 0.33
	}
	targeted_imports = {
		global_foreign_trade_power = 0.10
	}
	supply_meets_demand = {
		trade_efficiency = 0.10
	}
  	joint_stock_companies = {
  		trade_steering = 0.25
		caravan_power = 0.25
  	}
	exchange_of_customs = {
		dip_tech_cost_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

global_empire_ideas = {
	category = ADM

	trigger = {
		OR = { 
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
			AND = { 
				has_idea = martial_administration
				has_idea = faster_colonists
			}
		}
	}
	
	bonus = {
		stability_cost_modifier = -0.15
		cb_on_primitives = yes
	}
	
	exotic_goods = {
		global_unrest = -2.00
	}	
	colonial_investments = {
		colonist_placement_chance = 0.05
	}
	penal_colonies = {
		global_colonial_growth = 20
	}
	promise_of_the_new_world = {
		colonists = 1
	}
	vice_roys  = {
 		global_tariffs = 0.20
 	}	
	national_arsenal = {
		naval_maintenance_modifier = -0.10
	}
	colonial_race = {
		colonists = 1
	}	
	
	ai_will_do = {
		factor = 1
	}
}

grand_army_ideas = {
	category = MIL
	
	trigger = {
		OR = {
			AND = {
				has_idea = levy_system
				has_idea = standardised_calibres
			}
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
	}
	
	bonus = {
		global_regiment_cost = -0.15
	}

	organised_recruiting = {
		global_regiment_recruit_speed = -0.10
	}
	the_young_can_serve = { 
		manpower_recovery_speed = 0.20
	}
	military_enthusiasm = {
		global_manpower_modifier = 0.50
	}
	the_old_and_infirm = { 
		land_maintenance_modifier = -0.05
	}
	quick_reinforcements = {
		reinforce_speed = 0.15
	}
	grand_army = { 
		land_forcelimit_modifier = 0.50
	}
	swarm_tactics = {
		mil_tech_cost_modifier  = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

grand_fleet_ideas = {
	category = MIL
	
	trigger = {
		OR = {
			has_idea = sheltered_ports
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
		num_of_ports = 1
	}
	
	bonus = {
		may_explore = yes
		naval_maintenance_modifier = -0.10
	}
	
	naval_standardization = {
		galley_cost = -0.20
		transport_cost = -0.10
	}
	hull_designs = {
		heavy_ship_cost = -0.20
		light_ship_cost = -0.10
	}
	auxiliary_vessels = {
		blockade_efficiency = 0.50
	}
	grand_navy = { 
		naval_forcelimit_modifier = 0.50
		global_manpower_modifier = 0.15
	}
	ships_penny = { 
		global_ship_cost = -0.25
		global_ship_recruit_speed = -0.10
	}
	naval_glory = { 
		prestige_from_naval = 1.00
	}
	escort_ships = { 
		naval_morale = 0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

humanist_ideas = {
	category = ADM
	
	bonus = {
		idea_cost = -0.1
	}
	
	tolerance_idea = {
		religious_unity = 0.25
	}	
	local_traditions = {
		global_unrest = -2
	}
	ecumenism_hi = {
		tolerance_heretic = 1
	}	
	indirect_rule = {
		years_of_nationalism = -15
	}
	cultural_ties = {
		accepted_culture_threshold = -0.33
	}
	benevolence = {
		relations_decay_of_me = 0.33
	}	
	humanist_tolerance = {
		tolerance_heathen = 1
	}		
	
	ai_will_do = {
		factor = 1
	}
}

influence_ideas = {
	category = DIP

	trigger = {
		OR = {
			has_idea = benign_diplomats
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
	}
	
	bonus = {
		diplomatic_upkeep = 2
	}

	influent_beyond_our_borders = {
		diplomats = 1
	}
	tribute_system = {
		vassal_income = 0.25
	}
	integrated_elites = {
		diplomatic_annexation_cost = -0.25
	}
	state_propaganda = {
		unjustified_demands = -0.50
	}
	diplomatic_influence = {
		diplomatic_reputation = 3
	}
	marcher_lords = {
		vassal_forcelimit_bonus = 0.25
	}	
	the_art_of_influence = {
		dip_tech_cost_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}	
}

innovativeness_ideas = {
	category = ADM

	trigger = {
		adm_tech = 20
	}	
	
	bonus = {
		advisor_cost = -0.20
	}
	
	emancipation = { 
		manpower_recovery_speed = 0.10
	}
	abolished_serfdom = { 
		land_morale = 0.10
	}
	scientific_revolution = {
		technology_cost = -0.05
	}
	dynamic_court = {
		advisor_pool = 1
	}
	free_subjects = { 
		global_trade_goods_size_modifier = 0.10
	}
	optimism = {
		war_exhaustion = -0.03
	}
	formalized_officer_corps = {
		free_leader_pool = 1
	}
	
	ai_will_do = {
		factor = 1
	}
}

leadership_ideas = {
	category = MIL

	bonus = {
		discipline = 0.05
	}

	combat_leaders = {
		leader_land_shock = 1
	}
	combat_maneuver = {
		leader_land_manuever = 1
	}
	superior_firepower = {
		leader_land_fire = 1
	}
	siege_master = {
		leader_siege = 1
	}
	glorious_arms = {
		prestige_from_land = 1.00
	}
	leadership = {
		recover_army_morale_speed = 0.05
	}	
	militar_culture = {
		army_tradition = 0.5
		army_tradition_decay = -0.01
	}
	
	ai_will_do = {
		factor = 1
	}
}

logistic_ideas = {
	category = MIL

	bonus = {
		hostile_attrition = 1
	}

	city_guards = {
		global_garrison_growth = 0.20
	}	
	improved_manuever = {
		leader_land_manuever = 1
	}
	engineer_corps = {
		siege_ability = 0.20
	}
	regimental_system = {
		land_maintenance_modifier = -0.05
	}	
	defensive_mentality = {
		defensiveness = 0.2
	}
	supply_trains = {
		reinforce_speed = 0.15
	}
	improved_foraging = {
		land_attrition = -0.15
	}
	
	ai_will_do = {
		factor = 1
	}
}

mercantilism_ideas = {
	category = DIP
	
	trigger = {
		NOT = { has_idea_group = free_trade_ideas }
		OR = {
			has_idea = national_trade_policy
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
	}
	
	bonus = {
		stability_cost_modifier = -0.1
	}
	
	protectionism = {
		embargo_efficiency = 0.25
	}
	economic_advisor = { 
		merchants = 1
	}
	market_day  = { 
		global_prov_trade_power_modifier = 0.10
	}
	controlled_fairs  = {
		global_own_trade_power = 0.10
	}
	staple_right = {
		trade_efficiency = 0.10
	}
  	subsidized_economy = {
  		global_trade_goods_size_modifier = 0.10
  	}
	trade_practice_regulations = {
		dip_tech_cost_modifier = -0.10
		global_tariffs = 0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

mercenary_ideas = {
	category = MIL
	
	trigger = {
		NOT = { has_idea_group = standing_army_ideas }
	}
	
	bonus = {
		free_leader_pool = 1
	}
	
	organised_mercenary_payment = {
		mercenary_cost = -0.25
	}	
	dogs_of_war = {
		loot_amount = 0.2
	}
	benefits_for_mercenaries = { 
		merc_maintenance_modifier = -0.5
	}
	specialized_training = {
		discipline = 0.05
	}
	organised_mercenary_recruitment = { 
		possible_mercenaries = 0.50
	}
	mercenary_levies = {
		reinforce_speed = 0.15
	}
	mercenary_calm = { 
		war_exhaustion = -0.04
	}

	ai_will_do = {
		factor = 1
	}
}

merchant_marine_ideas = {
	category = DIP
	
	trigger = {
		OR = {
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
			AND = {
				has_idea = sheltered_ports
				has_idea = national_trade_policy
				num_of_ports = 1
			}
		}
	}
	
	bonus = {
		naval_morale = 0.10
		may_explore = yes
	}
	
	private_shipbuilding_incentives = {
		light_ship_cost = -0.20
		transport_cost = -0.10
	}
	fore-and-aft_rigged_vessels = {
		blockade_efficiency = 0.25
		embargo_efficiency = 0.25
	}
	privateers  = { 
		privateer_efficiency = 0.33
		trade_steering = 0.125
	}
	skeleton-based_shipbuilding  = {
		global_ship_recruit_speed = -0.10
	}
	experienced_navigators = {
		leader_naval_manuever = 1
	}
  	transportation_companies = {
  		merchants = 1
  	}
	convoy = {
		light_ship_power = 0.20
		transport_power = 0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

naval_ideas = {
	category = MIL
	
	trigger = {
		num_of_ports = 1
	}
	
	bonus = {
		sea_repair = yes
	}
	
	sea_hawks = { 
		navy_tradition = 0.5
		navy_tradition_decay = -0.01
	}
	naval_fighting_instruction = { 
		blockade_efficiency = 0.25
	}
	improved_shipyards  = { 
		global_ship_recruit_speed = -0.10
	}
	sheltered_ports = { 
		global_ship_repair = 0.15
		global_manpower_modifier = 0.075
	}
	press_gangs = { 
		naval_maintenance_modifier = -0.05
	}
	naval_cadets = {
		naval_forcelimit_modifier = 0.15
	}
	boarding_parties = { 
		naval_morale = 0.15
	}
	
	ai_will_do = {
		factor = 1
	}
}

naval_leadership_ideas = {
	category = MIL
	
	trigger = {
		OR = {
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
			has_idea = sheltered_ports
		}
		num_of_ports = 1
	}
	
	bonus = {
		global_ship_repair = 0.30
		may_explore = yes
	}

	improved_charting_techniques = {
		leader_naval_manuever = 1
	}	
	grapeshot_supported_boarding = { 
		leader_naval_shock = 2
	}
	chained_cannonballs = { 
		leader_naval_fire = 2
	}
	flagships = { 
		prestige_from_naval = 1
		free_leader_pool = 1
	}
	a_leader_to_follow = {
		naval_morale = 0.20
	}
	shanties = { 
		recover_navy_morale_speed = 0.05
	}
	naval_culture = { 
		navy_tradition = 1
		navy_tradition_decay = -0.02
	}
	
	ai_will_do = {
		factor = 1
	}
}

naval_quality_ideas = {
	category = MIL
	
	trigger = {
		OR = {
			has_idea = sheltered_ports
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
		}
		num_of_ports = 1
	}
	
	bonus = {
		free_leader_pool = 1
		may_explore = yes
	}
	
	good_quality_wood = { 
		ship_durability = 0.10
	}
	naval_drill = { 
		recover_navy_morale_speed = 0.05
	}
	superior_seamanship = { 
		naval_morale = 0.20
		naval_maintenance_modifier = -0.10
	}
	excellent_shipwrights = { 
		leader_naval_manuever = 1
	}
	improved_rams = {
		galley_power = 0.20
		transport_power = 0.10
	}
	oak_forests_for_ships = {
		heavy_ship_power = 0.20
		light_ship_power = 0.10
	}
	copper_bottoms = {
		naval_attrition = -0.25
	}
	
	ai_will_do = {
		factor = 1
	}
}

popular_religion_ideas = { #DG
	category = ADM
	
	bonus = {
		tolerance_own = 1
	}
	
	religious_art = {
		prestige = 1
	}
	pilgrimage = {
		global_missionary_strength = 0.005
		merchants = 1
	}
	reliquaries = {
		legitimacy = 0.50
		republican_tradition = 0.0025
		devotion = 1
	}
	faith_healing = {
		global_unrest = -1
	}
	evangelism = {
		missionaries = 1
	}
	flexible_theology = {
		tolerance_heathen = 1
	}
	messiahs = {
		war_exhaustion = -0.025
	}
	
	ai_will_do = {
		factor = 0.7
		modifier = {
			NOT = { piety = -0.20 }
			government = monarchy
			}
		modifier = {
			NOT = { piety = -0.60 }
			factor = 0
			}
		modifier = {
			NOT = { piety = 0.20 }
			government = republic
			factor = 0
			}
		modifier = {
			piety = 0.60
			factor = 1.4
			}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.7
			}
		modifier = {
			NOT = { religious_unity = 100 }
			factor = 1.4
			}
		modifier = {
			NOT = { religious_unity = 80 }
			factor = 1.4
			}
		modifier = {
			government = republic
			factor = 0.5
			}
		modifier = {
			government = theocracy
			factor = 2
			}
		}
}

professional_army_ideas = {
	category = MIL
	
	trigger = {
		OR = {
			NOT = { has_global_flag = meiou_and_taxes_mod_start }
			AND = {
				has_idea = levy_system
				has_idea = military_drill
			}
		}
	}
	
	bonus = {
		discipline = 0.05
	}

	military_reserve_force = {
		global_regiment_recruit_speed = -0.10
	}
	napoleonic_warfare = { 
		army_tradition = 1
		army_tradition_decay = -0.02
	}
	war_veterans = {
		land_morale = 0.10
	}
	standardised_equipment = {
		land_maintenance_modifier = -0.05
	}
	conscription_lists = {
		reinforce_speed = 0.15
	}
	formal_military_education = {
		free_leader_pool = 1
	}
	military_treatises = {
		mil_tech_cost_modifier  = -0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

plutocracy_ideas = {
	category = DIP

	trigger = {
		NOT = { government = monarchy }
		NOT = { government = noble_republic }
		NOT = { government = theocratic_government }
		NOT = {	government = monastic_order_government }
		NOT = { government = papal_government }
		NOT = { has_idea_group = aristocracy_ideas }
	}
	
	bonus = {
		republican_tradition = 0.02
	}
	
	tradition_of_payment = {
		possible_mercenaries = 0.50
	}
	pragmatism = { 
		mercenary_cost = -0.25
	}
	extra_advisors = {
		advisor_pool = 1
	}
	free_merchants = {
		merchants = 1
	}
	resilient_state = { 
		inflation_action_cost = -0.25
	}
	free_cities = {
		caravan_power = 0.25
	}
	competetive_merchants  = { 
		global_trade_power = 0.10
	}
	
	ai_will_do = {
		factor = 1
	}
}

quality_ideas = {
	category = MIL

	bonus = {
		discipline = 0.05
	}
	
	private_to_marshal = {
		infantry_power = 0.10
	}
	quality_education = {
		recover_army_morale_speed = 0.05
	}
	finest_of_horses = {
		cavalry_power = 0.10
	}
	military_drill = { 
		land_morale = 0.10
	}
	massed_battery = {
		artillery_power = 0.10
	}
	victorious_army = {
		prestige_from_land = 0.50
	}
	war_college = {
		free_leader_pool = 1
	}
	
	ai_will_do = {
		factor = 1
	}
}


quantity_ideas = {
	category = MIL

	bonus = {
		land_forcelimit_modifier  = 0.25
	}
	
	mass_army = {
		global_manpower_modifier = 0.25
	}
	manufactured_uniforms = {
		infantry_cost = -0.10
	}
	military_horse_breeding = {
		cavalry_cost = -0.10
	}
	standardised_calibres = {
		artillery_cost = -0.10
	}
	enforced_service = {
		global_regiment_cost = -0.10
	}
	military_parades = {
		prestige_from_land = 0.50
	}
	victory_behind_numbers = {
		land_morale = 0.10
	}

	
	ai_will_do = {
		factor = 1
	}
}

scholasticism_ideas = { #DG
	category = ADM
	
	trigger = {
		any_owned_province = { has_building = university }
		}
	
	bonus = {
		adm_tech_cost_modifier = -0.05
	}	
	
	scriptural_tradition = {
		prestige = 0.5
		global_missionary_strength = 0.01
	}
	religious_law = {
		global_autonomy = -0.06
	}
	itinerant_preachers = {
		missionaries = 1
	}
	clerical_teaching = {
		advisor_cost = -0.10
	}
	ecceleistical_judiciary = {
		global_unrest = -1
	}
	religious_advisors = {
		diplomats = 1
	}
	educated_priests = {
		advisor_pool = 1
	}
	
	ai_will_do = {
		factor = 0.7
		modifier = {
			NOT = { piety = -0.20 }
			government = monarchy
			}
		modifier = {
			NOT = { piety = -0.60 }
			factor = 0
			}
		modifier = {
			NOT = { piety = 0.20 }
			government = republic
			factor = 0
			}
		modifier = {
			piety = 0.60
			factor = 1.4
			}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.7
			}
		modifier = {
			government = republic
			factor = 0.5
			}
		modifier = {
			government = theocracy
			factor = 2
			}
	}
}

standing_army_ideas = {
	category = MIL
	
	trigger = {
		OR = {
			NOT = { has_idea_group = mercenary_ideas }
			mil_tech = 27
		}
	}
	
	bonus = {
		reinforce_speed = 0.10
	}
	
	battlefield_commisions = { 
		army_tradition = 0.5
		army_tradition_decay = -0.01
	}
	national_conscripts = { 
		global_regiment_recruit_speed = -0.10
	}
	forced_conscription = { 
		global_manpower_modifier = 0.25
	}
	levy_system = { 
		manpower_recovery_speed = 0.10
	}
	nationalistic_enthusiasm = { 
		land_maintenance_modifier = -0.05
	}
	expanded_supply_trains = { 
		land_attrition = -0.10
	}
	faith_in_victory = { 
		war_exhaustion = -0.03
	}
	
	ai_will_do = {
		factor = 1
	}
}

theology_ideas = { #DG
	category = ADM
	
	bonus = {
		global_missionary_strength = 0.02
	}
	
	religious_traditions = { 
		tolerance_own = 1.5
	}
	crusades = { 
		war_exhaustion = -0.025
		cb_on_religious_enemies = yes
	}
	just_war = {
		manpower_recovery_speed = 0.10
	}
	divine_right = { 
		legitimacy = 0.25
		republican_tradition = 0.001
		devotion = 0.5
		core_creation = -0.10
	}
	chosen_people = {
		prestige = 1
	}
	holy_orders = { 
		land_morale = 0.10
	}
	mandatory_church_attendance = { 
		stability_cost_modifier = -0.10
		global_autonomy = -0.02
	}
	
	ai_will_do = {
		factor = 0.7
		modifier = {
			NOT = { piety = -0.20 }
			government = monarchy
			}
		modifier = {
			NOT = { piety = -0.60 }
			factor = 0
			}
		modifier = {
			NOT = { piety = 0.20 }
			government = republic
			factor = 0
			}
		modifier = {
			piety = 0.60
			factor = 1.4
			}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.7
			}
		modifier = {
			government = theocracy
			factor = 1.4
			}
		modifier = {
			any_neighbor_country = { NOT = { religion_group = ROOT } }
			factor = 1.4
			}
		modifier = {
			at_war_with_religious_enemy = yes
			factor = 1.4
			}
		modifier = {
			government = republic
			factor = 0.5
			}
		modifier = {
			government = theocracy
			factor = 2
			}
		}
}

trade_ideas = {
	category = DIP

	bonus = {
		merchants = 1
	}	
	
	merchant_adventures = {
		trade_range_modifier = 0.30
	}
	free_trade = {
  		merchants = 1
  	}
	shrewd_commerce_practise = {
 		global_trade_power = 0.20
  	}
  	national_trade_policy = {
		trade_efficiency = 0.10
	}
	additional_merchants = { 
		merchants = 1
	}
	trade_manipulation = {
		trade_steering = 0.25
		caravan_power = 0.25
	}
	fast_negotiations = {
		global_foreign_trade_power = 0.10
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { num_of_cities = 6 }
			NOT = { total_base_tax = 50 }
			NOT = { government = merchant_republic }
			NOT = { government = venetian_republic }
		}
	}
}