heavy_ship_trigger = {
	dip_tech = 18
	OR = { 
		technology_group = western
		has_idea_group = grand_fleet_ideas
		AND = { 
			has_idea_group = naval_ideas
			has_country_modifier = western_arms_trade
		}
	}
} 
