bookmark =
{
	name = "ERA_OF_THE_STATE_NAME"
	desc = "ERA_OF_THE_STATE_DESC"
	date = 1530.8.13
	
	country = TUR
	country = PER
	country = MUG
	country = ENG
	country = FRA
	country = SPA
	country = HAB
	country = MOS
	country = POL
	
	easy_country = SPA
	easy_country = FRA
	easy_country = TUR

	effect = {
		#Country 1
		SWE = {
			add_stability = 4
		}
		DAN = {
			add_stability = 4
		}
		TUR = {
			add_stability = 4
		}
		MUG = {
			add_stability = 4
		}
		FRA = {
			add_stability = 4
		}
		MOS = {
			add_stability = 4
		}
		ENG = {
			add_stability = 4
		}
		HAB = {
			add_stability = 4
		}
		AHM = {
			add_stability = 4
		}
		TAU = {
			add_stability = 4
		}
		GUJ = {
			add_stability = 4
		}
		SPA = {
			add_stability = 4
		}
	}
}