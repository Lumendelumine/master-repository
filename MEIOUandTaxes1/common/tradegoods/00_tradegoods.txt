# tradegoods.txt


wheat			= { color = { 0.7 0.7 0.0 } modifier = { land_forcelimit_modifier = 0.1 } chance = { factor = 0 } }

millet			= { color = { 0.8 0.8 0.3 } modifier = { land_forcelimit_modifier = 0.1 }  chance = { factor = 0 } }

rice			= { color = { 0.9 0.9 0.6 } modifier = { land_forcelimit_modifier = 0.1 } chance = { factor = 0 } }

maize			= { color = { 0.0 0.3 0.8 } modifier = { land_forcelimit_modifier = 0.1 } chance = { factor = 0 } }

wine			= { color = { 0.7 0.0 0.0 } modifier = { stability_cost_modifier = -0.25 } chance = { factor = 0 } }

wax				= { color = { 1.0 0.9 0.0 } modifier = { stability_cost_modifier = -0.25 } chance = { factor = 0 } }

wool			= { color = { 0.9 0.8 0.9 } modifier = { land_maintenance_modifier = -0.02 } chance = { factor = 0 } }

subsistence		= { color = { 1.0 0.0 0.0 } chance = { factor = 0 } }

silk			= { color = { 0.8 1.0 1.0 } modifier = { prestige = 1 } chance = { factor = 0 } }

hemp			= { color = { 0.9 0.7 0.7 } modifier = { global_ship_cost = -0.10 } chance = { factor = 0 } }

gold			= { color = { 1.0 0.7 0.5 } chance = { factor = 0 } }

gems			= { color = { 0.6 1.0 1.0 } chance = { factor = 0 } }

iron			= { color = { 0.7 0.7 0.7 } modifier = { global_regiment_cost = -0.10} chance = { factor = 0 } }

copper			= { color = { 0.6 0.2 0.8 } modifier = { defensiveness = 0.10} chance = { factor = 0 } }

lead			= { color = { 0.3 0.3 0.3 } modifier = { global_regiment_cost = -0.10} chance = { factor = 0 } }

naval_supplies	= { color = { 0.8 0.6 0.4 } modifier = { global_ship_cost = -0.10} chance = { factor = 0 } }

fish			= { color = { 0.0 0.5 0.4 } modifier = { global_unrest = -1.0} chance = { factor = 0 } }

fur				= { color = { 0.8 0.5 0.8 } modifier = { prestige = 1.0} chance = { factor = 0 } }

salt			= { color = { 1.0 1.0 1.0 } modifier = { land_maintenance_modifier = -0.10} chance = { factor = 0 } }

slaves			= { color = { 0.3 0.1 0.7 } modifier = { global_tariffs = 0.10} chance = { factor = 0 } }

ivory			= { color = { 0.5 1.0 0.8 } modifier = { diplomatic_reputation = 2.0} chance = { factor = 0 } }

tea				= { color = { 0.5 0.8 0.1 } modifier = { prestige = 1} chance = { factor = 0 } }

chinaware		= { color = { 0.7 0.8 1.0 } modifier = { legitimacy = 0.25} chance = { factor = 0 } }

cinnamon		= { color = { 0.8 0.2 0.8 } modifier = { global_tariffs = 0.05} chance = { factor = 0 } }

clove			= { color = { 0.8 0.4 0.3 } modifier = { global_tariffs = 0.05} chance = { factor = 0 } }

pepper			= { color = { 1.0 0.2 0.2 } modifier = { global_tariffs = 0.05} chance = { factor = 0 } }

coffee			= { color = { 0.5 0.3 0.0 } modifier = { defensiveness = 0.10} chance = { factor = 0 } }

cotton			= { color = { 1.0 0.8 0.8 } modifier = { advisor_cost = -0.33} chance = { factor = 0 } }

sugar			= { color = { 0.2 0.6 0.3 } modifier = { war_exhaustion_cost = -0.10} chance = { factor = 0 } }

tobacco			= { color = { 0.8 0.7 0.8 } modifier = { global_tariffs = 0.10} chance = { factor = 0 } }

opium			= { color = { 0.7 0.6 1.0 } modifier = { global_spy_defence = 0.25} chance = { factor = 0 } }

glassware		= { color = { 0.4 0.4 1.0 } modifier = { global_trade_power = 0.10} chance = { factor = 0 } }

tin				= { color = { 0.8 0.0 0.1 } modifier = { defensiveness = 0.10} chance = { factor = 0 } }

coal			= { color = { 0.2 0.2 0.1 } modifier = { global_regiment_cost = -0.05} chance = { factor = 0 } }

sulphur			= { color = { 0.9 0.5 0.1 } modifier = { global_regiment_cost = -0.05} chance = { factor = 0 } }

sandal			= { color = { 0.3 0.7 0.0 } modifier = { global_trade_power = 0.10} chance = { factor = 0 } }

crops			= { color = { 1.0 0.0 0.0} chance = { factor = 0 } }

cloth			= { color = { 0.1 1.0 1.0 } modifier = { global_ship_cost = -0.10} chance = { factor = 0 } }

jewelery		= { color = { 0.0 0.7 0.7 } modifier = { prestige_decay = -0.01} chance = { factor = 0 } }

cacao			= { color = { 0.6 0.3 0.2 } modifier = { war_exhaustion_cost = -0.10} chance = { factor = 0 } }

cheese			= { color = { 0.9 0.2 0.0 } modifier = { land_maintenance_modifier = -0.05} chance = { factor = 0 } }

rum				= { color = { 0.6 0.7 0.4 } modifier = { global_tariffs = 0.10} chance = { factor = 0 } }

carmine			= { color = { 0.5 0.0 0.7 } modifier = { global_trade_power = 0.10} chance = { factor = 0 } }

steel			= { color = { 1.0 0.5 0.0 } modifier = { prestige = 1.0} chance = { factor = 0 } }

arms			= { color = { 1.0 0.6 0.2 } modifier = { global_regiment_recruit_speed = -0.10} chance = { factor = 0 } }

hardware		= { color = { 0.9 0.4 0.8 } modifier = { global_regiment_cost = -0.05} chance = { factor = 0 } }

ammunitions		= { color = { 0.7 0.2 0.9 } modifier = { global_regiment_recruit_speed = -0.10} chance = { factor = 0 } }

lumber			= { color = { 0.4 0.2 0.0 } modifier = { development_cost = -0.05} chance = { factor = 0 } }

furniture		= { color = { 0.6 0.0 0.0 } modifier = { prestige = 1.0} chance = { factor = 0 } }

livestock		= { color = { 0.2 1.0 0.7 } modifier = { land_maintenance_modifier = -0.05} chance = { factor = 0 } }

marble			= { color = { 0.3 0.0 0.0 } modifier = { development_cost = -0.05} chance = { factor = 0 } }

beer			= { color = { 0.7 0.4 0.4 } modifier = { stability_cost_modifier = -0.25} chance = { factor = 0 } }

olive			= { color = { 0.4 0.7 0.0 } modifier = { stability_cost_modifier = -0.25} chance = { factor = 0 } }

linen			= { color = { 0.7 1.0 0.4 } modifier = { global_ship_cost = -0.05} chance = { factor = 0 } }

brazil			= { color = { 0.6 0.5 0.8 } modifier = { global_trade_power = 0.10} chance = { factor = 0 } }

penal			= { color = { 0.4 0.0 0.5 } modifier = { stability_cost_modifier = -0.1} chance = { factor = 0 } }

carpet			= { color = { 1.0 0.3 0.7 } modifier = { prestige = 1.0} chance = { factor = 0 } }

palm			= { color = { 0.4 1.0 1.0 } modifier = { stability_cost_modifier = -0.1} chance = { factor = 0 } }

silver			= { color = { 0.0 0.1 0.4} chance = { factor = 0 } }

nutmeg			= { color = { 0.5 0.7 0.7 } modifier = { global_tariffs = 0.05} chance = { factor = 0 } }

ebony			= { color = { 0.0 1.0 1.0 } modifier = { global_trade_power = 0.10} chance = { factor = 0 } }

leather			= { color = { 1.0 0.4 1.0 } modifier = { global_regiment_recruit_speed = -0.10} chance = { factor = 0 } }

indigo			= { color = { 0.0 0.0 0.9 } modifier = { global_trade_power = 0.10} chance = { factor = 0 } }

services		= { color = { 1.0 0.0 0.0} chance = { factor = 0 } }

potatoes		= { color = { 0.2 0.2 0.1} chance = { factor = 0 } }

