
# Iberian Cortes
kingdom_of_castille = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "castile_cortes"
}

kingdom_of_portugal = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "portugal_cortes"
}

kingdom_of_leon = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "leon_cortes"
}

kingdom_of_andalucia = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "andalucia_cortes"
}

kingdom_of_granada = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "granada_cortes"
}

kingdom_of_murcia = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "murcia_cortes"
}

kingdom_of_aragon = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "aragon_cortes"
}

kingdom_of_navarra = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "navarra_cortes"
}

# Iberian Reinos
lordship_of_bizkaia = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "vizcaia_reino"
}

lordship_of_burgos = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "castile_reino"
}

lordship_of_pamplona = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "navarra_reino"
}

lordship_of_toledo = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "toledo_reino"
}

lordship_of_valencia = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "valencia_reino"
}

lordship_of_catalonia = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "catalonia_reino"
}

lordship_of_zaragossa = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "aragon_reino"
}

lordship_of_mallorca = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "mallorca_reino"
}

lordship_of_asturias = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "asturias_reino"
}

lordship_of_leon = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "leon_reino"
}

lordship_of_galicia = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "galicia_reino"
}

lordship_of_cordoba = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "cordoba_reino"
}

lordship_of_jaen = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "jaen_reino"
}

lordship_of_sevilla = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "sevilla_reino"
}

lordship_of_granada = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "granada_reino"
}

lordship_of_murcia = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "murcia_reino"
}

lordship_of_extremadura = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "extremadura_reino"
}

lordship_of_coimbra = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "portugal_reino"
}

lordship_of_tejo = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "portugal_reino"
}

lordship_of_algarve = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "algarve_reino"
}

lordship_of_canarias = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_unrest = -4.5
	local_autonomy = 0.010
	
	picture = "canarias_reino"
}

# Unruly Princelings
kurdish_princelings = {
	local_manpower_modifier = -0.9
	local_tax_modifier = -0.9
	local_production_efficiency = -0.9
	province_trade_power_modifier = -0.45
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.06
	local_unrest = -9.0
	local_autonomy = 0.020
	
	picture = "kurdish_estates"
}

# Autonomous Beyliks
beylik_of_karasi = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = 0.03
	local_unrest = -6
	local_autonomy = 0.015
	
	picture = "karesi_beylik"
}

beylik_of_saruhan = {
	local_manpower_modifier = -0.5
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	province_trade_power_modifier = -0.25
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = 0.06
	local_unrest = -5.0
	local_autonomy = 0.013
	
	picture = "saruhan_beylik"
}

beylik_of_isfendiyar = {
	local_manpower_modifier = -0.5
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	province_trade_power_modifier = -0.25
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = 0.06
	local_unrest = -5.0
	local_autonomy = 0.013
	
	picture = "isfendiyar_beylik"
}

beylik_of_artuqids = {
	local_manpower_modifier = -0.5
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	province_trade_power_modifier = -0.25
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = 0.06
	local_unrest = -5.0
	local_autonomy = 0.013
	
	picture = "artuqid_beylik"
}

beylik_of_ankara = {
	local_manpower_modifier = -0.5
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	province_trade_power_modifier = -0.25
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = 0.06
	local_unrest = -5.0
	local_autonomy = 0.013
	
	picture = "ankara_beylik"
}

ahis_fraternity = {
	local_manpower_modifier = -0.9
	local_tax_modifier = -0.9
	local_production_efficiency = -0.9
	province_trade_power_modifier = -0.45
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.06
	local_unrest = -9.0
	local_autonomy = 0.020
	
	picture = "ahiler"
}

#Miscellameous Local Rulers
ditmarsian_republic = {
	local_manpower_modifier = -0.9
	local_tax_modifier = -0.9
	local_production_efficiency = -0.9
	province_trade_power_modifier = -0.45
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.06
	local_unrest = -9.0
	local_autonomy = 0.020
	
	picture = "ditmarschen_republic"
}

duchy_of_wroclaw = {
	local_manpower_modifier = -0.5
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	province_trade_power_modifier = -0.25
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.06
	local_unrest = -5.0
	local_autonomy = 0.013
	
	picture = "silesia_duchy"
}

coalition_member = {
	local_manpower_modifier = -0.5
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	province_trade_power_modifier = -0.25
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.06
	local_unrest = -5.0
	local_autonomy = 0.013
	
	picture = "aymara_coalition"
}
