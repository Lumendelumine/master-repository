court_best = {
	diplomatic_upkeep = 2
	diplomatic_reputation = 2
	improve_relation_modifier = 0.10
	prestige = 2
	diplomats = 1
	advisor_pool = 1
	global_unrest = -1
	global_tax_modifier = -0.40
	production_efficiency = -0.20
	trade_efficiency = -0.20
	}
	
court_better = {
	diplomatic_upkeep = 1
	diplomatic_reputation = 1
	improve_relation_modifier = 0.05
	prestige = 1
	advisor_pool = 1
	global_unrest = -0.5
	global_tax_modifier = -0.20
	production_efficiency = -0.10
	trade_efficiency = -0.10
	}

court_bad = {
	diplomatic_reputation = -1
	improve_relation_modifier = -0.05
	prestige = -1
	advisor_pool = -1
	global_unrest = 0.25
	global_tax_modifier = 0.10
	production_efficiency = 0.05
	trade_efficiency = 0.05
	}
	
court_worst = {
	diplomatic_upkeep = -1
	diplomatic_reputation = -2
	improve_relation_modifier = -0.10
	diplomats = -1
	prestige = -2
	advisor_pool = -1
	global_unrest = 0.5
	global_tax_modifier = 0.20
	production_efficiency = 0.10
	trade_efficiency = 0.10
	}

