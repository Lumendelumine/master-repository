charter_trade_companies = {
	
	category = 2
	
	allow = {
		OR = {
			dip_tech = 20 # was 10
			has_idea_group = trade_ideas
		}
	}
	
	effect = {

	}
	
	modifier = {
		merchants = 1
		trade_range_modifier = 0.15
		global_foreign_trade_power = 0.10
	}

	ai_will_do = {
		factor = 1	
	}	
}