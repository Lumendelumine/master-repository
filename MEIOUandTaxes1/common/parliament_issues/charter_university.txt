charter_university = {

	category = 3

	allow = {
		university_trigger = yes
		OR = {
			has_idea_group = culture_ideas
			has_idea_group = humanist_ideas
			has_idea_group = asceticism_ideas
			has_idea_group = ceremony_ideas
			has_idea_group = popular_religion_ideas
			has_idea_group = scholasticism_ideas
			has_idea_group = theology_ideas
			has_idea_group = innovativeness_ideas
		}
	}
	
	effect = {
		add_adm_power = 15
		add_dip_power = 15
		add_mil_power = 15
	}
	
	modifier = {
		technology_cost = -0.05
	}

	ai_will_do = {
		factor = 1
	}	
}