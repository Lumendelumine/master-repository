technological_development_1 = {
	potential = {
		adm_tech = 20
		NOT = { adm_tech = 31 }
	}
	
	trigger = {
		adm_tech = 20
		NOT = { adm_tech = 31 }
	}
		development_cost = -0.03
}

technological_development_2 = {
	potential = {
		adm_tech = 31
		NOT = { adm_tech = 41 }
	}
	
	trigger = {
		adm_tech = 31
		NOT = { adm_tech = 41 }
	}
		development_cost = -0.06
}

technological_development_3 = {
	potential = {
		adm_tech = 41
		NOT = { adm_tech = 51 }
	}
	
	trigger = {
		adm_tech = 41
		NOT = { adm_tech = 51 }
	}
		development_cost = -0.09
}

technological_development_4 = {
	potential = {
		adm_tech = 51
	}
	
	trigger = {
		adm_tech = 51
	}
		development_cost = -0.12
}

integration_development_1 = {
	potential = {
		dip_tech = 20
		NOT = { dip_tech = 31 }
	}
	
	trigger = {
		dip_tech = 20
		NOT = { dip_tech = 31 }
	}
		development_cost = -0.03
		
}

integration_development_2 = {
	potential = {
		dip_tech = 31
		NOT = { dip_tech = 41 }
	}
	
	trigger = {
		dip_tech = 31
		NOT = { dip_tech = 41 }
	}
		development_cost = -0.06
		
}

integration_development_3 = {
	potential = {
		dip_tech = 41
		NOT = { dip_tech = 51 }
	}
	
	trigger = {
		dip_tech = 41
		NOT = { dip_tech = 51 }
	}
		development_cost = -0.09
		
}

integration_development_4 = {
	potential = {
		dip_tech = 51
	}
	
	trigger = {
		dip_tech = 51
	}
		development_cost = -0.12
		
}

superior_weapons_1 = {
	potential = {
		mil_tech = 21
		NOT = { mil_tech = 26 }
	}
	
	trigger = {
		mil_tech = 21
		NOT = { mil_tech = 26 }
	}
		global_regiment_cost = 0.15
}

superior_weapons_2 = {
	potential = {
		mil_tech = 26
		NOT = { mil_tech = 31 }
	}
	
	trigger = {
		mil_tech = 26
		NOT = { mil_tech = 31 }
	}
		global_regiment_cost = 0.225
}

superior_weapons_3 = {
	potential = {
		mil_tech = 31
		NOT = { mil_tech = 36 }
	}
	
	trigger = {
		mil_tech = 31
		NOT = { mil_tech = 36 }
	}
		global_regiment_cost = 0.3
}

superior_weapons_4 = {
	potential = {
		mil_tech = 36
		NOT = { mil_tech = 41 }
	}
	
	trigger = {
		mil_tech = 36
		NOT = { mil_tech = 41 }
	}
		global_regiment_cost = 0.375
}

superior_weapons_5 = {
	potential = {
		mil_tech = 41
		NOT = { mil_tech = 46 }
	}
	
	trigger = {
		mil_tech = 41
		NOT = { mil_tech = 46 }
	}
		global_regiment_cost = 0.45
}

superior_weapons_6 = {
	potential = {
		mil_tech = 46
		NOT = { mil_tech = 51 }
	}
	
	trigger = {
		mil_tech = 46
		NOT = { mil_tech = 51 }
	}
		global_regiment_cost = 0.525
}

superior_weapons_7 = {
	potential = {
		mil_tech = 51
		NOT = { mil_tech = 56 }
	}
	
	trigger = {
		mil_tech = 51
		NOT = { mil_tech = 56 }
	}
		global_regiment_cost = 0.6
}

superior_weapons_8 = {
	potential = {
		mil_tech = 56
	}
	
	trigger = {
		mil_tech = 56
	}
		global_regiment_cost = 0.675
}

superior_ship_1 = {
	potential = {
		dip_tech = 15
		NOT = { dip_tech = 23 }
	}
	
	trigger = {
		dip_tech = 15
		NOT = { dip_tech = 23 }
	}
		transport_cost = 0.15
		light_ship_cost = 0.15
		galley_cost = 0.15
}

superior_ship_2 = {
	potential = {
		dip_tech = 23
		NOT = { dip_tech = 30 }
	}
	
	trigger = {
		dip_tech = 23
		NOT = { dip_tech = 30 }
	}
		global_ship_recruit_speed = 0.15
		global_ship_cost = 0.25
}

superior_ship_3 = {
	potential = {
		dip_tech = 30
		NOT = { dip_tech = 37 }
	}
	
	trigger = {
		dip_tech = 30
		NOT = { dip_tech = 37 }
	}
		global_ship_recruit_speed = 0.30
		global_ship_cost = 0.50
}

superior_ship_4 = {
	potential = {
		dip_tech = 37
		NOT = { dip_tech = 44 }
	}
	
	trigger = {
		dip_tech = 37
		NOT = { dip_tech = 44 }
	}
		global_ship_recruit_speed = 0.45
		global_ship_cost = 0.75
}

superior_ship_5 = {
	potential = {
		dip_tech = 44
		NOT = { dip_tech = 54 }
	}
	
	trigger = {
		dip_tech = 44
		NOT = { dip_tech = 54 }
	}
		global_ship_recruit_speed = 0.60
		global_ship_cost = 1
}

superior_ship_6 = {
	potential = {
		dip_tech = 54
	}
	
	trigger = {
		dip_tech = 54
		
	}
		global_ship_recruit_speed = 0.75
		global_ship_cost = 1.5
}