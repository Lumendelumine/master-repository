###### DIPLO FIX FOR ORDER WARRING SAME RELIGION # STEVE-O ######

order_war_same = {
	potential = {
		# OR = {
			government = monastic_order_government
		# }
	}
	
	trigger = {
		any_country = {
			is_subject = no
			defensive_war_with = ROOT
			religion_group = ROOT
		}	
	}
	
	manpower_recovery_speed = -0.10
	global_regiment_recruit_speed = 0.10
	global_manpower_modifier = -0.10
	mercenary_cost = 0.25
}	
