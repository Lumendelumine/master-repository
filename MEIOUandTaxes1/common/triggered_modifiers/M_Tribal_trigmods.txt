######################################
# Large Tribes
######################################
large_tribe_bad_adm = {
	potential = {
		is_tribal = yes
	}

	trigger = {
		num_of_cities = 10
		NOT = { ADM = 3 }
		has_regency = no
	}

	global_tax_modifier = -0.33
}

large_tribe_bad_dip = {
	potential = {
		is_tribal = yes
	}

	trigger = {
		num_of_cities = 10
		NOT = { DIP = 3 }
		has_regency = no
	}

	global_unrest = 3
}

large_tribe_bad_mil = {
	potential = {
		is_tribal = yes
	}

	trigger = {
		num_of_cities = 10
		NOT = { MIL = 3 }
		has_regency = no
	}

	core_creation = 2.0
}