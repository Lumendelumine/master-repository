######################################
# triggered_modifiers.txt
######################################
# NATION TRAITS
######################################

french_traits_feudal = {
	potential = {
		has_country_flag = pre_jacques_coeur
		has_country_flag = pre_franc_archers
		OR = {
			culture_group = langue_d_oil
			culture_group = langue_d_oc
		}
#		NOT = { primary_culture = anglois }
	}
	trigger = {
		has_country_flag = pre_jacques_coeur
		has_country_flag = pre_franc_archers
	}
	land_morale = -0.05
	stability_cost_modifier = +0.5
	global_manpower_modifier = -0.2
	land_forcelimit_modifier = -0.3
	discipline = -0.10
	land_maintenance_modifier = 0.15
	global_tax_modifier = -0.3
	global_autonomy = -0.04
}

french_traits_reform = {
	potential = {
		NOT = { has_country_flag = pre_jacques_coeur }
		has_country_flag = pre_franc_archers
		OR = {
			culture_group = langue_d_oil
			culture_group = langue_d_oc
		}
#		NOT = { primary_culture = anglois }
	}
	trigger = {
		NOT = { has_country_flag = pre_jacques_coeur }
		has_country_flag = pre_franc_archers
	}
	stability_cost_modifier = +0.5
	global_manpower_modifier = -0.2
	land_forcelimit_modifier = 0.1
	discipline = -0.025
	land_maintenance_modifier = 0.075
	global_tax_modifier = -0.1
	global_autonomy = -0.04
}

french_traits_reform2 = {
	potential = {
		has_country_flag = pre_jacques_coeur
		NOT = { has_country_flag = pre_franc_archers }
		OR = {
			culture_group = langue_d_oil
			culture_group = langue_d_oc
		}
#		NOT = { primary_culture = anglois }
	}
	trigger = {
		has_country_flag = pre_jacques_coeur
		NOT = { has_country_flag = pre_franc_archers }
	}
	land_morale = -0.05
	stability_cost_modifier = +0.3
	land_forcelimit_modifier = -0.1
	land_maintenance_modifier = 0.075
	global_tax_modifier = -0.1
	global_autonomy = -0.04
}

godsent_jeanne_d_arc = {
	potential = {
		has_global_flag = hundred_year_war
		has_country_flag = jeanne_d_arc
	}
	trigger = {
		controls = 186
	}
	army_tradition = 3
	land_morale = 0.15
	discipline = 0.1
	global_autonomy = -0.01
}

colonial_nation_trait = {
	potential = {
		NOT = { government = trade_company_gov }
		OR = {
			is_former_colonial_nation = yes
			is_colonial_nation = yes
		}
	}
	trigger = {
		OR = {
			is_former_colonial_nation = yes
			is_colonial_nation = yes
		}
	}
	colonists = 1
	global_trade_power = 0.50
	development_cost = -0.30			#DO_added
}

longbowmen_modifier = {
	potential = {
		OR = {
			tag = ENG
			tag = WLS
		}
		NOT = { mil_tech = 20 }
	}
	trigger = {
		NOT = { mil_tech = 20 }
	}
	army_tradition = 0.3
	discipline = 0.02
	infantry_power = 0.05
}

mameluke_decline = {
	potential = {
		tag = MAM
	}
	trigger = {
		any_country = {
			capital_scope = { continent = europe }
			any_owned_province = { continent = asia }
		}
	}
	global_trade_power = -0.50
}

######################################
# STRAIT CONTROL
######################################

bosphorus_strait_good = {
	potential = {
		owns = 317
		owns = 1446
		owns = 2238
		owns = 1402
	}
	trigger = {
		owns = 317
		owns = 1446
		owns = 2238
		owns = 1402
	}
	trade_efficiency = 0.10
	#trade_efficiency = 0.05
}

bosphorus_strait_split = {
	potential = {
		OR = {
			AND = {
				owns = 1402
				owns = 1446
				NOT = { owns = 317 }
				NOT = { owns = 2238 }
			}
			AND = {
				owns = 317
				owns = 2238
				NOT = { owns = 1402 }
				NOT = { owns = 1446 }
			}
		}
	}
	trigger = {
		OR = {
			AND = {
				owns = 1402
				owns = 1446
				NOT = { owns = 317 }
				NOT = { owns = 2238 }
			}
			AND = {
				owns = 317
				owns = 2238
				NOT = { owns = 1402 }
				NOT = { owns = 1446 }
			}
		}
	}
	trade_efficiency = 0.05
	#trade_efficiency = 0.01
}

bosphorus_strait_bad = {
	potential = {
		owns = 1402
		NOT = { owns = 317 }
		NOT = { owns = 1446 }
		NOT = { owns = 2238 }
	}
	trigger = {
		owns = 1402
		NOT = { owns = 317 }
		NOT = { owns = 1446 }
		NOT = { owns = 2238 }
	}
	trade_efficiency = -0.10
	#trade_efficiency = -0.05
}

#######################################
#           Wrath of God              #
#######################################
wrath_of_god = {
	potential = {
		has_country_flag = wrath_of_god
	}
	trigger = {
		has_country_flag = wrath_of_god
	}
	relations_decay_of_me = -0.33
	global_unrest = 10
	global_tax_modifier = -0.25
	discipline = -0.05
	global_manpower_modifier = -0.25
	global_autonomy = 0.06
}

corruption_light = {
	potential = {
		check_variable = { which = State_Corruption value = 1 }
		NOT = { check_variable = { which = State_Corruption value = 6 } }
	}
	trigger = {
		check_variable = { which = State_Corruption value = 1 }
		NOT = { check_variable = { which = State_Corruption value = 6 } }
	}
	stability_cost_modifier = 0.10
	global_tax_modifier = -0.04
	production_efficiency = -0.04
	trade_efficiency = -0.04
	global_autonomy = 0.01
}

corruption_mild = {
	potential = {
		check_variable = { which = State_Corruption value = 6 }
		NOT = { check_variable = { which = State_Corruption value = 11 } }
	}
	trigger = {
		check_variable = { which = State_Corruption value = 6 }
		NOT = { check_variable = { which = State_Corruption value = 11 } }
	}
	stability_cost_modifier = 0.20
	global_tax_modifier = -0.08
	production_efficiency = -0.08
	trade_efficiency = -0.08
	global_autonomy = 0.02
}

corruption_important = {
	potential = {
		check_variable = { which = State_Corruption value = 11 }
		NOT = { check_variable = { which = State_Corruption value = 16 } }
	}
	trigger = {
		check_variable = { which = State_Corruption value = 11 }
		NOT = { check_variable = { which = State_Corruption value = 16 } }
	}
	stability_cost_modifier = 0.30
	global_tax_modifier = -0.12
	production_efficiency = -0.12
	trade_efficiency = -0.12
	global_autonomy = 0.03
}

corruption_heavy = {
	potential = {
		check_variable = { which = State_Corruption value = 16 }
		NOT = { check_variable = { which = State_Corruption value = 21 } }
	}
	trigger = {
		check_variable = { which = State_Corruption value = 16 }
		NOT = { check_variable = { which = State_Corruption value = 21 } }
	}
	stability_cost_modifier = 0.40
	global_tax_modifier = -0.16
	production_efficiency = -0.16
	trade_efficiency = -0.16
	global_unrest = 1
	global_autonomy = 0.04
}

corruption_rampant = {
	potential = {
		check_variable = { which = State_Corruption value = 21 }
	}
	trigger = {
		check_variable = { which = State_Corruption value = 21 }
	}
	stability_cost_modifier = 0.50
	global_tax_modifier = -0.20
	production_efficiency = -0.20
	trade_efficiency = -0.20
	global_unrest = 2
	global_autonomy = 0.05
}

blessing_of_god = {
	potential = {
		has_country_flag = rising_nation
		ai = yes
	}
	trigger = {
		has_country_flag = rising_nation
		ai = yes
	}
	land_morale = 0.15
	stability_cost_modifier = -0.3
	land_forcelimit_modifier = 0.1
	army_tradition = 3
	discipline = 0.05
	manpower_recovery_speed = 0.10
	global_autonomy = -0.10
}

#######################################
#           Policy Sliders            #
#######################################

centralisation_efforts = {
	potential = {
		has_country_flag = centralisation_efforts
	}
	trigger = {
		has_country_flag = centralisation_efforts
	}
	global_tax_modifier = -0.02
	global_unrest = 1
}

decentralisation_efforts = {
	potential = {
		has_country_flag = decentralisation_efforts
	}
	trigger = {
		has_country_flag = decentralisation_efforts
	}
	production_efficiency = -0.02
}
