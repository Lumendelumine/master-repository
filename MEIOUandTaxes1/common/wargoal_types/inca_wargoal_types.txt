wg_de_jure_inca = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.1

	allowed_provinces = {
		or = {
			region = peru_region
			region = chile_region
		}
	}

	po_annex = yes
	po_demand_provinces = yes
	
	war_name = DE_JURE_INCA
}