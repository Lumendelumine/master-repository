
NDefines.NGraphics.SHIP_IN_PORT_SCALE = 0.40
NDefines.NGraphics.MILD_WINTER_VALUE = 95
NDefines.NGraphics.DRAW_DETAILED_CUTOFF = 600
NDefines.NGraphics.DRAW_REFRACTIONS_CUTOFF = 450
NDefines.NGraphics.MAX_TRADE_NODE_FLAGS_SHOWN = -1					-- -1 is unlimited
NDefines.NGraphics.SHOW_TRADE_MODIFIERS_IN_TRADE_MAP_MODE = 1		-- 1 = true, 0 = false
