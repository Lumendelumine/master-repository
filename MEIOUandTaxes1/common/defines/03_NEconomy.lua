

NDefines.NEconomy.GOLD_MINE_SIZE_PRIMITIVES = 20
NDefines.NEconomy.GOLD_MINE_DEPLETION_THRESHOLD = 999			-- Gold mines above production level or above can be depleted
NDefines.NEconomy.GOLD_MINE_DEPLETION_CHANCE = 0				-- Chance of gold mine being depleted (yearly, per production above threshold) 

NDefines.NEconomy.AUTONOMY_AT_DIPLO_ANNEX = 30					-- Autonomy added when diplo-annexing
NDefines.NEconomy.AUTONOMY_AT_CONQUEST = 70						-- Autonomy added at conquest
NDefines.NEconomy.AUTONOMY_AT_CONQUEST_CLAIM = 40				-- Autonomy added at conquest if you have a claim
NDefines.NEconomy.AUTONOMY_AT_CONQUEST_CORE = 10					-- Autonomy added at conquest if you have a core
NDefines.NEconomy.OVERSEAS_MIN_AUTONOMY = 0						-- Overseas provinces always have at least this much autonomy
NDefines.NEconomy.DECREASE_AUTONOMY_STEP = -15
NDefines.NEconomy.INCREASE_AUTONOMY_STEP = 15
NDefines.NEconomy.INCREASE_AUTONOMY_MAX = 85
NDefines.NEconomy.AUTONOMY_CHANGE_DURATION = 3650				-- about 10 years

NDefines.NEconomy.LAND_TECH_MAINTENANCE_IMPACT = 0.05 				-- % each tech increases it.   #CHANGED FROM 0.05 BY DO OVERHAUL
NDefines.NEconomy.GOLD_INFLATION_THRESHOLD = 0.1				-- _EDEF_GOLD_INFLATION_THRESHOLD_
NDefines.NEconomy.GOLD_INFLATION = 0.75							-- _EDEF_GOLD_INFLATION_
NDefines.NEconomy.TREASURE_FLEET_INFLATION = 0.15
NDefines.NEconomy.INFLATION_FROM_LOAN = 0.20						-- increase per loan
NDefines.NEconomy.INFLATION_ACTION_REDUCTION = 2				-- amount per action
NDefines.NEconomy.WARTAXES_DURATION = 12						-- _EDEF_WARTAXES_DURATION_
NDefines.NEconomy.BASE_INTERESTS = 8.0							-- Base interests
NDefines.NEconomy.LAND_MAINTENANCE_FACTOR = 0.30				-- _EDEF_LAND_MAINTENANCE_FACTOR
NDefines.NEconomy.HEAVY_SHIP_MAINT_FACTOR = 0.08					-- _EDEF_HEAVY_SHIP_MAINT_FACTOR_
NDefines.NEconomy.LIGHT_SHIP_MAINT_FACTOR = 0.04					-- _EDEF_LIGHT_SHIP_MAINT_FACTOR_
NDefines.NEconomy.GALLEY_MAINT_FACTOR = 0.04						-- _EDEF_GALLEY_MAINT_FACTOR_
NDefines.NEconomy.TRANSPORT_MAINT_FACTOR = 0.04					-- _EDEF_TRANSPORT_MAINT_FACTOR_
NDefines.NEconomy.COLONIAL_MAINTENANCE_FACTOR = 10.0			-- _EDEF_COLONIAL_MAINTENANCE_FACTOR_
NDefines.NEconomy.MISSIONARY_MAINTENANCE_FACTOR = 10.0			-- DEI GRATIA _EDEF_MISSIONARY_MAINTENANCE_FACTOR_
NDefines.NEconomy.MAX_PROVINCE_SELL_PRICE = 2000				-- _EDEF_MAX_PROVINCE_SELL_PRICE_
NDefines.NEconomy.COASTAL_MODIFIER = 0.7						-- _EDEF_COASTAL_MODIFIER_
NDefines.NEconomy.EMBARGO_BASE_EFFICIENCY = 1.5					-- EMBARGO_BASE_EFFICIENCY
NDefines.NEconomy.TRADE_ADDED_VALUE_MODIFER = 0.075
NDefines.NEconomy.TRADE_PROPAGATE_THRESHOLD = 5
NDefines.NEconomy.TRADE_PROPAGATE_DIVIDER = 6.5
NDefines.NEconomy.ALLOW_DESTROY_MANUFACTORY = 1					-- Should the player be permitted to destroy manufactories?
NDefines.NEconomy.PIRATES_TRADE_POWER_FACTOR = 2
NDefines.NEconomy.TRADE_COMPANY_STRONG_LIMIT = 0.34
NDefines.NEconomy.LARGE_COLONIAL_NATION_LIMIT = 10
NDefines.NEconomy.PRIVATEER_INCOME_COLLECTION_EFF = 0.5
NDefines.NEconomy.CARAVAN_FACTOR = 25.0							-- Development is divided by this factor, do not set to zero!
NDefines.NEconomy.CARAVAN_POWER_MAX = 150
NDefines.NEconomy.CARAVAN_POWER_MIN = 2
NDefines.NEconomy.MAX_BUILDING_SLOTS = 16						-- vanilla 16, Maximum number of buildings slots, i.e. max buildings possible.
NDefines.NEconomy.ADVISOR_COST_INCREASE_PER_YEAR = 0.01			-- yearly increase in price in percent,
NDefines.NEconomy.FORT_BUDGET_FRACTION = 0.2 					-- AI will spend a maximum of this fraction of monthly income on forts