NDefines.NAI.AI_USES_HISTORICAL_IDEA_GROUPS = 1								-- If set to 0, ai will use ai_will_do instead of historical ideagroups when picking ideagroups (ai never uses historical ideagroups in custom/random setup)

NDefines.NAI.PEACE_WAR_EXHAUSTION_FACTOR = 1.2
NDefines.NAI.AI_CONVERT_CULTURES = 0										-- If set to 0, AI will not convert cultures
NDefines.NAI.AGGRESSIVENESS = 500											-- Base chance (out of 10000) of AI being willing to start a war each diplomatic tick (~1.5 times a month)
NDefines.NAI.AGGRESSIVENESS_BONUS_EASY_WAR = 1000							-- Added to aggressiveness if the war is against a weak or particularily hated enemy
NDefines.NAI.TRADE_INTEREST_THRESHOLD = 4 									-- Number of merchants required to be a nation with trade interest
NDefines.NAI.FORCE_COMPOSITION_CHANGE_TECH_LEVEL = 30						-- Tech level at which AI will double its artillery fraction
NDefines.NAI.OVER_FORCELIMIT_AVOIDANCE_FACTOR = 8							-- The higher this number is, the less willing the AI will be to exceed forcelimits
NDefines.NAI.COLONY_BUDGET_AMOUNT = 6.0									-- AI will reserve a maximum of this amount of monthly ducats for colonies (multiplied by number of colonists)
NDefines.NAI.DIPLOMATIC_ACTION_FABRICATE_CLAIM_BASE_FACTOR = 20				-- AI scoring for fabricating claims is always increased by this as long as the province is a conquest priority
NDefines.NAI.DIPLOMATIC_ACTION_FABRICATE_CLAIM_HRE_FACTOR = 0.5				-- AI scoring for fabricating claims if both parts are HRE
NDefines.NAI.DIPLOMATIC_ACTION_FABRICATE_CLAIM_HRE_EMPEROR_FACTOR = 0.25	-- AI scoring for fabricating claims if province is HRE and they are emperor (does not stack with the above penalty)

NDefines.NAI.PEACE_EXCESSIVE_DEMANDS_FACTOR = 0.01							-- AI unwillingness to peace based on demanding more stuff than you have warscore
NDefines.NAI.PEACE_EXCESSIVE_DEMANDS_THRESHOLD = 30							-- If you have less warscore than this, excessive demands will be factored in more highly
NDefines.NAI.PEACE_MILITARY_STRENGTH_FACTOR = 9								-- AI unwillingness to peace based on manpower & forcelimits
NDefines.NAI.PEACE_TIME_MONTHS = 36											-- Months of additional AI stubbornness in a war
NDefines.NAI.PEACE_TIME_MAX_MONTHS = 600									-- Max months applied to time factor in a war
NDefines.NAI.PEACE_TIME_EARLY_FACTOR = 0.5									-- During months of stubbornness the effect of time passed is multiplied by this
NDefines.NAI.PEACE_TIME_LATE_FACTOR = 0.5

NDefines.NAI.PEACE_ALLY_TIME_MULT = 0.5										-- Multiplies PEACE_TIME_FACTOR for allies in a war
NDefines.NAI.PEACE_WAR_DIRECTION_FACTOR = 0.8								-- AI willingness to peace based on who's making gains in the war
NDefines.NAI.PEACE_WAR_DIRECTION_WINNING_MULT = 5.0							-- Multiplies AI emphasis on war direction if it's the one making gains
