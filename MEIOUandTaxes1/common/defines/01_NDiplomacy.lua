
NDefines.NDiplomacy.ALLOW_LEADER_DEMAND_TOGGLE = 1						-- Whether or not player is allowed to set if warleader can negotiate for them
NDefines.NDiplomacy.VASSALIZE_BASE_DEVELOPMENT_CAP = 400				-- Countries with more total base tax than this cannot be vassalized
NDefines.NDiplomacy.PEACE_IMPACT_ADM_SCORE = 0.1
NDefines.NDiplomacy.PEACE_IMPACT_DIP_SCORE = 0.1
NDefines.NDiplomacy.PEACE_IMPACT_MIL_SCORE = 0.1
NDefines.NDiplomacy.AUTONOMY_WARSCORE_COST_MODIFIER = 0.33			-- How much autonomy reduces score by (at 1, 50% autonomy = 50% reduction)	
NDefines.NDiplomacy.MAX_FREE_CITIES = 15
NDefines.NDiplomacy.HRE_PRINCE_AUTHORITY_THRESHOLD = 80					-- Threshold below which you lose IA, and above which you gain it
NDefines.NDiplomacy.END_OF_CRUSADES = 1870 								-- End of Crusade/Excommunicate actions. AI might also befriend old religious enemies.		# DEI GRATIA CHANGED
NDefines.NDiplomacy.WE_IMPACT_ON_ANNEX_INTEGRATE = -0.075				-- multiplied with current WE
NDefines.NDiplomacy.TRUCE_YEARS = 3 									-- _DDEF_TRUCE_YEARS_; Years of Truce
NDefines.NDiplomacy.SCALED_TRUCE_YEARS = 7								-- Additional years of truce based on % of warscore taken in war (100% warscore = full scaled truce years)
NDefines.NDiplomacy.MONARCH_GOV_CHANGE_LEGITIMACY_PENALTY = 0.25		-- Penalty(%) on the legitimacy when changing gov type to the monarchy
NDefines.NDiplomacy.FABRICATE_CLAIMS_SLOWDOWN_IF_DISCOVERED = 0.5 		-- multiplies amount of time to claim if discovered, set to 0.5 as M&T already has longer claim time and 0.5 means it'd hit 4 years max just like vanilla

NDefines.NDiplomacy.AE_DISTANCE_BASE = 1.25
NDefines.NDiplomacy.AE_PROVINCE_CAP = 40								-- Province development above this will not count for AE
NDefines.NDiplomacy.AE_FABRICATE_CLAIM = 2.5

NDefines.NDiplomacy.DIP_PORT_FEES = 0.2									-- DIP_PORT_FEES
NDefines.NDiplomacy.DAYS_TO_FABRICATE_CLAIM = 730						-- DAYS_TO_FABRICATE_CLAIM
NDefines.NDiplomacy.CLAIM_PEACE_COST_DIP_FRACTION = -0.25				-- Fraction of dipcost you pay for cores/claims
NDefines.NDiplomacy.CORE_PEACE_COST_DIP_FRACTION = -0.5					-- Fraction of dipcost you pay for cores
NDefines.NDiplomacy.MONTHS_BEFORE_TOTAL_OCCUPATION = 24					-- Before this many months have passed in the war you cannot gain 100% warscore by just occupying the warleader
NDefines.NDiplomacy.PO_DEMAND_PROVINCES_AE = 0.6						-- _DDEF_PO_DEMAND_PROVINCES_AE = 10, (Per development)
NDefines.NDiplomacy.PO_RETURN_CORES_AE = 0.4		 					-- (Per core, only applied if returning cores to vassals of winner)
NDefines.NDiplomacy.PO_BECOME_VASSAL_AE = 0.3		 					-- _DDEF_PO_BECOME_VASSAL_AE = 10, (Per development)
NDefines.NDiplomacy.PO_BECOME_PROTECTORATE_AE = 0.25 					-- _DDEF_PO_BECOME_VASSAL_AE = 10, (Per development)
NDefines.NDiplomacy.PEACE_COST_BECOME_VASSAL = 0.7						-- Vassalize a country (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_CONVERSION = 0.3							-- scaled with countrysize for forced conversion in peace.
NDefines.NDiplomacy.PEACE_COST_CONCEDE = 5 								-- _DDEF_PEACE_COST_CONCEDE_ Base Peace cost for conceding defeat
NDefines.NDiplomacy.PEACE_COST_DEMAND_PROVINCE = 1						-- Demand a province (scales by province wealth, also used for annex)
NDefines.NDiplomacy.PEACE_COST_RETURN_CORE = 0.9						-- Return a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_REVOKE_CORE = 0.5						-- Revoke a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_RELEASE_ANNEXED = 0.9					-- Release annexed nation (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_RELEASE_VASSAL = 0.5						-- Release vassal (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_DEMAND_NON_OCCUPIED_PROVINCE_MULT = 1.25
NDefines.NDiplomacy.PEACE_COST_DEMAND_CAPITAL_MULT = 1.50
NDefines.NDiplomacy.INTEGRATE_UNION_MIN_YEARS = 10
NDefines.NDiplomacy.INTEGRATE_VASSAL_MIN_YEARS = 10
NDefines.NDiplomacy.AGITATE_FOR_LIBERTY_DESIRE = 15						-- Liberty Desire gained due to ongoing agitation.
NDefines.NDiplomacy.AGITATE_FOR_LIBERTY_RATE = 0.5						-- Monthly rate at which Liberty Desire rises towards the maximum during agitation, or otherwise falls towards zero.
NDefines.NDiplomacy.ANNEX_DIP_COST_PER_DEVELOPMENT = 6					-- per development
