##############################     Asian     ##############################
##############################  Governments  ##############################

early_medieval_japanese_gov = {		#Medieval Japanese governments
	monarchy = yes
	
	color = { 200 150 80 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes nation_designer_cost = 0
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}	
	
	rank = {
		6 = {
			production_efficiency = 0.05
			global_manpower_modifier = 0.15
			land_forcelimit_modifier = 0.10
			land_morale = 0.10
			army_tradition = 0.20
			global_regiment_cost = -0.13
			land_maintenance_modifier = -0.20
			
			global_autonomy = 0
		} #Emperor
		5 = {
			global_tax_modifier = -0.3
			stability_cost_modifier = 0.3
			trade_efficiency = -0.05
			vassal_income = -0.2
			global_regiment_cost = -0.05
			land_maintenance_modifier = -0.05
			
			global_autonomy = 0
		} #Shogun
		4 = {
			global_tax_modifier = -0.1
			land_morale = 0.10
			stability_cost_modifier = 0.4
			land_forcelimit_modifier = 0.1
			global_regiment_cost = -0.33
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0
		} #Shogun contender
		3 = {
			land_morale = 0.10
			stability_cost_modifier = 0.2
			land_forcelimit_modifier = 0.1
			global_regiment_cost = -0.33
			land_maintenance_modifier = -0.05
			
			global_autonomy = 0
		} #Daimyo
	}
	
}

late_medieval_japanese_gov = {	#Late Medieval Japanese governments
	monarchy = yes
	
	color = { 220 135 40 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes nation_designer_cost = 0
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}	
	
	rank = {
		6 = {
			trade_efficiency = 0.10
			production_efficiency = 0.05
			global_manpower_modifier = 0.15
			land_forcelimit_modifier = 0.10
			land_morale = 0.05
			army_tradition = 0.10
			land_maintenance_modifier = -0.20
			
			global_autonomy = 0
		} #Emperor
		5 = {
			global_tax_modifier = -0.2
			stability_cost_modifier = 0.1
			trade_efficiency = -0.02
			vassal_income = -0.1
			land_forcelimit_modifier = 0.05
			global_regiment_cost = -0.05
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0
		} #Shogun
		4 = {
			global_tax_modifier = 0.1
			land_morale = 0.10
			technology_cost = -0.05
			stability_cost_modifier = 0.2
			army_tradition = 0.1
			core_creation = -0.25
			land_forcelimit_modifier = 0.2
			global_regiment_cost = -0.33
			land_maintenance_modifier = -0.15
			
			global_autonomy = 0
		} #Shogun contender
		3 = {
			land_morale = 0.10
			technology_cost = -0.05
			stability_cost_modifier = 0.1
			army_tradition = 0.1
			core_creation = -0.25
			land_forcelimit_modifier = 0.2
			global_regiment_cost = -0.33
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0
		} #Daimyo
	}
	
}

early_modern_japanese_gov = {		#Modern Japanese governments
	monarchy = yes
	
	color = { 240 120 5 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes nation_designer_cost = 0
	allow_vassal_war = no
	allow_vassal_alliance = no
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}	
	
	rank = {
		6 = {
			trade_efficiency = 0.10
			production_efficiency = 0.15
			global_manpower_modifier = 0.25
			land_forcelimit_modifier = 0.10
			land_morale = 0.15
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0
		} #Emperor
		5 = {
			advisor_cost = -0.2
			advisor_pool = 1
			build_cost = -0.1
			land_forcelimit_modifier = 0.07
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.15
			
			global_autonomy = 0
		} #Shogun
		4 = {
			advisor_cost = -0.2
			land_morale = 0.10
			global_tax_modifier = 0.1
			core_creation = -0.25
			build_cost = -0.1
			land_forcelimit_modifier = 0.30
			
			global_autonomy = 0
		} #Shogun contender
		3 = {
			land_morale = 0.10
			global_tax_modifier = 0.1
			army_tradition = 0.2
			core_creation = -0.25
			trade_efficiency = 0.05
			build_cost = -0.1
			land_forcelimit_modifier = 0.30
			
			global_autonomy = 0
		} #Daimyo
	}
	
}

eastern_monarchy = {
	monarchy = yes
	
	color = { 200 150 80 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no

	rank = {
		1 = {
			production_efficiency = 0.01
			global_regiment_cost = -0.33
			
			global_autonomy = 0.05
		}
		2 = {
			production_efficiency = 0.02
			global_regiment_cost = -0.33
			
			global_autonomy = 0.05
		}
		3 = {
			production_efficiency = 0.03
			global_regiment_cost = -0.33
			
			global_autonomy = 0.05
		}
		4 = {
			production_efficiency = 0.05
			global_regiment_cost = -0.33
			
			global_autonomy = 0.05
		}
		5 = {
			production_efficiency = 0.10
			global_regiment_cost = -0.33
			
			global_autonomy = 0.05
		}
		6 = {
			production_efficiency = 0.15
			global_regiment_cost = -0.33
			
			global_autonomy = 0.05
		}
	}
}

indian_monarchy = {
	monarchy = yes

	color = { 175 30 175 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	
	rank = {
		1 = {
			production_efficiency = 0.01
			global_regiment_cost = -0.33
			liberty_desire = -10
			
			global_autonomy = 0.05
		}
		2 = {
			production_efficiency = 0.02
			global_regiment_cost = -0.33
			liberty_desire = -10
			
			global_autonomy = 0.05
		}
		3 = {
			production_efficiency = 0.03
			global_regiment_cost = -0.33
			liberty_desire = -10	
			
			global_autonomy = 0.05
		}
		4 = {
			production_efficiency = 0.05
			global_regiment_cost = -0.33
			liberty_desire = -10
			
			global_autonomy = 0.05
		}
		5 = {
			production_efficiency = 0.10
			global_regiment_cost = -0.33
			liberty_desire = -10
			
			global_autonomy = 0.05
		}
		6 = {
			production_efficiency = 0.15
			global_regiment_cost = -0.33
			liberty_desire = -10
			
			global_autonomy = 0.05
		}
	}
}

maratha_confederacy = {	
	monarchy = yes
	
	color = { 210 120 210 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	
	rank = {
		5 = {
			global_unrest = -2
			global_tax_modifier = -0.1
			global_regiment_cost = -0.33
			
			global_autonomy = 0
		}
	}
	fixed_rank = 5
}

# scholar government
chinese_monarchy = {
	monarchy = yes
	
	color = { 125 30 30 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}	
	

	rank = {
		3 = {	#Tusi
			production_efficiency = 0.02
			global_regiment_cost = -0.05
			
			global_autonomy = -0.06
		}
		4 = {	#Gong
			production_efficiency = 0.03
			global_regiment_cost = -0.10
			adm_tech_cost_modifier = -0.02
			
			global_autonomy = -0.06
		}
		5 = {	#Wang
			production_efficiency = 0.04
			global_regiment_cost = -0.20
			adm_tech_cost_modifier = -0.04
			
			global_autonomy = -0.06
		}
		6 = {	#Huangdi
			production_efficiency = 0.05
			global_regiment_cost = -0.30
			global_unrest = -3
			stability_cost_modifier = -0.25
			adm_tech_cost_modifier = -0.06
			
			global_autonomy = -0.06
		}
	}
}

# aristocratic government
chinese_monarchy_2 = {
	monarchy = yes
	
	color = { 175 30 30 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}	
	
	rank = {
		3 = {	#Tusi
			production_efficiency = 0.02
			global_regiment_cost = -0.05
			
			global_autonomy = 0.04
		}
		4 = {	#Gong
			production_efficiency = 0.03
			discipline = 0.025
			global_manpower_modifier = 0.05
			global_regiment_cost = -0.10
			mil_tech_cost_modifier = -0.02
			
			global_autonomy = 0.04
		}
		5 = {	#Wang
			production_efficiency = 0.04
			discipline = 0.025
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.20
			mil_tech_cost_modifier = -0.04
			
			global_autonomy = 0.04
		}
		6 = {	#Huangdi
			production_efficiency = 0.05
			discipline = 0.025
			global_manpower_modifier = 0.2
			global_regiment_cost = -0.30
			global_unrest = -3
			mil_tech_cost_modifier = -0.06
			
			global_autonomy = 0.04
		}
	}
}

# bureaucratic government
chinese_monarchy_3 = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}	
	
	rank = {
		3 = {
			production_efficiency = 0.02
			global_regiment_cost = -0.05
			
			global_autonomy = -0.01
		}
		4 = {
			production_efficiency = 0.03
			global_tax_modifier = 0.03
			global_manpower_modifier = 0.05
			global_regiment_cost = -0.10
			technology_cost = 0.02
			
			global_autonomy = -0.01
		}
		5 = {
			production_efficiency = 0.04
			global_tax_modifier = 0.04
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.20
			technology_cost = 0.04
			
			global_autonomy = -0.01
		}
		6 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			global_manpower_modifier = 0.2
			global_regiment_cost = -0.30
			global_unrest = -3
			technology_cost = 0.06
			
			global_autonomy = -0.01
		}
	}
}

# theocratic government
chinese_monarchy_4 = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}	

	rank = {
		3 = {
			production_efficiency = 0.02
			global_regiment_cost = -0.05
			
			global_autonomy = 0.04
		}
		4 = {
			production_efficiency = 0.03
			global_manpower_modifier = 0.10
			land_morale = -0.1
			global_regiment_cost = -0.10
			dip_tech_cost_modifier = -0.06
			tolerance_own = 1
			
			global_autonomy = 0.04
		}
		5 = {
			production_efficiency = 0.04
			global_manpower_modifier = 0.15
			land_morale = -0.1
			global_regiment_cost = -0.20
			dip_tech_cost_modifier = -0.06
			tolerance_own = 1
			
			global_autonomy = 0.04
		}
		6 = {
			production_efficiency = 0.05
			global_manpower_modifier = 0.20
			land_morale = -0.1
			global_regiment_cost = -0.30
			global_unrest = -3
			dip_tech_cost_modifier = -0.06
			tolerance_own = 1
			
			global_autonomy = 0.04
		}
	}
}

# peasant government
chinese_monarchy_5 = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}	

	rank = {
		3 = {
			production_efficiency = 0.02
			global_regiment_cost = -0.05
			
			global_autonomy = 0.04
		}
		4 = {
			production_efficiency = 0.03
			global_manpower_modifier = 0.20
			land_morale = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.04
		}
		5 = {
			production_efficiency = 0.04
			global_manpower_modifier = 0.30
			land_morale = 0.1
			global_regiment_cost = -0.20
			
			global_autonomy = 0.04
			
		}
		6 = {
			production_efficiency = 0.05
			global_manpower_modifier = 0.40
			land_morale = 0.1
			global_regiment_cost = -0.30
			global_unrest = -3
			
			global_autonomy = 0.04
		}
	}
}

# Celestial Empire
celestial_empire = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}	
	
	rank = {
		6 = {
			global_unrest = -3
			global_regiment_cost = -0.33
						
			global_autonomy = -0.01
		}
	}
	fixed_rank = 6
}

# Special Religious Governments
tibetan_theocracy = {
	religion = yes
	
	color = { 195 195 230 }
	
	valid_for_new_country = yes valid_for_nation_designer = yes nation_designer_cost = 0
	nation_designer_trigger = {
		culture_group = tibetic
	}	
	
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	rank = {
		5 = {
			tolerance_own = 2
			diplomatic_reputation = 3
			prestige = 1
			global_regiment_cost = -0.33
					
			global_autonomy = 0.01
		}
	}
	fixed_rank = 5
}
