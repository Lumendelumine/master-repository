##############################   Republic    ##############################
##############################  Governments  ##############################

noble_republic = {
	republic = yes
	
	valid_for_new_country = yes valid_for_nation_designer = yes nation_designer_cost = 10
	royal_marriage = yes
	
	color = { 140 180 220 }

	republican_name = yes
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = administrative_republic
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			has_idea = quality_education
			has_idea = finest_of_horses
			has_idea = oak_forests_for_ships
			has_idea = private_to_marshal
			has_idea = naval_drill
			has_idea = massed_battery
		}
	}

	rank = {
		3 = {
			tolerance_heretic = 1
			land_morale = 0.20
						
			land_forcelimit_modifier = 0.20
						
			global_autonomy = 0.05
		}
	}
	fixed_rank = 3
}

merchant_republic = {
	republic = yes

	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 40
	
	color = { 40 130 200 }
	
	duration = 4

	republican_name = yes
	royal_marriage = no

	boost_income = yes
	can_use_trade_post = yes

	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = bureaucratic_despotism
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 5
			has_idea = shrewd_commerce_practise
			has_idea = free_trade
			has_idea = merchant_adventures
			has_idea = national_trade_policy
			has_idea = overseas_merchants
			has_idea = trade_manipulation
			has_idea = fast_negotiations
		}
	}

	rank = {
		1 = {
			global_trade_power = 0.08
			merchants = 1
			trade_efficiency = 0.10
			caravan_power = 0.33
			tolerance_heretic = 1
			tolerance_heathen = 1
			mr_traders_influence = 0.01
			
			mercenary_cost = -0.10
					
			naval_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
		}
		2 = {
			global_trade_power = 0.09
			merchants = 1
			trade_efficiency = 0.10
			caravan_power = 0.33
			tolerance_heretic = 1
			tolerance_heathen = 1
			mr_traders_influence = 0.01
			
			mercenary_cost = -0.10
				
			naval_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
		}
		3 = {
			global_trade_power = 0.10
			merchants = 1
			trade_efficiency = 0.10
			caravan_power = 0.33
			tolerance_heretic = 1
			tolerance_heathen = 1
			mr_traders_influence = 0.01
			
			mercenary_cost = -0.10
				
			naval_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
		}
		4 = {
			global_trade_power = 0.10
			merchants = 1
			trade_efficiency = 0.10
			caravan_power = 0.33
			tolerance_heretic = 1
			tolerance_heathen = 1
			mr_traders_influence = 0.01
			
			mercenary_cost = -0.10
			
			naval_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05	
		}
		5 = {
			global_trade_power = 0.10
			merchants = 1
			trade_efficiency = 0.10
			caravan_power = 0.33
			tolerance_heretic = 1
			tolerance_heathen = 1
			mr_traders_influence = 0.01
			
			mercenary_cost = -0.10
			
			naval_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05			
		}
		6 = {
			global_trade_power = 0.10
			merchants = 1
			trade_efficiency = 0.10
			caravan_power = 0.33
			tolerance_heretic = 1
			tolerance_heathen = 1
			mr_traders_influence = 0.01
			
			mercenary_cost = -0.10
			
			naval_forcelimit_modifier = 0.15

			global_autonomy = 0.05	
		}
	}
	
}

administrative_republic = { 
	republic = yes
	
	valid_for_new_country = yes valid_for_nation_designer = yes nation_designer_cost = 0
	
	color = { 105 170 220 }
	
	duration = 5

	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = republican_dictatorship
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = noble_republic
		}
		modifier = {
			factor = 5
			has_idea = organised_mercenary_payment
			has_idea = benefits_for_mercenaries
			has_idea = organised_mercenary_recruitment
			has_idea = resilient_state
			has_idea = war_cabinet
		}
	}

	rank = {
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			
			production_efficiency = 0.1
			global_tax_modifier = 0.1
			
			global_autonomy = -0.05
		}
	}
	fixed_rank = 3
}

crowned_republic = { #Stadtholder
	republic = yes
	duration = 0 # rule for life.
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	
	color = { 125 150 220 }
	
	republican_name = no
	royal_marriage = yes

	rank = {
		5 = {
			heavy_ship_power = 0.10
			trade_efficiency = 0.10
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = -0.02
		}
	}
	fixed_rank = 5
}

republican_dictatorship = { #Lord Protector
	republic = yes
	
	valid_for_new_country = yes valid_for_nation_designer = yes nation_designer_cost = 0
	
	color = { 15 105 165 }

	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = constitutional_republic
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = administrative_republic
		}
		modifier = {
			factor = 5
			has_idea = national_arsenal
			has_idea = the_old_and_infirm
			has_idea = the_young_can_serve
			has_idea = enforced_service
			has_idea = ships_penny
			has_idea = expanded_supply_trains
			has_idea = mass_army
		}
	}
	
	rank = {
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			stability_cost_modifier = -0.05	
			
			global_autonomy = -0.05
		}
	}
	fixed_rank = 3
}

constitutional_republic = { #President
	republic = yes
	
	valid_for_new_country = yes valid_for_nation_designer = yes nation_designer_cost = 0
	
	color = { 65 155 220 }
	
	duration = 4
	
	republican_name = yes
	royal_marriage = no
	
	has_parliament = yes
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = bureaucratic_despotism
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = republican_dictatorship
		}
		modifier = {
			factor = 5
			has_idea = foreign_embassies
			has_idea = claim_fabrication
			has_idea = cabinet
			has_idea = adaptability
			has_idea = benign_diplomats
			has_idea = diplomatic_influence
			has_idea = flexible_negotiation
		}
	}

	rank = {
		3 = {
			production_efficiency = 0.1
			trade_efficiency = 0.1
			tolerance_heretic = 1
			tolerance_heathen = 1
			
			
			global_autonomy = -0.06
		}
	}
	fixed_rank = 3
}

bureaucratic_despotism = { #First Council
	republic = yes
	
	valid_for_new_country = yes valid_for_nation_designer = yes nation_designer_cost = 0
	
	color = { 25 65 205 }
	
	royal_marriage = no
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = revolutionary_republic
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = constitutional_republic
		}
		modifier = {
			factor = 5
			has_idea = bureaucracy
			has_idea = organised_construction
			has_idea = smithian_economics
			has_idea = national_bank
			has_idea = debt_and_loans
			has_idea = centralization
			has_idea = nationalistic_enthusiasm
		}
	}

	rank = {
		4 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			stability_cost_modifier = -0.20
			
			global_autonomy = -0.05
		}
	}
	fixed_rank = 4
}

oligarchic_republic = { # Pisa or Firenze at the start of the game
	republic = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0	
	
	color = { 140 200 240 }
	
	duration = 8

	republican_name = yes
	royal_marriage = no
	
	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds	
	
	ai_will_do = {
		factor = 0
	}
	ai_importance = 1

	#bonus
	rank = {
		3 = {
			tolerance_heretic = 1
			tolerance_heathen = 1
			production_efficiency = 0.05
			trade_efficiency = 0.05
			interest = -1
			mr_aristocrats_influence = 0.003
			mr_guilds_influence = 0.003
			
			mercenary_cost = -0.10
			merc_maintenance_modifier = -0.20
			
			global_autonomy = 0.05
		}
	}
	fixed_rank = 3
}
