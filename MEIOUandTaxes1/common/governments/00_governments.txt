# governments.txt
#
# Do not change tags in here without changing every other reference to them.
# If adding new forms of governemnts, make sure they are uniquely named.
# Uses all 'modifiers' possible thats exported as a Modifier.
#
###########################################################################
####################      Based on Kaigon's ATAGE      ####################
####################   Modified for MEIOU and Taxes   #####################
###########################################################################
#
# 2010-dec-18 FB	DW changes (increase number of officials)
# 2011-feb-12 FB	DW 5.1 changes
# 2011-feb-17 FB	made altaic_monarchy tribal
# 2011-mar-04 FB	Japanese governments - royal_marriage = yes
# 2013-aug-15 GG	EUIV adaptation
# 2013-oct-12 SK	Adjusted to new system with limits
#
# Default values:
# monarchy = yes
# royal_marriage = yes
# Uses all 'modifiers' possible that's exported as a Modifier.
# ai_will_do will only check if it's valid,and won't break unions or switch to republic from monarchy, nor check to convert to something earlier in the file.
#

### TIER  6 - Emperor
### TIER  5 - King
### TIER  4 - Prince
### TIER  3 - Duchy
### TIER  2 - Marquis
### TIER  1 - Count
##############################   Monarchic   ##############################
##############################  Governments  ##############################

# medieval_monarchy
# despotic_monarchy
# feudal_monarchy
# administrative_monarchy
# absolute_monarchy
# constitutional_monarchy
# enlightened_despotism

##############################   Republic    ##############################
##############################  Governments  ##############################

# noble_republic
# merchant_republic
# administrative_republic
# crowned_republic
# republican_dictatorship
# constitutional_republic
# bureaucratic_despotism
# oligarchic_republic

##############################   Religious   ##############################
##############################  Governments  ##############################

# papal_government
# theocratic_government
# monastic_order_government


##############################     Asian     ##############################
##############################  Governments  ##############################

# early_medieval_japanese_gov
# late_medieval_japanese_gov
# early_modern_japanese_gov

# eastern_monarchy_1
# eastern_monarchy_2
# eastern_monarchy_3

# indian_monarchy_1 - Maharaja
# indian_monarchy_2 - Raja
# indian_monarchy_3 - Thakore

# chinese_monarchy_1a - Bureaucratic Empire
# chinese_monarchy_2a - Administrative Empire
# chinese_monarchy_2 - Cultural Faction
# chinese_monarchy_2a - Warlord
# chinese_monarchy_2b - Mandarin
# chinese_monarchy_2c - Revolutionary Theocracy
# celestial_empire - Celestial Empire


##############################   Primitive   ##############################
##############################  Governments  ##############################

# tribal_monarchy

# tribal_republic

# native_council
# native_council_settled

# altaic_monarchy

# altaic_republic - Bey

# steppe_horde


##############################     Other     ##############################
##############################  Governments  ##############################

# revolutionary_republic
# revolutionary_empire

# imperial_city

# ambrosian_republic - Milan
# iqta - Arab nations

# dutch_republic
# elective_monarchy
# english_monarchy

# colonial_government - Colonial Nations

##############################  Serenissima  ##############################
##############################  Governments  ##############################

# venetian_republic
# signoria_monarchy
# siena_republic
