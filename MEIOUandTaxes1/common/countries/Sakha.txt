#Country Name: Please see filename.

graphical_culture = inuitgfx

color = { 89 221 255 }

historical_idea_groups = {
	humanist_ideas
	trade_ideas
	quantity_ideas
	leadership_ideas
	logistic_ideas
	economic_ideas
	administrative_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Altan #0" = 20
	"Baaragay #0" = 10
	"Keltegey #0" = 10
	"Tobuk #0" = 20
	"Badzhey #0" = 20
	"Bakhsy #0" = 20
	"Baltuga #0" = 20
	"Batulin #0" = 20
	"Batur #0" = 10
	"Bayan #0" = 20
	"Bayagantay #0" = 20
	"Bedzheke #0" = 10
	"Buruhay #0" = 10
	"Burukhu #0" = 10
	"Challay #0" = 10
	"Cheriktey #0" = 20
	"Dachik #0" = 20
	"Djennik #0" = 10
	"Dyupsin #0" = 10
	"Ellei #0" = 10
	"Elish #0" = 10
	"Haaran #0" = 20
	"Izhill #0" = 10
	"Kurekay #0" = 10
	"Kuogas #0" = 20
	"Kuukey #0" = 20
	"Legey #0" = 20
	"Odeu #0" = 15
	"Omogai #0" = 20
	"Omolon #0" = 10
	"Orukan #0" = 10
	"�krey #0" = 10
	"Malozherar #0" = 20
	"Macha #0" = 20
	"Masary #0" = 10
	"Mene #0" = 10
	"Milach #0" = 10
	"Mod'ogor #0" = 10
	"Munn'an #0" = 10
	"Myryla #0" = 10
	"Noguy #0" = 10
	"Usun-Oyuun #0" = 10
	"Uluu Horo #0" = 0
	"Semen #0" = 0
	"Serguy #0" = 0	
	"Sohhor #0" = 0
	"Taragay #0" = 0
	"Tarkan #0" = 0
	"Thatta #0" = 0	
	"Tygyn #0" = 0

	"Chang #0" = -1
	"Chyngis #0" = -1
	"Nyka #0" = -1
	"Harahsyn #0" = -1
	"Sahsary #0" = -1
	"Taalay #0" = -1
}

leader_names = {
	Darkhad Batulin Betan Borogon Khangalas Duuray Levental Megin
}

ship_names = {
	Yingchang Karakorum Hohhot Chagaan
	"Ghengis Khan" J�chi �g�dei Tolui
	M�ngke Qubilai H�leg� B�ke
	Jinggim Kamala Dharmapala "�r�g Tem�r"
	Adai Elbeg Batu Mugali "Bo'orchu" Borokhula Chilaun
	Khubilai Jelme Jebe Subutai
	"Yel� Chucai" Shikhikhutug "Sorqan Shira" "Khar Khiruge"
	"D�rben K�l�'�d" "D�rben Noyas"
}
