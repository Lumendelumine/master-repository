#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 155 0 0 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	administrative_ideas
	merchant_marine_ideas
	quantity_ideas
}

historical_units = { #Indonesian Muslim Group
	indonesian_simbalan_infantry
	indonesian_arab_horse
	indonesian_stockade_infantry
	indonesian_heavy_elephant_cavalry
	indonesian_armored_infantry
	indonesian_tumandar_cavalry
	indonesian_arquebusier_infantry
	indonesian_tabinan_cavalry
	indonesian_mansabdar_cavalry
	indonesian_matchlockmen_infantry
	indonesian_ekanda_cavalry
	indonesian_native_musketeer_infantry
	indonesian_regular_infantry
	indonesian_bayonet_infantry
	western_carabinier_cavalry
	indonesian_drill_infantry
	western_hunter_cavalry
	indonesian_impulse_infantry
	western_lancer_cavalry
	indonesian_breech_infantry
	}

leader_names = {
	Swarga Bagus Sugih "Bangun Tapa" Purbaya Angabehi "Bangun Kadaton" Wicaksana Ngabehi Tedjawulan
}

monarch_names = {
	"Pakubwono #4" = 10
}

ship_names = {
	Surakarta Salatiga
	"Gunung Merbabu" Sragen Sukoharjo Wonosari
	"Gunung Lawu" Delanggu Cepu Purwodarli
	Ngawi Grobogan Boja Amgarawa
}
