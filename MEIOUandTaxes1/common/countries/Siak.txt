#Country Name: Siak
#Tag: SIJ
#Siakis used to represent Siak + Jambi
#MEIOU-FB Indonesia mod v3 - for IN+JV
#FB-TODO colour

graphical_culture = indiangfx

color = { 9  127  38 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	administrative_ideas
	merchant_marine_ideas
	quantity_ideas
}

historical_units = { #Indonesian Non-Muslim Group
	indonesian_simbalan_infantry
	indonesian_war_elephant_cavalry
	indonesian_stockade_infantry
	indonesian_heavy_elephant_cavalry
	indonesian_armored_infantry
	indonesian_armored_elephant_cavalry
	indonesian_lantaka_infantry
	indonesian_kshatriya_cavalry
	indonesian_silhedar_cavalry
	indonesian_matchlockmen_infantry
	indonesian_ekanda_cavalry
	indonesian_carabao_guard_infantry
	indonesian_platoon_infantry
	indonesian_skirmisher_infantry
	western_carabinier_cavalry
	indonesian_rifled_infantry
	western_hunter_cavalry
	indonesian_impulse_infantry
	western_lancer_cavalry
	indonesian_breech_infantry
	}

monarch_names = {
	"'Abdu'l 'Alam #0" = 20
	"'Abdu'l Jali #0" = 20
	"'Abdu'l Mu'azzam #0" = 20
	"Aladdin #0" = 20
	"Ibrahim 'Abdu'l #0" = 20
	"Iskander #0" = 20
	"Ismail 'Abdu'l #0" = 20
	"Jalil Jalal #0" = 100
	"Jalil Khalil #0" = 100
	"Jalil Muzaffar #0" = 100
	"Jamal #0" = 20
	"Khoja Ahmad #0" = 20
	"Kiai Gede #0" = 20
	"Mahmud 'Abdu'l #0" = 20
	"Mara #0" = 20
	"Mudari #0" = 20
	"Muhammad 'Ali #0" = 20
	"Muzaffar #0" = 20
	"Rahmad #0" = 20
	"Ri'ayat #0" = 20
	"Sulaiman Muhammad #0" = 20
	"Yahya 'Abdu'l #0" = 20
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi
	Batuta
	Chairul
	Hamzah Hasanuddin
	Ibnu
	Kyai
	Mahat Mukhtar
	Najamuddin Natzar Nazmizan
	Roem
	Syarifuddin
	Zulkifli
}

ship_names = {
	Balla Bulu Lompo Sallo Karaeng Bambang
	Cipuru Doe Iyo Tena Tabe Apa Lakeko Battu Keko
	Tunitangkalopi Batara Gowa Djallori Parisi Palonga
	Butta "Karaeng Tunibatta" Parabung Pasulu
	al-Khayr al-Mans�r al-Qudus
	Abdulfatah Ahmad Amir Anwar Azmi
	Najamuddin Natzar Nazmizan
}

