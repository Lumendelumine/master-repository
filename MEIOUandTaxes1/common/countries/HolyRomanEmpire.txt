graphical_culture = westerngfx

color = { 150  177  161 }

historical_idea_groups = {
	quality_ideas
	economic_ideas
	aristocracy_ideas
	diplomatic_ideas
	logistic_ideas
	trade_ideas
	leadership_ideas
	administrative_ideas
}

historical_units = { #North German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	unique_PRU_frederickian_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	unique_PRU_oblique_infantry
	western_uhlan_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}


monarch_names = {
	"Friedrich #2" = 60
	"Albrecht #1" = 50
	"Friedrich #2 Wilhelm" = 40
	"Friedrich #2 August" = 30
	"Ferdinand #0" = 30
	"Wilhelm #0" = 30
	"Karl #4" = 20
	"Christian #0" = 20
	"Johann #0 Georg" = 20
	"Maximilian #0" = 20
		  "Franz #0" = 20
		  "Joseph #0" = 20
		  "Heinrich #7" = 15
	"Karl #4 Albrecht" = 15
	"Karl #4 Theodor" = 15
	"Friedrich #2 Albrecht" = 15
	"Friedrich #2 Christian" = 15
	"Albrecht #1 Achilles" = 15
	"August #0" = 15
	"Ernest #0" = 15
	"Ferdinand #0 Maria" = 15
	"Franz #0 Stefan" = 15
	"Georg #0" = 15
	"Georg #0 Wilhelm" = 15
	"Joachim #0 Hector" = 15
	"Joachim #0 Nestor" = 15
	"Johann #0" = 15
	"Johann #0 Cicero" = 15
	"Johann #0 Friedrich" = 15
	"Johann #0 Siegmund" = 15
	"Ladislav #0" = 15
	"Leopold #0" = 15
	"Matthias #0" = 15
	"Mortiz #0" = 15
	"Siegmund #0" = 15
	"Karl #4 Joseph" = 10
		  "Ludwig #4" = 10
	"August #0 Wilhelm" = 10
	"Wilhelm #0 Friedrich" = 10
	"Alexander #0" = 10
	"Anton #0" = 10
	"Christian #0 Albrecht" = 10
	"Hektor #0" = 10
	"Johann #0 Karl" = 10
	"Johann #0 Leopold" = 10
	"Leopold #0 Ferdinand" = 10 
	"Leopold #0 Johann" = 10
	"Maximilian #0 Philipp" = 10
	"Ruprecht #0" = 10
	"Wenzel #0" = 10
}

leader_names = { 
	Adam Angermann Appelt Arndt Beck Becker Bening Blasius
	Brilz Br�ser B�ttner Carl Dahm Deneke Eckstein Edelmann 
	Elsner Feil Fischer Franke Giewer Haase Hafner Hertel 
	Hirsch H�hn Kammel Keller Kessler Koeppe Kohn Kr�mer
	Kuhn Lindner Mertesdorf Meyer Mick M�ller Necker Noell 
	Oelgem�ller Raabe Rainer Scharfbillig Schave Scheler 
	Schmitt Schneider Schumann Stephan Thelen Trierweiler
	Uhlig "von Franken" "von Rothenburg" "von Schwaben"
	"von Vitzgau" "von Weimar" Wachsmuth Wacht "von Werl"
	Walther Wecker
}

ship_names = {
	Ariadne Habicht Adler Albatross Amazone
	Arcona Basilisk Beowulf Biene Wespe Wolf
	Nautilus Natter Otter Pelikan Pfeil Meteor
	M�we M�cke Luchs K�nig Kaiser Eber Drache
	Falke Frauenlob Freya Fuchs Gazelle Geier
	Greif Condor Cormoran Comet Wacht Tiger
	Seeadler Jaguar Hy�ne Blitz Bremse Brummer
	Bussard Vulkan
}

army_names = {
	"Kaiserliche Armee" "Armee von $PROVINCE$" 
}

fleet_names = {
	"Kaiserliche Flotte"
}