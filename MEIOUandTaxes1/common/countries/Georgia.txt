# Georgia
# GEO

graphical_culture = easterngfx

color = { 224  107  96 }

historical_idea_groups = {
	logistic_ideas
	popular_religion_ideas
	economic_ideas
	quality_ideas
	diplomatic_ideas
	aristocracy_ideas
	administrative_ideas
	spy_ideas
}

historical_units = { #Caucasian group
	eastern_mourtatoi_infantry
	eastern_mountain_nomad_cavalry
	eastern_skoutatoi_infantry
	eastern_pronoia_cavalry
	eastern_hand_gunner
	eastern_deli_cavalry
	eastern_draby_infantry
	eastern_cossack_cavalry
	eastern_drabant_infantry
	eastern_light_cossack_cavalry
	eastern_grenadier_infantry
	eastern_pancerni_cavalry
	eastern_platoon_infantry
	eastern_carabinier_cavalry
	eastern_skirmisher_infantry
	eastern_uhlan_cavalry
	eastern_rifled_infantry
	eastern_impulse_infantry
	eastern_lancer_cavalry
	eastern_breech_infantry
}

monarch_names = {
	"Giorgi #6" = 120
	"David #9" = 60
	"Vakhtang #3" = 60
	"Bagrat #5" = 40
	"Alexander #0" = 20
	"Erekle #0" = 40
	"Konstantine #0" = 40
	"Luarsab #0" = 40
	"Simon #0" = 40
	"Teimuraz #0" = 40
	"Archil #0" = 20
	"Bakar #0" = 20
	"Iesse #0" = 20
	"Kaihosro #0" = 20
	"Rostom-Mirza #0" = 20
	"Levan #0" = 15
	"Demetre #2" = 10
	"Thamar #1" = -10
	"Anna #0" = -10
	"Stefanoz #0" = 10
	"Anzori #0" = 0
	"Bebur #0" = 0
	"Birtveli #0" = 0
	"Djurkha #0" = 0
	"Gulikho #0" = 0
	"Kartlos #0" = 0
	"Koki #0" = 0
	"Kviria #0" = 0
	"Lukhum #0" = 0
	"Malkhazi #0" = 0
	"Mushni #0" = 0
	"Okhropir #0" = 0
	"Orbeli #0" = 0
	"Revaz #0" = 0
	"Takha #0" = 0
	"Torgva #0" = 0
	"Tornike #0" = 0
	"Tsotne #0" = 0
	"Xareba #0" = 0
	"Zviadi #0" = 0
}

leader_names = { 
	Andronikashvili
	Asrenidze Avashvili 
	Bakradze Chkheidze 
	Chkhenkeli Devdariani
	Gabashvili Gardapkhadze
	Gedevanishvili Gegechkori
	Gulisashvili Gvazava
	Javakhishvili Karalashvili
	Kereselidze Kvinitadze 
	Lomtatitdze Maglakelidze
	Mazniashvili Natadze
	Nikoladze Robakidze
	Sharashidze Surguladze
	Takaishvili Tsereteli
	Tsulukidze Vachnadze
	Zaldastanishvili Zhordania
}

ship_names = {
	Aragveti Anacopia Artaani Abkhazeti
	Bichvinta Batomi
	Dmanisi Demetre
	Ereti
	Geguti Gelati
	Kutatsi Kharnabuji Kura
	Lasharisjvari Lore
	Mtkvari
	Narikala
	Ruisi Racha
	Seti Svaneti
	Tamar Tbilisi Telavi Tashiri
	Urbnisi
}
