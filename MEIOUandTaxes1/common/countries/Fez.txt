#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 191  110  62 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	leadership_ideas
	naval_ideas
	quantity_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
}

historical_units = {
	#North African group
	muslim_mashain_walker_infantry
	muslim_arab_horseman
	muslim_crossbow_infantry
	muslim_jinete_cavalry
	muslim_rammaha_lancer_cavalry
	muslim_elchee_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_piyadegan_infantry
	muslim_savaran_saltani_cavalry
	muslim_taifat_al_rusa_infantry
	muslim_hunter_cavalry
	muslim_drill_infantry
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry

}

monarch_names = {
	"Muhammad #2" = 20
	"'Abdall�h #1" = 20
	"'Abd al-Malik #1" = 20
	"Ahmad #0" = 20
	"Sulaym�n #0" = 20
	"Yaz�d #0" = 0
	"Zayd�n #0" = 20
	"Alim #1" = 15
	"al-Wal�d #0" = 0
	"Ataullah #0" = 0
	"Azhar #0" = 0
	"Barakat #0" = 0
	"Dawud #0" = 0
	"Fahd #0" = 0
	"Faraj #0" = 0
	"Faruq #0" = 0
	"Firuz #0" = 0
	"Habib #0" = 0
	"Hafiz #0" = 0
	"Hakim #0" = 0
	"Hashim #0" = 0
	"Ibrahim #0" = 0
	"Imtiyaz #0" = 0
	"Jabril #0" = 0
	"Khalil #0" = 0
	"Latif #0" = 0
	"Lutfi #0" = 0
	"Maram #0" = 0
	"Muhsin #0" = 0
	"Najib #0" = 0
	"Qismat #0" = 0
	"Qusay #0" = 0
	"Rashad #0" = 0
	"Rasul #0" = 0
	"Ridwan #0" = 0
	"Shafaqat #0" = 0
	"Sharif #0" = 0
	"Yasir #0" = 0
	"Y�suf #0" = 0
	"Zulfiqar #0" = 0
}

leader_names = {
	Hadji
	Sediki
	Alami
	Aouita
	Behar
	Berui
	Barek
	Hadda
	Sellami
	Bidouane
}

ship_names = {
	Majid Wadid Hakim Wasi Mujib Raqib
	Karim Jalil Hasib Muqit Hafiz Kabir Ali
	Shakur Ghafur Azim Halim Khabir Latif Adl
}
