#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 175  82  16 }

historical_idea_groups = {
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	trade_ideas
	administrative_ideas
	diplomatic_ideas
	aristocracy_ideas
	spy_ideas
}

#vietnamese group
historical_units = {
	asian_mixed_crossbow_infantry
	asian_heavy_elephant_cavalry
	asian_war_elephant_cavalry
	asian_fire_lance_infantry
	unique_VIE_huochong_infantry
	asian_tower_elephant_cavalry
	asian_handgunner_infantry
	asian_chong_and_crossbow_infantry
	asian_armored_elephant_cavalry
	asian_chong_infantry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_war_elephant_artillery_cavalry
	asian_matchlock_musketeer_infantry
	asian_platoon_infantry
	asian_carabinier_cavalry
	asian_skirmisher_infantry
	asian_uhlan_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Thai Tong #0" = 20
	"Nguyen Duc Trung #0" = 20
	"Nguyen Van-Lang #0" = 20
	"Nguyen Hoang Du #0" = 20
	"Nguyen Kim #0" = 20
	"Nguyen Hoang #0" = 20
	"Nguyen Phuc-Nguyen #0" = 20
	"Nguyen Phuc-Lan #0" = 20
	"Nguyen Phuc-Tan #0" = 20
	"Nguyen Phuc-Tran #0" = 20
	"Nguyen Phuc-Chu #0" = 20
	"Nguyen Phuc-Khoat #0" = 20
	"Nguyen Phuc-Thuan #0" = 20
	"Nguyen Phuc-Anh #0" = 20
	"Nguyen Van-Hue #0" = 20
	"Nguyen Quang-Toan #0" = 20
	"Vi Muc De #0" = 20
	"Hien Tong #0" = 20
	"Thanh Tong #0" = 20
	"Nahn Tong #0" = 20
	"Thai To #1" = 20
	"Hau Tran #1" = 20
	"Chan-Tong #0" = 20
	"Nguyen Minh-Tri #0" = 20
}

leader_names = {
	Huynh 
	Le 
	Mac 
	Ng Nguyen Nguyen-Dinh Nguyen-Khoa Nguyen-Tan 
	Pham 
	Tran 
}

ship_names = {
	"Chiang-Mai"
	"Do Thu"
	"Kuan-yin"
	"T'an Minh" "T'an Lung"
	"T'an Bao" "Tai'Li"
	"Thanh Hoa"
	"Nan Chao"
	"Nam Pung"
}
