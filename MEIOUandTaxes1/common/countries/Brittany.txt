#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 118  99  151 }

historical_idea_groups = {
	naval_ideas
	trade_ideas
	administrative_ideas
	naval_quality_ideas
	exploration_ideas
	innovativeness_ideas
	quality_ideas
	diplomatic_ideas
}

historical_units = { #French group
	western_halberd_infantry
	western_knight_cavalry
	unique_FRA_chevalier_cavalry
	western_manatarms_infantry
	western_runner_cavalry
	western_lance_infantry
	western_gendarme_cavalry
	western_pikeandshot_infantry
	western_earlytercio_infantry
	western_caracole_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	western_carabinier_cavalry
	western_drill_infantry
	western_hunter_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_jominian_infantry
}


monarch_names = {
	"Fran�ois #0" = 60
	"Jean #4" = 40
	"Artur #2" = 20
	"Pierre #2" = 20
	"Anne #0" = -15
	"Claudine #0" = -10
	"Henri #0" = 10
	"Charles #1" = 5
	"Gilles #0" = 5
	"Richard #0" = 5
	"Corentin #0" = 0
	"Deniel #0" = 0
	"Dinan #0" = 0
	"Erwan #0" = 0
	"Ga�l #0" = 0
	"Gwena�l #0" = 0
	"Gwenneg #0" = 0
	"Gwilherm #0" = 0
	"Iouennic #0" = 0
	"Lo�c #0" = 0
	"Maeldan #0" = 0
	"Mikael #0" = 0
	"Olier #0" = 0
	"Padrig #0" = 0
	"Paol #0" = 0
	"Patern #0" = 0
	"Pier #0" = 0
	"Primel #0" = 0
	"Roparzh #0" = 0
	"Servan #0" = 0
	"Similien #0" = 0
	"Sklaer #0" = 0
	"Sulio #0" = 0
	"Uuinmael #0" = 0
	"Venec #0" = 0
	"Visant #0" = 0
	"Vonig #0" = 0
	"Vougay #0" = 0
	"Winifrid #0" = 0
	"Yann #0" = 0
}

leader_names = {
		  d'Argentr�
		  "de Bain" "de Beaudiez" "de Beaumetz" "de Boisgu�henneuc" "de Bou�tiez"
		  Carne "du Cou�dic" "de Cardelan" "de Chateaubriand" "de Ch�tillon" "de Coatgoureden" "de Co�tivy" "de Co�tmen" "Couasnon de la Barilli�re" "de Craon" 
		  "de Dol"
		  d'Elb�ne
		  "Guivarc'h" "de Gourcuff" Gourong
		  "de Halgoet" "de la Haye Saint-Hilaire"
		Jaouen
		  Kerouac "de Kerdavid" "de Kergariou" "de Kerdrel" Kervarker "de Kerdrel" "de Kerellec" "de Kergoulay" "de Kerguenech" "de Kerguerron" "de Kersauzon" "de Kersulguen" 
		  "de Lamballe" "de Locmaria" 
		  Morvan "de Machecoule" Madec "de Malestroit" "de Mercoeur" "de Monfort"
		  "de la Nou�-Bogat"
		  Prigent Penteur Penfentenyo "du Porho�t" "de Penguern" "de Penhoadic" "de Penthi�vre" "de Pontbriand" 
		  Rohan Rolland "de Rodellec" "de Rohan" "de Roscoat" "de Rostrenen"
		  Sala�n "de Saint Hoularn" "de Saint Meleuc" "de S�vign�"
		  Touronce "de Talhou�t" "de Tasher de la Pagerie" "de Tint�riac" "de Tonquedec" "de Tournemine" "de Tr�mereuc" "de la Tr�moille" "de Trogoff" "de Tromelin"
		  "de Vitr�"
}

ship_names = {
		  Achille Actif Active Admirable Agla�
		  Aimable Alcide Alg�siras Alose Amazone
		  Ambition Amphion An�mone Annibal Apollon
		  Argonaute Art�sien Audacieux Auguste Avenant
		  Badine Barbue Beaumont B�casse Belliqueux
		  Berg�re Berryer "Bien Aim�" Bienfaisant Bon
		  Bretagne Bretonne Brillant Brutus Cach�
		  C�l�bre Centaure Chameau Comte Courageux
		  D�fenseur Diad�me Diamant Dictateur Diligent
		  Dragon Duc Elephant Elisabeth Emerillon
		  Entreprenant Eole Fleuron Formidable Fortun�
		  Foudroyant Fougueux Fourgon Furieux Glorieux
		  "Grande Hermine" Imp�tueux Indien Indomptable Jupiter
		  L�opard Loire Massiac Neptune Nestor
		  Orion P�gase "Petite Hermine" Portefaix Profond
		  Prot�e Rolland "Saint Guillaume" Sarcelle S�v�re
		  Six-Corps Solide Solitaire Souverain Sph�re
		  Sphinx Superbe Tardif T�m�raire Terrible
		  Th�s�e Tonnant Triomphant Triton Union
		  Vaillant Vengeur Victoire Vigilant Volante
}

army_names = {
		  "Arm�e du Tr�gor"		 "Arm�e de Cornouaille"  "Arm�e du L�on"			
		  "Arm�e du Vannetais"	 "Arm�e du Pays de Dol"  "Arm�e du Pays de Saint Brieuc"
		  "Arm�e du Pays Rennais" "Arm�e du Pays Nantais" "Arm�e du Pays de Saint Malo"
		  "Arm�e de Bro�rec"		"Arm�e de Tr�guier"	  "Arm�e de Penthi�vre"
		  "Arm�e de Porho�t"
		"Arm�e de $PROVINCE$" 
}
