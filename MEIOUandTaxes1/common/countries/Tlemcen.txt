#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 93  160  163 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	leadership_ideas
	naval_ideas
	quantity_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
}

historical_units = {
	#North African group
	muslim_mashain_walker_infantry
	muslim_arab_horseman
	muslim_crossbow_infantry
	muslim_jinete_cavalry
	muslim_rammaha_lancer_cavalry
	muslim_elchee_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_piyadegan_infantry
	muslim_savaran_saltani_cavalry
	muslim_taifat_al_rusa_infantry
	muslim_hunter_cavalry
	muslim_drill_infantry
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry

}

monarch_names = {
	"Muhammad #0" = 80
	"Ahmad #0" = 50
	"M�s� #0" = 20
	"'Abdall�h #0" = 20
	"Ab� T�shuf�n #0" = 20
	"al-Hasan #0" = 20
	"'Abd ar-Rahm�n #0" = 10
	"'Abd al-W�hid #0" = 10
	"Sa'�d #0" = 10
	"Y�suf #0" = 10
	"Baba Aruj #0" = 10
	"Khidr Khair ad-Din #0" = 10
	"Selim #0" = 10
	"'Uthm�n #0" = 5
	"Yaghmuras�n #0" = 5
	"'Abd al-Karim #0" = 0
	"Barakat #0" = 0
	"Basir #0" = 0
	"Dawud #0" = 0
	"Fahd #0" = 0
	"Faris #0" = 0
	"Fuad #0" = 0
	"Haidar #0" = 0
	"Hashim #0" = 0
	"Husam #0" = 0
	"Iqbal #0" = 0
	"Jabbar #0" = 0
	"Jamal #0" = 0
	"Junayd #0" = 0
	"Malik #0" = 0
	"Mustafa #0" = 0
	"Najib #0" = 0
	"Qasim #0" = 0
	"Rahim #0" = 0
	"Rash�d #0" = 0
	"Safi #0" = 0
	"Sakhr #0" = 0
	"Umar #0" = 0
	"Wahid #0" = 0
	"Zahir #0" = 0
}

leader_names = {
	Selmi
	Amara
	Behar
	Ilaes
	Benzine
	Essaid
	Hacini
	Benzai
	Kelkal
	Merah
}

ship_names = {
	Barr Muta'al Wali Batin Zahir Akhir _Awwal
	Mu'akhkhir Muqaddim Muqtadir Qadir Samad
	Wahid Majid Wajed Qayyum Hayy Mumit Muhyi
	Mu'id Mubdi Muhsi Hamid Waliyy Matin Qawiyy
	Wakil Haqq Shahid Ba'ith
}
