#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 203  144  17 }

historical_idea_groups = {
	aristocracy_ideas
	trade_ideas
	popular_religion_ideas
	diplomatic_ideas
	administrative_ideas
	logistic_ideas
	economic_ideas
	spy_ideas
}

historical_units = { #North German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	unique_PRU_frederickian_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	unique_PRU_oblique_infantry
	western_uhlan_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}


monarch_names = {
	"Wilhelm #0" = 20
	"Ludwig #0" = 20
	"Friedrich #0" = 20
	"Karl #0" = 20
	"Moritz #0" = 20
	"Philipp #0" = 20
	"Hermann #2" = 20
	"Heinrich #1" = 20
	"Otto #1" = 20
	"Adolf #0" = 20
	"Christian #0" = 20
	"Georg #0" = 20
	"Leopold #0" = 20
	"Maximilian #0" = 20
	"Philipp Ludwig #0" = 20
	"Adelmar #0" = 20
	"Albrecht #0" = 20
	"Barnabas #0" = 20
	"Denis #0" = 20
	"Egon #0" = 20
	"Erich #0" = 20
	"Gerhard #0" = 20
	"Gottlieb #0" = 20
	"Gregor #0" = 20
	"Hagan #0" = 20
	"Helmut #0" = 20
	"Isaak #0" = 20
	"Isidor #0" = 20
	"Ivo #0" = 20
	"Joachim #0" = 20
	"Joseph #0" = 20
	"Kreszens #0" = 20
	"Lothar #0" = 20
	"Oskar #0" = 20
	"Siegfried #0" = 20
	"Stefan #0" = 20
	"Thomas #0" = 20
	"Wenzel #0" = 20
	"Werner #0" = 20
	"Wolfram #0" = 20
}

leader_names = {
	Arnsberg
	Bebra
	Eder
	Fulda
	Hamm
	Kassel Knyphausen
	Lippstadt
	M�nden
	Paderborn
	"von Scheideck"
}

ship_names = {
	"Heinrich I" Kassel Werra Lahn Fulda
	Wasserkuppe Stirnberg Hanau Marburg Wetzlar
}

army_names = {
	"Armee von $PROVINCE$" 
}
