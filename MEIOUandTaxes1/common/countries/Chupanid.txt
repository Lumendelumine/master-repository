#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 106  68  172 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Persian Group
	muslim_arab_horseman
	muslim_halqa_irregular_infantry
	muslim_halqa_regular_infantry
	muslim_mongol_cavalry
	muslim_qizilbash_cavalry
	muslim_ghulam_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_gharachoorlooha_musketeer_infantry
	muslim_two_horned_tofangchi_infantry
	muslim_uhlan_cavalry
	muslim_mass_infantry_tactics
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry
}


monarch_names = {
	"Husain #1" = 20
	"Uwais #1" = 20
	"Ahmad #0" = 20
	"Mahmud #0" = 20
	"Muhammad #0" = 20
	"Shah Walad #0" = 20
	"Hasan #1" = 15
	"Rash�d #0" = 15
	"Isma'il #0" = 10
	"Qasim #0" = 10
	"'Abbas #0" = 0
		  "'Abd al-Karim #0" = 0
	"'Abd al-Rahman #0" = 0
	"Anwar #0" = 0
	"Atuf #0" = 0
	"Bakr #0" = 0
	"Fahd #0" = 0
	"Faruq #0" = 0
	"Fouad #0" = 0
	"Hakim #0" = 0
		  "Harun #0" = 0
	"Hikmat #0" = 0
	"Hisham #0" = 0
	"Iqbal #0" = 0
	"Jafar #0" = 0
		  "Jamaal #0" = 0
	"Karim #0" = 0
	"Khalil #0" = 0
		  "Mal�k #0" = 0
	"Mirza #0" = 0
	"Najib #0" = 0
	"Rusul #0" = 0
	"Shimun #0" = 0
	"Tar�q #0" = 0
	"Usama #0" = 0
	"Yasir #0" = 0
	"Yusuf #0" = 0
	"Zaid #0" = 0
	"Z�yad #0" = 0
	"Zulqifar #0" = 0
}

leader_names = {
	Rukh
	Jahagir
	Sa'id
	Uways
	Hussain
	Ahmad
	Mahmud
	Mirza
	Mohsin
	Zaman
}

ship_names = {
	Ardabil
	Bakhura Baluches Balkh
	Herat
	Ispahan
	Kirman Kish Kharesm Kandahar
	Lahore
	Merv
	Nishupur
	Ormuz
	Rustam
	Sikandar Sebzewar Slurax Samarqand
	Tahriz
	"Shah Rukh"
}
