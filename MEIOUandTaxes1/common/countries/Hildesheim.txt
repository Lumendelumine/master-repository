#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 105  10 23 }

historical_idea_groups = {
	administrative_ideas
	trade_ideas
	logistic_ideas
	economic_ideas
	innovativeness_ideas
	diplomatic_ideas
	spy_ideas
	plutocracy_ideas
}

historical_units = {
	western_spearman_infantry
	western_knight_cavalry
	western_runner_cavalry
	western_manatarms_infantry
	western_pike_infantry # enable = swiss_landsknechten
	western_tercio_infantry # enable = dutch_maurician
	western_volley_infantry # enable = austrian_tercio
	western_platoon_infantry # austrian_grenzer
	western_mediumhussar_cavalry # austrian_hussar
	western_mass_infantry # prussian_frederickian
	# western_uhlan_cavalry # prussian_uhlan
	western_impulse_infantry # mixed_order_infantry
	western_uhlan_cavalry # open_order_cavalry
	western_breech_infantry # napoleonic_square
	western_lancer_cavalry # napoleonic_lancers
}

monarch_names = {
	"Balduin #0" = 20
	"Heinrich #2" = 20
	"Simon #1" = 20
	"Ruprecht #0" = 20
	"Johann #0" = 20
	"Bertrando #0" = 20
	"Wilhelm #0" = 20
	"Dietrich #2" = 20
	"Hermann #0" = 20
	"Erich #0" = 20
	"Rembert #0" = 20
	"Salentin #0" = 20
	"Ferdinand #0" = 20
	"Dietrich Adolf #0" = 20
	"Hermann Werner #0" = 20
	"Franz Arnold #0" = 20
	"Clemens August #0" = 20
	"Wilhelm Anton #0" = 20
	"Friedrich Wilhelm #0" = 20
	"Franz Egon #0" = 20
}

leader_names = {
	Arnsberg
	Bebra
	Eder
	Fulda
	Hamm
	Kassel Knyphausen
	Lippstadt
	M�nden
	Paderborn
	"von Scheideck"
}

army_names = {
	"Armee von $PROVINCE$" 
}
