#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 59 0 177 } 

historical_idea_groups = {
	quality_ideas
	economic_ideas
	aristocracy_ideas
	diplomatic_ideas
	logistic_ideas
	trade_ideas
	leadership_ideas
	administrative_ideas
}

historical_units = { #South German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_skirmisher_infantry
	western_mediumhussar_cavalry
	western_mass_infantry
	western_uhlan_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_jominian_infantry
}

monarch_names = {
	"Fryderyk #0" = 80
	"Jan #0" = 40
	"Henryk #6" = 20
	"Ludwik #2" = 20
	"Jan Krystian #0" = 20
	"Jerzy #0" = 20
	"Jerzy Rudolf #0" = 20
	"Jerzy Wilhelm #0" = 20
	"Joachim Fryderyk #0" = 20
	"Krystian #0" = 20
	"Jan Jerzy #0" = 10
	"Jerzy Ernest #0" = 10
	"August #0" = 5
	"Ernest #0" = 5
	"Krystian Ludwik #0" = 5
	"Rudolf #0" = 5
	"Ruprecht #1" = 1
	"Wladislaw #1" = 1
	"Albert #0" = 0
	"Aleksander #0" = 0
	"Andel #0" = 0
	"Branko #0" = 0
	"Bruno #0" = 0
	"Cezar #0" = 0
	"Dietrich #0" = 0
	"Eugeniusz #0" = 0
	"Filip #0" = 0
	"Hermann #0" = 0
	"Joachim #0" = 0
	"Krzysztof #0" = 0
	"Lech #0" = 0
	"Libor #0" = 0
	"Markus #0" = 0
	"Maurycy #0" = 0
	"Radomir #0" = 0
	"Siegfried #0" = 0
	"Tom�s #0" = 0
	"V�clav #0" = 0
	"Wilhelm #0" = 0
}

leader_names = { 
	Schwarz Hergesell Heintzel Hoppe
	Kahl Tilch Greve Abeln Liegnitz
	Jauernik Wiehle Gronau Sterner
}

ship_names = {
	Teschen Falkenberg "Cosel-Beuthen" Oppeln Ratibor
	Breslau Sagan Oels Steinau "Liegnitz-Brieg" Glogau
	M�nsterberg "Neisse-Ottmachau" "Schweidnitz-Jauer"
}
