# Cajamarca

graphical_culture = southamericagfx

color = { 220  160  5 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	diplomatic_ideas
	logistic_ideas
	economic_ideas
	aristocracy_ideas
	administrative_ideas
	quality_ideas
}

historical_units = {
	southamerican_club_infantry
	southamerican_central_sling_infantry
	southamerican_mixed_sling_infantry
	southamerican_guerilla_infantry
	southamerican_reformed_imperial_infantry
	southamerican_captured_cavalry
	southamerican_westernized_infantry
	southamerican_regular_cavalry
	southamerican_freeshooter_infantry
	southamerican_cuirassier_cavalry
	southamerican_tercio_infantry
	southamerican_irregular_infantry
	southamerican_gallop_cavalry
	southamerican_dragoon_cavalry
	southamerican_countermarch_infantry
	southamerican_volley_infantry
	southamerican_regular_infantry
	southamerican_skirmisher_infantry
	southamerican_bayonet_infantry
	southamerican_armeblanche_cavalry
	southamerican_mass_infantry
	southamerican_hunter_cavalry
	southamerican_impulse_infantry
	southamerican_lancer_cavalry
	southamerican_breech_infantry
}

leader_names = { #Real Aymara names, but with no concept of meaning or gender!
	Achank'Aray Achuqalla Ankalli Apumayta Amaru Axata Champi Champilla Chimakha Chuqimamani Chuqitinta Chuqiwanka Ch'ullqi
	Harawi Hila Ilawi Illatarku Illawara Imiri Inti Iraya Jalaru Jayri Kachi Kalisaya Kunturi Kusisa Khajiri Khantari Lariku Limachi Luk'Ana
	Llanqi Llallawa Llusk'u Majnu Mamani Mayta Mullu Naira Ninaqhipsi Ollantay Paqari Paqu Puma Pumakusi Phaksi Phaksiphat'i Phuyu
	Qalani Qantuta Qawaya Qinaya Qullqi Qhallalli Ruka Saywa Sanka Sillanki Sisa Suxsu Sullkana Tanqara Taypi Titi Thaya T'Alla T'It'U T'Ula
	Umantuu Usnayu Utuya Wallpa Wanka Warachi Way'ch'u Yanawara Yupanki 
}

monarch_names = { #Title is Apu Malku 
		"Illimani #0" = 10 #legendary king
		"Illampu #0" = 10 #legendary king


		"T�pac Amaru #0" = 40
					 "Atahualpa #0" = 20
					 "Hu�scar #0" = 20
					 "Huayna C�pac #0" = 20
					 "Manco C�pac #1" = 20
		"Pachac�tec #0" = 20
		"Sayri Tupac #0" = 20
		"Titu Cusi Yupanqui #0" = 20
					 "T�pac Hualpa #0" = 20
		"T�pac Yupanqui #0" = 20
					 "Viracocha #1" = 10
		"Yahuar Hacuac #1" = 10
		"Inca Roca #1" = 10
		"Anquimarca #0" = 0
		"Atoc Sopa #0" = 0
		"Cayo Topa #0" = 0
		"Coyllas #0" = 0
					 "Cusichaca #0" = 0
		"Guaritopa #0" = 0
		"Huachuri #0" = 0
		"Huaypalcon #0" = 0
		"Luyes #0" = 0
					 "Marcavilca #0" = 0
		"Ninan Cuyuchi #0" = 0
		"Orco Varanca #0" = 0
		"Pascac #0" = 0
		"Pomaguala #0" = 0
		"Pumi Sopa #0" = 0
		"Quispe Titu #0" = 0
					 "Ruminavi #0" = 0
		"Sahuaraura #0" = 0
		"Surandaman #0" = 0
		"Taraque #0" = 0
		"Tucuycuyche #0" = 0
		"Usca Curiatao #0" = 0
		"Villac Umu #0" = 0
		"Yamqui Mayta #0" = 0
					 "Yupanqui #0" = 0
					 "Zambiza #0" = 0
					 "Zopahua #0" = 0
}

ship_names = {
	Apo Apocatequil Ataguchu
	"Ayar Anca" "Ayar Cachi" "Ayar Uchu"
	Catequil Cavillace Chasca "Chasca Coyllur" Coniraya Copacati Cusco
	Ekkeko
	"Hanan Pacha"
	Inti Illapa
	Kon 
	"Manco Capac" "Mama Ocllo" "Mama Huaco" "Mama Raua" "Mama Cura"
	"Mama Coca" "Mama Allpa" "Mama Pacha" "Mama Quilla" "Mama Zara"
	"Pacha Camac" Pachacuti Pariacaca Paricia
	Supay
	Titicaca
	Urcaguary "Uku Pacha"
	Viracocha
}
