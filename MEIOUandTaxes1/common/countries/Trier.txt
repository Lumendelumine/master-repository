#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 208  104  44 }

historical_idea_groups = {
	popular_religion_ideas
	trade_ideas
	administrative_ideas
	logistic_ideas
	diplomatic_ideas
	spy_ideas
	economic_ideas
	aristocracy_ideas
}

historical_units = { #North German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	unique_PRU_frederickian_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	unique_PRU_oblique_infantry
	western_uhlan_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}

monarch_names = {
	"Johann #1" = 100
	"Jakob #0" = 60
	"Clemens Wenzel #0" = 20
	"Franz Georg #0" = 20
	"Franz Ludwig #0" = 20
	"Johann Hugo #0" = 20
	"Johann Ludwig #0" = 20
	"Johann Philipp #0" = 20
	"Karl Joseph #0" = 20
	"Karl Kaspar #0" = 20
	"Lothar #0" = 20
	"Otto #0" = 20
	"Philipp Christoph #0" = 20
	"Rhaban #0" = 20
	"Richard #0" = 20
	"Werner #0" = 20
	"Bohemond #2" = 15
	"Kuno #2" = 15
	"Heinrich #3" = 10
	"Arnold #2" = 10
	"Dieter #1" = 10
	"Alexander #0" = 0
	"Andreas #0" = 0
	"Baldur #0" = 0
	"Bruno #0" = 0
	"Clemens #0" = 0
	"Denis #0" = 0
	"Eckhard #0" = 0
	"Ernst #0" = 0
	"Florian #0" = 0
	"Gregor #0" = 0
	"Guntram #0" = 0
	"Helmfried #0" = 0
	"J�rgen #0" = 0
	"Martin #0" = 0
	"Maximilian #0" = 0
	"Michael #0" = 0
	"Rainer #0" = 0
	"Siegert #0" = 0
	"Siegmund #0" = 0
}

leader_names = {
	Backes Corgell Dahnfels Feltes Haerig Hecker Herrich
	Kettler Lohmann Longnich Pinkernell SChmitt Tristant
	Wellner Zimmer	
}

ship_names = {
	Schweich Kenn Longuich Mertesdorf Kasel
	Waldrach Morscheid Korlingen Gusterath
	Hockweiler Igel Aach Newel Kordel Zemmer
}

army_names = {
	"Erzbischofliche Garde" "Armee von $PROVINCE$" 
}
