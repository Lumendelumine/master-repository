#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 103  44  10 }

historical_idea_groups = {
	naval_ideas
	trade_ideas
	administrative_ideas
	naval_quality_ideas
	exploration_ideas
	innovativeness_ideas
	quality_ideas
	diplomatic_ideas
}

historical_units = { #Iberian group
	western_halberd_infantry
	western_raid_cavalry
	western_runner_cavalry
	western_manatarms_infantry
	western_pike_infantry
	western_jinete_cavalry
	western_pikeandshot_infantry
	western_earlytercio_infantry
	unique_SPA_tercio_infantry
	western_escopeteros_cavalry
	western_tercio_infantry
	unique_POR_tanger_infantry
	unique_SPA_coselete_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_platoon_infantry
	western_line_infantry
	western_carabinier_cavalry
	western_drill_infantry
	western_hunter_cavalry
	western_columnar_infantry
	unique_POR_cacadore_infantry
	western_lancer_cavalry
	western_breech_infantry
	}


monarch_names = {
	"Enrique #1" = 40
	"Carlos #2" = 20
	"Francisco #0 Febo" = 20
	"Juana #2" = -10
	"Juan #1" = 10
	"Luis #1" = 10
	"Andr�s #0 Febo" = 10
	"Catalina #0" = -10
        "Gast�n #0" = 10
	"Leonor #0" = -10
	"Blanca #1" = -10
        "Pedro #1" = 5
	"Buenaventura #0" = 5
        "Francisco #0" = 5
	"Godofredo #0" = 5
	"Jaime #0" = 5
	"Lanzarote #0" = 5
	"Leonel #0" = 5
	"Mart�n #0" = 5
	"Sancho #7" = 1
	"Garc�a #5" = 1
	"Felipe #3" = 1
	"Teobaldo #2" = 1
        "Alfonso #1" = 1
	"Agust�n #0" = 0
	"Alejandro #0" = 0
	"�lvaro #0" = 0
        "Andr�s #0" = 0
	"Antonio #0" = 0
	"Bernardo #0" = 0
	"C�sar #0" = 0
	"Crist�bal #0" = 0
	"Diego #0" = 0
	"Esteban #0" = 0
        "Fernando #0" = 0
	"Gaspar #0" = 0
	"Jos� #0" = 0
	"Melchor #0" = 0
	"Nicol�s #0" = 0
	"Sim�n #0" = 0
}

leader_names = {
	"de Aguirre" "de Albret" "de Antill�n" "de Aranda" "de Armendariz" "de Azagra" "de Azpilcueta"
	"de Beaumont" "de Beortegui" Bergera "de Borb�n" "de Carranza"	Elizalde
	"de Garay" "de Go�i" Goyeneche "de Heredia" "de Hijar"
	"de Jaso" "Jim�nez de Rada" Jimeno "de Land�var" Navarro
	Palafox "P�rez de Zabalza" "de Salazar" "de Uribe" "de Urs�a"
	"de Yanguas" "de Z�rate"
}

ship_names = {
	Atl�ntida "Conde de Evreux" "Conde de Foix" "Conde de Guendul�in" "Conde de Ler�n"
	"Don Jimeno" "Duque de Armagnac" "Duque de Champa�a"
	"El Agramont�s" "El Beaomont�s" "El Bearn�s" "El Cruzado Navarro" "El Monta��s" "El Navarro" "El Salvador" "El Trobador" "El Vi�ador" "Emperador Hisp�nico" Europa
	"Mar de Navarra" "Nuestra Sra de Leyre" "Pr�ncipe de Viana"
	"Reina Juana" "Rey Enrique" "Rey Garc�a" "Rey Sancho" "Reyno de Navarra" "Reyno de Pamplona"
	"San Agust�n" "San Andr�s" "San Antonio" "San Carlos" "San Enrique" "San Esteban" "San Ferm�n" "San Francisco" "San Francisco Xabier" "San Gregorio" "San Juan" "San Le�n" "San Miguel Arc�ngel" "San Raimundo" "San Virila"
	"Santa Ana" "Santa Catalina" "Santa Juana" "Santa Leonor" "Sant�sima Trinidad" "Se�or de Tudela"
	Vasconia
}
