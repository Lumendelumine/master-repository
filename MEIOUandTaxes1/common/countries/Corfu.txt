#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 211  172  185 }

historical_idea_groups = {
	popular_religion_ideas
	naval_ideas
	logistic_ideas
	trade_ideas	
	spy_ideas
	economic_ideas
	plutocracy_ideas
	expansion_ideas
}

historical_units = { #Greek group
	eastern_toxotai_infantry
	eastern_horse_archer_cavalry
	eastern_skoutatoi_infantry
	eastern_pronoia_cavalry
	eastern_latinikon_infantry
	eastern_deli_cavalry
	eastern_draby_infantry
	eastern_pomeste_cavalry
	eastern_drabant_infantry
	eastern_light_cossack_cavalry
	eastern_slavic_western_infantry
	eastern_reiter_cavalry
	eastern_regular_infantry
	eastern_armeblanche_cavalry
	eastern_bayonet_infantry
	eastern_uhlan_cavalry
	eastern_drill_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_breech_infantry
}


monarch_names = {
	"Carlo #0" = 40
	"Leonardo #1" = 20
	"Giovanni #1" = 10
	"Antonio #0" = 10
	"Carlo Antonio #0" = 10
	"Restaiano Gioachinno #0" = 10
	"Akakios #0" = 0
		  "Alexandros #0" = 0
	"Anargyros #0" = 0
	"Anastasios #0" = 0
		  "Andreas #0" = 0
	"Angelos #0" = 0
		  "Aniketos #0" = 0
	"Apostolos #0" = 0
	"Argyros #0" = 0
	"Athanasios #0" = 0
	"Charalampos #0" = 0
	"Chrysanthos #0" = 0
	"Demetrios #0" = 0
		  "Dimosthenes #0" = 0
	"Dionysios #0" = 0
	"Dorotheos #0" = 0
	"Efsthathios #0" = 0
	"Eleftherios #0" = 0
		  "Elias #0" = 0
	"Emmanouil #0" = 0
	"Evangelos #0" = 0
	"Georgios #0" = 0
	"Gregorios #0" = 0
	"Ioannes #0" = 0
	"Konstantinos #0" = 0
		  "Nikodemos #0" = 0
	"Pavlos #0" = 0
	"Petros #0" = 0
	"Philippos #0" = 0
	"Photios #0" = 0
	"Stephanos #0" = 0
	"Theophylaktos #0" = 0
	"Vasilis #0" = 0
	"Vlasis #0" = 0
}

leader_names = { 
	Balbi Bouas 
	Capodistria 
	Fadrigue Flamburiari
	Gentilini Giallina Giorgio
	Kourkoumelis
	Marmora Minotto
	Soranzo 
	Pieri Prosalendi
	Sordina Spathas
	Theotokis Tocco
	Voulgaris
	Zorzi
}

ship_names = {
	"Akra Mounta" "Akra Liakas" "Akra Gerogompos" "Akra Agiou Andreou"
	"Kaplos Lagana" "Kolpos Argostolion" "Kolpos Myrtou" Kefallonia 
	"Prothumos Zakynthou"
	"Steno Ithoakis"
}
