#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 118  143  45 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	leadership_ideas
	naval_ideas
	quantity_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
}

historical_units = {
	#Persian Group
	muslim_arab_horseman
	muslim_halqa_irregular_infantry
	muslim_halqa_regular_infantry
	muslim_mongol_cavalry
	muslim_qizilbash_cavalry
	muslim_ghulam_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_gharachoorlooha_musketeer_infantry
	muslim_two_horned_tofangchi_infantry
	muslim_uhlan_cavalry
	muslim_mass_infantry_tactics
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry
}


monarch_names = {
	"Ab� Sa'�d #0" = 20
	"B�bur #0" = 20
	"Bad� al-Zam�n #0" = 20
	"Husayn B�yqar� #0" = 20
	"Abu al-Qasim #0" = 20
	"Mahm�d #0" = 20
	"Sh�h Rukh #1" = 10
	"Ulugh Beg #1" = 10
	"'Abbas #0" = 0
		  "'Abd al-Karim #0" = 0
	"'Abd al-Rahman #0" = 0
	"'Al� #0" = 0
		  "'Abdall�h #0" = 0
	"Anwar #0" = 0
	"Bakr #0" = 0
	"Fahd #0" = 0
	"Faruq #0" = 0
	"Fouad #0" = 0
	"Hakim #0" = 0
		  "Harun #0" = 0
	"Hisham #0" = 0
	"Husayn #0" = 0
	"Isma'il #0" = 0
	"Jafar #0" = 0
		  "Jamaal #0" = 0
	"Karim #0" = 0
	"Khalil #0" = 0
		  "Mal�k #0" = 0
	"Mirza #0" = 0
	"Muhammad #0" = 0
	"Qasim #0" = 0
	"Rash�d #0" = 0
		  "Sulayman #0" = 0
	"Tar�q #0" = 0
	"'Umar #0" = 0
	"Usama #0" = 0
	"Yasir #0" = 0
	"Yusuf #0" = 0
	"Zaid #0" = 0
	"Z�yad #0" = 0
	"Zulqifar #0" = 0
}

leader_names = {
	Ali
	Bali
	Karawi
	Khan
	Mirza
	Nawaz
	Quli
	Tadj
	Wali
	Zaman
}

ship_names = {
	Badam
	Bardeskan
	Dasht
	Deyhuk
	Germi
	Jandaq
	Khvor
	Kondor
	Tabas
	Torud
}
