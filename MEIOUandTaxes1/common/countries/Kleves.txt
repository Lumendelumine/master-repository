#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 173  176  113 }

historical_idea_groups = {
	trade_ideas
	innovativeness_ideas
	aristocracy_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
	diplomatic_ideas
	quality_ideas
}

historical_units = { #North German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	unique_PRU_frederickian_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	unique_PRU_oblique_infantry
	western_uhlan_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}


monarch_names = {
	"Johann #0" = 60
	"Adolf #0" = 20
	"Joachim #0" = 20
	"Johann Wilhelm #0" = 20
	"Wilhelm #0" = 20
	"Achilles #0" = 10
	"Karl Friedrich #0" = 10
	"Philipp #8" = 5
	"Engelbert #0" = 5
	"Philipp #0" = 5
	"Albrecht #0" = 0
	"Barnabas #0" = 0
	"Burkhard #0" = 0
	"Denis #0" = 0
	"Eberhard #0" = 0
	"Elmer #0" = 0
	"Erich #0" = 0
	"Gerhard #0" = 0
	"Gottlieb #0" = 0
	"Gregor #0" = 0
	"Hagan #0" = 0
	"Helmut #0" = 0
	"Heribert #0" = 0
	"Isaak #0" = 0
	"Isidor #0" = 0
	"Ivo #0" = 0
	"Joseph #0" = 0
	"Kaspar #0" = 0
	"Kreszenz #0" = 0
	"Lothar #0" = 0
	"Mathias #0" = 0
	"Oskar #0" = 0
	"Reimund #0" = 0
	"Siegfried #0" = 0
	"Stefan #0" = 0
	"Thomas #0" = 0
	"Walter #0" = 0
	"Wenzel #0" = 0
	"Werner #0" = 0
	"Wolfram #0" = 0
}

leader_names = { 
	Angenendt Becker Becking "von Beinom" Contze
	Cronenberg Dennessen Derksen Engelbronner 
	Holler Jenster Kroeger Meurs Nagel Rundt 
	Sinsteden "Van Gelder" "von Oven"
}

ship_names = {
	"Arnold I von Kleve" "Arnold II von Kleve"
	"Adolf von Kleve" "Johann I von Kleve"
	"Johann II von Kleve" Margarete Agnes Elisabeth
	Mathilde "Dietrich von Kleve" "Gerhard von Kleve"
}

army_names = {
	"Armee von $PROVINCE$" 
}