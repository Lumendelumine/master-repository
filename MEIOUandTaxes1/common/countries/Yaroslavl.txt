#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 79  9  182 }

historical_idea_groups = {
	trade_ideas
	quantity_ideas
	expansion_ideas
	aristocracy_ideas
	exploration_ideas
	leadership_ideas
	naval_ideas
	spy_ideas
	economic_ideas
	administrative_ideas
}

historical_units = { #Russian group
	eastern_hospite_infantry
	eastern_druzhina_cavalry
	eastern_bandiera_infantry
	eastern_balkan_feudal_cavalry
	eastern_hand_gunner
	eastern_hussar_horse_archer_cavalry
	eastern_pischalniki_infantry
	eastern_cossack_cavalry
	eastern_streltsy_infantry
	eastern_light_cossack_cavalry
	eastern_soldaty_infantry
	eastern_pancerni_cavalry
	eastern_regular_infantry
	eastern_carabinier_cavalry
	eastern_line_infantry
	eastern_uhlan_cavalry
	eastern_mass_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_jominian_infantry
}


monarch_names = {
	"Aleksandr #2" = 20
	"Dmitriy #1" = 20
	"Ivan #0" = 20
	"Semen #0" = 15
	"Fyodor #2" = 10
	"Vasiliy #2" = 10
	"David #1" = 10
	"Adrian #0" = 0
	"Afanasiy #0" = 0
	"Anatoliy #0" = 0
	"Arseniy #0" = 0
	"Daniil #0" = 0
	"Dorofey #0" = 0
	"Eduard #0" = 0
	"Faddey #0" = 0
	"Feodosiy #0" = 0
	"Feofil #0" = 0
	"Ferapont #0" = 0
	"Foka #0" = 0
	"Foma #0" = 0
	"Gavriil #0" = 0
	"Grigoriy #0" = 0
	"Igor #0" = 0
	"Kliment #0" = 0
	"Mefodiy #0" = 0
	"Mikhail #0" = 0
	"Naum #0" = 0
	"Nikita #0" = 0
	"Nikolai #0" = 0
	"Pankrati #0" = 0
	"Prokhor #0" = 0
	"Radoslav #0" = 0
	"Ruslan #0" = 0
	"Serafim #0" = 0
	"Sevastian #0" = 0
	"Stanislav #0" = 0
	"Timofeiy #0" = 0
	"Varfolomeiy #0" = 0
	"Vikentiy #0" = 0
	"Viktor #0" = 0
}

leader_names = { 
	Alabyshe Alenkin Belsky Velikogagin Golygin Dulov Deyev 
	Zhirovy Zaozersky Zasekin Zubaty Kubensky Kurbsky 
	Mulozhsky Mortkin Okhlyabinin Pnekov Siseev Sitsky Sontsov
	Sudsky Temnosiny Troekurov Ukhorsky Ushaty Yukhotsky Shchetinin
}

ship_names = {
	Danilov Lyubim Myshkin Petrovskoye Rybinsk Uglich
	Borok Itolar Karabikha Borisoglebsky Berendeyevo
	Varegove Tunoshna Kurba Nekouz Pesochnoye Prechistoye
	Nekrasovskoye Semibratovo
}
