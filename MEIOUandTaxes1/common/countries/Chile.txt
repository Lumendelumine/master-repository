#Country Name: Please see filename.

graphical_culture = westerngfx

colonial_parent = SPA

color = { 240  150  150 }

historical_idea_groups = {
	exploration_ideas
	leadership_ideas
	administrative_ideas
	expansion_ideas
	trade_ideas
	quality_ideas
	economic_ideas
	popular_religion_ideas
}

historical_units = {
	western_spearman_infantry
	western_knight_cavalry
	western_runner_cavalry
	western_manatarms_infantry
	western_earlytercio_infantry # spanish_tercio
	western_volley_infantry # enable = austrian_tercio
	western_platoon_infantry # austrian_grenzer
	western_mediumhussar_cavalry # austrian_hussar
	western_line_infantry # austrian_white_coat
	western_uhlan_cavalry # austrian_jaeger
	western_impulse_infantry # mixed_order_infantry
	# western_uhlan_cavalry # open_order_cavalry
	western_breech_infantry # napoleonic_square
	western_lancer_cavalry # napoleonic_lancers
}

monarch_names = {
	"Agust�n #0" = 20
	"Fernando #0" = 20
	"Jos� #0 Miguel" = 20
	"Mart�n #0" = 20
	"Bernardo #0" = 15
	"Francisco #0" = 15
	"Jos� #0 Santiago" = 15
	"Pedro Jos� #0" = 15
	"Antonio Jos� #0" = 10
	"Francisco #0 Antonio" = 10
	"Juan #0" = 10
	"Juan #0 Antonio" = 10
	"Juan #0 Enrique" = 10
	"Mateo #0" = 10
	"Adri�n #0" = 0
	"Alfonso #0" = 0
	"Antonio #0" = 0
	"Benigno #0" = 0
	"Camilo #0" = 0
	"Carlos #0" = 0
	"Desiderio #0" = 0
	"Diego #0" = 0
	"Eduardo #0" = 0
	"Emilio #0" = 0
	"Federico #0" = 0
	"Germ�n #0" = 0
	"Hip�lito #0" = 0
	"Ismael #0" = 0
	"Joaqu�n #0" = 0
	"Jos� #0" = 0
	"Luis #0" = 0
	"Marcelino #0" = 0
	"Nicol�s #0" = 0
	"Ovidio #0" = 0
	"Porfirio #0" = 0
	"Rufino #0" = 0
	"Sergio #0" = 0
	"Te�filo #0" = 0
	"Urbano #0" = 0
	"V�ctor #0" = 0
}

leader_names = {
	�lvarez Argomedo Balmaceda Barros Bello Bilbao Blanco Borgo�o Bulnes
	Carrera "de Carrera" Cochrane "de la Cruz" Echevarr�a Ega�a Err�zuriz Freire
	Henr�quez Herrera Infante "de Irisarri" "de Lasta" Lastarria "de la Lastra"
	Mackenna Mar�n "M�rquez de la Plata" "Mart�nez de Aldunate" "Mart�nez de Rozas" Montt
	O'Higgins Ovalle P�rez Pinto Portales Prieto "de la Reina" Rengifo Rodr�guez Rosales
	"de San Mart�n" Sarmiento Soler Tocornal "de Toro" Varas Vidaurre "de Villegas"
	Za�artu Zenteno
}

ship_names = {
	Abtao
	"Blanco Encalada"
	Chacubuco
	Esmeralda
	Huascar
	Latorre 
	Orella
	Riquelme
	Serrano
	Valparaiso "Virgilio Uribe"
}
	
