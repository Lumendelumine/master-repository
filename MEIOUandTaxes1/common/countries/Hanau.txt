#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 96  131  80 }

historical_idea_groups = {
	trade_ideas
	innovativeness_ideas
	aristocracy_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
	diplomatic_ideas
	quality_ideas
}

historical_units = { #North German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	unique_PRU_frederickian_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	unique_PRU_oblique_infantry
	western_uhlan_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}

monarch_names = {
	"Wilhelm #0" = 170
	"Ludwig #1" = 40
	"Friedrich #0" = 40
	"Hermann #1" = 20
	"Karl #0" = 20
	"Moritz #0" = 20
	"Philipp #0" = 20
	"Heinrich #1" = 15
	"Otto #1" = 15
	"Adolf #0" = 10
	"Christian #0" = 10
	"Georg #0" = 10
	"Leopold #0" = 10
	"Maximilian #0" = 10
	"Philipp Ludwig #0" = 10
	"Adelmar #0" = 0
	"Albrecht #0" = 0
	"Barnabas #0" = 0
	"Denis #0" = 0
	"Egon #0" = 0
	"Erich #0" = 0
	"Gerhard #0" = 0
	"Gottlieb #0" = 0
	"Gregor #0" = 0
	"Hagan #0" = 0
	"Helmut #0" = 0
	"Isaak #0" = 0
	"Isidor #0" = 0
	"Ivo #0" = 0
	"Joachim #0" = 0
	"Joseph #0" = 0
	"Kreszens #0" = 0
	"Lothar #0" = 0
	"Oskar #0" = 0
	"Siegfried #0" = 0
	"Stefan #0" = 0
	"Thomas #0" = 0
	"Wenzel #0" = 0
	"Werner #0" = 0
	"Wolfram #0" = 0
}

leader_names = {
	Arnsberg
	Bebra
	Eder
	Fulda
	Hamm
	Kassel Knyphausen
	Lippstadt
	M�nden
	Paderborn
	"von Scheideck"
}

ship_names = {
	"Heinrich I" Kassel Werra Lahn Fulda
	Wasserkuppe Stirnberg Hanau Marburg Wetzlar
}

army_names = {
	"Armee von $PROVINCE$" 
}