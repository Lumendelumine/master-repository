# Koda.txt
# Tag : KOD
# Tech : steppestech

graphical_culture = muslimgfx

color = { 33  159  74 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Aksha #0" = 20
	"Alacam #0" = 20
	"Bildan #0" = 20
	"Ermak #0" = 20
	"Ichigey #0" = 20
	"Gindin #0" = 20
	"Hynda #0" = 20
	"Ladik #0" = 20
	"Luguy #0" = 20
	"Memruk #0" = 20
	"Moldan #0" = 20
	"Molik #0" = 20
	"Onzha #0" = 20
	"Pevgeyn #0" = 20
	"Rayduk #0" = 20
	"Senger #0" = 20
	"Setanziy #0" = 20
	"Shatrov #0" = 20
	"Taishi #0" = 20
	"Tuchabalda #0" = 20
	"Yangilik #0" = 20
	"Yuvan #0" = 20
	"Yuzor #0" = 20
}

leader_names = {
	Balna Salmiianov Ravna Taiborey Vylka
	Kanin Maimbava Arinin Aven Tsoy
}

ship_names = {
	"Ghengis Khan" Orda Kochu Bayan Sasibuga
	Ilbasan "Mubarrak Kwaja" Chimtai Urus Tuqtaqiya
	"Timur Malik" Toqtamish "Timur Qutlugh" "Shadi Beg"
	Pulad "Jalal Ad-Din" "Karim Berdi" Kebek "Jabbar Berdi"
	"Ulugh Muhammad" "Devlat Berdi" Baraq "Sayyid Ahmad"
}
