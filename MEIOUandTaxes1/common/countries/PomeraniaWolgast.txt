#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 250  220  20 } #�hnlich Elsa�

preferred_religion = protestant

historical_idea_groups = {
	trade_ideas
	innovativeness_ideas
	aristocracy_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
	diplomatic_ideas
	quality_ideas
}

historical_units = { #North German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	unique_PRU_frederickian_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	unique_PRU_oblique_infantry
	western_uhlan_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}


monarch_names = {
	"Boguslaw #9" = 100
	"Barnim #10" = 20
	"Ota #3" = 20
	"Eryk #1" = 20
	"Filip #1" = 20
	"Franciszek #0" = 20
	"Jan Fryderyk #0" = 20
	"Wratislaw #5" = 20
	"Georg #0" = 15
	"Ulrich #0" = 15
	"Kazimierz #5" = 10
	"Ernest Ludwik #0" = 10
	"Jan Ernest #0" = 10
    "Albrecht #0" = 0
	"Arnt #0" = 0
	"August #0" = 0
	"Bernhard #0" = 0
	"Bodo #0" = 0
	"Carl #0" = 0
	"Emil #0" = 0
	"Ernest #0" = 0
	"Fritz #0" = 0
	"Fryderyk #0" = 0
	"Gerhard #0" = 0
	"Gustav #0" = 0
	"Harald #0" = 0
	"Hendryk #0" = 0
	"Herbert #0" = 0
	"Hermann #0" = 0
	"Jan #0" = 0
	"Janusz #0" = 0
	"Jochem #0" = 0
	"Martin #0" = 0
	"Maximiliam #0" = 0
	"Paul #0" = 0
	"Reinhold #0" = 0
	"Richard #0" = 0
	"Siegfried #0" = 0
	"Walter #0" = 0
	"Wilhelm #0" = 0
}

leader_names = {
	"von Krockows" "von Lettows" "von Peglows"
	"von Neustreliz" "von Prenzlau" "von Strelows"
	"von Zitzewitzes" "von Szczecin" Witt Behm
	Beyse Kurzmann J�rgens Stellmacher Heidmann
	Grosch Krumtum Miercke R�hl Wessel Meuseling
	"von Wolin" "von Wolgatz"
}

ship_names = {
	Pommern "Herzog Kasimir" "Herzog Bogislaw"
	"Stettin Herzog Barnim" Wismar Rostock
	Stralsund Greifswald Demmin Anklam "Herzog Otto"
}
