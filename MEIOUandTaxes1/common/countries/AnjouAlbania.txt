# Anjou Kingdom of Albania
# ALC

graphical_culture = easterngfx

color = { 91  72  186 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	aristocracy_ideas
	trade_ideas
	spy_ideas
	economic_ideas
	quantity_ideas
	diplomatic_ideas
}

historical_units = { #Southern Slav group
	eastern_mourtatoi_infantry
	eastern_mountain_nomad_cavalry
	eastern_bandiera_infantry
	eastern_balkan_feudal_cavalry
	eastern_waggonberg_infantry
	eastern_deli_cavalry
	eastern_draby_infantry
	eastern_medium_hussar_cavalry
	eastern_drabant_infantry
	eastern_light_hussar
	eastern_slavic_western_infantry
	eastern_pancerni_cavalry
	eastern_platoon_infantry
	eastern_carabinier_cavalry
	eastern_bayonet_infantry
	eastern_uhlan_cavalry
	eastern_rifled_infantry
	eastern_impulse_infantry
	eastern_lancer_cavalry
	eastern_breech_infantry
}

monarch_names = {
	"Gjergj #0" = 20
	"Lek� #0" = 20
	"Gjon #0" = 15
	"Kostandin #0" = 15
	"Lul�zim #0" = 10
	"Pal #0" = 15
	"Tanush #1" = 10
	"Aleksand�r #0" = 10
	"Karolus #2" = 5
	"Filippo #1" = 5
	"Adnan #0" = 0
	"Agim #0" = 0
	"Bashkim #0" = 0
	"Besnik #0" = 0
	"Bujar #0" = 0
	"Dardan #0" = 0
	"Dhurim #0" = 0
	"Drit�ro #0" = 0
	"Elidon #0" = 0
	"Erjon #0" = 0
	"Fatmir #0" = 0
	"Flamur #0" = 0
	"Florin #0" = 0
	"Hekuran #0" = 0
	"Ilir #0" = 0
	"Ismail #0" = 0
	"Klodian #0" = 0
	"Luigj #0" = 0
	"Milaim #0" = 0
	"Nikolla #0" = 0
	"Pashko #0" = 0
	"P�llumb #0" = 0
	"Perparim #0" = 0
	"Pjet�r #0" = 0
	"Rrok #0" = 0
	"Shp�tim #0" = 0
	"Tahir #0" = 0
	"Viktor #0" = 0
	"Xhevat #0" = 0
	"Ylli #0" = 0
	"Zenel #0" = 0
	 "Gjylo #0" = -1
	 "Halil #0" = 1
	 "Ali #0" = 10
	 "Mustafa #0" = 10
	 "Qemal #0" = 1
	 "Enver #0" = 1
	 "Mehmet #0" = 1
	 "Ramiz #0" = 1
	 "Edi #0" = 1
	 "Demaci #0" = 1
	 "Edon #0" = 1
	 "Dukagjin #0" = 10
	 "Haxhi #0" = 10
	 "Lish�ndri #0" = 1
	 "Mensur #0" = 1
	 "Kreshnik #0" = 1
	 "Rahman #0" = 1
	 "Nxhiku #0" = 1
	 "Riza #0" = 1
	 "Rexhep #0" = 1
	 "Ramadan #0" = 1
	 "Sulejman #0" = 10
	 "Selim #0" = 1
	 "Sadik #0" = 1
}

leader_names = {
	Arianit
	Badera Balsha Bua
	Cervota Comneniates 
	Dukagjini 
	Faqemiri
	Gojcin Golemi
	Kastrioti 
	Losha 
	Musaki 
	Sguros Shpata Spani
	Thopia 
	Toptani
	Vishanj
	Zenevisi Zogu
	 Hoxha
	 �ar�ani
	 Chani
	 Dosti
	 Elezi
	 Duka
	 Fakaj
	 Corbajram
	 Halili
	 Hamza
	 Gecaj
	 Hajdaraga
	 Gjokaj
	 Gjika
	 Shehu
	 Alia
	 Vata
	 Veseli
	 Xhumba
	 Xhaferi
	 Ziberi
	 Useni
	 Ymeri
}

ship_names = {
	"Bishti i Palles"
	"Gjiri i Drinit" "Gjiri i Rondonit"
	Jube
	"Kalaja e Turres" "Kepi i Rodonit" Kyrekuq
	Shengjin Shetaj
	Velipoje
}
