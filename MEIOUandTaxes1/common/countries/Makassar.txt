#Country Name: Makassar
#Tag: MKS
#MEIOU-FB Indonesia mod

graphical_culture = indiangfx

color = { 233  88  88 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	administrative_ideas
	merchant_marine_ideas
	quantity_ideas
}

historical_units = { #Indonesian Non-Muslim Group
	indonesian_simbalan_infantry
	indonesian_arab_horse
	indonesian_stockade_infantry
	indonesian_heavy_elephant_cavalry
	indonesian_armored_infantry
	indonesian_tumandar_cavalry
	indonesian_arquebusier_infantry
	indonesian_tabinan_cavalry
	indonesian_mansabdar_cavalry
	indonesian_matchlockmen_infantry
	indonesian_ekanda_cavalry
	indonesian_native_musketeer_infantry
	indonesian_regular_infantry
	indonesian_bayonet_infantry
	western_carabinier_cavalry
	indonesian_drill_infantry
	western_hunter_cavalry
	indonesian_impulse_infantry
	western_lancer_cavalry
	indonesian_breech_infantry
	}

monarch_names = {
	"Tunitangkalopi #0" = 10
	"Batara Gowa #0" = 10
	"Tuni Djallori #0" = 10
	"Tuni Parisi #0" = 10
	"Tuni Palonga #0" = 10
	"Tuni Butta #0" = 10
	"Tunyachoka-ri #0" = 10
	"Karaeng Tunibatta #0" = 10
	"Tuni Djallo #0" = 10
	"Parabung #0" = 10
	"Tuni Pasulu #0" = 10
	"All�h ad-D�n #0" = 10
	"Muhammad Malik #0" = 10
	"Muhammad Sa'�d #0" = 10
	"Amir Hamza #0" = 10
	"'Abd al-Jal�l #0" = 10
	"Shih�b ad-D�n Ism�'�l #0" = 10
	"Muhammad Bakr Hasan #0" = 10
	"Hamaza Tumammalianga #0" = 10
	"Muhammad 'Ali #0" = 10
	"Fakhr #0" = 10
	"Shahab #0" = 10
	"Siraj ad-D�n #0" = 10
	"'Abd al-Khayr al-Mans�r #0" = 10
	"'Abd al-Qudus #0" = 10
	"'Uthm�n #0" = 10
	"Muhammad Imad #0" = 10
	"Zain #0" = 10
	"'Im�d ad-D�n #0" = 10
	"Zaynal-'Abid�n #0" = 10
	"Sankilang #0" = 10
	"'Abd al-H�d� #0" = 10
	"'Abd al-Khaliq #0" = 10
	"Limbangparang #0" = 10
	"Cinta #0" = -1
	"Wangi #0" = -1
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi
	Batuta
	Chairul
	Hamzah Hasanuddin
	Ibnu
	Kyai
	Mahat Mukhtar
	Najamuddin Natzar Nazmizan
	Roem
	Syarifuddin
	Zulkifli
}

ship_names = {
	Balla Bulu Lompo Sallo Karaeng Bambang
	Cipuru Doe Iyo Tena Tabe Apa Lakeko Battu Keko
	Tunitangkalopi Batara Gowa Djallori Parisi Palonga
	Butta "Karaeng Tunibatta" Parabung Pasulu
	al-Khayr al-Mans�r al-Qudus
	Abdulfatah Ahmad Amir Anwar Azmi
	Najamuddin Natzar Nazmizan
}
