# Denmark

graphical_culture = westerngfx
	
color = { 50  50 0 }

historical_idea_groups = {
	trade_ideas
	innovativeness_ideas
	naval_ideas
	administrative_ideas
	economic_ideas
	leadership_ideas
	diplomatic_ideas
	quality_ideas
}

historical_units = {
	western_spearman_infantry
	western_runner_cavalry
	western_manatarms_infantry
	western_knight_cavalry
	western_tercio_infantry # enable = dutch_maurician
	western_gallop_cavalry # enable = swedish_galoop
	# enable = swedish_gustavian
	western_carabinier_cavalry # swedish_caroline
	western_armeblanche_cavalry # swedish_arme_blanche
	western_mass_infantry # prussian_frederickian
	# western_uhlan_cavalry # prussian_uhlan
	western_impulse_infantry # mixed_order_infantry
	western_uhlan_cavalry # open_order_cavalry
	western_breech_infantry # napoleonic_square
	western_lancer_cavalry # napoleonic_lancers
}

monarch_names = {
	"Christian #0" = 140
	"Frederik #0" = 120
	"Erik #6" = 20
	"Christoffer #2" = 20
	"Hans #0" = 20
	"Adolf #0" = 10
	"Filip #0" = 10
	"Karl #0" = 10
	"Magnus #0" = 10
	"Margaret #0" = -10
	"Maximilian #0" = 10
	"Ulrik #0" = 10
	"Frans #0" = 5
	"Vilhelm #0" = 5
	"Knud #7" = 1
	"Valdemar #4" = 1
	"Harald #3" = 1
	"Oluf #3" = 1
	"Alfred #0" = 0
	"Anton #0" = 0
	"Bent #0" = 0
	"Emanuel #0" = 0
	"Esben #0" = 0
	"Fritjof #0" = 0
	"Frode #0" = 0
	"Gunnar #0" = 0
	"Henrik #0" = 0
	"Holger #0" = 0
	"Jorgen #0" = 0
	"Kasper #0" = 0
	"Peter #0" = 0
	"Poul #0" = 0
	"Rudolf #0" = 0
	"Sigrun #0" = 0
	"Stefan #0" = 0
	"Tobias #0" = 0
	"Torbald #0" = 0
	"Viktor #0" = 0
	"Vilfred #0" = 0
}

leader_names = {
	Adeler Ahlefeldt Akeleye Arenstorff 
	Bille Bielke Brahe Brockenhuus
	Christiansen
	Daa
	Ebbesen Eberstein Ellbrecht 
	Falkenskjold Fischer
	Galt Gersdorff Gjedde Gyldenl�ve Gyldenstjerne G�ye
	Harboe Hardenberg Holstein Huitfeldt H�egh
	Jensen Juel
	Krabbe Krag Krumpen Knudtzon Kvistgaard Kaas
	Lillienskjold Lingby Lunge L�tzow L�ven�rn
	Moltke Moth Munk
	Norby
	Pedersen Povlsen Pallesen Pil
	Rantzau Reventlow Rosenkrantz Rosen�rn
	Schlentz Schack Scholten Sehested Skram
	Thomsen "von Trampe" Trolle
	Ulfeldt
}

ship_names = {
	Achilles "Arche Noa" Arken Beskjermer "Byens L�ffue" Caritas "Charlotte Amalie" 
	Christiania Christiansand Christians� "Christianus IV" "Christianus V" Churprindsen 
	Concordia C�sar Dannebroge David Delmenhorst Ditmarsken Draken "Dronning Louisa" 
	Ebenetzer Elefanten Enighed "Engelske Fortuna" Enhi�rningen Fides Flores 
	"Flygende Hjorten" Fortuna "Fredericus III" "Fredericus IV" Fyen Gravenstein Griffe 
	Gyldenl�ve Hannibal Havfru Havhest Havmand Hector Hercules "Hertug Olufs Pincke" 
	Hi�lperen Hjort H�yenhald Hummer "Hvide �rn" H�jenhald Island Jernv�gen J�germester 
	Justitia "Kongens Jagt Krone" "Kronet Fisk" Lamprenen Leopard Lindorm Loss Lovisa 
	"L�vendals Gallej" Makal�s Mars Merkurius Morian Najaden Nelleblad Neptunus 
	"Norske L�ve" Oldenborg Papegoye Patienta Pelican Ph�nix "Prinds Carl" 
	"Prinds Christian" "Prinds Wilhelm" Raae Samson Solen 
	"Sophia Amalia" "Sophia Hedvig" "St. Erik" "St. Georg" "St. Oluf" Stormar Stralsund 
	Svaerdfisk Svan "Svenske Jomfru" S�ridder Tomler "To L�ver" "Tre L�ver" Trefoldighed 
	Victoria Vindhunden Wenden �rnen
}
