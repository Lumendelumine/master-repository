# Aragon
# ARA

graphical_culture = westerngfx

color = { 166  68  72 }

revolutionary_colors = { 4  8  4 }

historical_score = 500

historical_idea_groups = {
	aristocracy_ideas
	quality_ideas
	popular_religion_ideas
	administrative_ideas
	trade_ideas
	naval_ideas
	expansion_ideas
	merchant_marine_ideas
	logistic_ideas
	innovativeness_ideas
}

historical_units = { #Iberian group
	western_spearman_infantry
	western_raid_cavalry
	western_runner_cavalry
	western_manatarms_infantry
	western_pike_infantry
	western_jinete_cavalry
	western_pikeandshot_infantry
	western_earlytercio_infantry
	unique_SPA_tercio_infantry
	western_escopeteros_cavalry
	western_tercio_infantry
	unique_POR_tanger_infantry
	unique_SPA_coselete_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_platoon_infantry
	western_line_infantry
	western_carabinier_cavalry
	western_drill_infantry
	western_hunter_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_breech_infantry
	}



monarch_names = {
	"Ferran #0" = 40
	"Alfons #4" = 20
	"Joan #1" = 20
	"Carles #0" = 10
	"Pere #4" = 5
	"San� #1" = 5
	"Enric #0" = 5
	"Jaume #2" = 5
	"Mart� #0" = 20
	"Andreu #0" = 0
	"Antoni #0" = 0
	"Arnau #0" = 0
	"Ausi�s #0" = 0
	"Bartomeu #0" = 0
	"Bernat #0" = 0
	"C�sar #0" = 0
	"Dom�nec #0" = 0
	"Felip #0" = 0
	"Francesc #0" = 0
	"Frederic #0" = 0
	"Gaspar #0" = 0
	"Gil #0" = 0
	"Gon�al #0" = 0
	"Guillem #0" = 0
	"Hug #0" = 0
	"Ignasi #0" = 0
	"Ildefons #0" = 0
	"Jeroni #0" = 0
	"Jordi #0" = 0
	"Josep #0" = 0
	"Lloren� #0" = 0
	"Llu�s #0" = 0
	"Pere Llu�s #0" = 0
	"Pon� #0" = 0
	"Ramon #0" = 0
	"Roderic #0" = 0
	"Sime� #0" = 0
	"Tom�s #0" = 0
	"D�dac #0" = 0
	"Garc�a #0" = 0
	"Isabel #0" = -5
	"Joana #0" = -10
	"Mar�a #0" = -5
	"Catalina #0" = -15
	"Petronilla #1" = -10
}

leader_names = {
	"de Alag�n" Amic d'Arag� Arbu�s d'�ustria "de Ayerbe"
	Balaguer Barcel� "de Belchite" Bergued� Bielsa "de Borja" "de Cabrera" Cajal Colom
	Dom�nec d'Emp�ries "Fern�ndez de Hijar" Ferrer "Folc de Cardona" Franco
	Galindo "Gil de Biedma" Gisbert "de Guimer�" "de Gotor" "de Gurrea" d'Ixer Loaisa "de Luna"
	Martorell "de Montcada" Monteagudo Muntaner Oliva Ordov�s d'Ors Pe�a "P�rez de Coloma" Piquer
	"de Requesens" "de la Riba" Ripoll "de Ripperd�" 
	Sastre d'Urgell Vall�s Veldr�n Vilamar� Villarroya "de Zaporta" 
}

ship_names = {
	"Blanca Aurora" "Sant Jacob" Flama Lleona Falc� "La Bonaventura" "Sant Antoni" 
	"Sant Miquel" "Sant Bernat" "Sant Francesc" "Santa �gata" "Santa Eul�lia" 
	"Sant Gabriel" "Sant Climent" "Sant Jordi" "La Reial" Trinitat "Santa Creu" 
	"Santa Maria" "Santa Caterina" "Sant Joan Evangelista" "Santa B�rbara" 
	"Sant Pere de Roma" "La Gasela" "Santa Magdalena" "Sant V�ctor" Magnana 
	"Santa Ventura" "Santa Coloma" "Sant Ambr�s" "La Garsa"  
}

army_names = {
	"Ter� de $PROVINCE$"
}