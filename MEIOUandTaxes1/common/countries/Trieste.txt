# Trieste
# TTE

graphical_culture = westerngfx

color = { 200  80  100 }

historical_idea_groups = {
	trade_ideas
	innovativeness_ideas
	naval_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
	diplomatic_ideas
	quality_ideas
}

historical_units = { #Italian group
	western_spearman_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	unique_GEN_crossbow_infantry
	western_condottieri_cavalry
	western_jinete_cavalry
	western_pike_infantry
	western_stradioti_cavalry
	western_wheellock_infantry
	western_earlytercio_infantry
	western_escopeteros_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_line_infantry
	western_carabinier_cavalry
	western_drill_infantry
	western_lighthussar_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_breech_infantry
}



monarch_names = {
	"Antonio #0" = 50
	"Lodovico #1" = 20
	"Giovanni #5" = 15
	"Filippo #2" = 15
	"Niccol� #1" = 15
	"Pietro #2" = 10
	"Gastone #1" = 10
	"Ottobuono #1" = 10
	"Pagano #1" = 10
	"Raimondo #1" = 10
	"Achille #0" = 0
	"Alberto #0" = 0
	"Benedetto #0" = 0
	"Bonifacio #0" = 0
	"Camillo #0" = 0
	"Claudio #0" = 0
	"Dario #0" = 0
	"Demetrio #0" = 0
	"Emanuele #0" = 0
	"Ermes #0" = 0
	"Fabrizio #0" = 0
	"Felice #0" = 0
	"Francesco #0" = 0
	"Gaetano #0" = 0
	"Geronimo #0" = 0
	"Guido #0" = 0
	"Ignazio #0" = 0
	"Jacopo #0" = 0
	"Leonardo #0" = 0
	"Leopoldo #0" = 0
	"Luchino #0" = 0
	"Marcello #0" = 0
	"Massimiliano #0" = 0
	"Matteo #0" = 0
	"Ottaviano #0" = 0
	"Paolo #0" = 0
	"Raniero #0" = 0
	"Salvatore #0" = 0
	"Timoteo #0" = 0
	"Uberto #0" = 0
	"Vittorio #0" = 0
}

leader_names = {
		  Adelasio Aldini Alessandri "Arborio Mella"
		"Barbiano Belgioioso" Bargnani "Bigliori di Lava" Birago Borromeo Brunetti
		Caetani "di Campofregoso" Caracciolo Carafa "di Card�" "del Carreto" Containi
		Franchi
		Ghislieri Gonzaga "Grimaldi di Busca" "de Guzman"
		Lamberti "de Leiva" Luosi "Luserna Bigliori" "Luserna della Torre"
		Mancini "Medici di Marignano" Melzi "del Monte" Moscati
		Odescalchi
		"Paleologi Montferrato" Paradisi Pico
		Ruga
		Sabbati "di Saluzzo" Savoldi "della Scala" Serbelloni Sfondrati Sforza Sommariva Sopransi Spinola
		Testi Tizzone Visconti
}

ship_names = {
	Alliata "Ariberto d'Intimiano" Ansperto "Azzone Visconti"
	Celentano "come Angilberto" Como "Corrado II" Cremona
	"Filippo Maria" "Fiumi Lambro" "Fiumi l'Olona" "Fiume Seveso" "Francesco Sforza"
	"Gian Galeazzo Visconti"
	Lecco Lodi Lombardia
	Pavia
	"Re Alboino"
	"San Babila" "San Bernardino alle Ossa" "San Cristoforo sul Naviglio" "San Giovanni in Conca" "San Gottardo"
	"San Lorenzo" "San Marco" "San Nazaro Maggiore" "San Simpliciano" "San Satiro" "Santa Maria dei Miracoli"
	Sant'Agostino Sant'Ambrogio Sant'Eustorgio "Santo Stefano" Sondrio
	Terragni Teodolinda Torriani
	Valperto Varese Virgilio
}

army_names = {
	"Armata de $PROVINCE$"
}