#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 255  127  0 }

historical_idea_groups = {
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	trade_ideas
	administrative_ideas
	diplomatic_ideas
	aristocracy_ideas
	spy_ideas
}

historical_units = { #Indonesian Non-Muslim Group
	indonesian_simbalan_infantry
	indonesian_war_elephant_cavalry
	indonesian_stockade_infantry
	indonesian_heavy_elephant_cavalry
	indonesian_armored_infantry
	indonesian_armored_elephant_cavalry
	indonesian_lantaka_infantry
	indonesian_kshatriya_cavalry
	indonesian_silhedar_cavalry
	indonesian_matchlockmen_infantry
	indonesian_ekanda_cavalry
	indonesian_carabao_guard_infantry
	indonesian_platoon_infantry
	indonesian_skirmisher_infantry
	western_carabinier_cavalry
	indonesian_rifled_infantry
	western_hunter_cavalry
	indonesian_impulse_infantry
	western_lancer_cavalry
	indonesian_breech_infantry
	}

monarch_names = {
	"Girindrawardhana #0" = 20
	"Girishawardhana #0" = 20
	"Kertabumi #0" = 20
	"Kertawijaya #0" = 20
	"Rajasawardhana #0" = 20
	"Suraprabhawa #0" = 20
	"Wikramawardhana #0" = 20
	"Hayam Wuruk #1" = 15
	"Kalagamet #1" = 15
	"Raden Wijaya #1" = 15
	"Suhita #0" = -15
	"Sri Gitarja #1" = -10
	"Kertawardhana #0" = 10
	"Raden Sotor #0" = 10
	"Raden Sumirat #0" = 10
	"Singhawardhana #0" = 10
}

leader_names = {
	Hamengku
	Agung
	Slamet
	Budi
	Harto
	Karno
	Wani
	Mas
	Kencono
	Ratri
}

ship_names = {
	Pendet Legong Baris Topeng Barong
	Kecak Batur Catur Bratan Pohen Lesong
	Sangyang Batukaru Patas Musi Mesehe
	Agong Seraya Merbuk Sanglang Kelatakan
}
