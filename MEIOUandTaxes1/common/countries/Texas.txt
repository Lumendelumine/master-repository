#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 118  32  128 }

historical_idea_groups = {
	exploration_ideas
	leadership_ideas
	expansion_ideas
	quality_ideas
	trade_ideas
	administrative_ideas
	economic_ideas
	popular_religion_ideas
}

historical_units = {
	western_spearman_infantry
	western_knight_cavalry
	western_longbow_infantry
	western_lance_infantry # enable = gaelic_mercenary
	western_freeshooter_infantry # enable = gaelic_free_shooter
	western_tercio_infantry # enable = french_caracolle
	western_gallop_cavalry # enable = irish_charge
	western_regular_infantry # anglofrench_line
	western_mediumhussar_cavalry # french_dragoon
	western_line_infantry # british_redcoat
	# western_uhlan_cavalry # british_hussar
	western_columnar_infantry # british_square
	western_impulse_infantry # mixed_order_infantry
	western_uhlan_cavalry # open_order_cavalry
	western_breech_infantry # napoleonic_square
	western_lancer_cavalry # napoleonic_lancers
}

monarch_names = {
	"George #0" = 20
	"James #0" = 20
	"John #0" = 20
	"Thomas #0" = 20
	"Charles #0" = 15
	"Daniel #0" = 15
	"Henry #0" = 15
	"Samuel #0" = 15
	"Arthur #0" = 10
	"Cyrus #0" = 10
	"David #0" = 10
	"Elias #0" = 10
	"Frederick Augustus #0" = 10
	"Nathaniel #0" = 10
	"Peyton #0" = 10
	"Richard #0" = 10
	"Aaron #0" = 0
	"Angus #0" = 0
	"August #0" = 0
	"Benjamin #0" = 0
	"Calvin #0" = 0
	"Darren #0" = 0
	"Elbridge #0" = 0
	"Erick #0" = 0
	"Francis #0" = 0
	"Frederick #0" = 0
	"Isaac #0" = 0
	"Jonathan #0" = 0
	"Joseph #0" = 0
	"Kyran #0" = 0
	"Luke #0" = 0
	"Milton #0" = 0
	"Patrick #0" = 0
	"Paul #0" = 0
	"Philip #0" = 0
	"Randolf #0" = 0
	"Silvester #0" = 0
	"Thimoty #0" = 0
	"Wilbur #0" = 0
	"William #0" = 0
}

leader_names = {
	Houston 
	Bowie
	Clinton
	Jefferson 
	Moore
	King
	Langdon
	Austin
	Pickney
	Smith
}

ship_names = {
	Burnet Argo Argus
	"Bohomme Richard" 
	Cabot Chesapeake Champion "Colonel Moore" Columbus Alamo Congress Constellation Constitution
	Dolphin Deane Delaware Diligent 
	Effingham Enterprise Essex
	Fly
	"General Austin" "General Houston" Gonzales
	Hampden Hancock Hornet Houston
	Independence 
	Lexington
	Montgomery Mosquito
	Nautilus
	Philadelphia Pigot President Providence
	"Queen of France"
	Raleigh Randolph Racehorse Ranger Reprisal Repulse Resistance Retaliation Revenge
	Sachem Saratoga Serapis Siren Surprise
	Texas Trumbull
	"United States"
	Viper Virginia Vixen
	Warren Wasp 
}
