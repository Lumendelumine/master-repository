#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 150  75  10 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	administrative_ideas
	merchant_marine_ideas
	quantity_ideas
}

historical_units = { #Indonesian Muslim Group
	indonesian_simbalan_infantry
	indonesian_arab_horse
	indonesian_stockade_infantry
	indonesian_heavy_elephant_cavalry
	indonesian_armored_infantry
	indonesian_tumandar_cavalry
	indonesian_arquebusier_infantry
	indonesian_tabinan_cavalry
	indonesian_mansabdar_cavalry
	indonesian_matchlockmen_infantry
	indonesian_ekanda_cavalry
	indonesian_native_musketeer_infantry
	indonesian_regular_infantry
	indonesian_bayonet_infantry
	western_carabinier_cavalry
	indonesian_drill_infantry
	western_hunter_cavalry
	indonesian_impulse_infantry
	western_lancer_cavalry
	indonesian_breech_infantry
	}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

monarch_names = {
	"Muzaffar Shah #2" = 20
	"Muazzam Shah #1" = 10
	"Mohammed Shah #1" = 10
	"Maazul Shah #1" = 10
	"Mahmud Shah #2" = 20
	"Ibrahim Shah #1" = 10
	"Sulaiman Shah #2" = 20
	"Rijaluddin Shah #1" = 10
	"Muhiyuddin Shah #1" = 10
	"Ziyauddin al-Mukarram Shah #1" = 10
	"Abdullah al Muazzam Shah #1" = 10
	"Ahmad Tajuddin Halim Shah #2" = 20
	"Mohammad Jiwa Zainal Al Abidin #2" = 20
	"Abdullah Makarram Shah #0" = 10
	"Ziyauddin Mukkaram Shah #0" = 10
	"Zainal Rashid al Muazzam Shah #0" = 20
	"Abdul Hamid Halim Shah #0" = 10
	"Badlishah #0" = 10
	"Abdul Halim Muadzam Shah #0" = 10
}


ship_names = {
	"Sultan Iskandar" Bendahara Laksamana
	Temenggung "Penghulu bendahari" Shahbandar
	"Hukum Kanun" "Undang-Undang" "Pulau Bakung"
	"Pulau Lingga" Kepulauan "Pulau Singkep" "Kepulauan Riau"
	"Pulau Rempang" "Pulau Kundur" "Pulau Padang"
	"Pulau Bengkaus" "Pulau Tinggi" "Pulau Sibu"
	"Pulau Tioman" "Pulau Aur" "Pulau Rupat"
	"Pulau Rangsang" "Pulau Tebing Tinggi"
}
