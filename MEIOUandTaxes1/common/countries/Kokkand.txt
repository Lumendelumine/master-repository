#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 98  140  84 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	leadership_ideas
	naval_ideas
	quantity_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
}

historical_units = {
	#Persian Group
	muslim_arab_horseman
	muslim_halqa_irregular_infantry
	muslim_halqa_regular_infantry
	muslim_mongol_cavalry
	muslim_qizilbash_cavalry
	muslim_ghulam_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_gharachoorlooha_musketeer_infantry
	muslim_two_horned_tofangchi_infantry
	muslim_uhlan_cavalry
	muslim_mass_infantry_tactics
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry
}


monarch_names = {
	"Shah Rukh #0" = 30
	"Rustam Hajji Sultan #0" = 20
	"Ashur-qul #0" = 20
	"Abd ar-Rahman #0" = 20
	"Abd al-Karim #0" = 20
	"Iradana Bi Erdeni #0" = 20
	"Sulayman #0" = 20
	"Nar Buta #0" = 20
	"'Alim Khan #0" = 20
	"Muhammad 'Umar Khan #0" = 20
}

leader_names = {
	Bachi
	Tura
	Murad
	Bahadur
	Alim
	Quli
	Malla
	Murad
	Erdeni
	Khudayar
}

ship_names = {
	Khavakend "Amu Darja" Fergana Margilan
	Rishdan Besharik Khamza Kuva Kuvasay Nar
	"Syr Darja" Uzeravsan "Alajskij Chrebet"
}
