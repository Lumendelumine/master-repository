# Skewbald.txt
# Tag : SKP
# Tech : steppestech

graphical_culture = muslimgfx

color = { 122  186  188 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Taybuga Khan #0" = 20
	"Khwaj�' Khan #0" = 20
	"Mar Khan #0" = 20
	"Obder Khan #0" = 20
	"Ibaq Khan #0" = 20
	"Mamuk Khan #0" = 20
	"Abalak Khan #0" = 20
	"Aguis Khan #0" = 20
	"Qasim Khan #0" = 20
	"Bekbulat Khan #0" = 20
	"Yadiger Khan #0" = 20
	"Kucum Khan #0" = 20
	"Aley Khan #0" = 20
	"Seidyak Khan #0" = 20
	"Suker Khan #0" = 20
	"Boibegus Khan #0" = 20
	"Kuldalang Khan #0" = 20
	"Ucirtu Khan #0" = 20
	"Dagal Khan #0" = 20
	"Aldark Khan #0" = 20
	"Dsungar Khan #0" = 20
}

leader_names = {
	Balna Salmiianov Ravna Taiborey Vylka
	Kanin Maimbava Arinin Aven Tsoy
}

ship_names = {
	"Ghengis Khan" Orda Kochu Bayan Sasibuga
	Ilbasan "Mubarrak Kwaja" Chimtai Urus Tuqtaqiya
	"Timur Malik" Toqtamish "Timur Qutlugh" "Shadi Beg"
	Pulad "Jalal Ad-Din" "Karim Berdi" Kebek "Jabbar Berdi"
	"Ulugh Muhammad" "Devlat Berdi" Baraq "Sayyid Ahmad"
}
