#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 255 61 61 }

historical_idea_groups = {
	quality_ideas
	economic_ideas
	aristocracy_ideas
	diplomatic_ideas
	logistic_ideas
	trade_ideas
	leadership_ideas
	administrative_ideas
}

historical_units = { #South German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_skirmisher_infantry
	western_mediumhussar_cavalry
	western_mass_infantry
	western_uhlan_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_jominian_infantry
}

monarch_names = {
	"Konrad #1" = 60
	"Bolko #1" = 40
	"Bernard #2" = 40
	"Henryk #0" = 20
	"Karol #0" = 20
        "Fryderyk #0" = 20
	"Jan #0" = 20
	"Boleslaw" = 20
	"Sylwiusz #0" = 20
	"Jerzy #0" = 20
	"Jerzy Wilhelm #0" = 20
	"Karol Fryderyk #0" = 20
	"Krystian #0" = 20
	"Jan Jerzy #0" = 10
	"Jerzy Ernest #0" = 10
	"August #0" = 5
	"Ernest #0" = 5
	"Joachim #0" = 5
	"Krystian Ludwik #0" = 5
	"Rudolf #0" = 5
	"Wladyslaw #0" = 1
	"Herman #1" = 1
	"Albert #0" = 0
	"Aleksander #0" = 0
	"Andel #0" = 0
	"Branko #0" = 0
	"Bruno #0" = 0
	"Cezar #0" = 0
	"Dytrych #0" = 0
	"Eugeniusz #0" = 0
	"Filip #0" = 0
	"Krzysztof #0" = 0
	"Lech #0" = 0
	"Markus #0" = 0
	"Maurycy #0" = 0
	"Radomir #0" = 0
	"Siegfried #0" = 0
	"Tomasz #0" = 0
	"Waclaw #0" = 0
	"Wilhelm #0" = 0
}

leader_names = { 
	Schwarz Hergesell Heintzel Hoppe
	Kahl Tilch Greve Abeln Liegnitz
	Jauernik Wiehle Gronau Sterner
}

ship_names = {
	Teschen Falkenberg "Cosel-Beuthen" Oppeln Ratibor
	Breslau Sagan Oels Steinau "Liegnitz-Brieg" Glogau
	Münsterberg "Neisse-Ottmachau" "Schweidnitz-Jauer"
}
