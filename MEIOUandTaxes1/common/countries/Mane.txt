#Country Name: Mane
#Tag: MNE
#MEIOU-FB - Sudan tech group Nov 08
#dharper's African cavalry added

graphical_culture = africangfx

color = { 225   235   125 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	unique_MAN_sumba_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Mule #0" = 20
	"Mamadu #1" = 40
	"Al-Hajj #0" = 60
	"'Ali Zalil #0" = 20
	"Muhammad #0" = 60
	"Da'ud #0" = 20
	"Bakari #0" = 20
	"'Abd al-Rahman #0" = 20
	"Bakr #0" = 20
	"Al-Mukhtar #0" = 20
	"Mahmud #1" = 20
	"Majan #0" = 0
	"Birahim #0" = 0
	"'Ali #0" = 0
	"Amada Sire #0" = 0
	"Nguia #0" = 0
	"Hammadi #0" = 0
	"Bubu #0" = 0
	"Ilo #0" = 0
	"Kanta #0" = 0
	"Al-Mansur #0" = 0
	"'Ammar #0" = 0
	"Sa'ud 'Arjud #0" = 0
	"Haddu #0" = 0
	"Sa'id #0" = 0
	"Dhu al-Nun #0" = 0
	"'Abd Allah #0" = 0
	"Nasr #0" = 0
	"Yahya #0" = 0
	"Sanibar #0" = 0
	"'Abbas #0" = 0
	"Benkano #0" = 0
	"Zenka #0" = 0
	"Mami #0" = 0
	"Ba-Bakribin #0" = 0
	"'Abd al-Qadir #0" = 0
	"Yusuf #0" = 0
	"Santa'a #0" = 0
	"Al-Fa' Ibrahim #0" = 0
	"Sandaki #0" = 0
}

leader_names = {
	Konare
	Deo
	Keita
	Traor�
	Toure
	Diarra
	Moussa
	Modibo
	Amadou
	Toumani
}

ship_names = {
	Kiringa Timbuktu "Koumbi Saleh" Gao
	Jenne "Sundiata Keita" "Kankan Musa"
	"Kouroukan Fouga" "Wali Keita" "Quati Keita"
	"Khalifa Keita" Sakura
}
