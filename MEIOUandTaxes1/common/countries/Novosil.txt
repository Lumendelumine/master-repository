# Novosil

graphical_culture = easterngfx

color = { 101  5  37 }

historical_idea_groups = {
	trade_ideas
	quantity_ideas
	expansion_ideas
	aristocracy_ideas
	exploration_ideas
	leadership_ideas
	naval_ideas
	spy_ideas
	economic_ideas
	administrative_ideas
}

historical_units = { #Russian group
	eastern_hospite_infantry
	eastern_druzhina_cavalry
	eastern_bandiera_infantry
	eastern_balkan_feudal_cavalry
	eastern_hand_gunner
	eastern_hussar_horse_archer_cavalry
	eastern_pischalniki_infantry
	eastern_cossack_cavalry
	eastern_streltsy_infantry
	eastern_light_cossack_cavalry
	eastern_soldaty_infantry
	eastern_pancerni_cavalry
	eastern_regular_infantry
	eastern_carabinier_cavalry
	eastern_line_infantry
	eastern_uhlan_cavalry
	eastern_mass_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_jominian_infantry
}

monarch_names = {
	"Svyatoslav #2" = 25
	"Mstislav #3" = 20
	"Roman #1" = 20
	"Davyd #1" = 20
	"Fedor #1" = 20
	"Mikhail #1" = 20
	"Gleb #2" = 20
	"Ivan #1" = 15
	"Rostislav #2" = 15
	"Vsevolod #1" = 10
	"Iaropolk #1" = 10
	"Aleksandr #1" = 10
	"Vladimir #1" = 10
	"Yury #0" = 5
	"Olgierd #0" = 5
	"Bryachislav #0" = 5
	"Vasiliy #0" = 5
	"Izyaslav #0" = 5
	"Vseslav #0" = 5
	"Boris #0" = 5
	"Adrian #0" = 0
	"Afanasiy #0" = 0
	"Daniil #0" = 0
	"Dmitriy #0" = 0
	"Dorofey #0" = 0
	"Faddey #0" = 0
	"Feodosiy #0" = 0
	"Feofil #0" = 0
	"Foma #0" = 0
	"Gavriil #0" = 0
	"Grigoriy #0" = 0
	"Igor #0" = 0
	"Mefodiy #0" = 0
	"Naum #0" = 0
	"Nikita #0" = 0
	"Nikolai #0" = 0
	"Pankrati #0" = 0
	"Prokhor #0" = 0
	"Pyotr #0" = 0
	"Radoslav #0" = 0
	"Ruslan #0" = 0
	"Semen #0" = 0
	"Serafim #0" = 0
	"Sevastian #0" = 0
	"Stanislav #0" = 0
	"Timofeiy #0" = 0
	"Varfolomeiy #0" = 0
	"Vikentiy #0" = 0
	"Yaroslav #0" = 0
}

leader_names = { 
	Vitebsky Izyaslavsky Dursky Galisky Borovsky Vereisky Volotsky Galitsky
	Mozhaisky Uglisky Shemyakin Satin Aladyin Beznosov Bokeev Burukhin Vnukov Korobyin
	Zabolotsky Dalmatov Kisleevsky Travin Tsyplyatev Shukalovsky
}

ship_names = {
	Velikaya Chudskoye Pskovskoye Polotsk Smolensk Bryansk
	Rakovor Bolotovo Kulikovo Ostrov Izborsk
	Pechory Porhov "Velikiye Luki" Sebezh
}
