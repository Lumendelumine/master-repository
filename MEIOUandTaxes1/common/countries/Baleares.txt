# Kingdom of Mallorca
# BLE

graphical_culture = westerngfx

color = { 113  182  76}

historical_idea_groups = {
	popular_religion_ideas
	aristocracy_ideas
	trade_ideas
	quality_ideas
	exploration_ideas
	logistic_ideas
	naval_ideas
	innovativeness_ideas
	economic_ideas
}

historical_units = { #Iberian group
	western_spearman_infantry
	western_raid_cavalry
	western_runner_cavalry
	western_manatarms_infantry
	western_pike_infantry
	western_jinete_cavalry
	western_pikeandshot_infantry
	western_earlytercio_infantry
	unique_SPA_tercio_infantry
	western_escopeteros_cavalry
	western_tercio_infantry
	unique_POR_tanger_infantry
	unique_SPA_coselete_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_platoon_infantry
	western_line_infantry
	western_carabinier_cavalry
	western_drill_infantry
	western_hunter_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_breech_infantry
	}



monarch_names = {
	"Pere #0" = 40
	"Llu�s Ramon #0" = 40
	"Ferran #0" = 20
	"Joan #0" = 20
	"Felip #0" = 20
	"Francesc #0" = 20
	"Enric Ramon #0" = 20
		  "Llu�s #0 Antoni" = 20
		  "Llu�s #0 Francesc" = 20
		  "Llu�s #0 Maria" = 20
	"Nicolau #0" = 20		
		  "Alfons #0" = 10
	"Ambr�s #0" = 10
		  "Antoni #0" = 10
	"Caterina #0" = -10
	"Enric #0" = 10
	"Joana #0" = -10
	"Joaquim #0" = 10
	"Llu�s #0" = 10
	"Manel #0" = 10
		  "Jaume #0" = 1
	"Mart� #0" = 1
	"Andreu #0" = 0
	"Arnau #0" = 0
	"Bernat #0" = 0
		  "Climent #0" = 0
	"Dom�nec #0" = 0
	"Francesc #0 Anton" = 0
	"Gon�al #0" = 0
	"Guillem #0" = 0
	"Hug #0" = 0
	"Jordi #0" = 0
		  "Josep #0" = 0
	"Josep #0 Antoni" = 0
	"Lloren� #0" = 0
	"Manel #0 Josep" = 0
	"Miquel #0" = 0
	"Pau #0" = 0
	"Rafel #0" = 0
	"Roderic #0" = 0
}

leader_names = {
	 Alemany Ametller Amic "de Balaguer" Barcel� Blanc Canals Casanovas "de Guimer�" Casaus
	Dalmau Despl� Destorrent Dom�nec "d�Esp�s" Ferrer "Folc de Cardona" Fontana
	Gallart Gassol Gisbert Graner "de Grau" Guimet
	Lleix� Llull "de Margarit" Muntaner "de Montcada" Moragues "de Mur" Nadal "d�Oms"
	Pallar�s Pin�s Piquer Pons Rasqu� "de la Riba" Ribalta Ripio Rif�s "de Ripoll"
	Salavert "de Subirats" Tamarit Vall�s Verg�s Vilamar� Vilaplana
}

ship_names = {
	"Blanca Aurora" "Sant Jacob" Flama Lleona Falc� "La Bonaventura" "Sant Antoni" 
	"Sant Miquel" "Sant Bernat" "Sant Francesc" "Santa �gata" "Santa Eul�lia" 
	"Sant Gabriel" "Sant Climent" "Sant Jordi" "La Reial" Trinitat "Santa Creu" 
	"Santa Maria" "Santa Caterina" "Sant Joan Evangelista" "Santa B�rbara" 
	"Sant Pere de Roma" "La Gasela" "Santa Magdalena" "Sant V�ctor" Magnana 
	"Santa Ventura" "Santa Coloma" "Sant Ambr�s" Constan�a "Sant Ferran"
	Invencible Tel�mac Sobir� "Verge del Carme" "Verge de Montserrat" Juno
	Vigilant Eclipsi "Sant Joan Baptista" "Sant�ssima Trinitat" Terrible
	Temer�ria "Verge de les Neus" "Jaume el Conqueridor" Mart Alerta Ceres
	Sorpresa Empord� Catalunya "Roger de Flor" "Roger de Ll�ria"
	"La Garsa"
}
