#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 144  0  32 } # Burgundy color

revolutionary_colors = { 1  8  5 } #Belgium

historical_score = 650

historical_idea_groups = {
	administrative_ideas
	trade_ideas
	mercenary_ideas
	economic_ideas
	diplomatic_ideas
	quality_ideas
	logistic_ideas
	expansion_ideas
	free_trade_ideas
	standing_army_ideas
}

historical_units = { #Dutch group
	western_halberd_infantry
	western_raid_cavalry
	western_manatarms_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	western_freeshooter_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_platoon_infantry
	western_line_infantry
	western_mediumhussar_cavalry
	western_drill_infantry
	western_lighthussar_cavalry
	western_impulse_infantry
	western_lancer_cavalry
	western_breech_infantry
	}

monarch_names = {

	"Antoine #0" = 0
	"Charles #0" = 40
	"Claude #0" = 0
	"Etienne #0" = 0
	"Eudes #4" = 5
	"Geoffroy #0" = 0
	"Girard #0" = 0
	"Guillaume #0" = 0
	"Guy #0" = 0
	"Henri #2" = 5
	"Hugues #5" = 5
	"Jacques #0" = 0
	"Jean #0" = 40
	"Michel #0" = 0
	"Nicolas #0" = 0
	"Philippe #1" = 80
	"Pierre #0" = 0
	"Robert #2" = 5
	"Marie #0" = -5
}

leader_names = {
	d'Auxy
	Armenier Bouton Chuffaing Perron Poinceot Pot
	"le Marlet"
	"de l'Aubespin" "de la Guiche" "de la Tr�moille" "des Barres"
	"de Baubigny" "de Bauffremont" "de Blaisy" "de Brimeu" "de Buxy"
	"de Ch�lon" "de Chancey" "de Chantemerle" "de Chaugy" "de Choiseul" "de Clugny" "de Couches" "de Courcelles" "de Courtiamble" "de Cr�quy" "de Croy" "de Crux"
	"de Damas"
	"de Frolois"
	"de Grailly" "de Grancey" "de Granson"
	"de Janly" "de Jaucourt"
	"de Lalaing" "de Lannoy" "de Larrey" "de Lugny"
	"de Mailly" "de Masmines" "de Mello" "de Montaigu" "de Montconis" "de Neufch�tel"
	"de Noyelles" "de Noyers"
	"de Pontailler"
	"de Ray" "de Rochefort" "de Roussillon" "de Roye"
	"de Salins" "de Saulx" "de Sercey" "de Sombernon"
	"de Tenarre" "de Ternant" "de Thil" "de Thoisy" "de Toulongeon"
	"de Vergy" "de Vienne" "de Villers"
#		  d'Aban d'Abonde Arbelot d'Auxy
#		  "de Balay" "de Bauffremont" "de Beaumont" "de Beugre" "de Boulay" "de Bourgeois" Bourgogne-Montaigu "de Brachet" "de Brimeu"
#		  "de Castaing" "de Chabot" "de Ch�lon" "de Ch�lon-Arlay" "de Chandieu" "de Chastellux" "de Chin" "de Cicon" "de Coligny" "de Cr�quy" "de Cuiseaux"
#		  "de Damas" "de Dammartin" "de Donzy" "de Dyo"
#		  "d'Egmont"
#		  "de Ferrette"
#		  "de Lalaing" "de Lannoy" "Le Corgne" "de Lugny"
#		  "de Marle" "de Melun" "de Montfaucon"
#		  "Nagu de Varennes" "de Nanton" "de Neufch�tel" "de Noyelles"
#		  "de Pontailler" "de la Porte"
#		  "Pot de la Roche" "de Rochebaron" "de Rochechouart" "de Roye"
#		  "de Scorailles" "de S�mur"
#		  "de Ternant" "de Tonnerre" "de Toucy" "de Toulonjon"
#		  "de Vaubresson" "de Vaudrey" "de Vellexon" "de Vergy" "de Vienne" "de Villaines" "de Villiers"
}

ship_names = {
	Arman�on Arroux
	Bourgogne Brugge
	Cambrai "Castrum Divionense" Childebert "Childebert II" Chilperic Citeaux Chlothar Cluny Comtesse "C�te-d'Or" 
	Dijon "Duc de Bourgogne" Duchesse 
	Gebicca Giselher Godegisel Godomar Gunderic Gundobad Gundomar "Gundomar II" Gunther Guntram
	"Jean sans Peur"
	"Marie de Bourgogne"
	"Philippe le Bon" "Philippe le Hardi"
	"Richard de Bourgogne"
	Sa�ne Sigismund
	Theudebert "Theuderic II"
	V�zelay
}

army_names = {
			"Arm�e du Charolais"	  "Arm�e de Chalon"	 "Arm�e du Comt�" 
			"Arm�e de l'Autunois"	 "Arm�e du M�connais" "Arm�e de l'Auxerrois"
			"Arm�e de l'Avallonnais" "Arm�e de Tonnerre"  "Arm�e de l'Auxonnois"
			"Arm�e de la Saone"		
	 "Arm�e de $PROVINCE$" 
}