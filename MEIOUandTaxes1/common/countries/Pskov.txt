# Pskov
# PSK

graphical_culture = easterngfx

color = { 82  123  95 }

historical_idea_groups = {
	trade_ideas
	plutocracy_ideas
	economic_ideas
	administrative_ideas
	naval_ideas
	spy_ideas
	logistic_ideas
	expansion_ideas
}

historical_units = { #Russian group
	eastern_hospite_infantry
	eastern_druzhina_cavalry
	eastern_bandiera_infantry
	eastern_balkan_feudal_cavalry
	eastern_hand_gunner
	eastern_hussar_horse_archer_cavalry
	eastern_pischalniki_infantry
	eastern_cossack_cavalry
	eastern_streltsy_infantry
	eastern_light_cossack_cavalry
	eastern_soldaty_infantry
	eastern_pancerni_cavalry
	eastern_regular_infantry
	eastern_carabinier_cavalry
	eastern_line_infantry
	eastern_uhlan_cavalry
	eastern_mass_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_jominian_infantry
}

monarch_names = {
	"Aleksandr #3" = 80
	"Ivan #1" = 80
	"Vasiliy #2" = 60
	"Fyodor #0" = 60
	"Andrei #1" = 40
	"Dmitriy #0" = 40
	"Konstantin #0" = 40
	"Pyotr #0" = 40
	"Vladimir #2" = 20
	"Yaroslav #1" = 20
	"Daniil #0" = 20
	"Grigoriy #0" = 20
	"Semen #0" = 20
	"Adrian #0" = 0
	"Afanasiy #0" = 0
	"Dorofey #0" = 0
	"Eduard #0" = 0
	"Faddey #0" = 0
	"Feodosiy #0" = 0
	"Feofil #0" = 0
	"Ferapont #0" = 0
	"Foka #0" = 0
	"Foma #0" = 0
	"Gavriil #0" = 0
	"Igor #0" = 0
	"Kliment #0" = 0
	"Mefodiy #0" = 0
	"Mikhail #0" = 0
	"Naum #0" = 0
	"Nikita #0" = 0
	"Nikolai #0" = 0
	"Pankrati #0" = 0
	"Radoslav #0" = 0
	"Ruslan #0" = 0
	"Serafim #0" = 0
	"Sevastian #0" = 0
	"Stanislav #0" = 0
	"Timofeiy #0" = 0
	"Varfolomeiy #0" = 0
	"Vikentiy #0" = 0
}

leader_names = { 
	Vitebsky Izyaslavsky Dursky Galisky Borovsky Vereisky Volotsky Galitsky
	Mozhaisky Uglisky Shemyakin Satin Aladyin Beznosov Bokeev Burukhin Vnukov Korobyin
	Zabolotsky Dalmatov Kisleevsky Travin Tsyplyatev Shukalovsky
}

ship_names = {
	Velikaya Chudskoye Pskovskoye Narva
	Rakovor Bolotovo Kulikovo Ostrov Izborsk
	Pechory Porhov "Velikiye Luki" Sebezh
}
