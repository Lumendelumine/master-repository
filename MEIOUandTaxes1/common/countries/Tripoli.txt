#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 156  177  53 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	leadership_ideas
	naval_ideas
	quantity_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
}

historical_units = {
	#North African group
	muslim_mashain_walker_infantry
	muslim_arab_horseman
	muslim_crossbow_infantry
	muslim_jinete_cavalry
	muslim_rammaha_lancer_cavalry
	muslim_elchee_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_piyadegan_infantry
	muslim_savaran_saltani_cavalry
	muslim_taifat_al_rusa_infantry
	muslim_hunter_cavalry
	muslim_drill_infantry
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry

}

monarch_names = {
	"'Al� #0" = 60
	"Ahmad #0" = 40
	"Muhammad #0" = 40
	"Y�suf #0" = 40
	"'Abdall�h #0" = 20
	"Mammi #0" = 20
	"Mansur #0" = 20
	"Yahy� #0" = 20
	"Thabit #2" = 10
	"'Abd al-'Aziz #0" = 0
	"Ab� Bakr #0" = 0
	"Ala ad-Din #0" = 0
	"Butrus #0" = 0
	"Fakhri #0" = 0
	"Fayiz #0" = 0
	"Hadj #0" = 0
	"Hafiz #0" = 0
	"Haidar #0" = 0
	"Harun #0" = 0
	"Hasan #0" = 0
	"Hashim #0" = 0
	"Husayn #0" = 0
	"Ibrahim #0" = 0
	"Ja'far #0" = 0
	"Khalil #0" = 0
	"Mahir #0" = 0
	"Mas'ud #0" = 0
	"Mukhtar #0" = 0
	"Murad #0" = 0
	"Mustafa #0" = 0
	"Nizar #0" = 0
	"Qadir #0" = 0
	"Rasim #0" = 0
	"Salah #0" = 0
	"Shadi #0" = 0
	"Tahir #0" = 0
	"Tariq #0" = 0
	"Torgud #0" = 0
	"Ziyad #0" = 0
	"'Usman #0" = 0
}

leader_names = {
	Negib
	Reis
	Emin
	Asker
	Asim
	Izzet
	Nedim
	Nazhar
	Halid
	Nuri
}

ship_names = {
	Ba'ith Shahid Haqq Wakil Qawiyy Matin
	Waliyy Hamid Muhsi Mubdi Mu'id Muhyi Mumit
	Hayy Qayyum Wajid Majid Wahid Samad Qadir
	Muqtadir Muqaddim Mu'akhkhir Awwal Akhir
	Zahir Batin Wali Muta'al Barr Tawwab
}
