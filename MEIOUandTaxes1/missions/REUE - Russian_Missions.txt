# Russian Missions

conquer_finland_region = {

	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		region = finland_region
		owned_by = SWE
	}
	allow = {
		tag = RUS
		is_subject = no
		mil = 4
		is_neighbor_of = SWE
		NOT = { war_with = SWE }
		finland_region = { owned_by = SWE }
		NOT = { finland_region = { owned_by = RUS } }
		NOT = { has_country_modifier = baltic_ambition }
		NOT = { has_country_flag = conquered_finland_region_rus }
	}
	abort = {
		OR = {
			is_subject = yes
			any_target_province = {
				NOT = { owned_by = ROOT }
				NOT = { owned_by = SWE }
			}
		}
	}
	success = {
		NOT = { war_with = SWE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_adm_power = 100
		set_country_flag = conquered_finland_region_rus
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


subjugate_tver = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = TVE
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		NOT = { has_country_flag = tver_subjugated }
		TVE = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
		}
		NOT = { has_country_flag = tver_subjugated }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = TVE }
			NOT = { religion_group = ROOT }
			TVE = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = TVE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TVE value = 0 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		if = {
			limit = {
				any_target_province = {
					province_id = 294
				}
			}
			294 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		if = {
			limit = {
				NOT = {
					any_target_province = {
						province_id = 294
					}
				}
			}
			random_target_province = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1				
			}
		}
		set_country_flag = tver_subjugated
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


annex_yaroslavl = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = YAR
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		YAR = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
		}
		NOT = { has_country_flag = yaroslavl_subjugated }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = YAR }
			YAR = { NOT = { religion_group = ROOT } }
			YAR = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = YAR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = YAR value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = YAR value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		if = {
			limit = {
				any_target_province = {
					province_id = 308
				}
			}
			308 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		if = {
			limit = {
				NOT = {
					any_target_province = {
						province_id = 308
					}
				}
			}
			random_target_province = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1				
			}
		}
		set_country_flag = yaroslavl_subjugated
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


annex_ryazan = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = RYA
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		RYA = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
		}
		NOT = { has_country_flag = ryazan_subjugated }
	}
	abort = {
		OR = {
			is_subject = yes	
			NOT = { exists = RYA }
			RYA = { NOT = { religion_group = ROOT } }
			RYA = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = RYA }
		owns = 301
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = RYA value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = RYA value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		if = {
			limit = {
				any_target_province = {
					province_id = 301
				}
			}
			301 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		if = {
			limit = {
				any_target_province = {
					province_id = 1286
				}
			}
			1286 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		if = {
			limit = {
				NOT = {
					any_target_province = {
						OR = {
							province_id = 301
							province_id = 1286
						}
					}
				}
			}
			random_target_province = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1				
			}
		}
		set_country_flag = ryazan_subjugated
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


access_to_the_baltic_sea = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		OR = {
			AND = {
				province_id = 34
				NOT = { owned_by = ROOT }
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			AND = {
				region = baltic_region
				has_port = yes
				NOT = { owned_by = ROOT }
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
		}
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		NOT = { has_country_modifier = baltic_ambition }
		NOT = {
			owns = 34
			NOT = { baltic_region = { owned_by = ROOT } }
		}
		OR = {
			34 = {
				NOT = { owned_by = ROOT }
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
			baltic_region = {
				has_port = yes
				NOT = { owned_by = ROOT }
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
		}
	}
	abort = {
		is_subject = yes
	}
	success = {
		any_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
		modifier = {
			factor = 2
			naval_ideas = 3
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_navy_tradition = 25
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


russia_discovers_western_siberia = {

	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		has_idea = quest_for_the_new_world
		steppes_region = { has_discovered =  ROOT }
		NOT = { west_siberia_region = { has_discovered =  ROOT } }
		west_siberia_region = { range = ROOT }
		NOT = { has_country_modifier = colonial_enthusiasm }
	}
	abort = {
		NOT = { has_idea = quest_for_the_new_world }
	}
	success = {
		west_siberia_region = { has_discovered =  ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			innovativeness_ideas = 4
		}
		modifier = {
			factor = 2
			num_of_explorers = 2
		}
	}
	effect = {
		add_treasury = 100
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


russian_colony_in_west_siberia = {

	type = country
	
	category = DIP
	
	allow = {
		tag = RUS
		NOT = { has_country_modifier = colonial_enthusiasm }
		west_siberia_region = {
			has_discovered = ROOT
			is_empty = yes
		}
		has_idea = quest_for_the_new_world
		NOT = { west_siberia_region = { country_or_vassal_holds = ROOT } }
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			AND = {
				NOT = { west_siberia_region = { is_empty = yes } }
				NOT = { west_siberia_region = { country_or_vassal_holds = ROOT } }
			}	
		}
	}
	success = {
		west_siberia_region = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 2
			num_of_explorers = 1
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


russia_discovers_eastern_siberia = {

	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		has_idea = quest_for_the_new_world
		NOT = { has_country_modifier = colonial_enthusiasm }
		west_siberia_region = { has_discovered =  ROOT }
		NOT = { east_siberia_region = { has_discovered =  ROOT } }
		east_siberia_region = { range = ROOT }
	}
	abort = {
		NOT = { has_idea = quest_for_the_new_world }
	}
	success = {
		east_siberia_region = { has_discovered =  ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			innovativeness_ideas = 4
		}
	}
	effect = {
		add_prestige = 5
		add_adm_power = 50
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


russian_colony_in_east_siberia = {

	type = country
	
	category = DIP
	
	allow = {
		tag = RUS
		has_idea = quest_for_the_new_world
		NOT = { has_country_modifier = colonial_enthusiasm }
		west_siberia_region = { country_or_vassal_holds = ROOT }
		east_siberia_region = {
			has_discovered = ROOT
			is_empty = yes
		}
		NOT = { east_siberia_region = { country_or_vassal_holds = ROOT } }
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			AND = {
				NOT = { east_siberia_region = { is_empty = yes } }
				NOT = { east_siberia_region = { country_or_vassal_holds = ROOT } }
			}
			NOT = { west_siberia_region = { country_or_vassal_holds = ROOT } }
		}
	}
	success = {
		east_siberia_region = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


defend_russia_against_the_mongols = {
	
	type = country

	category = MIL
	
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { has_country_flag = defended_against_GOL }
		BLU = {
			is_neighbor_of = ROOT
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			any_owned_province = { culture = russian }
		}
	}
	abort = {
		OR = {
			NOT = { exists = BLU }
			NOT = { war_with = BLU }
		}
	}
	success = {
		NOT = { war_with = BLU }
		BLU = {
			NOT = {
				any_owned_province = { culture = russian }
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_prestige = 5
		add_army_tradition = 5
		set_country_flag = defended_against_GOL
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
	}
}


cross_the_mongol_border = {
	
	type = country

	category = MIL
	
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		mil = 4
		BLU = { is_neighbor_of = ROOT }
		NOT = { any_owned_province = { is_core = BLU } }
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = cross_the_mongol_border }
	}
	abort = {
		is_subject = yes
		NOT = { exists = BLU }
		NOT = { any_owned_province = { is_core = BLU } }
	}
	success = {
		NOT = { war_with = BLU }
		any_owned_province = { is_core = BLU }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_adm_power = 50
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		random_owned_province = {
			limit = { is_core = BLU }
			add_base_tax = 1
			add_base_production = 1
			add_base_manpower = 1
		}
	}
}


subjugate_novgorod = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = NOV
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = military_victory }
		NOV = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = NOV }
			NOV = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = NOV }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = NOV value = 0 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = NOV
			}
		}
	}
}


annex_novgorod = {
	
	type = country

	category = DIP
	
	target_provinces = {
		owned_by = NOV
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		dip = 4
		NOV = {
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
		NOT = { has_country_flag = novogorod_subjugated }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = NOV }
			NOV = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = NOV }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = NOV value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = NOV value = 200 }
		}
	}
	effect = {
		add_prestige = 10
		if = {
			limit = {
				any_target_province = {
					province_id = 309
				}
			}
			309 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		if = {
			limit = {
				NOT = {
					any_target_province = {
						province_id  = 309
					}
				}
			}
			random_target_province = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		set_country_flag = novogorod_subjugated
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}

russia_partitions_poland = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = POL
	}
	allow = {
		tag = RUS
		is_year = 1700
		is_subject = no
		mil = 4
		is_neighbor_of = POL
		NOT = { has_country_modifier = polish_partitions }
		NOT = { has_country_flag = partitioned_poland }
		NOT = { last_mission = russia_partitions_poland }
		POL = {
			owns = 1393		# Minsk
			owns = 1395		# Pinsk
			owns = 1433		# Podolia
			is_neighbor_of = PRU
			is_neighbor_of = HAB
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = POL }
			NOT = { is_neighbor_of = POL }
		}
	}
	success = {
		OR = {
			owns = 1393
			owns = 1395
			owns = 1433
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			POL = { NOT = { num_of_cities = ROOT } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = 0 } }
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = partitioned_poland
		add_country_modifier = {
			name = "polish_partitions"
			duration = 3650
		}
	}
}


subjugate_crimea = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = CRI
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_crimea }
		CRI = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = CRI }
			CRI = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = CRI }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			CRI = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


subjugate_kazan = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = KAZ
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_kazan }
		KAZ = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = KAZ }
			KAZ = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = KAZ }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			KAZ = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


subjugate_the_siberian_khanate = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = SIB
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_the_siberian_khanate }
		SIB = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = SIB }
			SIB = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = SIB }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			KAZ = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


subjugate_astrakhan = {
	
	type = country

	category = MIL
	
	
	target_provinces = {
		owned_by = AST
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_astrakhan }
		AST = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = AST }
			AST = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = AST }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			KAZ = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}
