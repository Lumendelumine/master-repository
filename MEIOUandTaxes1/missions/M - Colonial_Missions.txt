# Colonial Missions

discover_mission = {

	type = empty_provinces
	
	category = DIP
	
	allow = {
		FROM = { has_idea = quest_for_the_new_world }
		range = FROM
		has_port = yes
		NOT = { has_discovered = FROM }
		FROM = {
			is_at_war = no
			num_of_ports = 1
		}
		NOT = {
			FROM = { has_country_modifier = "colonial_enthusiasm" }
		}
		
	}
	abort = {
		OR = {
			FROM = { NOT = { has_idea = quest_for_the_new_world } }
			FROM = { NOT = { has_country_modifier = "colonial_enthusiasm" } }
			FROM = { NOT = { num_of_ports = 1 } }
		}
	}
	success = {
		has_discovered = FROM
	}
	chance = {
		factor = 1000
		#tweak for some historical preferences.
		modifier = {
			factor = 0
			FROM = { tag = POR }
			continent = north_america
		}
		modifier = {
			factor = 1.25
			FROM = { tag = POR }
			OR = {
				region = brazil_region
				west_africa_region_trigger = yes
			}
		}		
		modifier = {
			factor = 0
			FROM = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				continent = north_america
				region = brazil_region
				west_africa_region_trigger = yes
			}
		}
		modifier = {
			factor = 1.25
			FROM = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				province_group = the_spanish_main_group
				region = central_america_region
				region = carribeans_region
				region = la_plata_region
			}
		}
		modifier = {
			factor = 1.25
			FROM = {
				OR = {
					tag = ENG
					tag = GBR
					tag = FRA
				}
			}
			continent = north_america
		}
	}
	effect = {
		FROM = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 1825
			}
		}
	}
}


build_colony_to_city = {

	type = our_provinces
	
	category = ADM
	
	allow = {
		FROM = { is_at_war = no }
		is_colony = yes
		owner = {
			num_of_colonists = 1
			monthly_income = 10
			NOT = {
				any_owned_province = {
					has_province_modifier = local_colonial_expansion
				}
			}
		}
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	success = {
		is_colony = false
		owned_by = FROM
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.2
			base_tax = 3
		}		
		modifier = {
			factor = 1.25
			base_tax = 5
		}		
		
	}
	effect = {
		add_province_modifier = {
				name = "local_colonial_expansion"
				duration = 3650
			}
		if = {
			limit = {
				num_free_building_slots = 1
				NOT = { has_building = temple }
			}
			add_building = shipyard
		}
	}
}


convert_the_pagans = {

	type = our_provinces
	
	category = ADM
	
	allow = {
		FROM = { is_at_war = no }
		owner = { religion_group = christian }
		religion_group = pagan
		is_overseas = yes
		owner = { num_of_missionaries = 1 }
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	success = {
		religion_group = christian
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_base_manpower = 1
		owner = {
			add_country_modifier = {
				name = "converting_the_pagans"
				duration = 3650
			}
		}
	}
}


establish_carribean_colony = {

	type = country
	
	category = DIP
	
	allow = {
		is_at_war = no
		num_of_ports = 1
		has_idea = quest_for_the_new_world 
		religion_group = christian
		carribeans_region = {
			is_empty = yes
			base_tax = 1
			range = ROOT
			has_discovered = ROOT
		}
		NOT = {
			carribeans_region = {
				owned_by =  ROOT
			}
		}
		NOT = {
			carribeans_region = {
				owner = {
					is_subject_of = ROOT
				}
			}
		}
		NOT = {
			has_country_modifier = "colonial_enthusiasm" 
		}
	}
	abort = {
		OR = {
			NOT = {
				carribeans_region = {
					is_empty = yes
					has_discovered = ROOT
				}
			}
			NOT = { num_of_ports = 1 }
			NOT = { has_idea = quest_for_the_new_world }
		}
	}
	success = {
		carribeans_region = {
			owned_by =  ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}

	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


establish_usa_colony = {

	type = country
	
	category = DIP
	
	allow = {
		is_at_war = no
		num_of_ports = 1
		has_idea = quest_for_the_new_world 
		religion_group = christian
		northeast_america_region = {
			is_empty = yes
			base_tax = 1
			has_port = yes
			range = ROOT
			has_discovered = ROOT
		}
		NOT = {
			northeast_america_region = {
				owned_by =  ROOT
			}
		}
		NOT = {
			northeast_america_region = {
				owner = {
					is_subject_of = ROOT
				}
			}
		}
		NOT = {
			has_country_modifier = "colonial_enthusiasm" 
		}
	}
	abort = {
		OR = {
			NOT = {
				northeast_america_region = {
					is_empty = yes
					has_port = yes
					has_discovered = ROOT
				}
			}
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		northeast_america_region = {
			owned_by =  ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 0.5
			NOT = { tag = ENG }
			NOT = { tag = GBR }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


establish_canada_colony = {

	type = country
	
	category = DIP
	
	allow = {
		is_at_war = no
		num_of_ports = 1
		religion_group = christian
		has_idea = quest_for_the_new_world 
		north_america_superregion = {
			is_empty = yes
			base_tax = 1
			has_port = yes
			range = ROOT
			has_discovered = ROOT
		}
		NOT = {
			north_america_superregion = {
				owned_by =  ROOT
			}
		}
		NOT = {
			north_america_superregion = {
				owner = {
					is_subject_of = ROOT
				}
			}
		}
		NOT = {
			has_country_modifier = "colonial_enthusiasm" 
		}
	}
	abort = {
		OR = {
			NOT = {
				north_america_superregion = {
					is_empty = yes
					has_port = yes
					has_discovered = ROOT
				}
			}
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		north_america_superregion = {
			owned_by =  ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 0.5
			NOT = { tag = FRA }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}

get_presence_in_india = {

	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		dip_tech = 31
		num_of_ports = 3
		OR = {
			1712 = { has_discovered = ROOT }
			2038 = { has_discovered = ROOT }
		}
		OR = {
			2027 = { has_discovered = ROOT }
			2028 = { has_discovered = ROOT }
			2033 = { has_discovered = ROOT }
		}
		religion_group = christian
		monthly_income = 70
		indian_coast_group = {
			base_tax = 1
			has_port = yes
			range = ROOT
			has_discovered = ROOT
		}
		NOT = {
			indian_coast_group = {
				owned_by =  ROOT
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		indian_coast_group = {
			owned_by =  ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 20
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}		
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
	}
	immediate = {
		indian_coast_group = {
			limit = {
				NOT = { owned_by = ROOT }
				has_port = yes
				range = ROOT
				has_discovered = ROOT
			}
			add_claim = ROOT
		}
	}
	abort_effect = {
		indian_coast_group = {
			limit = {
				NOT = { owned_by = ROOT }
				has_port = yes
				range = ROOT
				has_discovered = ROOT
			}
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		indian_coast_group = {
			limit = {
				NOT = { owned_by = ROOT }
				has_port = yes
				range = ROOT
				has_discovered = ROOT
			}
			remove_claim = ROOT
		}
		indian_coast_group = {
			limit = {
				owned_by = ROOT
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


get_foothold_in_china = {

	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		dip_tech = 31
		num_of_ports = 5
		OR = {
			1712 = { has_discovered = ROOT }
			2038 = { has_discovered = ROOT }
		}
		OR = {
			2027 = { has_discovered = ROOT }
			2028 = { has_discovered = ROOT }
			2033 = { has_discovered = ROOT }
		}
		religion_group = christian
		monthly_income = 100
		chinese_coast_group = {
			has_port = yes
			range = ROOT
			has_discovered = ROOT
		}
		NOT = {
			chinese_coast_group = {
				owned_by =  ROOT
			}
		}
		indian_coast_group = {
			owned_by =  ROOT
		}	
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		chinese_coast_group = {
			owned_by =  ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 5
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}			
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
	}
	immediate = {
		chinese_coast_group = {
			limit = {
				NOT = { owned_by = ROOT }
				has_port = yes
				range = ROOT
				has_discovered = ROOT
			}
			add_claim = ROOT
		}
	}
	abort_effect = {
		chinese_coast_group = {
			limit = {
				NOT = { owned_by = ROOT }
				has_port = yes
				range = ROOT
				has_discovered = ROOT
			}
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		chinese_coast_group = {
			limit = {
				NOT = { owned_by = ROOT }
				has_port = yes
				range = ROOT
				has_discovered = ROOT
			}
			remove_claim = ROOT
		}
		chinese_coast_group = {
			limit = {
				owned_by = ROOT
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


establish_colony_mission = {

	type = empty_provinces
	
	category = DIP
	
	allow = {
		FROM = { 
			num_of_ports = 1
			num_of_colonists = 1
		}
		range = FROM
		has_port = yes
		has_discovered = FROM
		base_tax = 1
		NOT = {
			FROM = { has_country_modifier = "colonial_enthusiasm" }
		}
	}
	abort = {
		OR = {
			is_empty = no
			FROM = { NOT = { num_of_ports = 1 } }
		}
	}
	success = {
		owned_by = FROM
	}
	chance = {
		factor = 1000
		#tweak for some historical preferences.
		modifier = {
			factor = 0
			FROM = { tag = POR }
			continent = north_america
		}
		modifier = {
			factor = 1.15
			FROM = { tag = POR }
			OR = {
				region = brazil_region
				west_africa_region_trigger = yes
			}
		}		
		modifier = {
			factor = 0
			FROM = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				continent = north_america
				region = brazil_region
				west_africa_region_trigger = yes
			}
		}
		modifier = {
			factor = 1.15
			FROM = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				province_group = the_spanish_main_group
				region = central_america_region
				region = carribeans_region
				region = la_plata_region
			}
		}	
		modifier = {
			factor = 1.15
			FROM = {
				OR = {
					tag = ENG
					tag = GBR
					tag = FRA
				}
			}
			continent = north_america
		}
		modifier = {
			factor = 1.05
			base_tax = 3
		}		
		modifier = {
			factor = 1.05
			base_tax = 5
		}
	}
	effect = {
		FROM = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 1825
			}
		}
	}
}
