# American Missions

defend_the_american_colonies = {
	
	type = country
	
	category = MIL

	allow = {
		tag = USA
		is_subject = no
		northeast_america_region = {
			owned_by = USA
		}
		northeast_america_region = {
			owned_by = GBR
		}
	}
	abort = {
		is_subject = yes
	}
	success = {
		NOT = { war_with = GBR }
		northeast_america_region = {
			type = all
			owned_by = USA
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			ENG = { NOT = { mil = 2 } }
		}
	}
	effect = {
		add_army_tradition = 50
		add_stability = 2
	}
}


conquer_florida = {
	
	type = country
	
	category = MIL
	ai_mission = yes

	target_provinces = {
		OR = {
			province_id = 926
			province_id = 927
			province_id = 929
			province_id = 1456
		}
		NOT = { owned_by = ROOT }
	}
	allow = {
		tag = USA
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = military_victory }
		OR = {
			926 = {
				NOT = { owned_by = ROOT }
				any_neighbor_province = { owned_by = ROOT }
			}
			927 = {
				NOT = { owned_by = ROOT }
				any_neighbor_province = { owned_by = ROOT }
			}
			929 = {
				NOT = { owned_by = ROOT }
				any_neighbor_province = { owned_by = ROOT }
			}
			1456 = {
				NOT = { owned_by = ROOT }
				any_neighbor_province = { owned_by = ROOT }
			}
		}
	}
	abort = {
		is_subject = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


reach_the_mississippi = {
	
	type = country
	
	category = ADM
	ai_mission = yes

	allow = {
		tag = USA
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = military_victory }
		mississippi_region = {
			any_neighbor_province = { owned_by = ROOT }
		}
		NOT = { mississippi_region = { owned_by = ROOT } }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { mississippi_region = { any_neighbor_province = { owned_by = ROOT } } }
		}
	}
	success = {
		mississippi_region = { owned_by = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}


the_united_states_france_relations = {
	
	type = country
	
	category = DIP

	allow = {
		tag = USA
		exists = FRA
		NOT = { war_with = FRA }
		NOT = { has_opinion = { who = FRA value = 100 } }
		FROM = { is_rival = ROOT }
		ROOT = { is_rival = FROM }
		NOT = { has_country_modifier = foreign_contacts }
	}
	abort = {
		OR = {
			NOT = { exists = FRA }
			war_with = FRA
		}
	}
	success = {
		FRA = { has_opinion = { who = USA value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			FRA = { NOT = { has_opinion = { who = USA value = 0 } } }
		}
		modifier = {
			factor = 2
			FRA = { NOT = { has_opinion = { who = USA value = -100 } } }
		}	
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}


the_united_states_rev_france_relations = {
	
	type = country
	
	category = DIP

	allow = {
		tag = USA
		exists = FRA
		FRA = { is_revolution_target = yes }
		NOT = { war_with = FRA }
		NOT = { has_opinion = { who = FRA value = 100 } }
		FROM = { is_rival = ROOT }
		ROOT = { is_rival = FROM }
		NOT = { has_country_modifier = foreign_contacts }
	}
	abort = {
		OR = {
			NOT = { exists = FRA }
			war_with = FRA
		}
	}
	success = {
		FRA = { has_opinion = { who = USA value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			FRA = { NOT = { has_opinion = { who = USA value = 0 } } }
		}
		modifier = {
			factor = 2
			FRA = { NOT = { has_opinion = { who = USA value = -100 } } }
		}	
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}


tobacco_factory = {
	
	type = country

	category = ADM

	allow = {
		tag = USA
		NOT = {
			any_owned_province = {
				trade_goods = tobacco
				has_building = refinery
			}
		}
		any_owned_province = {
			can_build = refinery
			trade_goods = tobacco
			NOT = { has_building = refinery }
		}
	}
	abort = {
		NOT = { any_owned_province = { trade_goods = tobacco } }
	}
	success = {
		any_owned_province = {
			has_building = refinery
			trade_goods = tobacco
		}
	}
	chance = {
		factor = 1000
	}
	effect = {
		random_owned_province = {
			limit = {
				has_building = refinery
				trade_goods = tobacco
			}
			add_base_tax = 1
			add_base_production = 1
		}
	}
}
