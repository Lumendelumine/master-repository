name = "War of the Breton Succession"
#casus_belli = cb_claim_throne
war_goal = {
	type = take_capital_throne
	casus_belli = cb_claim_throne
	tag = BLO
}
#war_goal = {
#	type = take_claim
#	casus_belli = cb_conquest
#	province = 1388			# Nantes
#}

1341.4.30  = {
	add_attacker = MNF
	add_defender = BLO
	add_attacker = ENG
	add_defender = FRA
} # Death of Jean III de Bretagne

1356.10.17 = {
	add_attacker = NAV
}

1364.9.29  = {
	battle = {
		name = "Auray"
		location = 174
		attacker = {
			commander =	"Jean Chandos"
			infantry = 3000
			cavalry = 500
			losses = 20	# percent
			country = ENG
		}
		defender = {
			commander =	"Bertrand du Guesclin"
			infantry = 3500
			cavalry = 500
			losses = 55	# percent
			country = FRA
		}
		result = yes
	}
}

1365.1.1   = {
	rem_attacker = MNF
	rem_attacker = NAV
	rem_attacker = ENG
	rem_defender = BLO
	rem_defender = FRA
} # Trait� de Gu�rande
