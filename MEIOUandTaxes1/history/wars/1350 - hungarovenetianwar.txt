name = "Conquest of Dalmatia"
#casus_belli = cb_conquest
war_goal = {
	type = take_claim
	casus_belli = cb_conquest
	province = 2571			# Zadar
}

1350.1.1   = {
	add_attacker = CRO
	add_attacker = HUN
	add_defender = VEN
	add_defender = RAG
	add_defender = NAX
}

1358.2.18  = {
	rem_attacker = HUN
	rem_attacker = CRO
	rem_defender = VEN
	rem_defender = RAG
	rem_defender = NAX
}
