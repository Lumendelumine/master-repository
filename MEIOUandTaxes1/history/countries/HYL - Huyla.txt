# HYL - Huyla

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = quechuan
religion = inti
technology_group = south_american
capital = 819

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1356.1.1 = {
	monarch = {
		name = "Tribal Council"
		dynasty = "Huyla"
		ADM = 4
		DIP = 2
		MIL = 4
	}
}
