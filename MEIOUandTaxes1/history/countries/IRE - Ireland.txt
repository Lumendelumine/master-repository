# IRE - Ireland
# 2010-jan-16 - FB - HT3 changes

government = feudal_monarchy government_rank = 5
mercantilism = 0.0
technology_group = western
primary_culture = irish
religion = catholic
capital = 373	# Dublin

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1542.8.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
	} # Queen Elizabeth's Offer

1641.1.30 = {
	monarch = {
		name = "Aedh XIV"
		dynasty = "mac E�ghain"
		ADM = 2
		DIP = 2
		MIL = 4
	}
}
