# QAH - Qhatan

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
technology_group = muslim
primary_culture = najdi
religion = sunni
capital = 470

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1354.1.1 = {
	monarch = {
		name = "Ya'rub"
		dynasty = "Al Qahtani"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

# 1744 - into Najd
