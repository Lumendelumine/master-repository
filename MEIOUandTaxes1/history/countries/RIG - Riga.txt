# RIG - Riga

government = theocratic_government government_rank = 3
mercantilism = 0.0
technology_group = western
primary_culture = baltendeutsche
religion = catholic
capital = 38	# Riga

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 1 }
}

#Archbishops
1348.10.19 = {
	monarch = {
		name = "Fromhold von Vyshusen"
		DIP = 1
		ADM = 1
		MIL = 3
	}
}

1370.1.2 = {
	monarch = {
		name = "Siegfried Blomberg"
		DIP = 2
		ADM = 3
		MIL = 2
	}
}

1374.3.13 = {
	monarch = {
		name = "Johann IV von Zinten"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1393.1.1 = {
	monarch = {
		name = "Johann V"
		DIP = 3
		ADM = 2
		MIL = 3
	}
}

1399.1.1 = { set_country_flag = hanseatic_league }

1418.1.1 = {
	monarch = {
		name = "Johann VI"
		DIP = 1
		ADM = 3
		MIL = 3
	}
}

1424.1.1 = {
	monarch = {
		name = "Henning Scharffenberg"
		DIP = 4
		ADM = 2
		MIL = 2
	}
}

1448.10.9 = {
	monarch = {
		name = "Silvester Stodwescher"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1479.7.12 = {
	monarch = {
		name = "Stephan Grube"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1483.12.10 = {
	monarch = {
		name = "Michael Hildebrand"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1509.2.6 = {
	monarch = {
		name = "Jasper Linde"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1524.6.29 = {
	monarch = {
		name = "Johann IV Blankenfelde"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1528.2.6= {
	monarch = {
		name = "Thomas Sch�ning"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1539.8.11 = {
	monarch = {
		name = "Wilhelm von Brandenburg"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1555.11.1 = {
	monarch = {
		name = "Christoph von Mecklenburg"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

# City of Riga becomes a Free Imperial City
1561.1.1 = {
	government = imperial_city
	monarch = {
		name = "Stadtrat"
		adm = 3
		dip = 3
		mil = 3
		regent = yes
	}
}

# Annexed by Poland 1563.8.4
