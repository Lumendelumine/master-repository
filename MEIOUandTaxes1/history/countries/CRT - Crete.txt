# CRT - Crete

government = feudal_monarchy government_rank = 5 #KINGDOM
mercantilism = 0.0
technology_group = eastern
primary_culture = greek
#add_accepted_culture = greek
religion = orthodox
capital = 163	# Heraklion

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 0 }
	set_country_flag = crusader_kingdom
}
