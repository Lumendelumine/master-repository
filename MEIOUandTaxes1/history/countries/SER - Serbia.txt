# SER - Serbia
# Knezevina Srbija (Principality of Serbia)

government = feudal_monarchy government_rank = 6
mercantilism = 0.0
primary_culture = serbian
religion = orthodox
technology_group = eastern
capital = 148 # Skpoje
historical_rival = BYZ
historical_rival = HUN

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1346.4.16 = {
	add_country_modifier = { name = obstacle_feudal_fragmentation duration = -1 }
}

1355.4.16 = {
	monarch = {
		name = "Stefan Uros V"
		dynasty = "Nemanjic"
		ADM = 1
		DIP = 1
		MIL = 1
	}
	set_country_flag = no_heir
	add_ruler_modifier = {
		name = no_worthy_heir
		duration = -1
	}
}

1371.12.4 = {
	monarch = {
		name = "Lazar"
		dynasty = "Hrebeljanovic"
		ADM = 3
		DIP = 5
		MIL = 3
	}
}

1389.1.1  = {
	capital = 140 # Kosovo
}

1389.6.28 = {
	monarch = {
		name = "Stefan III"
		dynasty = "Hrebeljanovic"
		ADM = 6
		DIP = 4
		MIL = 5
	}
	heir = {
		name = "Djurad"
		monarch_name = "Djurad"
		dynasty = "Brankovic"
		birth_date = 1377.1.1
		death_date = 1456.12.24
		claim = 85
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1402.1.1 = {
	government = feudal_monarchy government_rank = 4
}

1427.7.20 = {
	monarch = {
		name = "Djuradj"
		dynasty = "Brankovic"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1427.7.20 = {
	heir = {
		name = "Lazar"
		monarch_name = "Lazar II"
		dynasty = "Brankovic"
		birth_date = 1421.1.1
		death_date = 1458.2.20
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 2
	}
}

1456.12.25 = {
	government = despotic_monarchy government_rank = 4
	remove_country_modifier = obstacle_feudal_fragmentation
	monarch = {
		name = "Lazar II"
		dynasty = "Brankovic"
		ADM = 3
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Stefan"
		monarch_name = "Stefan III"
		dynasty = "Brankovic"
		birth_date = 1417.1.1
		death_date = 1476.10.9
		claim = 85
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1458.1.21 = {
	monarch = {
		name = "Stefan III"
		dynasty = "Brankovic"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1459.1.1   = {
	capital = 3773
}

1459.4.9 = {
	monarch = {
		name = "Stjepan Tomaševic"
		dynasty = "Kotromanic"
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1521.8.28 = {
	capital = 2612
}

1781.1.1 = { leader = {	name = "Peter" type = general	rank = 0	fire = 2	shock = 3	manuever = 3	siege = 1	death_date = 1830.10.18 } }

1804.2.14 = {
	monarch = {
		name = "Djordje"
		dynasty = "Karadordevic"
		ADM = 6
		DIP = 5
		MIL = 5
	}
}

1806.10.11 = {
	heir = {
		name = "Alexander"
		monarch_name = "Alexander"
		dynasty = "Karadordevic"
		claim = 95
		birth_date = 1806.10.11
		death_date = 1885.5.3
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1813.10.4 = {
	monarch = {
		name = "Milosh"
		dynasty = "Obrenovic"
		ADM = 5
		DIP = 6
		MIL = 6
	}
}

1819.10.21 = {
	heir = {
		name = "Milosh"
		monarch_name = "Milosh II"
		dynasty = "Obrenovic"
		birth_date = 1819.10.21
		death_date = 1839.7.6
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
