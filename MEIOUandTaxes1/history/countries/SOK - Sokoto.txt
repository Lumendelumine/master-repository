#SOK - Sokoto
#10.01.27 FB-HT3 - make HT3 changes

government = tribal_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = mali
religion = sunni
technology_group = soudantech

capital = 1555	# Sokoto

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -1 }
}

1804.1.1 = {
	monarch = {
		name = "'Uthman"
		dynasty = "Fodio"
		DIP = 3
		ADM = 4
		MIL = 3
	}
}
