# MUD - Udege

government = altaic_monarchy government_rank = 4
mercantilism = 0.0
technology_group = steppestech
primary_culture = evenki
religion = shamanism
capital = 3247 # Ussuri

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "Council of Elders"
		ADM = 2
		DIP = 2
		MIL = 2
		regent = yes
	}
}
