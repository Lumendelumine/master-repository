# SCH - Schwarzburg

government = feudal_monarchy
government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = high_saxon
religion = catholic
capital = 3737

1000.1.1   = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1308.1.1   = {
	monarch = {
		name = "Heinrich VII"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

# 1324 - 1526 : unknown

1526.1.1   = {
	monarch = {
		name = "G�nther XL"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1529.10.3 = {
	heir = {
		name = "G�nther"
		monarch_name = "G�nther XLI"
		dynasty = "von Schwarzburg"
		birth_date = 1529.9.25
		death_date = 1583.5.23
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1540.1.1   = {
	religion = protestant
}

1552.1.1   = {
	monarch = {
		name = "G�nther XLI"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1571.1.1   = {
	monarch = {
		name = "Johan G�nther I"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1586.1.1   = {
	monarch = {
		name = "G�nther XLII"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1643.1.1   = {
	monarch = {
		name = "Anton G�nther I"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1666.1.1   = {
	monarch = {
		name = "Christian Wilhelm"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1721.5.10  = {
	monarch = {
		name = "G�nther XLIII"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1740.11.28 = {
	monarch = {
		name = "Heinrich XXXV"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1758.11.6  = {
	monarch = {
		name = "Christian G�nther III"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1794.10.14 = {
	monarch = {
		name = "G�nther Friedrich I"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1835.8.19  = {
	monarch = {
		name = "G�nther Friedrich II"
		dynasty = "von Schwarzburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
