# No previous file for Majeerteen

government = despotic_monarchy government_rank = 4
mercantilism = 0.15
technology_group = indian
unit_type = sub_saharan
religion = sunni
primary_culture = somali
capital = 3053

1530.1.1 = {
	monarch = {
		name= "Mahmud I"
		dynasty = "Harti"
		dip = 3
		mil = 3
		adm = 3
	}
}

1650.1.1 = {
	monarch = {
		name= "Mahmud I"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1679.1.1 = {
	monarch = {
		name= "Yusuf II"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1680.1.1 = {
	monarch = {
		name= "Uthman I"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1720.1.1 = {
	monarch = {
		name= "Yusuf I"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1740.1.1 = {
	monarch = {
		name= "Mahmud II"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1760.1.1 = {
	monarch = {
		name= "Mahmud III"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1780.1.1 = {
	monarch = {
		name= "Yusuf III"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1810.1.1 = {
	monarch = {
		name= "Mahmud IV"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}

1815.1.1 = {
	monarch = {
		name= "Uthman II"
		dynasty = "Harti"
		dip = 1
		mil = 1
		adm = 1
	}
}
