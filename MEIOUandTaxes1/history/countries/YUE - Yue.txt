government = chinese_monarchy_3 government_rank = 4
# innovative_narrowminded = -2
mercantilism = 0.0
primary_culture = yueyu
add_accepted_culture = hakka
religion = confucianism
technology_group = chinese
capital = 2121	# Guangzhou

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1352.1.1 = {
	monarch = {
		name = "Zhen"
		dynasty = "He"
		ADM = 5
		DIP = 5
		MIL = 5
	}
}

1662.1.1 = {
	monarch = {
		name = "Kexi"
		dynasty = "Shang"
		ADM = 4
		DIP = 4
		MIL = 4
	}
	heir = {
		name = "Zhixin"
		monarch_name = "Zhixin"
		dynasty = "Shang"
		birth_date = 1643.1.1
		death_date = 1680.8.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 5
	}
	change_government = chinese_monarchy_2
}
1673.11.1 = {
	monarch = {
		name = "Zhixin"
		dynasty = "Shang"
		ADM = 3
		DIP = 3
		MIL = 5
	}
}