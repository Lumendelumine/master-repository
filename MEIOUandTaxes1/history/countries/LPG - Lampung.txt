# LPG - Lampung
# MEIOU-FB Indonesia mod
# FB-TODO needs rulers until 1552.1.1
# 2010-jan-21 - FB - HT3 changes

government = indian_monarchy government_rank = 5
mercantilism = 0.0
technology_group = austranesian		# MEIOU-FB - was: chinese
primary_culture = sumatran
religion = hinduism
capital = 623

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 0 }
}

1337.1.1 = {
	monarch = {
		name = "Purwawisesa"
		dynasty = "Gunungjati"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
