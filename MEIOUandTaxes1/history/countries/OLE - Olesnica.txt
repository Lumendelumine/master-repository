# Duchy of Olesnica
# Tag : OLE

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
technology_group = western
primary_culture = silesian
religion = catholic
capital = 1359

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 0 }
}

#Duchy of Jawor-Swidnica (Jauer-Schweidnitz)
1326.1.1 = {
	monarch = {
		name = "Bolko II"
		dynasty = "Piast"
		ADM = 4
		DIP = 7
		MIL = 5
		leader = {
			name = "Bolko II"
			type = general
			rank = 1
			fire = 3
			shock = 3
			manuever = 3
			siege = 1
		}
	}
}
#Duchy of Olesnica (Oels) from now on
#This line of Piast dukes had a very confusing manner of naming all of their sons Konrad and making them all joint rulers.
#The game does not handle this, so some unhistorical meddling had to be made in order to keep the numbering.
1338.3.25 = {
	heir = {
		name = "Konrad"
		monarch_name = "Konrad II"
		dynasty = "Piast"
		birth_date = 1338.3.25
		death_date = 1403.6.10
		claim = 95
		ADM = 4
		DIP = 2
		MIL = 2
	}
}

1368.7.28 = {
	monarch = {
		name = "Konrad II"
		dynasty = "Piast"
		ADM = 4
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad III"
		dynasty = "Piast"
		birth_date = 1359.1.1
		death_date = 1412.12.28
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 2
	}
}
1403.6.10 = {
	monarch = {
		name = "Konrad III"
		dynasty = "Piast"
		ADM = 2
		DIP = 3
		MIL = 2
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad IV"
		dynasty = "Piast"
		birth_date = 1380.1.1
		death_date = 1416.1.1
		claim = 95
		ADM = 1
		DIP = 5
		MIL = 4
	}
}
1412.12.28 = {
	monarch = {
		name = "Konrad IV"
		dynasty = "Piast"
		ADM = 1
		DIP = 5
		MIL = 4
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad V"
		dynasty = "Piast"
		birth_date = 1382.1.1
		death_date = 1427.1.1
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 2
	}
}
1416.1.1 = {
	monarch = {
		name = "Konrad V"
		dynasty = "Piast"
		ADM = 4
		DIP = 3
		MIL = 2
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad VI"
		dynasty = "Piast"
		birth_date = 1386.1.1
		death_date = 1427.12.31
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 2
	}
}
1427.1.1 = {
	monarch = {
		name = "Konrad VI"
		dynasty = "Piast"
		ADM = 1
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad VII"
		dynasty = "Piast"
		birth_date = 1400.1.1
		death_date = 1439.9.10
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 2
	}
}
1427.12.31 = {
	monarch = {
		name = "Konrad VII"
		dynasty = "Piast"
		ADM = 1
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad VIII"
		dynasty = "Piast"
		birth_date = 1401.1.1
		death_date = 1452.2.14
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 3
	}
}
1439.9.10 = {
	monarch = {
		name = "Konrad VIII"
		dynasty = "Piast"
		ADM = 1
		DIP = 1
		MIL = 3
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad IX"
		dynasty = "Piast"
		birth_date = 1415.1.1
		death_date = 1471.8.14
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

1452.2.14 = {
	monarch = {
		name = "Konrad IX"
		dynasty = "Piast"
		ADM = 3
		DIP = 2
		MIL = 4
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad X"
		dynasty = "Piast"
		birth_date = 1415.1.1
		death_date = 1492.9.21
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 4
	}
}
1471.8.14 = {
	monarch = {
		name = "Konrad X"
		dynasty = "Piast"
		ADM = 3
		DIP = 2
		MIL = 4
	}
	heir = {
		name = "Konrad"
		monarch_name = "Konrad X"
		dynasty = "Piast"
		birth_date = 1415.1.1
		death_date = 1492.9.21
		claim = 95
		ADM = 4
		DIP = 5
		MIL = 1
	}
}
1492.9.21 = {
	monarch = {
		name = "Henryk I"
		dynasty = "z Podebrad"
		ADM = 4
		DIP = 5
		MIL = 1
	}
	heir = {
		name = "Karol"
		monarch_name = "Karol I"
		dynasty = "z Podebrad"
		birth_date = 1476.5.4
		death_date = 1536.5.31
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 3
	}
}
1498.6.24 = {
	monarch = {
		name = "Karol I"
		dynasty = "z Podebrad"
		ADM = 1
		DIP = 4
		MIL = 3
	}
}

1507.5.29 = {
	heir = {
		name = "Henryk"
		monarch_name = "Henryk II"
		dynasty = "z Podebrad"
		birth_date = 1507.5.29
		death_date = 1548.8.2
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
1536.5.31 = {
	monarch = {
		name = "Henryk II"
		dynasty = "z Podebrad"
		ADM = 3
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Jan"
		monarch_name = "Jan I"
		dynasty = "z Podebrad"
		birth_date = 1509.11.5
		death_date = 1565.2.28
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
1548.8.2 = {
	monarch = {
		name = "Jan I"
		dynasty = "z Podebrad"
		ADM = 3
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Karol"
		monarch_name = "Karol II"
		dynasty = "z Podebrad"
		birth_date = 1545.4.15
		death_date = 1617.1.28
		claim = 95
		ADM = 5
		DIP = 2
		MIL = 2
	}
}
1565.2.28 = {
	monarch = {
		name = "Karol II"
		dynasty = "z Podebrad"
		ADM = 5
		DIP = 2
		MIL = 2
	}
}
1593.10.18 = {
	heir = {
		name = "Karol Fryderyk"
		monarch_name = "Karol I Fryderyk"
		dynasty = "z Podebrad"
		birth_date = 1593.10.18
		death_date = 1647.5.31
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 1
	}
}
1617.1.28 = {
	monarch = {
		name = "Karol I Fryderyk"
		dynasty = "z Podebrad"
		ADM = 2
		DIP = 4
		MIL = 1
	}
}
1622.5.2 = {
	heir = {
		name = "Sylwiusz"
		monarch_name = "Sylwiusz I"
		dynasty = "von Württemberg"
		birth_date = 1622.5.2
		death_date = 1664.4.24
		claim = 95
		ADM = 6
		DIP = 4
		MIL = 2
	}
}
1647.5.31 = {
	monarch = {
		name = "Sylwiusz I"
		dynasty = "von Württemberg"
		ADM = 6
		DIP = 4
		MIL = 2
	}
}
1650.1.1 = {
	heir = {
		name = "Karol"
		monarch_name = "Karol II"
		dynasty = "von Württemberg"
		birth_date = 1650.1.1
		death_date = 1669.12.12
		claim = 95
		ADM = 6
		DIP = 4
		MIL = 2
	}
}
1664.4.24 = {
	monarch = {
		name = "Karol II"
		dynasty = "von Württemberg"
		ADM = 6
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Sylwiusz"
		monarch_name = "Sylwiusz II"
		dynasty = "von Württemberg"
		birth_date = 1651.2.21
		death_date = 1697.7.3
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 2
	}
}
1669.12.12 = {
	monarch = {
		name = "Sylwiusz II"
		dynasty = "von Württemberg"
		ADM = 1
		DIP = 3
		MIL = 2
	}
	heir = {
		name = "Krystian"
		monarch_name = "Krystian I"
		dynasty = "von Württemberg"
		birth_date = 1652.4.19
		death_date = 1704.4.5
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
1697.7.3 = {
	monarch = {
		name = "Krystian I"
		dynasty = "von Württemberg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Karol Fryderyk"
		monarch_name = "Karol II Fryderyk"
		dynasty = "von Württemberg"
		birth_date = 1690.2.7
		death_date = 1761.12.14
		claim = 95
		ADM = 4
		DIP = 1
		MIL = 1
	}
}
1704.4.5 = {
	monarch = {
		name = "Karol II Fryderyk"
		dynasty = "von Württemberg"
		ADM = 4
		DIP = 1
		MIL = 1
	}
}
