# KRC - Principality of Karachev

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 1194

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1356.1.1 = {
	monarch = {
		name = "Mikhael"
		dynasty = "Odoyevsky"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
