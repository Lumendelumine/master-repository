# PTA - Pubjab #way more versitile as a tag, keeps me from having to use Punjab

government = indian_monarchy government_rank = 5
mercantilism = 0.0
technology_group = indian
primary_culture = panjabi
religion = sunni
capital = 2113	# Lahore
historical_friend = KSH

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
	set_country_flag = indian_sunni_state
}

#Lodis

1431.4.20 = {
	add_accepted_culture = multani
	add_accepted_culture = kanauji
	set_country_flag = rising_nation
	monarch = {
		name = "Bahl�l"
		dynasty = "Lodi"
		ADM = 4
		DIP = 5
		MIL = 6
		leader = { name = "Bahl�l Ludi"		type = general	rank = 0	fire = 4	shock = 4	manuever = 4	siege = 2 }
	}
}

1450.1.1 = {
	heir = {
		name = "Nizam Khan"
		monarch_name = "Sikandar Sh�h"
		dynasty = "Lodi"
		birth_Date = 1450.1.1
		death_Date = 1517.11.21
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 4
		leader = { name = "Sikandar Sh�h"	type = general	rank = 0	fire = 3	shock = 3	manuever = 4	siege = 0 }
	}
}

#Lodi's conquer Delhi
