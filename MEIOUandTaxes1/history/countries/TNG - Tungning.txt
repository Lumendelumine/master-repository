# rough setup

government = chinese_monarchy_3 government_rank = 5
# innovative_narrowminded = -2
mercantilism = 0.0
primary_culture = minyu
religion = confucianism
technology_group = chinese
capital = 2305	# 

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -2 }
}

1662.2.1 = {
	monarch = {
		name = "Chenggong"
		dynasty = "Zheng"
		ADM = 6
		DIP = 3
		MIL = 7
		leader = {	name = "Zheng Chenggong"	type = general	rank = 3	fire = 4	shock = 4	manuever = 4	siege = 3	death_date = 1662.6.23 }
	}
	heir = {
		name = "Jing"
		monarch_name = "Jing"
		dynasty = "Zheng"
		birth_date = 1642.10.25
		death_date = 1681.3.17
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 3
	}
}
1662.6.23 = {
	monarch = {
		name = "Jing"
		dynasty = "Zheng"
		ADM = 5
		DIP = 4
		MIL = 7
	}
}
1664.1.1 = {
	heir = {
		name = "Kezang"
		monarch_name = "Kezang"
		dynasty = "Zheng"
		birth_date = 1664.1.1
		death_date = 1681.3.17
		claim = 95
		ADM = 5
		DIP = 3
		MIL = 3
	}
}
1678.8.17 = {
	monarch = {
		name = " Regency Council"
		regent = yes
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Keshuang"
		monarch_name = "Keshuang"
		dynasty = "Zheng"
		birth_date = 1670.8.13
		death_date = 1717.9.22
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}