# MAS - Marquessate of Massa
# 2010-jan-21 - FB - HT3 changes

government = signoria_monarchy government_rank = 2
mercantilism = 0.0
# land_naval = -2		##1
technology_group = western
primary_culture = emilian
religion = catholic
capital = 2564

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1442.1.1 = {
	monarch = {
		name = "Alberico I"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1445.1.1 = {
	monarch = {
		name = "Iacopo"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1481.1.1 = {
	monarch = {
		name = "Alberico II"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1519.1.1 = {
	monarch = {
		name = "Ricciarda"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1546.1.1 = {
	monarch = {
		name = "Giulio I"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1547.1.1 = {
	monarch = {
		name = "Ricciarda"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1554.1.1 = {
	monarch = {
		name = "Alberico I"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1623.1.1 = {
	monarch = {
		name = "Carlo I"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1662.1.1 = {
	monarch = {
		name = "Alberico II"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1664.1.1 = {
	government = feudal_monarchy government_rank = 3
}

1690.1.1 = {
	monarch = {
		name = "Carlo II"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1710.1.1 = {
	monarch = {
		name = "Alberico III"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1715.1.1 = {
	monarch = {
		name = "Alderano I"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1731.1.1 = {
	monarch = {
		name = "Maria Teresa"
		dynasty = "Malaspina"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1790.1.1 = {
	monarch = {
		name = "Maria Beatrice"
		dynasty = "d'Este"
		ADM = 3
		DIP = 3
		MIL = 3
		female = yes
	}
}

# Annexed by Cispandana and Cisalpina Republic during Napoleonic invasion (1796-1806) 

1806.1.1 = {
	monarch = {
		name = "Elisa Bonaparte"
		dynasty = "Baciocchi"
		ADM = 3
		DIP = 3
		MIL = 3
		female = yes
	}
}

1815.1.1 = {
	monarch = {
		name = "Maria Beatrice"
		dynasty = "d'Este"
		ADM = 3
		DIP = 3
		MIL = 3
		female = yes
	}
}

# (From 1829 to 1859 the Duchy di Massa-Carrara is annexed by the Duchy of Modena)
