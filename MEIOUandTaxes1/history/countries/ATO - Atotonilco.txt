# ATO - Atotonilco

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = tepanec
religion = nahuatl
technology_group = mesoamerican
capital = 3533 # Atotonilco

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}
