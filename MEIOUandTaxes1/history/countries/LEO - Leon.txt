# LEO - Kingdom  of Le�n
# 2010-jan-21 - FB - HT3 changes

government = feudal_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = asturian
add_accepted_culture = galician
religion = catholic
technology_group = western
capital = 208

historical_rival = CAS
historical_rival = GAL
historical_rival = SPA

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -3 }
}
