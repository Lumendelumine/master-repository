# OOE - Oye

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = tohoku
religion = mahayana
technology_group = chinese
capital = 2298	# Uzen
1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1306.1.1 = {
	monarch = {
		name = "Motomasa" 
		dynasty = "Oye"
		ADM = 4
		DIP = 4
		MIL = 4
		}
}
1330.1.1 = {
	heir = {
		name = "Tokishige"
		monarch_name = "Tokishige"
		dynasty = "Oye"
		birth_date = 1330.1.1
		death_date = 1373.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1345.1.1 = {
	heir = {
		name = "Shigenobu"
		monarch_name = "Shigenobu"
		dynasty = "Oye"
		birth_date = 1345.1.1
		death_date = 1368.1.1
		claim = 90
		ADM = 3
		DIP = 4
		MIL = 5
	}
}
1359.1.1 = {
	monarch = {
		name = "Tokishige" 
		dynasty = "Oye"
		ADM = 2
		DIP = 5
		MIL = 3
		}
}
#1373.1.1 Shigenobu's adopted son becomes clan leader but adopts the surname of Sagaye from the castle fief he's given(end of Oye clan)