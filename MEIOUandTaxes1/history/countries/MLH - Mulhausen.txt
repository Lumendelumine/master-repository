# MLH - Mulhausen

government = imperial_city
mercantilism = 0.0
primary_culture = schwabisch
religion = catholic
technology_group = western
capital = 4007

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1349.1.1 = {
	monarch = {
		name = "City Council"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
