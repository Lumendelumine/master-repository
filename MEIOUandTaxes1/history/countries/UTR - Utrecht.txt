# UTR - Utrecht

government = theocratic_government government_rank = 3
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = dutch
capital = 98	# Utrecht

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

#Prince Bishops of Utrecht
1342.1.1 = {
	monarch = {
		name = "Jan IV van Arkel"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1364.1.1 = {
	monarch = {
		name = "Jan V van Virneburg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1371.1.1 = {
	monarch = {
		name = "Arnold II van Hoom"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1379.1.1 = {
	monarch = {
		name = "Floris van Wevelinkhoven"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1393.1.1 = {
	monarch = {
		name = "Frederick III"
		ADM = 4
		DIP = 5
		MIL = 2
	}
}

1423.1.1 = {
	monarch = {
		name = "Rudolf I"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1455.4.7 = {
	monarch = {
		name = "Gijsbrecht I"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1456.8.3 = {
	monarch = {
		name = "David I"
		ADM = 6
		DIP = 6
		MIL = 3
	}
}

1494.4.16 = {
	monarch = {
		name = "Frederik IV"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1517.5.9 = {
	monarch = {
		name = "Filips I"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1524.4.7 = {
	monarch = {
		name = "Hendrik II"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

#Sold to Charles V on 21 August 1528
