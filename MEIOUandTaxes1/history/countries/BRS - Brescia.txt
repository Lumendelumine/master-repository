# BRS - Brescia

government = merchant_republic
mercantilism = 0.0
primary_culture = venetian
religion = catholic
technology_group = western
capital = 1345	# Brescia

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}
1356.1.1 = { #FB - temp pending further research
#1403.1.1 = {
	monarch = {
		name = "Gian"
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1404.9.12 = {
	monarch = {
		name = "Pandolf"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}
