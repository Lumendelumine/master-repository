# TPQ - Tepequacuilco

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = chontal
religion = nahuatl
technology_group = mesoamerican
capital = 1470 # Tepequacuilco

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}
