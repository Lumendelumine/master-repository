# PIP - Cuzcatlan

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = nawa
religion = mesoamerican_religion
technology_group = mesoamerican
capital = 834

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1   = {
	monarch = {
		name = "Monarch"
		dynasty = "Cuzcatlan"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
