# BLU - Blue Horde
# MEIOU-GG Government changes

government = steppe_horde government_rank = 5
mercantilism = 0.0
primary_culture = tartar
religion = sunni
technology_group = steppestech
capital = 1310	# Sarai
historical_rival = WHI
historical_friend = MOS

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}
1356.1.1 = {
	monarch = {
		name = "Berdi Beg"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 3
	}
}

1360.5.28 = {
	monarch = {
		name = "Hizr"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1361.8.1 = {
	monarch = {
		name = "Temur Hoja"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1362.2.14 = {
	monarch = {
		name = "Amurat"
		dynasty = "Jochid"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1367.10.27 = {
	monarch = {
		name = "Aziz Hoja"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1369.1.5 = {
	monarch = {
		name = "Jani Beg II"
		dynasty = "Jochid"
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

1370.6.4 = {
	monarch = {
		name = "Tulun Beg Khanum"
		dynasty = "Jochid"
		ADM = 3
		DIP = 3
		MIL = 2
		female = yes
	}
}

1373.3.19 = {
	monarch = {
		name = "Ai Beg"
		dynasty = "Jochid"
		ADM = 1
		DIP = 3
		MIL = 2
	}
}

1378.1.1 = {
	monarch = {
		name = "Mamai"
		dynasty = "Jochid"
		ADM = 1
		DIP = 3
		MIL = 2
	}
}

1380.9.8 = {
	monarch = {
		name = "Tokhtamysh"
		dynasty = "Jochid"
		ADM = 7
		DIP = 7
		MIL = 7
		leader = {
			name = "Tokhtamysh"
			type = general
			rank = 1
			fire = 4
			shock = 4
			manuever = 5
			siege = 0
			death_date = 1406.1.1
		}
	}
}

# Unification of the Golden Horde under one banner
