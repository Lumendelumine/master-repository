# MUZ - Muzaffarids (Kerman)

government = despotic_monarchy government_rank = 5 #SHAHDOM
mercantilism = 0.0
technology_group = muslim
primary_culture = persian
religion = sunni
capital = 435
fixed_capital = 435

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1314.1.1   = {
	monarch = {
		name = "Mubariz ad-Din Muhammad"
		dynasty = "Muzaffarid"
		ADM = 4
		DIP = 3	
		MIL = 5
	}
	heir = {
		name = "Abu'l Fawaris Djamal"
		monarch_name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		dynasty = "Muzaffarid"
		birth_date = 1300.1.1
		death_date = 1358.1.1
		claim = 95
		ADM = 4
		DIP = 3	
		MIL = 5
	}
	add_ruler_modifier = { name = "cruel" }
	add_country_modifier = { name = obstacle_succession duration = -1 }
	add_country_modifier = { name = obstacle_feuding duration = -1 }
	add_country_modifier = { name = obstacle_shifting_loyalties duration = -1 }
}

1335.1.1   = {
	monarch = {
		name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		dynasty = "Muzaffarid"
		ADM = 4
		DIP = 3	
		MIL = 5
	}
	heir = {
		name = "Qutb"
		monarch_name = "Qutb Al-Din Shah Mahm"
		dynasty = "Muzaffarid"
		birth_date = 1320.1.1
		death_date = 1366.1.1
		claim = 95
		ADM = 4
		DIP = 3	
		MIL = 5
	}
}

1358.1.1   = {
	monarch = {
		name = "Qutb Al-Din Shah Mahm"
		dynasty = "Muzaffarid"
		ADM = 4
		DIP = 3	
		MIL = 5
	}
	heir = {
		name = "Abu'l Fawaris Djamal"
		monarch_name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		dynasty = "Muzaffarid"
		birth_date = 1340.1.1
		death_date = 1384.1.1
		claim = 95
		ADM = 4
		DIP = 3	
		MIL = 5
	}
}

1366.1.1   = {
	capital = 429
	monarch = {
		name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		dynasty = "Muzaffarid"
		ADM = 4
		DIP = 3	
		MIL = 5
	}
	heir = {
		name = "Mujahid"
		monarch_name = "Mujahid ad-Din Zain Al-Abidin 'Ali"
		dynasty = "Muzaffarid"
		birth_date = 1360.1.1
		death_date = 1420.1.1
		claim = 95
		ADM = 4
		DIP = 3	
		MIL = 5
	}
}

1384.1.1   = {
	monarch = {
		name = "Mujahid ad-Din Zain Al-Abidin 'Ali"
		dynasty = "Muzaffarid"
		ADM = 4
		DIP = 3	
		MIL = 5
	}
}

# 1387 - Fall to Tamerlane
