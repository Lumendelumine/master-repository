#RYU - Ryukyu

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
technology_group = chinese
religion = animism
primary_culture = ryukyuan
capital = 1013	# Chuzan

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

#Kings of Ryukyu (Chuzan/Zhongshan) 
#Japanese form of names given
1355.1.1 = {
	monarch = {
		name = "Satto"
		dynasty = "Satto"
		ADM = 3
		DIP = 4
		MIL = 2
	}

}

1395.1.1 = {
	monarch = {
		name = "Bunei"
		dynasty = "Satto"
		ADM = 1
		DIP = 4
		MIL = 2
	}
}

1400.1.1 = {
	monarch = {
		name = "Sho Shisho"
		dynasty = "Sho"
		ADM = 2
		DIP = 3
		MIL = 3
	}
}

1400.1.1 = {
	heir = {
		name = "Sho Hashi"
		monarch_name = "Sho Hashi"
		dynasty = "Sho"
		birth_Date = 1400.1.1
		death_Date = 1440.1.1
		claim = 95
		ADM = 4
		DIP = 1
		MIL = 1
	}
}

1421.1.1 = {
	monarch = {
		name = "Sho Hashi"
		dynasty = "Sho"
		ADM = 4
		DIP = 1
		MIL = 1
	}
}

1421.1.1 = {
	heir = {
		name = "Sho Chu"
		monarch_name = "Sho Chu"
		dynasty = "Sho"
		birth_Date = 1420.1.1
		death_Date = 1445.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1440.1.1 = {
	monarch = {
		name = "Sho Chu"
		dynasty = "Sho"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1445.1.1 = {
	monarch = {
		name = "Sho Shitatsu"
		dynasty = "Sho"
		ADM = 4
		DIP = 1
		MIL = 2
	}
}

1450.1.1 = {
	monarch = {
		name = "Sho Kimpuku"
		dynasty = "Sho"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1452.1.1 = {
	monarch = {
		name = "Sho Taikyu"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1452.1.1 = {
	heir = {
		name = "Sho Toku"
		monarch_name = "Sho Toku"
		dynasty = "Sho"
		birth_Date = 1452.1.1
		death_Date = 1469.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1460.1.1 = {
	monarch = {
		name = "Sho Toku"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1460.1.1 = {
	heir = {
		name = "Sho En"
		monarch_name = "Sho En"
		dynasty = "Sho"
		birth_Date = 1460.1.1
		death_Date = 1477.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1469.1.1 = {
	monarch = {
		name = "Sho En"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1469.1.1 = {
	heir = {
		name = "Sho Sen-i"
		monarch_name = "Sho Sen-i"
		dynasty = "Sho"
		birth_Date = 1450.1.1
		death_Date = 1477.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1477.1.1 = {
	monarch = {
		name = "Sho Sen-i"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1477.7.1 = {
	monarch = {
		name = "Sho Shin"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1477.7.1 = {
	heir = {
		name = "Sho Sei"
		monarch_name = "Sho Sei"
		dynasty = "Sho"
		birth_Date = 1477.1.1
		death_Date = 1556.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1526.1.1 = {
	monarch = {
		name = "Sho Sei"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1526.1.1 = {
	heir = {
		name = "Sho Gen"
		monarch_name = "Sho Gen"
		dynasty = "Sho"
		birth_Date = 1526.1.1
		death_date = 1571.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1556.1.1 = {
	monarch = {
		name = "Sho Gen"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1556.1.1 = {
	heir = {
		name = "Sho Yei"
		monarch_name = "Sho Yei"
		dynasty = "Sho"
		birth_Date = 1556.1.1
		death_date = 1588.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1571.1.1 = {
	monarch = {
		name = "Sho Yei"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1571.1.1 = {
	heir = {
		name = "Sho Nei"
		monarch_name = "Sho Nei"
		dynasty = "Sho"
		birth_date = 1571.1.1
		death_Date = 1621.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1588.1.1 = {
	monarch = {
		name = "Sho Nei"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1588.1.1 = {
	heir = {
		name = "Sho Ho"
		monarch_name = "Sho Ho"
		dynasty = "Sho"
		birth_Date = 1588.1.1
		death_date = 1648.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1621.1.1 = {
	monarch = {
		name = "Sho Ho"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1621.1.1 = {
	heir = {
		name = "Sho Shitsu"
		monarch_name = "Sho Shitsu"
		dynasty = "Sho"
		birth_Date = 1621.1.1
		death_date = 1669.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1624.1.1 = { religion = animism }

1648.1.1 = {
	monarch = {
		name = "Sho Shitsu"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1648.1.1 = {
	heir = {
		name = "Sho Tei"
		monarch_name = "Sho Tei"
		dynasty = "Sho"
		birth_Date = 1640.1.1
		death_Date = 1710.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1669.1.1 = {
	monarch = {
		name = "Sho Tei"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1669.1.1 = {
	heir = {
		name = "Sho Yeki"
		monarch_name = "Sho Yeki"
		dynasty = "Sho"
		birth_Date = 1669.1.1
		death_date = 1716.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1710.1.1 = {
	monarch = {
		name = "Sho Yeki"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1710.1.1 = {
	heir = {
		name = "Sho Kei"
		monarch_name = "Sho Kei"
		dynasty = "Sho"
		birth_Date = 1710.1.1
		death_Date = 1751.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1716.1.1 = {
	monarch = {
		name = "Sho Kei"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1716.1.1 = {
	heir = {
		name = "Sho Boku"
		monarch_name = "Sho Boku"
		dynasty = "Sho"
		birth_Date = 1716.1.1
		death_date = 1796.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1751.1.1 = {
	monarch = {
		name = "Sho Boku"
		dynasty = "Sho"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1751.1.1 = {
	heir = {
		name = "Sho On"
		monarch_name = "Sho On"
		dynasty = "Sho"
		birth_Date = 1751.1.1
		death_Date = 1803.1.1
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1796.1.1 = {
	monarch = {
		name = "Sho On"
		dynasty = "Sho"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1796.1.1 = {
	heir = {
		name = "Sho Sei"
		monarch_name = "Sho Sei"
		dynasty = "Sho"
		birth_Date = 1796.1.1
		death_Date = 1804.1.1
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1803.1.1 = {
	monarch = {
		name = "Sho Sei"
		dynasty = "Sho"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1803.1.1 = {
	heir = {
		name = "Sho Ko"
		monarch_name = "Sho Ko"
		dynasty = "Sho"
		birth_Date = 1790.1.1
		death_Date = 1828.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 2
	}
}

1804.1.1 = {
	monarch = {
		name = "Sho Ko"
		dynasty = "Sho"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}
