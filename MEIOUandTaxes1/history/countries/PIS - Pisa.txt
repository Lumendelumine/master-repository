#PIS - Pisa.txt
#2011-jan-15 FB Ghibelline Alliances

government = oligarchic_republic
mercantilism = 0.05
primary_culture = tuscan
religion = catholic
technology_group = western
capital = 1380	# Pisa
historical_friend = SIE
historical_rival = FIR
historical_rival = GEN

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1356.1.1 = {
	monarch = {
		name = "Gian dell'Angelo"
		ADM = 1
		DIP = 4
		MIL = 5
	}
}

1369.3.22 = {
	monarch = {
		name = "Pietro Gambacorta"
		ADM = 6
		DIP = 3
		MIL = 3
	}
}

1392.1.7 = {
	monarch = {
		name = "Jacopo Appiani"
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1398.1.1 = {
	monarch = {
		name = "Giordano Appiani"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}

1402.7.1 = {
	monarch = {
		name = "Gabriel Maria Visconti"
		ADM = 4
		DIP = 6
		MIL = 3
	}
}

1405.1.1 = {
	monarch = {
		name = "Gian Gambacorta"
		ADM = 2
		DIP = 2
		MIL = 4
	}
}

1494.1.1 = {
	monarch = {
		name = "Simone Orlandi"
		adm = 2
		dip = 4
		mil = 2
	}
}
