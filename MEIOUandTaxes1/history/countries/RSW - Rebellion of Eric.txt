# RSW - Rebellion of Erik Magnusson

government = feudal_monarchy government_rank = 5
mercantilism = 0.0
# serfdom_freesubjects = 4
technology_group = western
religion = catholic
primary_culture = swedish
add_accepted_culture = finnish
capital = 27	# �bo

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1356.1.1 = {
	set_country_flag = erik_s_rebellion
	monarch = {
		name = "Erik Magnusson"
		dynasty = "Birger"
		ADM = 4
		DIP = 2
		MIL = 5
	}
	set_country_flag = no_heir
}
1360.1.1   = {
	clr_country_flag = erik_s_rebellion
}
