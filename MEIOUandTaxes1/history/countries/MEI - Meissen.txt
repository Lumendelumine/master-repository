# MEI - Meissen

government = feudal_monarchy government_rank = 2 #MARGRAVIATE
mercantilism = 0.0
primary_culture = high_saxon
religion = catholic
technology_group = western
capital = 2617	# Dresden / Meissen

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1349.1.1 = {
	monarch = {
		name = "Friedrich III"
		dynasty = "von Wettin"
		DIP = 3
		ADM = 4
		MIL = 2
	}
}

1382.1.1 = {
	monarch = {
		name = "Wilhelm"
		dynasty = "von Wettin"
		DIP = 3
		ADM = 4
		MIL = 2
	}
	heir = {
		name = "Wilhelm"
		monarch_name = "Wilhelm II"
		dynasty = "von Wettin"
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 2
		birth_date = 1371.4.23
		death_date = 1425.3.13
	}
}

1407.2.11 = {
	monarch = {
		name = "Wilhelm II"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 3
		MIL = 2
	}
}

1425.3.13 = {
	monarch = {
		name = "Friedrich"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 3
		MIL = 2
	}
	heir = {
		name = "Friedrich"
		monarch_name = "Friedrich II"
		dynasty = "von Wettin"
		birth_date = 1412.8.22
		death_date = 1464.9.7
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 1
	}
}

1428.1.4 = {
	monarch = {
		name = "Friedrich II"
		dynasty = "von Wettin"
		DIP = 4
		ADM = 5
		MIL = 1
	}
}

1464.1.1 = {
	monarch = {
		name = "Ernst"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 3
		MIL = 2
	}
}

1464.9.7 = { elector = yes }

1485.8.26 = {
	monarch = {
		name = "Albrecht der Beherzte"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 3
		MIL = 6
	}
	heir = {
		name = "Georg"
		monarch_name = "Georg der B�rtige"
		dynasty = "von Wettin"
		birth_date = 1471.8.27
		death_date = 1539.4.17
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1485.8.26 = {
	elector = no
}

1500.9.12 = {
	monarch = {
		name = "Georg der B�rtige"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 2
		MIL = 3
	}
}

1500.9.12 = {
	heir = {
		name = "Heinrich"
		monarch_name = "Heinrich der Fromme"
		dynasty = "von Wettin"
		birth_date = 1473.3.16
		death_date = 1541.8.18
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1505.1.1 = { leader = {	name = "Mauritz"               	type = general	rank = 0	fire = 4	shock = 3	manuever = 3	siege = 0	death_date = 1532.1.1 } }

1531.1.1 = {
	religion = protestant
	government = constitutional_monarchy government_rank = 6}

1539.4.17 = {
	monarch = {
		name = "Heinrich der Fromme"
		dynasty = "von Wettin"
		DIP = 3
		ADM = 2
		MIL = 2
	}
}

1539.4.17 = {
	heir = {
		name = "Moritz"
		monarch_name = "Moritz"
		dynasty = "von Wettin"
		birth_date = 1521.3.21
		death_date = 1553.7.11
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 5
		leader = { name = "Moritz von Wettin"	type = general	rank = 0	fire = 3	shock = 3	manuever = 4	siege = 1 }
	}
}