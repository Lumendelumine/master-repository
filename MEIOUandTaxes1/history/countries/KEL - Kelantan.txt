# KEL - Kelantan
# GG - Adapted from Divide et Impera
# 2010-jan-16 - FB - HT3 changes

government = indian_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = malayan
religion = mahayana
technology_group = austranesian
capital = 599	# Kelantan

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1353.8.1 = {
	monarch = {
		name = "Bahar Raja"
		dynasty = "Choya"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

#1375-1411 to Ayutthaya

1411.1.1 = {
	monarch = {
		name = "Kumar Raja"
		dynasty = "Choya"
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1416.1.1 = {
	religion = sunni
		government = despotic_monarchy government_rank = 4
}

1416.1.1 = {
	monarch = {
		name = "Iskander Shah"
		dynasty = "Choya"
		ADM = 6
		DIP = 5
		MIL = 4
	}
}

1500.1.1 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Choya"
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1580.1.1 = {
	monarch = {
		name = "Husain"
		dynasty = "Choya"
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1760.1.1 = {
	monarch = {
		name = "Pandak of Kubang Labu"
		dynasty = "Long"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1764.1.1 = {
	monarch = {
		name = "Yunus"
		dynasty = "Long"
		ADM = 2
		DIP = 6
		MIL = 4
	}
}

1795.1.1 = {
	monarch = {
		name = "Muhammad Terengganu Sa-bagai Penangku"
		dynasty = "Long"
		ADM = 3
		DIP = 3
		MIL = 5
	}
}

1800.1.1 = {
	monarch = {
		name = "Muhammad I ibni al-Marhum Long Yunus"
		dynasty = "Long"
		ADM = 4
		DIP = 2
		MIL = 6
	}
}