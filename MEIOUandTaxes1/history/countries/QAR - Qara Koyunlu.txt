#QAR - Qara Koyunlu

government = altaic_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = yorouk
add_accepted_culture = azerbadjani 
religion = shiite
technology_group = muslim
capital = 3093
historical_rival = AKK

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1351.1.1 = {
	monarch = {
		name = "Bayram Khoya"
		dynasty = "Qara Koyunlu"
		ADM = 3
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Qara Mohammed"
		monarch_name = "Qara Mohammed"
		dynasty = "Qara Koyunlu"
		birth_date = 1340.1.1
		death_date = 1390.1.1
		claim = 50
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1360.1.1 = {
	capital = 1320
}

1380.1.1 = {
	monarch = {
		name = "Qara Mohammed"
		dynasty = "Qara Koyunlu"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Qara Yusuf"
		monarch_name = "Qara Yusuf"
		dynasty = "Qara Koyunlu"
		birth_date = 1360.1.1
		death_date = 1420.1.1
		claim = 50
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1390.1.1 = {
	monarch = {
		name = "Qara Yusuf"
		dynasty = "Qara Koyunlu"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1390.1.1 = {
	heir = {
		name = "Iskander"
		monarch_name = "Iskander"
		dynasty = "Qara Koyunlu"
		birth_date = 1390.1.1
		death_date = 1438.4.22
		claim = 50
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1409.1.1 = {
	capital = 416
}

1420.1.1 = {
	monarch = {
		name = "Iskander"
		dynasty = "Qara Koyunlu"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1420.1.1 = {
	heir = {
		name = "Jah�n"
		monarch_name = "Jah�n Sh�h"
		dynasty = "Qara Koyunlu"
		birth_date = 1420.1.1
		death_date = 1467.11.11
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1438.4.22 = {
	monarch = {
		name = "Jah�n Sh�h"
		dynasty = "Qara Koyunlu"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1438.4.22 = {
	heir = {
		name = "Hasan 'Ali"
		monarch_name = "Hasan 'Ali"
		dynasty = "Qara Koyunlu"
		birth_date = 1438.1.1
		death_date = 1468.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1467.11.11 = {
	monarch = {
		name = "Hasan 'Ali"
		dynasty = "Qara Koyunlu"
		ADM = 3
		DIP = 2
		MIL = 1
	}
}
