# MRV - Moravia

government = feudal_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = moravian
religion = catholic
capital = 265	# Brno
technology_group = western

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1415.5.4 = {
	} # Council of Constance and execution of Jan Huss

1419.11.1 = {
				set_country_flag = hussite_crusade
} #start of Hussite wars

1424.1.1 = {
	clr_country_flag = hussite_crusade
	set_country_flag = hussite_negotiation
}

1436.7.5 = {
		clr_country_flag = hussite_negotiation
	set_country_flag = hussite_compacta
} #Hussite wars end, freedom to worship

1462.3.31 = { set_country_flag = compacta_keep }

1621.1.1 = {
	decision = judiciary_act
}