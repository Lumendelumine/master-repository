# KKZ - Kakizaki

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = tohoku
religion = mahayana
technology_group = chinese
capital = 2299
#daimyo = yes

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1352.1.1 = {
	monarch = {
		name = "Takahiro"		dynasty = "Matzumae"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
