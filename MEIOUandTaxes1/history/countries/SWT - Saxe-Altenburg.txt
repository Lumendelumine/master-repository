# No previous file for Saxe-Altenburg

government = feudal_monarchy government_rank = 4 #DUCHY
mercantilism = 0.0
primary_culture = high_saxon
religion = catholic
elector = yes
technology_group = western
capital = 3853	# Altenburg

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1356.1.1 = {
	monarch = {
		name = "Rudolph II"
		dynasty = "Askanier"
		DIP = 3
		ADM = 3
		MIL = 1
	}
}

1370.1.1 = {
	monarch = {
		name = "Wenzel"
		dynasty = "Askanier"
		DIP = 3
		ADM = 3
		MIL = 1
	}
}

1388.1.1 = {
	monarch = {
		name = "Rudolf III"
		dynasty = "Askanier"
		DIP = 2
		ADM = 4
		MIL = 2
	}
}

1388.1.1 = {
	heir = {
		name = "Albrecht"
		monarch_name = "Albrecht III"
		dynasty = "Askanier"
		birth_date = 1375.1.1
		death_date = 1422.11.12
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1419.1.1 = {
	monarch = {
		name = "Albrecht III"
		dynasty = "Askanier"
		DIP = 4
		ADM = 3
		MIL = 3
	}
}

1423.1.6 = {
	elector = no
}

# 1423 - into Wettin Saxony
