# BGL - Bagelkhand

government = indian_monarchy government_rank = 5
mercantilism = 0.0
technology_group = indian
religion = hinduism
primary_culture = bagheli
capital = 2687	# Baghelkhand
fixed_capital = 2687 # 

 1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

#Baghelas - Need to find more info about this dynasty

1399.1.1 = {
	monarch = {
		name = "Vyaghra Dev" #Should be dead for atleast a few hundred years but have not been able to find info on this dynasty prior to 1495.
		dynasty = "Baghela"
		ADM = 2
		DIP = 3
		MIL = 3
	}
}

1495.1.1 = {
	monarch = {
		name = "Shaktivan Deo"
		dynasty = "Baghela"
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1495.1.1 = {
	heir = {
		name = "Veer Singh"
		monarch_name = "Veer Singh"
		dynasty = "Baghela"
		birth_Date = 1495.1.1
		death_Date = 1540.1.1
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1500.1.1 = {
	monarch = {
		name = "Veer Singh"
		dynasty = "Baghela"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1500.1.1 = {
	heir = {
		name = "Virbhan"
		monarch_name = "Virbhan Singh"
		dynasty = "Baghela"
		birth_Date = 1500.1.1
		death_Date = 1555.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1540.1.1 = {
	monarch = {
		name = "Virbhan Singh"
		dynasty = "Baghela"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1540.1.1 = {
	heir = {
		name = "Ramachandra"
		monarch_name = "Ramachandra Singh"
		dynasty = "Baghela"
		birth_Date = 1500.1.1
		death_Date = 1555.1.1
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 5
	}
}

1555.1.1 = {
	monarch = {
		name = "Ramachandra Singh"
		dynasty = "Baghela"
		ADM = 4
		DIP = 3
		MIL = 5
	}
}

1555.1.1 = {
	heir = {
		name = "Duryodhan"
		monarch_name = "Duryodhan Singh"
		dynasty = "Baghela"
		birth_Date = 1555.1.1
		death_Date = 1618.1.1 #Really deposed
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 2
	}
}

1592.1.1 = {
	monarch = {
		name = "Duryodhan Singh"
		dynasty = "Baghela"
		ADM = 1
		DIP = 4
		MIL = 2
	}
}

1592.1.1 = {
	heir = {
		name = "Vikramaditya"
		monarch_name = "Vikramaditya"
		dynasty = "Baghela"
		birth_Date = 1592.1.1
		death_Date = 1630.1.1
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 2
	}
}

1618.1.1 = {
	monarch = {
		name = "Vikramaditya"
		dynasty = "Baghela"
		ADM = 1
		DIP = 4
		MIL = 1
	}
}

1618.1.1 = {
	heir = {
		name = "Amar"
		monarch_name = "Amar Singh II"
		dynasty = "Baghela"
		birth_Date = 1618.1.1
		death_Date = 1643.1.1
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1630.1.1 = {
	monarch = {
		name = "Amar Singh II"
		dynasty = "Baghela"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1630.1.1 = {
	heir = {
		name = "Anoop"
		monarch_name = "Anoop Singh"
		dynasty = "Baghela"
		birth_Date = 1630.1.1
		death_Date = 1660.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

1643.1.1 = {
	monarch = {
		name = "Anoop Singh"
		dynasty = "Baghela"
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

1643.1.1 = {
	heir = {
		name = "Bhao"
		monarch_name = "Bhao Singh"
		dynasty = "Baghela"
		birth_Date = 1643.1.1
		death_Date = 1690.1.1
		claim = 95
		ADM = 1
		DIP = 5
		MIL = 6
	}
}

1660.1.1 = {
	monarch = {
		name = "Bhao Singh"
		dynasty = "Baghela"
		ADM = 1
		DIP = 5
		MIL = 6
	}
}

1660.1.1 = {
	heir = {
		name = "Anirudh"
		monarch_name = "Anirudh Singh"
		dynasty = "Baghela"
		birth_Date = 1660.1.1
		death_Date = 1700.1.1
		claim = 95
		ADM = 5
		DIP = 5
		MIL = 1
	}
}

1690.1.1 = {
	monarch = {
		name = "Anirudh Singh"
		dynasty = "Baghela"
		ADM = 5
		DIP = 5
		MIL = 1
	}
}

1690.1.1 = {
	heir = {
		name = "Avadhut"
		monarch_name = "Avadhut Singh"
		dynasty = "Baghela"
		birth_Date = 1690.1.1
		death_Date = 1755.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

1700.1.1 = {
	monarch = {
		name = "Avadhut Singh"
		dynasty = "Baghela"
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

1720.1.1 = {
	heir = {
		name = "Ajit"
		monarch_name = "Ajit Singh"
		dynasty = "Baghela"
		birth_Date = 1720.1.1
		death_Date = 1809.1.1
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1755.1.1 = {
	monarch = {
		name = "Ajit Singh"
		dynasty = "Baghela"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1755.1.1 = {
	heir = {
		name = "Jai"
		monarch_name = "Jai Singh"
		dynasty = "Baghela"
		birth_Date = 1755.1.1
		death_Date = 1835.1.1
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 1
	}
}

1809.1.1 = {
	monarch = {
		name = "Jai Singh"
		dynasty = "Baghela"
		ADM = 1
		DIP = 4
		MIL = 1
	}
}
