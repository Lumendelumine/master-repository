# DLM - Dalmatia
# 2015-JUL-13 - Suaske666

government = feudal_monarchy
government_rank = 5
mercantilism = 0.1
primary_culture = dalmatian
add_accepted_culture = croatian
religion = catholic
technology_group = eastern
capital = 136		# Split

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 0 }
}
