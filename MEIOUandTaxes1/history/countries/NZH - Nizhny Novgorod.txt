# Principality of Nizhny Novgorod-Suzdal

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 1292	# Nizhny Novgorod

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1350.1.1 = {
	monarch = {
		name = "Dmitry I"
		dynasty = "Rurikovich"
		ADM = 5
		DIP = 3
		MIL = 5
	}
}

# 1392 to Muscowy
