# 80 - Ulm

owner = ULM
controller = ULM
culture = schwabisch
religion = catholic
trade_goods = linen
capital = "Ulm"
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
fort_14th = yes
add_core = ULM
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1111.1.1 = { post_system = yes }
1500.1.1 = { road_network = yes }
1530.1.1   = { religion = protestant }
1560.1.1   = { fort_14th = no fort_16th = yes }
1803.1.1   = { owner = BAV controller = BAV add_core = BAV }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1810.1.1   = { owner = WUR controller = WUR add_core = WUR }
