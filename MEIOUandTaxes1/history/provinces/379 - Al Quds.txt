# 379 - Judea

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "al Quds"
trade_goods = olive
hre = no
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes
fort_14th = yes
estate = estate_church
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1100.1.1 = { marketplace = yes }
1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = MAM
	add_core = SYR
	add_core = CYP
}
1516.1.1   = { add_core = TUR }
1516.11.8  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1750.1.1 = {
	
}
#1831.1.1 = {
#	controller = EGY
#}
#1833.6.1 = {
#	owner = EGY
#}
