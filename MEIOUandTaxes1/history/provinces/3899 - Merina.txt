# 3899 - Merina

owner = MER
controller = MER
add_core = MER
culture = vazimba
religion = animism
capital = "Merina"
is_city = yes
trade_goods = rice
base_tax = 3
base_production = 3
base_manpower = 2
hre = no
discovered_by = ANT
discovered_by = BET
discovered_by = MER
discovered_by = MHF
discovered_by = MNB
discovered_by = SIH
discovered_by = TAM

1500.9.1   = {
	discovered_by = POR
} # Diego Dias
1596.1.1   = {
	discovered_by = NED
} # Cornelis de Houtman
1885.12.17 = {
	add_core = FRA
}
1897.2.28  = {
	owner = FRA
	controller = FRA
}
