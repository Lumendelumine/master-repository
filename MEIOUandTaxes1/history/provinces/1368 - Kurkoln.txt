# 1368 - Kurk�ln

owner         = KOL
controller    = KOL
add_core      = KOL
culture       = ripuarianfranconian
religion      = catholic
trade_goods   = wheat
capital       = "Bonn"
base_tax      = 4
base_production      = 4
base_manpower      = 3
is_city       = yes

fort_14th         = yes

hre           = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1300.1.1 = { road_network = yes }
1466.1.1  = {
	
} # First Printer opens in K�ln
#1500.1.1 = { road_network = yes }
1520.1.1  = {
	fort_14th = yes
	
}
1553.1.1  = {
	
} # Stock Exchange Founded
1638.1.1  = {
	
	base_tax = 10
base_production = 10
	
} # K�ln manages to stay neutral in the 30 years war and prospers through weapon sales. 
1716.1.1  = {
	refinery = yes
	weapons = no
	base_tax = 12
base_production = 12
} # Farnia begins exporting "Eau de Cologne"
1801.2.9  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Lun�ville
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = PRU
	controller = PRU
	add_core = PRU
   	remove_core = FRA
} # Congress of Vienna
