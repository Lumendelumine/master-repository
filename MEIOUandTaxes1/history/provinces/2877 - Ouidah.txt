# 2877 - Ouidah

culture = fon
religion = west_african_pagan_reformed
capital = "Gl?xw�"
trade_goods = crops # palm
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 80
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech

1471.1.1   = { discovered_by = POR } 
1580.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
	capital = "Ajuda"
		trade_goods = slaves
}
