# 1357 - Bremen

owner = FRB
controller = FRB
culture = old_saxon
religion = catholic
capital = "Bremen"
trade_goods = fish
hre = yes
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes

add_core = FRB
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1000.1.1   = {
	add_permanent_province_modifier = {
		name = weser_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
	}
}
1088.1.1 = { dock = yes shipyard = yes }
1100.1.1 = { merchant_guild = yes }
1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1510.1.1   = { fort_14th = yes }
1529.1.1   = { religion = protestant }
1620.1.1   = { fort_14th = no fort_15th = yes }
1700.1.1   = { fort_15th = no fort_16th = yes }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
