# 271 - Podlassa

owner = LIT
controller = LIT
culture = polish
religion = catholic
capital = "Drohiczyn"
trade_goods = lumber
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
add_core = LIT
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1386.1.1   = {
	religion = catholic
}

1569.1.1   = { fort_14th = yes }
1569.3.5 = {
	owner = POL
	controller = POL
	add_core = POL
	remove_core = LIT
}# Annexed to the crown of poland before Union of Lublin
1569.7.1  = {	owner = PLC
		controller = PLC
		add_core = PLC
	    } # Union of Lublin
1702.5.1   = { controller = SWE } # Occupied again
1706.9.24  = { controller = PLC } # Karl XII defeated in the battle of Poltava
1794.3.24  = { unrest = 6 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 }
1795.10.24  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Third partition
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1806.11.3  = { controller = REB } # Polish uprising instigated by Napoleon
1807.7.9   = {
	owner = RUS
	controller = RUS
     	remove_core = PRU
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1814.4.11  = { controller = POL }
1815.6.9   = {
	controller = RUS
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
