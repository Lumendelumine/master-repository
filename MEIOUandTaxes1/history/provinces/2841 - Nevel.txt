# 2841 - Nevel

owner = SMO
controller = SMO
culture = ruthenian
religion = orthodox 
hre = no
base_tax = 3
base_production = 3
trade_goods = fur
base_manpower = 2
is_city = yes
capital = "Nevel"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech

1356.1.1   = {
	add_core = LIT
	add_core = SMO
	add_core = PLT
	owner = LIT
	controller = LIT
}
1567.1.1  = { fort_14th = yes }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1772.8.5  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = LIT
	remove_core = PLC
	culture = byelorussian
} # First partition of Poland
1779.1.1  = {  } # Almost entirely rebuilt.
 # Became a large trade center.
