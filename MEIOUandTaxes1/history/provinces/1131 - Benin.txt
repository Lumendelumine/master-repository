# 1131 - Benin

owner = BEN
controller = BEN
culture = yorumba
religion = west_african_pagan_reformed
capital = "Edo"
base_tax = 8
base_production = 8
base_manpower = 4
is_city = yes
trade_goods = gems

discovered_by = soudantech
hre = no

1356.1.1 = {
	add_core = BEN
}
