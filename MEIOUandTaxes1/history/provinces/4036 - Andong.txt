# No previous file for Andong

owner = YUA
controller = YUA
culture = jurchen
religion = shamanism
capital = "Andong"
trade_goods = fish
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1235.1.1  = {
	add_core = YUA
}
1308.1.1 = {#Shen viceroy
	owner = CSE
	controller = CSE
	add_core = CSE
}
1355.1.1  = {
	remove_core = YUA
}
1381.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CSE
}
#1530.1.1 = { fort_14th = no fort_15th = yes }
1619.4.29 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
