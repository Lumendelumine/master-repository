# 1137 - Yula

owner = ZZZ
controller = ZZZ
culture = haussa		
religion = west_african_pagan_reformed		 
capital = "Yula"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = pepper
discovered_by = soudantech
hre = no

1356.1.1 = {
	add_core = ZZZ
}
1807.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
1810.1.1 = {
	remove_core = ZZZ
}
