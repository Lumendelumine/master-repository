# 2291 - Echigo
# GG/LS - Japanese Civil War

owner = USG
controller = USG
culture = chubu
religion = mahayana #shinbutsu
capital = "Kasugayama"
trade_goods = rice
hre = no
base_tax = 7
base_production = 7
base_manpower = 10
is_city = yes

discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = USG
}
1542.1.1   = { discovered_by = POR }
1600.9.15  = {
	owner = DTE
	controller = DTE
} # Battle of Sekigahara
1630.1.1   = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
