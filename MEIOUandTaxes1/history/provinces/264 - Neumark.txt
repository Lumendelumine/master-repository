
owner = BRA
controller = BRA
culture = polabian # low_saxon
religion = catholic
capital = "Soldin"
trade_goods = livestock
hre = yes
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim

1119.1.1 = { bailiff = yes }
1356.1.1  = {
	add_core = BRA
}
1402.1.1  = { owner = TEU controller = TEU add_core = TEU }
1433.6.1  = { controller = POL }
1433.12.1 = { controller = TEU }
1444.1.1 = { add_core = BRA }
1454.1.1  = { owner = BRA controller = BRA remove_core = TEU }
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1530.1.1  = { religion = protestant culture = low_saxon }
1535.1.1  = { revolt = { type = pretender_rebels size = 3 leader = "Hans von K�strin" } controller = REB } 
1571.1.1  = { revolt = {} }
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king of Prussia
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
