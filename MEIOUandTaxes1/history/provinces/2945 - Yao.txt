# 2945 - Yao

owner = YAO
controller = YAO
culture = chadic
religion = sunni
capital = "Yao"
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
trade_goods = millet
discovered_by = soudantech
hre = no

1356.1.1   = {
	add_core = YAO
}
1750.1.1   = {
	owner = WAD
	controller = WAD
	add_core = WAD
}
