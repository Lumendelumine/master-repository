# No previous file for Maysan

owner = JAI
controller = JAI
culture = iraqi
religion = shiite
capital = "Maysan"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1   = {
	add_core = JAI
}
1393.1.1   = {
	owner = TIM
	controller = TIM
}
1408.1.1 = {
	controller = QAR
}
1409.1.1 = {
	owner = QAR
}
1444.1.1 = {
	remove_core = JAI
}	
1444.1.1 = {
	add_core = IRQ
	remove_core = QAR
	add_core = AKK
}
1469.2.4   = {
	owner = AKK
	controller = AKK
	add_core = AKK
	remove_core = QAR
}
1474.1.1   = { unrest = 4 } # Rebellion
1475.1.1   = { unrest = 0 }
1501.1.1   = {
	controller = SAM
	revolt = { }
} # Safawid Expansion
1508.1.1   = {
	owner = SAM
} # Safawid Expansion
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	religion = shiite
	remove_core = SAM
	remove_core = QAR
	remove_core = AKK
} # Safawids "form persia"
1530.1.1 = { add_core = TUR } #As Caliph, duty to rescue Baghdad
1534.11.28 = { controller = TUR } # Ottoman conquest
1536.1.1   = {
	owner = TUR
	add_core = TUR
	remove_core = PER
} # Annexed by the Ottomans
1624.1.1   = { controller = PER }
1638.12.24 = { controller = TUR }
