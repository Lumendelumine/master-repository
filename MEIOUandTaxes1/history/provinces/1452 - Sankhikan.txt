# No previous file for Sankhikan

culture = lenape
religion = totemism
capital = "Sankhikan"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 3
native_hostileness = 4

1566.1.1 = { discovered_by = SPA } #De Aviles
1684.1.1  = {	owner = ENG
		controller = ENG
		citysize = 200
		culture = english
		trade_goods = tobacco
		religion = protestant
	    } # Cambridge, earliest settlement in eastern Maryland
1707.5.12 = {
	owner = GBR
	controller = GBR
	discovered_by = GBR
} 
1709.1.1 = {	add_core = GBR }
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = {	remove_core = GBR unrest = 0   } # Preliminary articles of peace, the British recognized Amercian independence
