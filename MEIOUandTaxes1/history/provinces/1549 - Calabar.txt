# 1549 - Calabar

owner = CLB
controller = CLB
culture = bakongo		
religion = animism		 
capital = "Calabar"
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = palm
discovered_by = soudantech

1356.1.1   = {
	add_core = CLB
}
