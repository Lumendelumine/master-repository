# 2610 - Norkolk Island

culture = papuan
religion = polynesian_religion
capital = "Pukapuka"
trade_goods = crops # spices
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 0.5
native_hostileness = 1

1595.8.20  = {
	discovered_by = SPA
} # Discovered by Alvaro de Mendana
1765.6.21  = {
	discovered_by = GBR
} # Discovered by Commodore John Byron
1796.4.3   = {
	discovered_by = FRA
	discovered_by = USA
} # Discovered by Pierre Fran�ois P�ron
