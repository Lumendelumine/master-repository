# 2979 - Ngola

owner = NDO
controller = NDO
add_core = NDO
culture = mbundu
religion = animism
capital = "Canbari"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = slaves
hre = no
discovered_by = LOA
discovered_by = KON
discovered_by = NDO

1628.1.1   = {
	discovered_by = POR 
	add_core = POR
	owner = POR
	controller = POR
} # Effectively under Portuguese control
1670.1.1 = {
	revolt = {
		type = nationalist_rebels
		size = 1
		leader = "Filipe Hari"
	}
	controller = REB
}
1671.1.1 = {
	revolt = { }
	controller = POR
}
