#291 - Beloozero

owner = BOZ
controller = BOZ
culture = novgorodian
religion = orthodox
capital = "Beloozero"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = lumber 
fort_14th = yes 
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1356.1.1  = {
	add_core = BOZ
	add_permanent_claim = MOS
	add_permanent_claim = NOV
}
1486.1.1  = {
	owner = MOS
	controller = MOS
} # Formally incorporated into the Grand Duchy of Muscowy
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1565.1.1  = { fort_14th = yes } # Included in oprichnina, construction of a masonry fortress
1598.1.1  = { unrest = 5 base_manpower = 5 } # "Time of troubles", peasantry brought into serfdom
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1711.1.1  = {
	base_tax = 7
	base_production = 7
} # Governmental reforms and the absolutism
1767.1.1  = {
	base_tax = 8
	base_production = 8
} # Legislative reform
