# No previous file for Tipperah

owner = TPR
controller = TPR
culture = bengali
religion = hinduism
capital = "Comilla"
trade_goods = cotton
hre = no
base_tax = 8
base_production = 8
#base_manpower = 2.5
base_manpower = 5.0
citysize = 10000
add_local_autonomy = 25
estate = estate_bodo
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1120.1.1 = { plantations = yes }
1200.1.1 = { road_network = yes }
1356.1.1   = { add_core = TPR }
1732.1.1   = {
	owner = BNG
	controller = BNG
	add_core = BNG
}
1760.1.1   = { discovered_by = GBR }
1765.1.1   = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
