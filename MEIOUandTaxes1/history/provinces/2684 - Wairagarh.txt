# 2684 - Wairagarh

owner = GDW
controller = GDW
culture = marathi
religion = hinduism
capital = "Wairagarh"
trade_goods = cotton
hre = no
base_tax = 5
base_production = 5
base_manpower = 5
is_city = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = steppestech
1100.1.1 = { plantations = yes }
1356.1.1  = {
	add_core = BRR
	add_core = GDW
	add_core = DGA
	add_core = CHD
	fort_14th = yes
}
1400.1.1  = { controller = BAH }
1401.1.1  = { owner = BAH }
1444.1.1  = { add_core = BAH }
1490.1.1  = {
	remove_core = BAH
	controller = BRR
	owner = BRR
} # The Breakup of the Bahmani sultanate
1572.1.1 = {
	controller = BAS
} # captured by Ahmednagar
1574.1.1 = {
	owner = CHD
	controller = CHD
} # Absorbed by Chanda
1672.1.1 = {
	owner = DGA
	controller = DGA
	capital = "Nagpur"
	fort_14th = no
	fort_16th = yes
}
1724.1.1 = {
	owner = BHO
	controller = BHO
	add_core = BHO
}	#Forms kingdom of Nagpur
#1743.1.1 = {	   } # Marathas take control over Nagpur kingdom
