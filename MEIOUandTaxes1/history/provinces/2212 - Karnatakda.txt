# 2212 - Karnatakda

owner = BAH
controller = BAH
culture = kannada
religion = hinduism
capital = "Gulbarga"		#FB was: "Karnatakda"
trade_goods = carmine
hre = no
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
add_local_autonomy = 25
estate = estate_dakani
fort_14th = yes

discovered_by = indian
discovered_by = muslim

1356.1.1  = {
	add_core = BAH
}
1515.12.17 = { training_fields = yes }
1600.1.1  = { discovered_by = turkishtech }
1618.1.1  = {
	controller = BIJ
	add_core = BIJ
} # captured by Bijapur
1619.1.1  = {
	owner = BIJ
} # captured by Bijapur
1674.1.1  = { add_core = MAR remove_core = BIJ } # Maratha Empire
1687.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # The Mughal Empire
1707.5.12 = { discovered_by = GBR }
1712.1.1  = {
	add_core = HYD
}
1724.1.1  = {
	owner = HYD
	controller = HYD
	remove_core = MUG
} # Asif Jah declared himself Nizam-al-Mulk of Hyderabad

