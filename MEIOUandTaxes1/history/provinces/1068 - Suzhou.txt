# 1068 - jiangsu_area Taicang

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Suzhou"
trade_goods = silk
hre = no
base_tax = 11
base_production = 11
base_manpower = 8
is_city = yes
fort_14th = yes




discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = yangtze_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
}
1100.1.1 = { 
	temple = yes bailiff = yes constable = yes customs_house = yes
	marketplace = yes road_network = yes
}
1111.1.1 = { post_system = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1356.1.1  = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	add_core = MNG
}
1367.10.1  = {
	owner = MNG
	controller = MNG
	remove_core = ZOU
}
1519.1.1  = { discovered_by = POR } # Tome Pires
1535.1.1  = { fort_14th = yes }
1542.1.1  = { unrest = 5 } # Assassination attemp on Shi Zong
1543.1.1  = { unrest = 0 }

1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1644.4.1  = { unrest = 8 } # Beijing is attacked
#1644.5.27 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
