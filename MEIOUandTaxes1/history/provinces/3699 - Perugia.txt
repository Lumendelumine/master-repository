# No previous file for Perugia

owner = PEA
controller = PEA
culture = umbrian 
religion = catholic 
hre = no 
base_tax = 7
base_production = 7        
trade_goods = wine    
base_manpower = 3       
fort_14th = yes 
capital = "Perugia" 
is_city = yes
medieval_university = yes	# Founded 1308
add_core = PAP
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1309.1.1 = { add_core = PA2 add_core = PEA }
1337.1.1 = { revolt = { type = noble_rebels size = 0 } controller = REB }
1354.1.1 = { revolt = { } controller = PEA }
1370.1.1 = { owner = PAP controller = PAP }
1375.1.1 = { owner = PA2 controller = PA2 }
1400.1.1 = { owner = MLO controller = MLO } #Changes hands multiple times in the Italian wars
1403.1.1 = { owner = PAP controller = PAP }
1408.1.1 = { owner = NAP controller = NAP }
1414.1.1 = { owner = PA2 controller = PA2 } #Ruled by vassals of pope
1503.9.1 = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Loss of Papal authority after the death of Alexander III, Venetian influence
1506.1.1 = { revolt = { } controller = PA2 }
1540.1.1 = { remove_core = PA2 owner = PAP controller = PAP } #privileges revoked by Pope

1805.3.17 = { owner = ITA controller = ITA add_core = ITA } # Treaty of Pressburg
1814.4.11   = {	owner = PAP controller = PAP remove_core = ITA } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
