# 1320 - Mosul

owner = JAI
controller = JAI
culture = kurdish
religion = sunni
capital = "Mosul"
trade_goods = wool
hre = no
base_tax = 5
base_production = 5
base_manpower = 5
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1100.1.1 = { marketplace = yes }
1111.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = JAI
	add_core = AKK
}
1360.1.1   = {
	owner = QAR
	controller = QAR
	add_core = QAR
}
1393.1.1   = {
	owner = TIM
	controller = TIM
}
1408.1.1 = {
	controller = QAR
}
1409.1.1 = {
	owner = QAR
}
1444.1.1 = {
	remove_core = JAI
}	
1444.1.1 = {
	add_core = IRQ
	remove_core = QAR
	add_core = AKK
}
1453.1.1   = { discovered_by = VEN }
1468.1.1  = {
	controller = AKK
}
1470.1.1  = {
	owner = AKK
}
1478.1.1   = { revolt = { type = pretender_rebels } controller = REB } # Civil war
1481.1.1   = { revolt = {} controller = AKK }
1490.1.1   = { revolt = { type = pretender_rebels size = 1 } controller = REB } # Civil war
1494.1.1   = { revolt = {} controller = AKK }
1497.1.1   = { revolt = { type = pretender_rebels size = 1 } controller = REB } # Civil war
1507.9.1   = {
	controller = SAM
	revolt = {}
}
1508.1.1   = {
	owner = SAM
}
1512.1.1   = {
	owner = PER
	controller = PER
	remove_core = JAI
	add_core = PER
	remove_core = AKK
} # Safawids "form persia"
1519.1.1 = { bailiff = yes }
1529.1.1   = { revolt = { type = anti_tax_rebels } controller = REB } # Ottomans instigate rebellion
1530.1.1   = { revolt = {} controller = PER }
1530.1.1 = { add_core = TUR add_core = IRQ } #As Caliph, duty to rescue Baghdad
1534.11.28 = { controller = TUR } # Ottoman conquest
1535.1.1   = {	owner = TUR     } # Annexed by the Ottomans
1624.1.1   = { controller = PER }
1635.1.1   = { controller = TUR }
