# 1521 - Antakarana

owner = ANT
controller = ANT
add_core = ANT
culture = vezu
religion = animism
capital = "Antankarana"
is_city = yes
trade_goods = slaves
base_tax = 2
base_production = 2
base_manpower = 1
hre = no
discovered_by = ANT
discovered_by = BAA
discovered_by = BET
discovered_by = BNA
discovered_by = MER
discovered_by = MHF
discovered_by = MNB
discovered_by = SIH
discovered_by = TAM

1500.9.1 = {
	discovered_by = POR
} # Diego Dias
1832.1.1 = {
	owner = MER
	controller = MER
	add_core = MER
}
1885.12.17 = {
	add_core = FRA
}
1897.2.28  = {
	owner = FRA
	controller = FRA
}
