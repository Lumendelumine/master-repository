# 1061 - Hosuud

owner = YUA
controller = YUA
add_core = KHA
culture = khalkas
religion = shamanism
capital = "Sain-Noin"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = YUA
}
1368.1.1  = {
	remove_core = YUA
}
1381.1.1 = {
	owner = KHA
	controller = KHA
}
1454.1.1 = {
	owner = OIR
	controller = OIR
	add_core = OIR
}
1510.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
	remove_core = OIR
	remove_core = KHA
} # The Oirads are defeated & Mongolia is reunited under Dayan Khan
1515.1.1 = { training_fields = yes }
1543.1.1 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Fighting broke out between the Mongols
# 1586.1.1 = { religion = shamanism } # State religion
1635.4.1 = {
	owner = KHA
	controller = KHA
	add_core = KHA
}
1682.1.1 = { discovered_by = SPA } #Ferdinand Verbiest
1688.1.1 = {
	owner = ZUN
	controller = ZUN
}
1696.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Kangxi leads Qing army pushing Zunghars back
