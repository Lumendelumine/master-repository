# No previous file for Kara��'y

culture = ge
religion = pantheism
capital = "Kara��'y"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 15
native_ferocity = 1
native_hostileness = 8

1580.1.1   = { discovered_by = POR }
1770.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 280
	culture = brazilian
	religion = catholic
	trade_goods = brazil
	change_province_name = "Grajau"
	rename_capital = "Grajau"
}
1811.4.29  = { is_city = yes }
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
