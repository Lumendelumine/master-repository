#Province: Lasbela
#file name: 576 - Lasbela
#MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG - Turko-Mongol mod

owner = BAL
controller = BAL
culture = baluchi
religion = sunni
capital = "Bela"
trade_goods = salt
hre = no
base_tax = 4
base_production = 4
#base_manpower = 1.5
base_manpower = 3.0
citysize = 1980
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = BAL
}
1450.1.1 = { citysize = 1500 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 2000 }
1550.1.1 = { citysize = 3000 }
1600.1.1 = { citysize = 4000 }
1649.1.1 = {
	controller = PER
}
1650.1.1 = {
	citysize = 5000
	owner = PER
}
1666.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
1758.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
