# 3736 - Coburg

owner = THU
controller = THU
culture = high_saxon
religion = catholic
capital = "Coburg"
trade_goods = wheat
hre = yes
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
fort_14th = yes
add_core = THU
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1440.1.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Duchy of Thuringia is inherited by Saxony
1500.1.1 = { road_network = yes }
1530.1.1   = { religion = protestant }
1546.1.1   = { fort_14th = yes }
1547.5.19  = { 
	owner = THU
	controller = THU 
	add_core = THU
	remove_core = SAX 
}

1596.1.1   = { 
	owner = SCB
	controller = SCB 
	add_core = SCB
	remove_core = THU
}
1633.1.1   = { 
	owner = SWR
	controller = SWR 
	add_core = SWR
}
1680.1.1   = { 
	owner = SCB
	controller = SCB 
	add_core = SCB
	remove_core = SWR
}
