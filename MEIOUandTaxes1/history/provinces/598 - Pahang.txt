# 598 - Pahang
#Notes:
#if MEIOU ever has a start date prior to 1280 then this province should be part of the
#Srivijaya Empire.
#"Under the control of the maritime empire of Srivijaya (centered around Palembang in southeast Sumatra), Pahang had expanded to cover the entire southern portion of the Malay peninsula in the eight and ninth centuries"
#opportunity for improvement: add the sultanate of Pahang
#the city-state of Pahang was an independant sultanate in 1475
#city-state of Pahang was allied with Johor against Aceh from 1613

owner = PAH
controller = PAH
add_core = PAH
culture = malayan
religion = buddhism
capital = "Kuantan"
base_tax = 8
base_production = 8
base_manpower = 4
is_city = yes
trade_goods = sandal

discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = austranesian
hre = no

1460.1.1 = {
	owner = MLC
	controller = MLC
}
1500.1.1 = { religion = sunni }
1511.9.10 = {
	owner = PAH
	controller = PAH
}
1725.1.1 = {
	owner = TRG
	controller = TRG
	add_core = TRG
}
