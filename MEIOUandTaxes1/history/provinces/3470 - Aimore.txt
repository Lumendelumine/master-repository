# No previous file for Aimor�

culture = ge
religion = pantheism
capital = "Aimor�"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 3

1500.1.26   = {
	discovered_by = POR
	add_core = POR
} # Pedro �lvares Cabral 
1516.1.23 = {
	discovered_by = SPA
}
1534.6.30  = {
	owner = POR
	controller = POR
	change_province_name = "Porto Seguro"
	rename_capital = "Porto Seguro"
	citysize = 160
	culture = portugese
	religion = catholic
	trade_goods = sugar
	base_tax = 2
base_production = 2
} # Pedro �lvares Cabral 
1550.1.1   = {
	citysize = 500
}
1600.1.1   = {
	citysize = 1000
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
