# 1440 - Malatya

owner = DUL
controller = DUL
culture = cilician
religion = coptic
capital = "Malatya"
trade_goods = wax
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = DUL
}
1492.1.1 = { remove_core = ERE } ###
1509.1.1 = { controller = REB } # Civil war
1513.1.1 = { controller = TUR }
1514.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1656.1.1  = {
	culture = kurdish
	religion =  sunni
}
1658.1.1 = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1 = { controller = TUR }
