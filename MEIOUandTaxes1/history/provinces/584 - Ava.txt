# 584 - Ava

owner = AVA
controller = AVA
culture = burmese
religion = buddhism
capital = "Awa"	#offcial burmese is INWA, western name is AVA
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
trade_goods = opium
discovered_by = chinese
discovered_by = indian
# Awa between 2 rivers and many moats made from swamps

1356.1.1 = {
	#capital = "Pinya"	founded in 1313
	add_core = AVA
	add_core = MYA
	add_core = PEG
}
#1365.2.26 = { capital = "Awa" }
1510.1.16 = {
	owner = MYA
	controller = MYA
}
1527.1.1 = {
	#owner = TAU
	#controller = TAU
	add_core = TAU
	remove_core = MYA
	remove_core = PEG
	unrest = 8
}
1530.1.1 = {
	add_core = TAU
	remove_core = AVA
	remove_core = MYA
	remove_core = PEG
	#unrest = 50
}
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
	remove_core = AVA
}
1857.2.13 = { capital = "Mandalay" }
1885.1.1 = {		
	owner = GBR
	controller = GBR
}
