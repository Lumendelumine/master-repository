# 2690 - Tirhut

owner = TRT
controller = TRT
culture = bihari
religion = hinduism
capital = "Sugauna"
trade_goods = cotton
hre = no
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = tirhut_state
		duration = -1
	}
}

1115.1.1 = { bailiff = yes }
1120.1.1 = { plantations = yes }
1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
	#add_core = DLH
	add_core = TRT
	add_core = AHM
	unrest = 6
}
1396.1.1 = {
	owner = AHM
	controller = AHM
} # By the end of the 14th century, the whole of Tirhut passed on to the kings of Jaunpur
1499.1.1  = {
	owner = DLH
	controller = DLH
} # Prehemptive alltack to counter the Bengal advance
1515.12.17 = { training_fields = yes }
1524.1.2  = {
	controller = BNG
}
1524.3.1  = {
	owner = BNG
	capital = "Darbhanga"
}
#1529.1.1  = {
#	revolt = { type = pretender_rebels }
#} # Sur control
1530.1.1 = {
	owner = AHM
	controller = AHM
	add_core = AHM
}
1538.1.1  = {
	controller = BNG
	revolt = { }
} # Gaur has fallen
1538.6.1  = {
	controller = MUG
} # Mughal Invasion
1539.1.1  = {
	controller = BNG
	owner = BNG
} # Surs again in control

1575.1.1  = {
	owner = MUG
	controller = MUG
  	add_core = MUG
} # Annexed to the Mughal Empire
1627.1.1  = { discovered_by = POR }
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1765.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mughal Empire (was formerly british puppet)
