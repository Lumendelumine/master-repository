# 2579 - Noxciyo

owner = GAZ
controller = GAZ
culture = dagestani
religion = sunni
capital = "Kumukh"
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
trade_goods = lead  
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1  = {
	#owner = GEO
	#controller = GEO
	#add_core = GEO
	add_core = GAZ
}
#1424.1.1  = {
#	owner = CIR
#	controller = CIR
#}
#1444.1.1 = {
#	remove_core = GEO
#}
#1466.1.1  = {
#	remove_core = GEO
#}
#1555.1.1 = {
#	add_core = TUR
#}
