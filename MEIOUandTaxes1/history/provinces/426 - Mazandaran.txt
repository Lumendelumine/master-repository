# 426 - Mazandaran

owner = MZA
controller = MZA
culture = tabari
religion = sunni
capital = "Mazandaran"
trade_goods = wool
hre = no
medieval_university = yes
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes # citysize = 1600
discovered_by = muslim
discovered_by = western
discovered_by = eastern
discovered_by = turkishtech
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = MZA
}
1382.1.1   = {
	owner = TIM
	controller = TIM
}
1405.1.1  = {
	owner = MZA
	controller = MZA
	#add_core = PER
} # Death of Tamerlane
1595.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	religion = shiite
} # The Safavids took over, Shi'ism becomes the state religion
1747.1.1  = { unrest = 3 } # Shah Nadir is killed, local khanates emerged
1749.1.1  = {
	unrest = 0
	owner = MZA
	controller = MZA
} # The empire began to decline
1779.1.1  = {
	owner = PER
	controller = PER
} # With the Qajar dynasty the situation stabilized
