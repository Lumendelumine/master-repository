# 1240 - Hawai'i
# MEIOU-GG
# Anarchovampire - corrections

owner = HAW 
controller = HAW 
culture = hawaian
religion = polynesian_religion
capital = "Kowahu" 
trade_goods = fish 
hre = no 
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
add_core = HAW 
discovered_by = hawaii_tech

1555.1.1 = {
	discovered_by = SPA
}
1778.1.1 = {
	discovered_by = GBR
}
#1795.1.1 = {
#	discovered_by = KOH
#	owner = KOH
#	controller = KOH
#	add_core = KOH
#}
#1810.1.1 = {
#	remove_core = HAW #Kaua'i joins Kingdom
#}
1820.1.1 = {
	religion = protestant
}
1898.1.1  = {
	owner = USA
	controller = USA
}
