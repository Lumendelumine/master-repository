# 2270 - Naiman

owner = YUA
controller = YUA
culture = oirats
religion = shamanism
capital = "Khobdo"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = steppestech

1356.1.1 = {
	add_core = YUA 
	add_core = OIR
}
1368.1.1  = {
	remove_core = YUA
}
1375.1.1 = {
	owner = OIR
	controller = OIR
}
1515.1.1 = { training_fields = yes }
1530.1.1 = { remove_core = YUA }
1530.2.1 = { discovered_by = muslim }
1552.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
} # The Oirads are defeated, reunited under Altan Khan
1615.1.1 = { religion = vajrayana } # State religion
1618.1.1 = { discovered_by = RUS }
1623.1.1 = {
	owner = OIR
	controller = OIR
} # The Oirads gained independence
1635.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
}
1754.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
