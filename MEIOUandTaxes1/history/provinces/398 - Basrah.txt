# 398 - Basrah

owner = JAI
controller = JAI
culture = iraqi
religion = sunni #Dei Gratia
capital = "Basrah"
trade_goods = wool
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
discovered_by = muslim
discovered_by = western
discovered_by = eastern
discovered_by = turkishtech
discovered_by = steppestech
discovered_by = indian

1000.1.1 = {
	add_permanent_province_modifier = {
		name = euphrates_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = minor_center_of_trade_modifier
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1356.1.1   = {
	add_core = JAI
}
1393.1.1   = {
	owner = TIM
	controller = TIM
}
1408.1.1 = {
	controller = QAR
}
1409.1.1 = {
	owner = QAR
}
1444.1.1 = {
	remove_core = JAI
	remove_core = QAR
	add_core = AKK
}	
1444.1.1 = {
	add_core = IRQ
}
1453.1.1   = { discovered_by = VEN }
1469.1.1   = {
	controller = AKK
}
1470.1.1   = {
	owner = AKK
}
1501.1.1   = {
	controller = SAM
}
1508.1.1   = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	religion = shiite
	remove_core = QAR
	remove_core = AKK
} # Safawids "form persia"
1529.1.1   = { revolt = { type = anti_tax_rebels } controller = REB } # Ottomans instigate rebellion
1530.1.1   = { revolt = {} controller = PER }
1530.1.1 = { add_core = TUR } #As Caliph, duty to rescue Baghdad
1534.11.28 = { controller = TUR } # Ottoman conquest
1535.1.1   = {	owner = TUR   } # Annexed by the Ottomans
1624.1.1   = { controller = PER }
1638.1.1   = { controller = TUR }
