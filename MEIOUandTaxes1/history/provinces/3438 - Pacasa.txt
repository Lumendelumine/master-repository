# 3438 - Pacasa

owner = AYP
controller = AYP
add_core = AYP
culture = aimara
religion = inti
capital = "Pacasa"
trade_goods = gold
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = south_american

1356.1.1 = {
	add_permanent_province_modifier = {
		name = "coalition_member"
		duration = -1
	}
}
1493.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
}
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
}
1750.1.1  = {
	add_core = BOL
	culture = peruvian
} # Decline as the mining began to wane
1809.7.16 = {
	owner = BOL
	controller = BOL
} # Bolivian War of Independence
1825.8.6  = {
	remove_core = SPA
}
