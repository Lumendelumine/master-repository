# No previous file for Nashit'ush

culture = caddo
religion = totemism
capital = "Nashit'ush"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 20
native_ferocity = 2
native_hostileness = 5
