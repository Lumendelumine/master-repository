# No previous file for Katerokwi

culture = huron
religion = totemism
capital = "Katerokwi"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 5

1609.1.1  = { discovered_by = FRA } # Samuel de Champlain.
1664.1.1  = { discovered_by = ENG }
1674.1.1 = { owner = FRA
		controller = FRA
		citysize = 250
		trade_goods = fur
		culture = francien
		religion = catholic
} #Seigneurie of L'Orignal granted
1699.1.1 = {	add_core = FRA citysize = 1000 }
1707.5.12  = { discovered_by = GBR }
1763.2.10 = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	remove_core = FRA
} # Treaty of Paris - ceded to Britain, France gave up its claim
1783.1.1  = { 	culture = english
		religion = protestant 
	} #Loyalist migration after the revolution
1788.2.10 = { add_core = GBR }
