# 1154 - Mbuilla

culture = kongolese
religion = animism
capital = "Mbuilla"
native_size = 50
native_ferocity = 1
native_hostileness = 7
trade_goods = crops # ivory
hre = no
discovered_by = LOA
discovered_by = KON
discovered_by = NDO
