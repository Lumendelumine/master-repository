# 727 - Hailanpao

owner = MYR
controller = MYR
culture = daur
religion = shamanism
capital = "Aigun"
trade_goods = fur
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = steppestech

1356.1.1 = {
	add_core = MYR
}
1515.1.1 = { training_fields = yes }
1628.1.1 = {
   	owner = MCH
   	controller = MCH
	add_core = MCH
} # The Later Jin Khanate
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1643.1.1  = { discovered_by = RUS } # Vasily Poyarkov
1858.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
