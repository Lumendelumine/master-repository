# 1092 - Toowoomba

culture = aboriginal
religion = polynesian_religion
capital = "Toowoomba"
trade_goods = crops #naval_supplies
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 2

1770.7.1 = {
	discovered_by = GBR
} # Cook's 1st voyage
