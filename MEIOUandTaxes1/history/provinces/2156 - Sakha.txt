# Sakha

owner = SAK
controller = SAK
culture = yakut
religion = shamanism
capital = "Ust-Kut"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = steppestech

1356.1.1  = {
	add_core = SAK
}
1391.1.1 = {
	owner = BRT
	controller = BRT
	add_core = BRT
	culture = buryat
}
1632.1.1  = { discovered_by = RUS }
1632.9.25 = {
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
	trade_goods = fur
}
1657.1.1 = {
	add_core = RUS
}
