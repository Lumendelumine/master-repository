# 2679 - Kolhapur

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Kolhapur"
trade_goods = naval_supplies
hre = no
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes

discovered_by = indian
discovered_by = muslim 

1356.1.1  = {
	add_core = BAH
	add_core = BIJ
}
1490.1.1  = {
	remove_core = BAH
	controller = BIJ
	owner = BIJ
	fort_14th = yes
} # The Breakup of the Bahmani sultanate
1498.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1650.1.1  = {
	add_core = MAR #Maratha Identity
}
1657.1.1  = {
	owner = MAR
	controller = MAR
	remove_core = MUG
	fort_14th = no fort_15th = yes
	capital = "Satara"
} # Inheritance of Shivaj
1680.1.1  = { fort_15th = no fort_16th = yes }
