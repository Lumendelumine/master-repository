# No previous file for Walinakiak

culture = abenaki
religion = totemism
capital = "Walinakiak"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 15
native_ferocity = 1 
native_hostileness = 6

1609.1.1  = { discovered_by = FRA } # Samuel de Champlain
1629.1.1  = { discovered_by = ENG }
1650.1.1  = { trade_goods = fur } #Extent of the Abenaki/Wabanaki at start of the Beaver Wars
1707.5.12 = { discovered_by = GBR }
1754.1.1 = {	owner = GBR
		controller = GBR
		citysize = 500
		culture = english
		religion = protestant } #Settlement around Augusta
1764.7.1  = { culture = american unrest = 6 } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
	    } # Declaration of independence
1782.11.1 = { unrest = 0 } # Preliminary articles of peace, the British recognized American independence
1801.7.4  = {	add_core = USA }
