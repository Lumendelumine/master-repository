# 2216 - Luzon

culture = lussong
religion = polynesian_religion
capital = "Tondo"
trade_goods = crops
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
native_size = 50
native_ferocity = 1
native_hostileness = 3
discovered_by = chinese
discovered_by = austranesian

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1200.1.1 = { marketplace = yes }
1355.1.1 = {
	owner = LUS
	controller = LUS
	add_core = LUS
	is_city = yes
	trade_goods = clove
	base_manpower = 3
	base_tax = 3
base_production = 3
	set_province_flag = trade_good_set
}
1500.1.1 = {
	capital = "Maynila"
	owner = MNL
	controller = MNL
	add_core = MNL
}
1521.1.1 = { discovered_by = SPA } # Ferdinand Magellan
1575.1.1 = {
	owner = SPA
	controller = SPA
	culture = castillian
	religion = catholic
	
}
1595.1.1 = { add_core = SPA }
