# 853 - Tamaholipa

culture = coahuiltec
religion = nahuatl
capital = "Coachichitl"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 15
native_ferocity = 4
native_hostileness = 7
discovered_by = mesoamerican

1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	citysize = 2000
	culture = castillian
	religion = catholic
	set_province_flag = trade_good_set
	trade_goods = maize
	}
1546.1.1   = {
	add_core = SPA
}
1600.1.1   = {
	citysize = 5000
}
1650.1.1   = {
	citysize = 10000
}
1700.1.1   = {
	citysize = 20000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 35000
}
1800.1.1   = {
	citysize = 75000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
