# 2681 - Bidar

owner = BAH
controller = BAH
culture = kannada
religion = hinduism
capital = "Bidar"
trade_goods = cloth	#silk
hre = no
base_tax = 7
base_production = 7
base_manpower = 5
is_city = yes
fort_14th = yes
discovered_by = indian
discovered_by = muslim 

1100.1.1 = { marketplace = yes }
1111.1.1 = { post_system = yes }
1115.1.1 = { bailiff = yes }
1120.1.1 = { textile = yes }
1356.1.1  = { add_core = BAH }
1432.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1498.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1618.1.1  = {
	controller = BIJ
} # captured by Bijapur
1619.1.1  = {
	owner = BIJ
} # captured by Bijapur
1685.1.1  = {
	controller = MUG
}
1686.1.1  = {
	owner = MUG
}
1712.1.1  = { add_core = HYD }	#Viceroyalty of the Deccan
1724.1.1  = {
	owner = HYD
	controller = HYD
} # Asif Jah declared himself Nizam-al-Mulk
1760.1.1  = {
	owner = MAR
	controller = MAR
} # Battle of Udgir
1798.1.1  = {
	owner = HYD
	controller = HYD
}
