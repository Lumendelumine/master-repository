# 2078 - Guanxi Youjiang

owner = DLI
controller = DLI
culture = zhuang
religion = mahayana
capital = "Sicheng"
trade_goods = tin
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = steppestech



1253.1.1 = {
	owner = YUA
	controller = YUA
}
1274.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1356.1.1 = {
}

1382.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = LNG
} # Last Mongol stronghold surrenders.
1520.1.1 = { 
	culture = wuhan 
	religion = confucianism
}
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
