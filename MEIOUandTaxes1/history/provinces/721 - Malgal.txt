# 721 - Ikeres

owner = YUA
controller = YUA
culture = khalkas
religion = shamanism
capital = "Malgal"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = steppestech

1356.1.1 = {
	add_core = KHR
	add_core = YUA
}
1368.1.1  = {
	owner = KHR
	controller = KHR
	remove_core = YUA
}
1454.1.1 = {
	owner = OIR
	controller = OIR
}
1510.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
	remove_core = KHR
	remove_core = OIR
} # The Oirads are defeated & Mongolia is reunited under Dayan Khan
1515.1.1 = { training_fields = yes }
1530.1.1 = { add_core = YUA }
1552.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
} # The Oirads are defeated & Mongolia is reunited under Altan Khan
1586.1.1 = { religion = vajrayana } # State religion
1594.1.1 = {
	owner = MYR
	controller = MYR
} #Solon Khanate
1624.1.1 = {
	owner = MCH
	controller = MCH
} # Part of the Manchu empire
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG } # The government in Taiwan surrendered
