# 1317 - Shemakha

owner = SHI
controller = SHI
culture = azerbadjani
religion = sunni
capital = "Shamaki"
trade_goods = wool
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_core = SHI
	#add_core = QAR
	unrest = 4
}
1357.1.1   = {
	owner = BLU
	controller = BLU
}
1360.1.1   = {
	owner = JAI
	controller = JAI
}
1375.1.1   = {
	owner = SHI
	controller = SHI
} # Independance secured
1444.1.1 = {
	remove_core = JAI
}	
1467.1.1 = {
	controller = AKK
}
1470.1.1 = {
#	owner = AKK
#	add_core = AKK
#	remove_core = QAR
	controller = SHI
}
1504.1.1 = {
	remove_core = TIM
	add_core = PER
}
1538.1.1 = {
	owner = PER
	controller = PER
	unrest = 1
} # Weakened by internal conflicts, Shirvan is an easy prey for the Shah
1588.1.1   = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR add_core = TUR } # Peace of Istanbul
1605.1.1   = { controller = PER }
1612.11.20 = { owner = PER }
1722.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1730.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
1747.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1760.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
1805.1.1 = {
	controller = RUS
}
1813.10.24 = {
	owner = RUS
	add_core = RUS
	remove_core = PER
} # The Treaty of Gulistan
