# 2353 - Cambresis
# cambrai , Valenciennes , Maubeuge

owner = HAI
controller = HAI
culture = picard
religion = catholic
capital = "Kimbr�"
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
trade_goods = linen
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = yes

1299.11.10  = {
	add_core = HAI
	add_core = FLA
	add_core = HOL
} #John unites Holland and Hainaut in personal union until 1433.1.1 when absorbed by Burgundy
1433.1.1   = { owner = BUR controller = BUR add_core= BUR remove_core = FLA }
1444.1.1 = { remove_core = FRA }

1472.1.1 = { temple = yes }
1477.1.5 = { add_core = FRA }
1482.3.27  = { owner = HAB controller = HAB add_core = HAB remove_core = BUR } # Charles the Bold dies, Lowlands to Austria

1500.1.1 = { road_network = yes }
1512.1.1   = { fort_14th = yes }
1529.8.3   = { remove_core = FRA } # 'Ladies Peace' (Damesvrede) of Cambrai: France renounces its claims
1530.1.1  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
	hre = no
}
1550.1.1   = { add_core = NED }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }
1559.1.1 = {  }
1559.5.12  = { unrest = 3 } # New bishoprics established in the Lowlands create an outrage
1566.8.1   = { unrest = 4 } # 'Beeldenstorm' at hand
1566.8.10  = { controller = REB } # 'Beeldenstorm' also hits Hainaut
1567.1.8   = { controller = SPA } # Spain is back in control
1567.9.10  = { unrest = 5 } # Counts of Egmont & Hoorne arrested
1568.6.5   = { unrest = 7 } # Counts of Egmont & Hoorne beheaded
1569.1.1   = { unrest = 10 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = { unrest = 13  } # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1572.5.24  = { controller = REB } # Louis of Nassau captures Mons
1572.8.1   = { controller = SPA } # Don Felip� takes back Mons
1577.2.12  = { unrest = 5 } # 'Perpetual Edict' (Eeuwig Edict) accepted by Don Juan
1579.1.6   = { unrest = 0 } # Union of Arras established
 # Start of switch to industry
1635.1.1   = { controller = FRA } # French troops capture parts of the Southern Lowlands
1640.1.1   = {  }
1648.1.30  = { controller = SPA } # Peace of M�nster/Westphalia
1650.1.1   = { trade_goods = iron textile = no } # Focus more and more on iron & coal industry
1665.9.17  = { add_core = FRA } # Louis XIV's father-in-law, Philip IV of Spain, dies
1667.9.25  = { controller = FRA } # Mar�chal captures alot of cities in the region for Louis XIV
1668.5.2   = { controller = SPA } # Peace of Aachen
1672.5.5   = { controller = FRA } # France blitzes through the Lowlands in the Franco-Dutch War
1678.9.19  = {
	owner = FRA
	add_core = FRA
	hre = no
} # Peace of Nijmegen (France-Spain)
1792.9.21  = {
	hre = no
} # Proclamation of the First Republic
