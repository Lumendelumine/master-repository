# 455 - Mirzacho'l

owner = CHG
controller = CHG
add_core = CHG
culture = turkmeni
religion = sunni
capital = "Chardzhou"
trade_goods = wool
hre = no


base_tax = 5
base_production = 5
base_manpower = 5
is_city = yes
discovered_by = KSH
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1500.6.1   = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = TIM
} # Shaybanids break free from the Timurids
1511.1.1 = {
	owner = KHI 
	controller = KHI
	add_core = KHI
	remove_core = SHY
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1550.1.1   = {
	
	} # The city declined after the capital was moved to Bukhara
1740.1.1 = {
	
}
1785.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
