# 372 - Tir Chonaill

owner = LEI #the O'Donnells of Tyrconal
controller = LEI
culture = irish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = hemp
is_city = yes
base_manpower = 2
capital = "D�n na nGall" # Donegal
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1356.1.1   = {
	add_core = LEI
}
1607.9.4   = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # Flight of the Earls
1641.10.22 = {
	controller = REB
}
1642.5.1   = {
	controller = ENG
} # Estimated
1646.6.5   = {
	controller = IRE
}
1650.6.21  = {
	controller = ENG
} # Battle of Scarrifhollis
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1798.5.23  = {
	controller = REB
} # Irish rebellion
1798.7.14  = {
	controller = GBR
} # The rebels are defeated
