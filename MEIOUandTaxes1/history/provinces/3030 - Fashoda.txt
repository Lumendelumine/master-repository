# No previous file for Fashoda

owner = SLL
controller = SLL
add_core = SLL
culture = shilluk 
religion = animism
capital = "Fashoda"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = wool
hre = no
discovered_by = ALW
discovered_by = YAO
discovered_by = ETH
discovered_by = ADA
discovered_by = DAR
discovered_by = KIT
discovered_by = MKU
discovered_by = DSL
discovered_by = KDF
discovered_by = SLL

1820.1.1 = {
	discovered_by = TUR
	owner = TUR
	controller = TUR
}
