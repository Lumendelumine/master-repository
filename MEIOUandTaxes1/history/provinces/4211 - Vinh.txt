# No previous file for Vinh

owner = DAI
controller = DAI
add_core = DAI
culture = vietnamese
religion = mahayana
capital = "Vinh"

base_tax = 2
base_production = 2
#base_manpower = 2.0
base_manpower = 1.0
trade_goods = fish


discovered_by = DAI
discovered_by = chinese
discovered_by = indian
discovered_by = muslim

hre = no
1356.1.1 = {
	fort_14th = yes
}
1370.1.1 = { owner = CHA controller = CHA }
1390.1.1 = { owner = DAI controller = DAI } 
1407.1.1 = { owner = MNG controller = MNG }
1427.1.1 = { owner = DAI controller = DAI } 
1515.1.1 = { regimental_camp = yes }
1517.2.3 = { bailiff = yes }
1519.4.2 = { wharf = yes }
1522.4.4 = { marketplace = yes }
1527.1.1 = {
	owner = TOK
	controller = TOK
	add_core = TOK
}
1730.1.1 = { unrest = 5 }							# Peasant revolt
1731.1.1 = { unrest = 0 }
1769.1.1 = { unrest = 6 } # Tay Son Rebellion
1776.1.1 = {
	unrest = 0
	owner = DAI
	controller = DAI
	add_core = DAI
} # Tay Son Dynasty conquered the Nguyen Lords
1786.1.1 = { unrest = 5 } # Unsuccessful revolt
1787.1.1 = { unrest = 0 }
1802.7.22 = {
	owner = ANN
	controller = ANN
	remove_core = DAI
} # Nguyen Phuc Anh conquered the Tay Son Dynasty
1883.8.25 = { 
	owner = FRA
	controller = FRA
}
