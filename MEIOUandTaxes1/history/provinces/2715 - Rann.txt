# 2715 - Kharan

owner = BAL
controller = BAL
culture = baluchi
religion = hinduism #was majority Hindu at partition
capital = "Kharan"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
base_manpower = 2.0
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1356.1.1   = {
	add_core = BAL
}
1500.1.1  = { discovered_by = POR }
1649.1.1 = {
	controller = PER
}
1650.1.1 = {
	owner = PER
}
1666.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
1758.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
