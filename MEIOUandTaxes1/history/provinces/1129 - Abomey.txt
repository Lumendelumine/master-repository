# 1129 - Abomey

owner = DAH
controller = DAH
culture = fon
religion = west_african_pagan_reformed
capital = "Abomey"
base_tax = 8
base_production = 8
base_manpower = 5
is_city = yes
trade_goods = slaves
discovered_by = soudantech
hre = no

1356.1.1 = {
	add_core = DAH
}
1520.1.1 = { unrest = 4 } #Benin raids and claimst to loyalty of local Yoruba chiefs
1525.1.1 = { unrest = 0 }
1585.1.1 = { unrest = 4 } #Nupe Raids
1590.1.1 = { unrest = 0 }
