# 1565 - Tanga

owner = MBA
controller = MBA
culture = kiunguja
religion = sunni
capital = "Tanga"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = slaves

discovered_by = ZAN
discovered_by = KIL
discovered_by = QLM
discovered_by = SOF
discovered_by = MLI
discovered_by = MBA
discovered_by = PTA
discovered_by = MOG
discovered_by = muslim
discovered_by = indian
hre = no

1356.1.1 = {
	add_core = MBA
}
1498.4.1 = { discovered_by = POR } #Vasco Da Gama
#1505.1.1 = {
#	owner = POR
#	controller = POR
#	add_core = POR
#}
1588.1.1 = {
	owner = OMA
	controller = OMA
} # Liberated with help of the Turks
1593.1.1 = {
	owner = MLI
	controller = MLI
} # Fall under Malindi rule
1594.1.1 = {
	owner = POR
	controller = POR
} # Portuguese reconquest
1600.1.1 = { discovered_by = TUR }
1698.1.1 = {
	owner = OMA
	controller = OMA
	add_core = OMA
} #Omanis establish direct control of Swahili coast
1728.1.1 = {
	owner = POR
	controller = POR
}
1729.1.1 = {
	owner = OMA
	controller = OMA
}
1762.1.1 = { unrest = 7 } #Kamba migrations in wake of Maasai expansion disturb region
1824.1.1 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1826.1.1 = {
	owner = OMA
	controller = OMA
}
1856.6.1 = {
	owner = ZAN
	controller = ZAN
   	remove_core = OMA
} # Said's will divided his dominions into two separate principalities, with Thuwaini to become the Sultan of Oman and Majid to become the first Sultan of Zanzibar.
