# 3727 - Hildesheim

owner = HIL
controller = HIL
culture = old_saxon
religion = catholic
capital = "Hildesheim"
trade_goods = livestock
hre = yes
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
fort_14th = yes
add_core = HIL
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1542.1.1   = {
	religion = protestant
}
1813.10.14 = {
	owner = HAN
	controller = HAN
	add_core = HAN
}
