# 665 - sichuan_area Shunqing

owner = SHU
controller = SHU
culture = lanyin
religion = confucianism
capital = "Fengjie"
trade_goods = tea
hre = no
base_tax = 8
base_production = 8
base_manpower = 5
is_city = yes
discovered_by = chinese
discovered_by = steppestech

0967.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1241.1.1 = {
	owner = SHU
	controller = SHU
}
1259.1.1 = {
	owner = YUA
	controller = YUA
	remove_core = SNG
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1354.1.1 = {
	owner = XIA
	controller = XIA
}
1356.1.1 = {
	add_core = SHU
	add_core = XIA
}
1371.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = SHU
	remove_core = XIA
}
1644.3.19  = { owner = DXI
	      controller = DXI
	      add_core = DXI
}
1646.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = DXI
	remove_core = MNG
} # The Qing Dynasty