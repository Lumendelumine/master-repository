# No previous file for Irtysh

owner = WHI
controller = WHI
culture = khazak
religion = sunni
capital = "Irtysh"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = steppestech

1356.1.1 = {
	add_core = KZH
	add_core = WHI
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1428.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
}
1444.1.1 = {
	remove_core = GOL
	remove_core = WHI
	remove_core = BLU
}
1465.1.1 = {
	owner = KZH
	controller = KZH
	remove_core = SHY
}
1515.1.1 = { training_fields = yes }
1740.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1 = { owner = KZH controller = KZH }
