# 2536 - S�deckie

owner = POL
controller = POL
add_core = POL
culture = polish
religion = catholic
capital = "Nowy Sacz"
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
trade_goods = salt

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = bochnia
		duration = -1
	}
}
1508.1.1   = { fort_14th = yes }

1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
 } # Union of Lublin

1587.1.1   = { unrest = 6 }
1587.2.14  = { unrest = 0 } # After a 6 week siege, Maximillian drops his bid for the Polish trone
1588.1.1   = { controller = REB } # Civil war, Polish succession
1589.1.1   = { controller = PLC } # Coronation of Sigismund III
1600.1.1   = {  }

1733.1.1   = { controller = REB } # The war of Polish succession
1735.1.1   = { controller = PLC }
1768.2.28  = { unrest = 8 } # Became a center of the first Polish uprisings against the Polish king & Russia
1772.8.5   = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = PLC
} # First partition
1794.3.24  = { unrest = 5 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 } # The end of the uprising
