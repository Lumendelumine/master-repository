# 593 - Nakhon Si Thammarat (ligore)

owner = NST
controller = NST
culture = monic
religion = buddhism
capital = "Chaiya"

base_tax = 5
base_production = 5
#base_manpower = 1.5
base_manpower = 3.0
citysize = 4200
trade_goods = fish


discovered_by = chinese
discovered_by = indian
discovered_by = muslim

hre = no

1356.1.1 = {
	add_core = NST
}
1467.4.8 = {
	owner = AYU
	controller = AYU
    	add_core = AYU
	remove_core = NST
	unrest = 0
	culture = thai
}
1500.1.1 = { citysize = 5155 }
1535.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 5890 }
1600.1.1 = { citysize = 6530 }
1650.1.1 = { citysize = 7254 }
1700.1.1 = { citysize = 8226 }
1750.1.1 = { citysize = 9100 }
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
1800.1.1 = { citysize = 10500 }
