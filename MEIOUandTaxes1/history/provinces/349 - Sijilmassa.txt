# 349 - Sijilmassa

owner = TFL
controller = TFL
culture = tamazight
religion = sunni
capital = "Sijilmassa"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = wool



discovered_by = muslim
discovered_by = western
discovered_by = turkishtech
hre = no

1356.1.1 = {
	add_core = TFL
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1515.1.1 = { training_fields = yes }
1530.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SPA
	remove_core = CAS
	remove_core = FEZ
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
