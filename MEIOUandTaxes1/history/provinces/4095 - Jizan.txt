# 4095 - Jizan

owner = HEJ
controller = HEJ
culture = hejazi
religion = shiite
capital = "Jizan"
trade_goods = olive
hre = no
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
discovered_by = ADA
discovered_by = MKU
discovered_by = KIL
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1356.1.1 = {
	owner = MAM
	controller = MAM
	add_core = HEJ
	add_core = MAM
}
1516.1.1   = { add_core = TUR }
1517.4.13  = { 
	owner = TUR
	controller = TUR
	remove_core = MAM
} # Conquered by Ottoman troops
1530.1.1   = {
	owner = YRA
	controller = YRA
	add_core = YRA
	remove_core = TUR
}
1802.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
} # Incorporated into the First Saudi State
1818.9.9 = {
	owner = TUR
	controller = TUR 
} # The end of the Saudi State
