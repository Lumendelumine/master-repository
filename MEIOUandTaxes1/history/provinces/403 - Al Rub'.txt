#403 - Al Rug'

culture = omani
religion = ibadi
capital = "Ramlat Al Wahiba"
hre = no
base_manpower = 1
trade_goods = crops # slaves
native_size = 50
native_ferocity = 1
native_hostileness = 7
discovered_by = muslim
discovered_by = turkishtech
