owner = KIL
controller = KIL
add_core = KIL
culture = kimgao
religion = sunni
capital = "Mtwara"
hre = no

base_tax = 2
base_production = 2
#base_manpower = 0.5
base_manpower = 1.0
citysize = 4000
trade_goods = slaves
discovered_by = ZAN
discovered_by = KIL
discovered_by = QLM
discovered_by = SOF
discovered_by = MLI
discovered_by = MBA
discovered_by = PTA
discovered_by = MOG
discovered_by = indian

1498.3.16 = { discovered_by = POR } #Vasco Da Gama
1600.1.1  = { discovered_by = TUR }
1763.1.1  = { unrest = 7 }
1784.1.1  = {
	owner = OMA
	controller = OMA
	add_core = OMA
} #Omanis impose direct rule in Kilwa
1856.6.1 = {
	owner = ZAN
	controller = ZAN
   	remove_core = OMA
} # Said's will divided his dominions into two separate principalities, with Thuwaini to become the Sultan of Oman and Majid to become the first Sultan of Zanzibar.
