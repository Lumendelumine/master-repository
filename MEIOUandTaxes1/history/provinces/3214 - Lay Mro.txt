# No previous file for Lay Mro

owner = ARK
controller = ARK
culture = arakanese
religion = buddhism
capital = "Akyab"
trade_goods = fish
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = chinese
discovered_by = muslim

1356.1.1 = { add_core = ARK }
1509.1.1 = { discovered_by = POR }
1752.2.28 = { add_core = BRM }
1784.12.31 = {
	owner = BRM
	controller = BRM
	add_core = BRM
} # Annexed by Burma
1811.1.1  = { controller = REB }
1815.1.1  = { controller = BRM }
1826.2.24 = {
	owner = GBR
	controller = GBR
	add_core = GBR
} # Treaty of Yandabo
