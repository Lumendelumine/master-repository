# No previous file for Yakama

culture = salish
religion = totemism
capital = "Yakama"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 4

1811.1.1 = {	owner = USA
		controller = USA
		citysize = 350
		trade_goods = fur
		religion = protestant
		culture = american } #Fort Okanogan
1814.12.24 = {	owner = GBR
		controller = GBR
		citysize = 200
		culture = english
		religion = protestant
} # British control after the War of 1812
1815.1.1 = { add_core = GBR is_city = yes }
