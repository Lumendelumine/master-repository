# 2668 - Chanthaburi

owner = AYU
controller = AYU
add_core = AYU
culture = thai
religion = buddhism
capital = "Chanthaburi"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1564.2.1 = { add_core = TAU } # Burmese vassal
1584.5.3 = { remove_core = TAU }
1767.4.1 = { unrest = 7 } # The Ayutthaya kingdom began to crumble
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
