# 324 - K�tahya

owner = GRN
controller = GRN
culture = turkish
religion = sunni
capital = "K�tahya"
trade_goods = wool
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = GRN
	set_province_flag = turkish_name
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1402.1.1  = {
	owner = TIM
	controller = TIM
}
1428.1.1  = {
	owner = TUR
	controller = TUR
	remove_core = TIM
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR  }
1515.12.17 = { training_fields = yes }
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR  } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1720.1.1  = {  }
