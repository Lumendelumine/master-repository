# 2567 - Cuenca + Huete

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = castillian # culture = new_castillian
religion = catholic 
hre = no
base_tax = 7
base_production = 7
trade_goods = leather 
base_manpower = 5
is_city = yes
fort_14th = yes
capital = "Cuenca" 
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	owner = ENR	
	controller = ENR
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_toledo"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11  = {
	remove_core = CAS
}
1808.6.6   = {
	controller = REB
}
1811.1.1   = {
	controller = SPA
}
1812.10.1  = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
