# 2111 - Damo

owner = MPH
controller = MPH
culture = javan
religion = hinduism		#FB this region began to be Islamified c1300
capital = "Rembang"
trade_goods = rice
hre = no
base_tax = 3
base_production = 3
base_manpower = 4
is_city = yes
add_core = MPH
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1133.1.1 = { mill = yes }
1450.1.1 = { religion = sunni }
1478.1.1   = {
	owner = DEM
	controller = DEM
	add_core = DEM
}
1512.1.1 = { discovered_by = POR }		#FB Antonio de Abreu
1515.2.1 = { training_fields = yes }
1523.1.1 = {
	owner = DEM
	controller = DEM
	add_core = DEM
}
#exact date Demak could be said to be the dominant city in this province is uncertain
1613.1.1 = { discovered_by = NED citysize = 32000 } # The Dutch arrived
1625.1.1 = {
	owner = MTR
	controller = MTR
	add_core = MTR
	add_core = SBT
	remove_core = MPH
	remove_core = DEM
	unrest = 2
} 
1650.1.1 = { citysize = 35200 unrest = 1 }
1676.10.15 = { controller = REB } #rebels defeat Mataram army at Gogodog
1677.7.13 = {
	controller = MTR
	unrest = 2
} # Amangkurat's death
#after 1680 MTR had little real control in this province
1700.1.1 = { citysize = 37877 unrest = 1 }
1717.1.1 = { controller = REB } #Surabaya rebellion/2nd war of Javanese Succession
1721.1.1 = { controller = MTR unrest = 2 } 
1743.11.1 = { 
	owner = NED
	controller = NED
	unrest = 1
}
1746.5.1 = { unrest = 2 } #3rd war of Javanese Succession starts as rebellion
1750.1.1 = { citysize = 38404 }
1800.1.1 = { citysize = 41240 add_core = NED unrest = 0 }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
