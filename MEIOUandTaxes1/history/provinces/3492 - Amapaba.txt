# No previous file for Amapaba

culture = guajiro
religion = pantheism
capital = "Amapaba"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 30
native_ferocity = 2
native_hostileness = 7

1498.1.1   = { discovered_by = CAS } # Christopher Columbus
1516.1.23  = { discovered_by = SPA }
1600.1.1   = {
	discovered_by = POR
	add_core = POR
}
1637.1.1 = {
	owner = POR
	controller = POR
	change_province_name = "Amap�"
	rename_capital = "Amap�"
	citysize = 560
	culture = portugese
	religion = catholic
	trade_goods = sugar
		set_province_flag = trade_good_set
}
1650.1.1   = {
	citysize = 1030
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
