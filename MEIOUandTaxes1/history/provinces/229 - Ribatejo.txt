# 229 - Ribatejo

owner = POR
controller = POR
culture = portugese
religion = catholic
capital = "Santar�m"
trade_goods = livestock  
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
fort_14th = yes
add_core = POR
discovered_by = western
discovered_by = muslim
discovered_by = eastern


1372.5.5   = { unrest = 3 } # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5   = { unrest = 0 } # Civil unrest repressed.
1382.3.1   = { controller = CAS } # Third Fernandine War
1382.12.31 = { controller = POR }
1384.6.30  = { controller = CAS } # Portuguese Interregnum
1384.11.30 = { controller = POR } # CAS retreats definetly.
1515.1.1 = { training_fields = yes }
1580.8.25  = { controller = SPA }
1580.8.26  = { controller = POR }
1640.1.1   = { unrest = 8 } # Revolt headed by John of Bragan�a
1640.12.1  = { unrest = 0 }


1710.1.1   = {  }
1807.11.30 = { controller = FRA } # Occupied by France
1808.8.30  = { controller = POR }
1810.7.25  = { controller = FRA } # Invaded after the battle of C�a
1811.1.1   = { controller = POR } # The Napoleonic army retreats from Portugal
