# 2409 - Cholollan

owner = TXL
controller = TXL
add_core = TXL
culture = tlaxcallan
religion = nahuatl
capital = "Cholollan"

base_tax = 3
base_production = 3
#base_manpower = 1.0 
base_manpower = 2.0 
citysize = 25000
trade_goods = cacao 


hre = no

discovered_by = mesoamerican

1356.1.1 = {
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1520.1.1   = {
	owner = SPA
	controller = SPA
} #Tlaxcala allies with Spain against the Aztec
1545.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 15000
}
1650.1.1   = {
	citysize = 25000
}
1700.1.1   = {
	citysize = 40000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 60000
}
1800.1.1   = {
	citysize = 100000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba


