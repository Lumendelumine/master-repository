# 730 - Paeseo
# FL - Korea Universalis
# LS - Alpha 5

owner = KOR
controller = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Pyeongyang"
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = steppestech
hre = no

1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1270.1.1  = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1290.1.1 = {	#Yuan returns Dongnyeong Prefecture
	owner = KOR
	controller = KOR
	add_core = KOR
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1392.1.1  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1593.1.1 = {
	unrest = 5
} # Japanese invasion
1593.5.1 = {
	controller = JOS
	unrest = 0
} # Japanese invasion ends
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
