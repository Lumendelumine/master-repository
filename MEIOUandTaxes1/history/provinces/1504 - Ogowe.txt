#1504 - Ogow�

culture = bakongo
religion = animism
capital = "Ogow�"
trade_goods = crops # slaves
hre = no
native_size = 7
native_ferocity = 6
native_hostileness = 7
discovered_by = KON

1483.1.1 = { discovered_by = POR } # Diogo Cao
