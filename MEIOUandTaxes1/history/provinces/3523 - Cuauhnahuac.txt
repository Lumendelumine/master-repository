# 3523 - Cuauhnahuac

owner = TLC
controller = TLC
add_core = TLC
culture = nahuatl_c
religion = nahuatl
capital = "Tlachco" 

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 5000
trade_goods = maize



hre = no

discovered_by = mesoamerican

1356.1.1 = {
}
1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
	remove_core = TEN
}

1522.1.1 = {
	discovered_by = SPA

}
1529.1.1 = {
	owner = SPA
	controller = SPA
} 
1540.1.1 = {
	culture = castillian
	religion = catholic
} # Capital moved to another indian town, because of turmoil
1547.1.1   = {
	add_core = SPA
	citysize = 1000
}

1600.1.1   = {
	citysize = 2000
}
1650.1.1   = {
	citysize = 5000
}
1700.1.1   = {
	citysize = 10000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 20000
}
1800.1.1   = {
	citysize = 50000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba


