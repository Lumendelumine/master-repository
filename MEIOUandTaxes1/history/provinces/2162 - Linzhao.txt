# 2162 - Shaan-gan Liangzhou

owner = YUA
controller = YUA
culture = lanyin
religion = confucianism
capital = "Didao"
trade_goods = wool
hre = no
base_tax = 10
base_production = 10
base_manpower = 4
is_city = yes

discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = { 
		name = great_wall_ruins
		duration = -1
	}
}
1200.1.1 = { road_network = yes }
1225.1.1  = {
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1280.1.1 = { #Warlord state Qin
	add_core = QIN
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = QIN
}
1530.1.1 = { fort_14th = no fort_15th = yes }
1539.1.1 = {
	remove_province_modifier = great_wall_ruins
	add_permanent_province_modifier = {
		name = great_wall_full
		duration = -1
	}
}
1643.11.1  = {
	owner = DSH
	controller = DSH
	add_core = DSH
}
1645.3.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
	remove_core = DSH
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
