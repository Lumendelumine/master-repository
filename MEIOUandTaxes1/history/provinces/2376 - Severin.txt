#2376 - Severin

owner = WAL
controller = WAL 
culture = vlach
religion =  orthodox
capital = "Severin"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = iron
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1356.1.1  = {
	add_core = WAL
	add_core = HUN 
}
1444.1.1 = { remove_core = HUN }
#1462.1.1  = {
#	add_core = TUR
#} # Vlad III Dracula accepts to pay tribute to Mehmed II
1650.1.1  = {controller = REB } # Boyar rebellions
1658.1.1  = { controller = WAL }

1663.1.1  = { unrest = 7 } # Cantacuzino is murdered, struggle for power between the boyars
1688.1.1  = { unrest = 0 } # Period of stability, Constantine Brancoveanu reigns
1718.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # Treaty of Passarowitz, Banat & Oltenia passed to the Habsburg empire
1739.1.1  = { owner = WAL controller = WAL remove_core = HAB } # Returned to Wallachia after the Russian-Austrian-Turkish war
