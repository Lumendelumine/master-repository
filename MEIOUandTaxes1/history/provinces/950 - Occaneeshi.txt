# No previous file for Occaneeshi

culture = woccon
religion = totemism
capital = "Occaneeshi"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1607.1.1  = { discovered_by = ENG } # John Smith
1646.1.1 = { owner = ENG
		controller = ENG
		citysize = 500
		religion = protestant
		trade_goods = tobacco
		culture = english } #Fort Henry
1671.1.1 = {	add_core = ENG citysize = 1000 }
1707.5.12  = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
		culture = american
	    } # Declaration of independence
1782.11.1 = { remove_core = GBR unrest = 0 } # Preliminary articles of peace, the British recognized American independence
