# 2327 - Dawi S�mi

capital = "Kien Kasjarkka"
culture = sapmi
religion = finnish_pagan_reformed
hre = no
trade_goods = crops
native_hostileness = 1
native_size = 5
native_ferocity = 2
discovered_by = NOR
discovered_by = SWE
discovered_by = DEN
discovered_by = DAN

1634.1.1 = { capital = "Kengis" }
1640.1.1 = {
	owner = SWE
	controller = SWE 
	add_core = SWE
	citysize = 200
	trade_goods = livestock
	discovered_by = western
	discovered_by = eastern
	set_province_flag = trade_good_set
} # The border vs Norway was set earlier but at this point colonialism had also started
1640.1.2 = { culture = swedish }
1640.1.2 = { religion = protestant }
1650.1.1 = { trade_goods = iron citysize = 320 }
1652.1.1 = { capital = "Kiruna" }
1700.1.1 = { citysize = 430 }
1740.1.1 = { fort_14th = yes }
1750.1.1 = { citysize = 670 }
1800.1.1 = { fort_14th = yes citysize = 800 }
1809.3.29  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SWE
} # Treaty of Fredrikshamn
1809.9.17 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
}
