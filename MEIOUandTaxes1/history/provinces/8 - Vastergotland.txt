# Skaraborg
# MEIOU - Gigau

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = wheat
base_manpower = 4
is_city = yes
fort_14th = yes #Old �lvsborg Castle
capital = "Skara"
 #"Domkyrkan i Skara"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
#1088.1.1 = { dock = yes shipyard = yes }
#1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1523.8.16 = { mill = yes }
1527.6.1   = { religion = protestant}
1529.12.17 = { merchant_guild = yes }
1580.1.1   = { fort_14th = yes } #Skaraborg Castle
1598.8.15  = { controller = PLC } #Sigismund tries to reconquer his crown
1598.12.15 = { controller = SWE } #Duke Karl get it back
1601.1.1   = { fort_14th = no fort_15th = yes } #New �lvsborg Castle
1612.5.24  = { controller = DAN } #The War of Kalmar-Captured by Christian IV
1613.1.20  = { controller = SWE }#The Peace of Kn�red
1621.1.1   = { capital = "Goteborg" }

 #V�stg�tsdals regemente/V�stg�ta regemente till H�st
 #minor court belonging to G�ta Hovr�tt
1740.1.1   = { fort_15th = no fort_16th = yes }
1780.1.1   = { fort_16th = no fort_17th = yes }
