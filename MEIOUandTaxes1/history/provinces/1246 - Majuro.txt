# 1246 - Aorangi

culture = polynesian
religion = polynesian_religion
capital = "Aorangi"
trade_goods = crops # wool
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 10
native_ferocity = 4
native_hostileness = 9

1356.1.1  = {
	add_permanent_province_modifier = { name = remote_island duration = -1 }
}
1642.1.1 = { discovered_by = NED } # Abel Tasman
