
owner = TRI
controller = TRI
culture = moselfranconian
religion = catholic
trade_goods = wheat
capital = "Trier"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes

add_core = TRI

hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim



1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
#1500.1.1 = { road_network = yes }
1560.1.1   = { medieval_university = yes fort_14th = yes } # University of Trier is now handled by the Jesuites who bring a higher quality in education
 # The whole country of Trier receives one law.
1690.1.1   = { base_manpower = 1.0  } # Trier is repeatedly victim of French agression and population declines.

1792.10.4  = { controller = FRA } # Occupied by French troops
1797.10.17 = {
	owner = FRA
	add_core = FRA
} # The Treaty of Campo Formio
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1814.4.6   = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Napoleon abdicates
