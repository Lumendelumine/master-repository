# 2886 - Ketu

owner = OYO
controller = OYO
culture = yorumba
religion = west_african_pagan_reformed
capital = "Oyo-Ile"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = slaves
discovered_by = soudantech
hre = no

1356.1.1 = {
	add_core = OYO
}
1520.1.1 = { add_core = BEN unrest = 4 }
1525.1.1 = { unrest = 0 }
1585.1.1 = { unrest = 4 } #Nupe Raids
1590.1.1 = { unrest = 0 }
