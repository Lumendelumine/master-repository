# 1388 - Naunnt Retz
# MEIOU-GG - Hundred Year War

owner = BRI
controller = BRI 
add_core = BRI
culture = angevin
religion = catholic
capital = "Nantes"
base_tax = 8
base_production = 8
base_manpower = 6
is_city = yes
trade_goods = salt
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1000.1.1   = {
	add_permanent_province_modifier = {
		name = loire_estuary_modifier
		duration = -1
	}
}
1119.1.1 = { bailiff = yes }
1341.4.30  = {
	owner = BLO
	controller = BLO
	add_core = MNF
	add_core = BLO
	remove_core = BRI
} # Jean III de Bretagne dies in Caen
1365.4.12   = {
	owner = BRI
	controller = BRI
	add_core = BRI
	remove_core = BLO
	remove_core = MNF
} # End of the Brittany war of succession with the death of Charles de Blois
1378.1.1   = {
	add_core = FRA
} # Charles V invades Brittany without resistance
1460.1.1 = { medieval_university = yes }
1515.1.1 = { training_fields = yes }
1530.8.4   = { owner = FRA controller = FRA } # Union Treaty
1550.1.1   = { fort_14th = yes }
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1650.1.1   = {	
	fort_14th = no fort_15th = yes
}


1740.1.1   = { fort_15th = no fort_16th = yes }
1793.3.7   = {
} # Guerres de l'Ouest
1796.12.23 = {
} # The last rebels are defeated at the battle of Savenay
1799.10.15 = {
} # Guerres de l'Ouest
1800.1.18  = {
}
