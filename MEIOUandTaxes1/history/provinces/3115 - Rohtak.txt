# No previous file for Rohtak

owner = DLH
controller = DLH
culture = kanauji
religion = hinduism
capital = "Rohtak"
trade_goods = silk
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1115.1.1 = { bailiff = yes }
1120.1.1 = { textile = yes }
1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = DLH
}
1405.1.1  = { controller = DLH }
1444.1.1 = {
	controller = PTA
	add_core = PTA
}
1445.1.1 = {
	controller = DLH
	revolt = {  }
} #Bahlul Lodi returns to the Punjab
1447.1.1 = {
	#revolt = { type = pretender_rebels size = 5 leader = "Bahlul Lodi" }
	controller = PTA
} #Second revolt of Bahlul Lodi
1451.4.20 = {
	remove_core = PTA
	controller = DLH
} #Final triumph of the Lodi
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
} # Battle of Panipat
1530.1.1 = { add_core = AHM }
1540.1.1 = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1553.1.1 = {
	owner = DLH
	controller = DLH
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23 = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1556.10.7 = { controller = DLH }	# Hemu
1556.11.5 = { controller = MUG }	#Aftermath to second battle of Panipat
1566.1.1 = { revolt = { type = noble_rebels size = 1 } }
1566.6.1 = { revolt = { } }
1707.5.12 = { discovered_by = GBR }
1770.1.1 = {
	owner = MAR
	controller = MAR
	add_core = MAR
	remove_core = MUG
} # The Marathas
1803.12.30 = {
	owner = GBR
	controller = GBR
}
1818.6.3 = { remove_core = MAR }
