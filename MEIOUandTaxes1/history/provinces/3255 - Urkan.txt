# No previous file for Urkan

culture = evenki
religion = shamanism
capital = "Urkan"
trade_goods = fur
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1356.1.1 = {
	add_core = MYR
}
1584.1.1 = {
	owner = MYR
	controller = MYR
} #Solon Khanate
1643.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Founded by Pyotr Beketov
1655.1.1 = { unrest = 7 } # Yakutsk rebellion
1660.1.1 = { unrest = 0 }
1668.1.1 = {
	add_core = RUS
}
1684.1.1 = { unrest = 4 } # Yakut rebellion
1686.1.1 = { unrest = 0 }
