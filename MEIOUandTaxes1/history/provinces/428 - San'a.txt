# 428 - San'a

owner = YRA
controller = YRA
culture = yemeni
religion = shiite
capital = "San'a"
trade_goods = marble
hre = no
base_tax = 8
base_production = 8
base_manpower = 4
is_city = yes
discovered_by = ADA
discovered_by = TUR
discovered_by = MKU
discovered_by = muslim
discovered_by = turkishtech

1111.1.1 = { post_system = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = YRA
}
1465.1.1   = {
	trade_goods = coffee
}
1516.1.1   = { add_core = TUR }
1517.4.13  = { 
	owner = TUR
	controller = TUR
	remove_core = MAM
} # Conquered by Ottoman troops
1524.1.1 = { discovered_by = POR }
1530.1.1   = {
	owner = YRA
	controller = YRA
	add_core = YRA
	remove_core = TUR
}
1567.1.1 = { unrest = 4 } # Revolt against the Ottomans
1570.1.1 = { unrest = 0 }
1597.9.1 = { unrest = 5 } # Qasimi state, revolt against the Ottomans
1602.1.1 = { unrest = 0 }
1636.1.1 = {
	owner = YEM
	controller = YEM
	remove_core = TUR
}
