# 2645 - Tlaxiaco

owner = MIX
controller = MIX
add_core = MIX
culture = mixtec
religion = nahuatl
capital = "Tlachquiyauhco" 

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 5000
trade_goods = maize



discovered_by = mesoamerican

1356.1.1 = {
}
1440.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
}
1522.3.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
} #Pedro de Alvanado
1547.1.1   = {
	add_core = SPA
}
1572.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 5000
}
1650.1.1   = {
	citysize = 10000
}
1700.1.1   = {
	citysize = 20000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 35000
}
1800.1.1   = {
	citysize = 75000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba

