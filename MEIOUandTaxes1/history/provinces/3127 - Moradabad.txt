# No previous file for Moradabad

owner = DLH
controller = DLH
culture = kanauji
religion = sunni #muslim dominated 
capital = "Sambhal"
trade_goods = cotton #Calicoes
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
# 
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
discovered_by = indian

1115.1.1 = { bailiff = yes }
1120.1.1 = { plantations = yes }
1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = DLH
	add_core = AHM
	unrest = 6
}
1399.1.1 = {
	owner = AHM
	controller = AHM
	unrest = 0
} #guessed date for independance from Delhi Sultanate
1444.1.1 = {
	add_core = DLH
}
1483.1.1  = {
	controller = DLH
	owner = DLH
}
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
} # Battle of Panipat
1540.1.1  = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1553.1.1  = {
	owner = DLH
	controller = DLH
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23 = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1565.1.1  = {
	controller = REB
	revolt = { type = noble_rebels }
} #Revolt of Uzbek commanders
1566.6.1  = {
	controller = MUG
	revolt = { }
}
1625.1.1 = {
	capital = "Moradabad"
}
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1721.1.1  = {
	owner = RMP
	controller = RMP
	remove_core = MUG
} # Rohilkandh independent
1757.1.1  = {
	owner = ODH
	controller = ODH
} # Oudh
1761.1.1  = {
	owner = RMP
	controller = RMP
} # Rohilkandh independent
1794.1.1  = {
	owner = ODH
	controller = ODH
} # Oudh
1801.1.1  = {
	owner = GBR
	controller = GBR
} # Great Britain
