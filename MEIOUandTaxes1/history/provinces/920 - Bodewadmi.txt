# No previous file for Bodewadmi

culture = potawatomi
religion = totemism
capital = "Bodewadmi"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 4
native_hostileness = 8

1659.1.1  = { discovered_by = FRA } # M�dard Chouart Des Groseilliers
1664.1.1  = { discovered_by = ENG }
1680.1.1  = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1690.1.1  = { culture = potawatomi } #Retaken by French/Allies in Beaver Wars
1707.5.12 = { discovered_by = GBR }
1813.10.5 = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
} #The death of Tecumseh mark the end of organized native resistance
