# 749 - Tembe

culture = ge
religion = pantheism
capital = "Tembe"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 8

1500.1.1   = {
	discovered_by = CAS
	discovered_by = POR
	add_core = POR
} # Pinz�n, Pedro �lvares Cabral 
1516.1.23  = {
	discovered_by = SPA
}
1613.7.8   = {
	owner = POR
	controller = POR
	capital = "Bragan�a"
	citysize = 560
	culture = portugese
	religion = catholic
	trade_goods = sugar
	set_province_flag = trade_good_set
}
1650.1.1   = {
	citysize = 1530
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
