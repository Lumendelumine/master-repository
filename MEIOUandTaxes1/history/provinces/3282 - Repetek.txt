# No previous file for Repetek

owner = KTD
controller = KTD
culture = turkmeni
religion = sunni
capital = "Karamet-Niyaz"
trade_goods = subsistence  #########
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = KHI
	add_core = TIM
	add_core = KHO
	add_core = KTD
}
1381.4.1   = {
	owner = TIM
	controller = TIM
}
1444.1.1 = {
	remove_core = KTD
}
1500.6.1   = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = TIM
} # Shaybanids break free from the Timurids
1512.1.1  = {
	owner = PER 
	controller = PER
	add_core = PER
	remove_core = SHY
	add_core = KHI
	add_core = BUK
} # Safavids expel Uzbeks from Merv
1785.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
