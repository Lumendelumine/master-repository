# 2445 - Yenisei

culture = evenki
religion = shamanism
capital = "Turukhansk"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 50
native_ferocity = 2
native_hostileness = 1

1750.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
 	citysize = 100
   	culture = russian
   	religion = orthodox
	trade_goods = fur
	set_province_flag = trade_good_set
}
