# Shetlands
# MEIOU - Gigau

owner = NOR
controller = NOR
culture = norse
religion = catholic
hre = no
base_tax = 1
base_production = 1
trade_goods = fish
base_manpower = 1
is_city = yes
capital = "Lerwick"
add_core = NOR
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1088.1.1 = { dock = yes }

1469.1.1   = {
	owner = SCO
	controller = SCO
	add_core = SCO
	remove_core = NOR
	culture = norse_gaelic
} # Dowry of Margaret, Daughter of Christian I of Denmark and Norway and Wife of James III of Scotland
1609.1.1   = {
	religion = reformed
} #First Protestant Minister Arrives in the Orkneys
1622.1.1   = {
	trade_goods = wool
}
1707.5.12   = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
