# 3537 - Pames

culture = chichimecha
religion = nahuatl
capital = "Xallpan"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 25
native_ferocity = 4
native_hostileness = 8
discovered_by = mesoamerican

1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	capital = "Jalpan de Serra"
	
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 15000
}
1650.1.1   = {
	citysize = 25000
}
1700.1.1   = {
	citysize = 40000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 60000
}
1800.1.1   = {
	citysize = 100000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
