#1139 - Doala

culture = bakongo
religion = animism
capital = "Doala"
trade_goods = crops # millet
hre = no
native_size = 80
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan
