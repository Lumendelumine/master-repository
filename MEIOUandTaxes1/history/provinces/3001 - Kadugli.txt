# 3001 - Kadugli

owner = SLL
controller = SLL
add_core = SLL
culture = shilluk 
religion = animism
capital = "Kadugli"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = salt
hre = no
discovered_by = ALW
discovered_by = YAO
discovered_by = ETH
discovered_by = ADA
discovered_by = DAR
discovered_by = KIT
discovered_by = MKU
discovered_by = DSL
discovered_by = KDF
discovered_by = SLL

1620.1.1 = {
	owner = SEN
	controller = SEN
	add_core = SEN
	discovered_by = SEN
	discovered_by = ETH
}
1820.1.1 = {
	discovered_by = TUR
	owner = TUR
	controller = TUR
}
