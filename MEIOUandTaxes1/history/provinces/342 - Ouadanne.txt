#342 - Ouadanne

culture = hassaniya
religion = sunni
capital = "Ouadanne"
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = crops
discovered_by = CNA
discovered_by = DJO
discovered_by = TUA
discovered_by = MAL
discovered_by = MOR
discovered_by = FEZ
hre = no

1437.1.1   = { discovered_by = POR } #Cadamosto
