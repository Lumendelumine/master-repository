# 2222 - Urgench

owner = CHG
controller = CHG
culture = kwarezhmi
religion = sunni
capital = "Urgench"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 3
is_city = yes
discovered_by = muslim
discovered_by = turkishtech
discovered_by = mongol_tech
discovered_by = steppestech

1356.1.1   = {
	add_core = CHG
	add_core = KHI
	unrest = 3
}
1360.1.1   = {
	owner = KHI
	controller = KHI
}
1379.1.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1469.8.27 = {
	owner = KHI
	controller = KHI
	remove_core = TIM
}
1505.1.1 = {
	owner = SHY
	controller = SHY
} # Captured by the Shaybanid horde
1511.1.1 = {
	owner = KHI 
	controller = KHI
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1677.1.1 = { discovered_by = FRA }
