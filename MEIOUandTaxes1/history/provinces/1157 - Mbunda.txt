#1157 - Mbunda

culture = mbundu
religion = animism
capital = "Mbunda"
trade_goods = crops # slaves
native_size = 50
native_ferocity = 1
native_hostileness = 7
hre = no
discovered_by = KON
