# 2871 - Gor�e

culture = senegambian
religion = west_african_pagan_reformed
capital = "Gor�e"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech

1437.1.1   = { discovered_by = POR } #Cadamosto
1536.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
		trade_goods = slaves
	change_province_name = "Cabo Verde"
}
1664.1.23  = {
	owner = ENG
	controller = ENG
	add_core = ENG
	remove_core = POR
}
1677.1.1   = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = ENG
}