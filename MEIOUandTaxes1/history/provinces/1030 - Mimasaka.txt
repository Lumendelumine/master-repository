# 1030 - Mimasaka
# GG/LS - Japanese Civil War

owner = AKM
controller = AKM
culture = chugoku
religion = mahayana #shinbutsu
capital = "Tsuyama"
trade_goods = linen
hre = no
is_city = yes # citysize = 1500
base_tax = 3
base_production = 3
base_manpower = 4

discovered_by = chinese

1356.1.1 = {
	add_core = AKM
	add_core = UKI
	add_core = MRI
}
1525.1.1 = {
	owner = UKI
	controller = UKI
}
1542.1.1 = { discovered_by = POR }
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}