# No previous file for Purniah

owner = BNG
controller = BNG
culture = bihari
religion = hinduism
capital = "Purnia"
trade_goods = rice
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1115.1.1 = { bailiff = yes }
1120.1.1 = { farm_estate = yes }
1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BNG
	add_core = TRT	
}
1500.1.1  = {
	discovered_by = POR
}
1515.12.17 = { training_fields = yes }
1530.1.1 = {
	owner = AHM
	controller = AHM
	add_core = AHM
}
1538.6.1  = {
	controller = MUG
} # Mughal Invasion
1539.1.1  = {
	controller = BNG
	owner = BNG
} # Surs again in control
1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1627.1.1  = { discovered_by = POR }
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1760.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
1810.1.1  = {
	add_core = GBR
}
