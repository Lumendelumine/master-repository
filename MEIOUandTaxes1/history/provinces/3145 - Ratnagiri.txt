# No previous file for Ratnagiri

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Ratnagiri"
trade_goods = pepper
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
add_local_autonomy = 25
estate = estate_marathas

1356.1.1  = {
	add_core = BIJ
	add_core = BAH
	fort_14th = yes
}
#1380.1.1  = {
#	controller = VIJ
#	owner = VIJ
#} # Taken by Harihara II
#1469.1.1  = {
#	controller = BAH
#} # Taken by Bahmanis
#1470.1.1  = {
#	owner = BAH
#} # Taken by Bahmanis
1490.1.1  = {
	owner = BIJ
	controller = BIJ
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1502.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1596.2.1  = { discovered_by = NED } # Cornelis de Houtman
1600.1.1  = {discovered_by = turkishtech discovered_by = ENG discovered_by = FRA }
1636.1.1  = { add_core = JAJ controller = JAJ owner = JAJ } #Ceded in treaty to Mughals by Bijapur but never actually carried out. De facto under Sidhi, Maratha and Bijapur control without a clear stronger party or claim.
1659.11.30 = {
	owner = MAR
	controller = MAR
	add_core = MAR
	fort_14th = no
	fort_17th = no
} # battle of Pratapgarh
1686.1.1  = { controller = MUG } # Aurangzeb
1703.1.1  = { controller = MAR }
1707.5.12 = { discovered_by = GBR }
1818.6.3  = {
	owner = GBR
	controller = GBR
}
