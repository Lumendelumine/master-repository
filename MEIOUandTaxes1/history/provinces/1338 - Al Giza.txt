# 1338 - Al Giza

owner = MAM
controller = MAM
culture = egyptian
religion = sunni
capital = "Al Giza"
trade_goods = wheat
hre = no
base_tax = 11
base_production = 11
base_manpower = 4
is_city = yes
discovered_by = ADA
discovered_by = ALW
discovered_by = MKU
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1133.1.1 = { mill = yes farm_estate = yes }
1356.1.1 = {
	add_core = MAM
	add_core = EGY
}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.1   = {
	#owner = MAM
	#controller = MAM
	add_core = MAM
	#remove_core = TUR
}
1796.1.1   = { controller = REB } # Revolts against the Ottomans
1798.8.10  = {
	revolt = {}
	controller = FRA
} # Occupied by the French
1802.10.1  = {
	controller = TUR
	unrest = 8
} # Turkish rule is restored but a few troublesome years follow
1805.1.1 = {
	unrest = 0
	owner = EGY
	controller = EGY
}
1811.6.1   = {
	owner = TUR
	controller = TUR
} # Order is restored
