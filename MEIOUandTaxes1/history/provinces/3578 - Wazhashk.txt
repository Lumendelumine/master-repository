# No previous file for Wazhashk

culture = ojibwa
religion = totemism
capital = "Wazhashk"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1654.1.1  = { discovered_by = FRA } # M�dard Chouart Des Groseilliers
1664.1.1  = { discovered_by = ENG }
1710.1.1  = { 	owner = XXX
		controller = XXX
		# remove_core = CHY
		citysize = 0 } #Fleeing westward
1737.1.1  = {	owner = FRA
		controller = FRA
		citysize = 400
		culture = francien
		religion = catholic
	    } # Construction of Fort St. Pierce
1763.1.1  = { add_core = FRA }
1763.2.10 = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = protestant
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.10.9 = {	owner = XXX
		controller = XXX
		# add_core = XXX
		is_city = yes
		culture = potawatomi
		religion = totemism
	    } # Royal Proclamation, British recognize native territory
1800.1.1  = { citysize = 1870 }
1813.10.5 = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #The death of Tecumseh mark the end of organized native resistance 
