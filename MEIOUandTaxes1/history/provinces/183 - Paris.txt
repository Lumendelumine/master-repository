# 183 - Paris

owner = FRA
controller = FRA
add_core = FRA
culture = francien
religion = catholic
capital = "Paris"
base_tax = 13
base_production = 13
base_manpower = 17
is_city = yes
trade_goods = services
fort_14th = yes
medieval_university = yes # La Sorbonne
discovered_by = eastern
discovered_by = western
discovered_by = muslim

hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1419.1.19  = {
	owner = ENG
	controller = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
}
1449.1.1   = {
	owner = FRA
	controller = FRA
}
1515.1.1 = { training_fields = yes }
1571.1.1   = { unrest = 5 } # Unrest spreads in catholic territory: massacres of protestants in Rouen, Orange & Paris
1573.8.23  = { unrest = 10 } # Saint Barthelew's Day Massacre
1574.5.1   = { unrest = 0 } # Charles IX dies, situation cools a bit	
1584.1.1   = { unrest = 7 } # Situation heats up again, even Paris is restless
1588.1.1   = { unrest = 10 } # Paris turns against the King and sides with the de Guises
1588.12.1  = { unrest = 12 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1589.1.1   = { base_tax = 14
	base_production = 14
	early_modern_university = yes
} # At the Walls of Paris: French Wars of Religion
1590.1.1   = {  }
1594.1.1   = { unrest = 8 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13  = { unrest = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { unrest = 0 } # Peace of Vervins, formal end to the Wars of Religion
1631.1.1   = { unrest = 3 }
1633.1.1   = { unrest = 0 }
1648.5.1   = { unrest = 5 } # New tax levied by the Parliament of Paris, first Fronde coming up
1648.8.1   = { unrest = 10 } # Cardinal Mazarin arrests the leaders of the Parliament, Paris rises up and barricades the streets
1648.10.21 = { revolt = { type = noble_rebels size = 2 } controller = REB unrest = 3 } # The First Fronde hits Paris, officials flee from the city
1649.3.1   = { revolt = { } controller = FRA unrest = 0 } # Mar�chal Cond� besieges and takes back the city, little to no bloodshed - Peace of Rueil signed, peace returns, although Paris remains anti-cardinalist
1650.1.14  = { revolt = { type = noble_rebels size = 2 } controller = REB unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde.
1651.4.1   = { revolt = { } controller = FRA unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.7.2   = { revolt = { type = noble_rebels size = 2 } controller = REB unrest = 3 } # The gates of Paris opened for Cond�'s troops
1652.10.21 = { revolt = { } controller = FRA unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1789.7.14  = {
	revolt = { type = revolutionary_rebels size = 2 }
	controller = REB
} # The storming of the Bastille
1789.7.15  = {
	revolt = { }
	controller = FRA
}
