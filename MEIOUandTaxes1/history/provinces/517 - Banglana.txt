# 517 - Baglana

owner = BGA
controller = BGA
culture = khandeshi
religion = hinduism
capital = "Mayuragiri"
trade_goods = millet	#Millet
hre = no
base_tax = 2
base_production = 2
#base_manpower = 0.5
base_manpower = 1.0
citysize = 3000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = dharampur_state
		duration = -1
	}
}
1120.1.1 = { farm_estate = yes }

1356.1.1  = {
	add_core = BGA
}
1450.1.1  = { citysize = 4000 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 5000 }
1550.1.1  = { citysize = 7000 }
1573.6.1  = {
	owner = MUG
	controller = MUG
	capital = "Khuldabad"
} # Conquered by Akbar
1600.1.1  = {
	citysize = 9000
}
1623.1.1  = { add_core = MUG }
1650.1.1  = {
	citysize = 14000
	add_core = MAR #Maratha Identity
}
1674.1.1  = {
	owner = MAR
	controller = MAR
	remove_core = MUG
	capital = "Nashik"
} # The Marathan Empire
1700.1.1  = { citysize = 17000 }
1750.1.1  = { citysize = 21000 }
1800.1.1  = { citysize = 25000 }
1818.6.3  = {
	owner = GBR
	controller = GBR
}
