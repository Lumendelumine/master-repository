# 600 - Ayutthaya
# TM-Smiles indochina-mod for meiou

owner = AYU 
controller = AYU
add_core = AYU
culture = thai
religion = buddhism
capital = "Ayutthaya"
base_tax = 13
base_production = 13
base_manpower = 9
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1133.1.1 = { mill = yes }
1351.1.1 = { temple = yes }
1356.1.1 = { fort_14th = yes}
1564.2.1 = {
	add_core = TAU
} # Burmese vassal
1584.5.3 = {
	remove_core = TAU
} 
1600.1.1 = {
	
}
1650.1.1 = {
	
}
1767.4.1 = {
	unrest = 7
} # The Ayutthaya kingdom began to crumble
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
	
}
