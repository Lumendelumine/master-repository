#257 - Plock

owner = POL
controller = POL
add_core = MAZ
add_core = POL
culture = polish
religion = catholic
capital = "Plock"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
fort_14th   = yes
trade_goods = hemp
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
hre = no

1250.1.1 = { temple = yes }
1370.11.6   = { owner = MAZ controller = MAZ }
1495.2.16 = {
	owner = POL
	controller = POL
	remove_core = MAZ
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1580.1.1 = {
	
}
1588.1.1 = {
	controller = REB
} # Civil war, Polish succession
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1606.1.1 = {
	controller = REB
} # Civil war
1608.1.1 = {
	controller = PLC
} # Minor victory of Sigismund
1655.1.1 = {
	controller = SWE
} # The Deluge
1660.1.1 = {
	controller = PLC
}
1793.1.23 = {
	controller = PRU
	owner = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Second partition
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1806.11.3  = { controller = REB } # Polish uprising instigated by Napoleon
1807.7.9   = {
	owner = POL
	controller = POL
     	remove_core = PRU
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = { controller = PRU }
1814.4.11  = { controller = POL }
1815.6.9   = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
