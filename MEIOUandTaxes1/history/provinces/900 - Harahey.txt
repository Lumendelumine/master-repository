# No previous file for Harahey

culture = comanche
religion = totemism
capital = "Harahey"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 6
