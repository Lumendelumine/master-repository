# 3341 - Alentejo Central

owner = POR
controller = POR
culture = portugese
religion = catholic
capital = "�vora"
trade_goods = livestock        # could be olive
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
add_core = POR
estate = estate_church
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1340.1.1 = { temple = yes }
1372.5.5   = { unrest = 1 } # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5   = { unrest = 0 } # Civil unrest repressed.

1382.1.1   = { controller = CAS } # Third Fernandine War
1382.12.31 = { controller = POR }
1384.1.1   = { controller = CAS } # Portuguese Interregnum
1384.9.3   = { controller = POR } # CAS retreats.
1515.1.1 = { training_fields = yes }
1580.8.25  = { controller = SPA }
1580.8.26  = { controller = POR }

1640.1.1   = { unrest = 6 } # Revolt headed by John of Bragan�a
1640.12.1  = { unrest = 0 }


1710.1.1   = {  }
1807.11.30 = { controller = FRA } # Occupied by France
1808.8.30  = { controller = POR }
1810.7.25  = { controller = FRA } # Invaded after the battle of C�a
1811.1.1   = { controller = POR } # The Napoleonic army retreats from Portugal
