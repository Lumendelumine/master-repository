# No previous file for Na'isha

culture = cheyenne
religion = totemism
capital = "Na'isha"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 8
