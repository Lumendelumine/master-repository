# 1506 - Benguela
# FB - added discovered_by = POR

culture = mbundu
religion = animism
capital = "Namib"
trade_goods = crops # slaves
hre = no
native_size = 40
native_ferocity = 4.5
native_hostileness = 9
discovered_by = KON

1481.1.1 = { discovered_by = POR } # Bartolomeu Dias
1617.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 144
	capital = "Sa� Felipe de Benguela"
	trade_goods = slaves
	set_province_flag = trade_good_set
}
1641.1.1 = { owner = NED controller = NED } # Captured by the Dutch
1648.1.1 = { owner = POR controller = POR } # A Brazilian-Portuguese expedition expelled the Dutch
