# No previous file for Yman

owner = MUD
controller = MUD
culture = evenki
religion = shamanism
capital = "Yman"
trade_goods = naval_supplies
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1356.1.1 = {
	add_core = MUD
}
1515.1.1 = { training_fields = yes }
1616.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
} # The Later Jin Khanate
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1709.1.1 = { discovered_by = SPA }
1858.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
