# 882 - Xicuahua
# GG - 22/07/2008

culture = tarachatitian
religion = nahuatl
capital = "Xicuahua"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 4
native_hostileness = 8
discovered_by = mesoamerican
discovered_by = north_american

1540.1.1   = {
	discovered_by = SPA
} # Francisco Vázquez de Coronado y Luján
1563.1.1   = {	owner = SPA
		controller = SPA
		culture = castillian
		religion = catholic 
		capital = "Durango"
	   	citysize = 1340
		trade_goods = wool
	     } # Francisco de Ibarra
1565.1.1   = { add_core = SPA is_city = yes }
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
