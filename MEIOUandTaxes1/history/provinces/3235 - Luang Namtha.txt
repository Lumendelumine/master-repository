# No previous file for Luang Namtha

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Luang Namtha"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
hre = no

1356.1.1 = {
}
1707.1.1 = {
	owner = LUA
	controller = LUA
	add_core = LUA
	remove_core = LXA
} # Declared independent, Lan Xang was partitioned into three kingdoms; Vientiane, Champasak & Luang Prabang

#1870s 'haw marauders' invasion
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
 