# No previous file for U�bh Eachach

owner = ENG
controller = ENG
culture = irish
religion = catholic
hre = no
base_tax = 5
base_production = 5
trade_goods = hemp
is_city = yes
base_manpower = 3
capital = "D�n P�draig"
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1356.1.1   = {
	add_core = ENG
}
1607.9.4   = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # Flight of the Earls
1609.1.1   = {
	culture = lowland_scottish
	religion = protestant #anglican
	fort_14th = yes
	capital = "B�al Feirste"
} # Jacobean Plantations
1625.1.1   = {
	
} # Estimated
1630.1.1   = {
	culture = english
}
1641.10.22 = {
	controller = REB
}
1642.5.1   = {
	controller = ENG
} # Estimated
1646.6.5   = {
	controller = IRE
}
1650.6.21  = {
	controller = ENG
} # Battle of Scarrifhollis
1675.1.1   = {
	
	
} # Estimated
1698.1.1   = {
	culture = lowland_scottish
}
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1711.1.1   = {
	trade_goods = linen
} # Formation of Linen Board
1775.1.1   = {
	
	
} # Estimated
1798.5.23  = {
	controller = REB
} # Irish rebellion
1798.7.14  = {
	controller = GBR
} # The rebels are defeated
