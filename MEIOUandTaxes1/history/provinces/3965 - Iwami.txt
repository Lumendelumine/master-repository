# 3965 - Iwami

owner = AKW
controller = AKW
culture = chugoku
religion = mahayana
capital = "Hamada"
trade_goods = fish
is_city = yes # citysize = 1500
hre = no
base_tax = 3
base_production = 3
base_manpower = 4
discovered_by = chinese

1356.1.1 = {
	add_core = YMN
	add_core = MRI
	add_core = OUC
	add_core = AKW 
}
1366.1.1 = {
	controller = OUC
	owner = OUC
}
1376.1.1 = {
	controller = AKW
	owner = AKW
}
1379.1.1 = {
	controller = OUC
	owner = OUC
}
1399.1.1 = {
	controller = KYO
	owner = KYO
}
1401.1.1 = {
	owner = YMN
	controller = YMN
}
1481.1.1 = {
	owner = OUC
	controller = OUC
}
1526.1.1 = {
	trade_goods = silver #Iwami Ginzan Silver Mine
}
1542.1.1 = { discovered_by = POR }
1561.1.1   = {
	owner = MRI
	controller = MRI
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}