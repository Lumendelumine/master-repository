#308 - Yaroslavl

owner = YAR
controller = YAR
culture = russian
religion = orthodox
capital = "Yaroslavl"
base_tax = 7
base_production = 7
base_manpower = 5
is_city = yes
trade_goods = iron
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = YAR
	add_core = MOS
}
1474.1.1  = {
	owner = MOS
	controller = MOS
}
 # Spasskiy Monastery
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = { unrest = 5 } # "Time of troubles", peasantry brought into serfdom
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1667.1.1  = { controller = REB } # Peasant uprising, Stenka Razin
1670.1.1  = { controller = RUS } # Crushed by the Tsar's army
