
owner = BAD
controller  = BAD
add_core = BAD
capital = "Baden"
trade_goods = wine
religion = catholic
culture = schwabisch
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }
1500.1.1 = { road_network = yes }

1538.1.1  = { religion = protestant } # Protestant majority

1650.1.1  = { religion = catholic }

1706.1.1 = { capital = "Rastatt" }
1750.1.1  = {   }
1792.10.3 = { controller = FRA } # Occupied by French troops
1796.8.7  = { controller = BAD }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
