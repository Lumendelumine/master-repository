# No previous file for Bagasra

owner = GUJ
controller = GUJ
culture = saurashtri
religion = hinduism
capital = "Bagasra"
trade_goods = cotton
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1100.1.1 = { plantations = yes }
1356.1.1 = {
	#add_core = DLH
	add_core = GUJ
	unrest = 6
}
1396.1.1 = {
	owner = GUJ
	controller = GUJ
	remove_core = DLH
	unrest = 0
}
1498.1.1 = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1556.1.1 = {
	owner = KAT
	controller = KAT
	add_core = KAT
} # Kathiawar independent while Ahmad Shahis are busy with internal conflicts
1591.1.1 = {
	controller = MUG
} # Conquered by Mughals
1592.1.1 = {
	owner = MUG
	add_core = MUG
} # Conquered by Mughals
1725.1.1 = {
	owner = GAK
	controller = GAK
	add_core = GAK
	remove_core = MUG	
}
1802.1.1 = {
	owner = KAT
	controller = KAT
	remove_core = GAK	
}
