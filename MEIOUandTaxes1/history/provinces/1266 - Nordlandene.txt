# Nordlandene

owner = NOR
controller = NOR
add_core = NOR
culture = sapmi
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = fish
base_manpower = 1
is_city = yes
capital = "Helgi"
discovered_by = eastern
discovered_by = western

1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
}
1524.1.1 = { dock = yes shipyard = yes }
1529.12.17 = { merchant_guild = yes }
1536.1.1   = { religion = protestant} #Unknown date
1564.3.3   = {
	controller = SWE
}#Nordic Seven-years War
1564.5.21  = {
	controller = DAN
}#Nordic Seven-years War
1814.1.14  = {
	owner = SWE
	revolt = { type = nationalist_rebels size = 0 } controller = REB
	remove_core = DAN
} # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {  } owner = NOR controller = NOR }
