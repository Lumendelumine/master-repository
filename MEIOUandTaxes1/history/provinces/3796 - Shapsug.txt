# No previous file for Shapsug

owner = CIR
controller = CIR
culture = circassian
religion = orthodox
capital = "Dzhubga"
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
trade_goods = wool
discovered_by = eastern
discovered_by = muslim
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	add_core = GEO
	add_core = CIR
}
1444.1.1 = {
	remove_core = GEO
}
1505.1.1 = {
	#add_core = PER
}
1520.1.1  = {
#	owner = TUR
#	controller = TUR
#	add_core = TUR
	remove_core = IME
}
1570.1.1  = {
	religion = sunni
}
1829.7.21 = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Treaty of Adrianople
