# 2803 - Hoseo

owner = KOR
controller = KOR
add_core = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Chungju"
base_tax = 7
base_production = 7
#base_manpower = 2.5
base_manpower = 5.0
citysize = 30000
trade_goods = chinaware



discovered_by = chinese
discovered_by = steppestech

hre = no

1356.1.1 = {
}
1392.1.1  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1500.1.1 = {
	citysize = 30000
}
1550.1.1 = {
	citysize = 30000
}
1592.4.28 = {
	unrest = 5
} # Japanese invasion
1593.5.18 = {
	controller = JOS
	unrest = 0
} # With Chinese help the Japanese are driven back
1600.1.1 = {
	citysize = 15000
} # Great devastation as a result of the Japanese invasion
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1650.1.1 = {
	citysize = 30000
}
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
1700.1.1 = {
	citysize = 40000
}
1750.1.1 = {
	citysize = 50000
}
1800.1.1 = {
	citysize = 60000
}
