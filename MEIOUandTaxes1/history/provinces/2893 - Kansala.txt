# 2893 - Kansala

owner = MAL
controller = MAL
culture = senegambian
religion = west_african_pagan_reformed
capital = "Kansala"
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes
trade_goods = slaves
discovered_by = soudantech
hre = no

1356.1.1 = {
	add_core = MAL
	add_core = KAA
}
1530.1.1 = {
	owner = KAA
	controller = KAA
	remove_core = MAL
}
