# No previous file for Mandi

owner = MND
controller = MND
culture = pahari
religion = hinduism
capital = "Kangra"
trade_goods = cotton
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = suket_state
		duration = -1
	}
}
1356.1.1 = {
	add_core = MND
}
1620.1.1 = {
	controller = MUG
} # Mughals
1621.1.1 = {
	owner = MUG
} # Mughals
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1783.1.1  = {
	controller = MND
	owner = MND
}
1846.1.1  = {
	owner = GBR
	controller = GBR
}
