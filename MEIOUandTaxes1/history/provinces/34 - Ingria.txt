# Ingria
# MEIOU - Gigau

owner = NOV
controller = NOV
add_core = NOV
culture = ingrian
religion = orthodox
hre = no
base_tax = 2
base_production = 2
trade_goods = hemp
base_manpower = 1
is_city = yes
fort_14th = yes
capital = "Koporiye"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
 
1478.1.14  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
} #Muscovite annectation of Novgorod
1492.1.1   = {
	capital = "Ivangorod"
	fort_14th = no
	fort_15th = yes
}
1547.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1583.8.10  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = RUS
} #The Armistice of Pliusa
1590.2.23  = {
	owner = RUS
     	controller = RUS
     	add_core = RUS
     	remove_core = SWE
} #The Armistice of Narva
1612.12.4  = { controller = SWE } #The Ingermanian War-Captured by Evert Horn
1617.2.17  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = RUS
	} #The Peace of Stolbova
1617.2.17  = { religion = protestant }
1721.8.30  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SWE
	
} #The Peace of Nystad
1733.1.1   = {
	
}
