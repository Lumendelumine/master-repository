# 1540 - Taino

culture = arawak
religion = pantheism
capital = "Habana"
trade_goods = crops 
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 40
native_ferocity = 1
native_hostileness = 3

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1492.10.24 = { discovered_by = CAS } # Christopher Columbus's arrival
1511.1.1   = {
	owner = CAS
	controller = CAS
   	citysize = 540
  	culture = castillian
	religion = catholic
	capital = "Havana"
	trade_goods = tobacco
	set_province_flag = trade_good_set
} # Founded by Diego Vel�zquez
1516.1.23  = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	citysize = 1500
}
1522.1.1   = { unrest = 2 } # Relatively small attack against the conquistadors
1532.1.1   = { unrest = 0 } # Guama is killed
1536.1.1   = { add_core = SPA }
1550.1.1   = { citysize = 3010 }
1600.1.1   = { citysize = 3460 }
1650.1.1   = { citysize = 3980  }
1700.1.1  = {
	citysize = 4570
	culture = caribean
}1750.1.1   = { citysize = 5260 } 
1800.1.1   = { citysize = 6050 }
