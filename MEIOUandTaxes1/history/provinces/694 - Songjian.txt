# 694 - jiangsu_area Songjiang

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Songjiang"
trade_goods = steel
hre = no
base_tax = 5
base_production = 5
#base_manpower = 3.0
base_manpower = 3
citysize = 150000

discovered_by = chinese
discovered_by = steppestech

1000.1.1   = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = {
		name = yangtze_estuary_modifier
		duration = -1
	}
}
1111.1.1 = { post_system = yes }
1200.1.1 = { marketplace = yes }
1201.1.1 = { road_network = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1351.1.1  = {
	owner = YUA
	controller = YUA
#	add_core = YUA
}
1356.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
    add_core = ZOU
#	remove_core = YUA # Red Turbans
}
1366.1.1 = {
	remove_core = ZOU
}
1500.1.1  = { citysize = 150000 }
1550.1.1  = { citysize = 150000 }
1554.1.1  = { fort_14th = yes } #walls to resist Wokou
1600.1.1  = { citysize = 150000}
1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1640.1.1  = { controller = REB } # Financial crisis
1641.1.1  = { controller = MNG } 
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1644.1.1 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1650.1.1  = { citysize = 150000 }
1662.1.1 = { remove_core = MNG }
1700.1.1  = { citysize = 200000 }
1750.1.1  = { citysize = 250000 }
1800.1.1  = { citysize = 300000 }
