# 334 - tangiers

owner = FEZ
controller = FEZ 
culture = fassi
religion = sunni
capital = "Tanja"
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
trade_goods = wool
fort_14th = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
hre = no

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes shipyard = yes }
# 1075 = almoravides
# 1149 = almohades
1274.1.1 = {
	add_core = FEZ
}
1471.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
}
1530.1.1 = {
	add_core = MOR
}
1530.1.2 = {
	remove_core = FEZ
}
1580.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	rename_capital = "Tanjer" 
	change_province_name = "Tanjer"
}
1600.1.1 = {
	
}
1640.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
}
1661.1.1 = {
	owner = ENG
	controller = ENG
	add_core = ENG
	remove_core = POR
} # Tangier is given to Charles II of England

1673.1.1 = {
	fort_14th = yes
}
1679.1.1 = {
	unrest = 4
} # Moulay Ismail attempted to seize the town
1684.1.1 = {
	owner = MOR
	controller = MOR
	remove_core = ENG
	unrest = 0
	
	rename_capital = "Tanja" 
	change_province_name = "Tanja"
} # Turned into a pirates nest, returned to Moroccan control, British retreat
1884.1.1 = {
	owner = SPA
	controller = SPA
	rename_capital = "Tanjer" 
	change_province_name = "Tanjer"
}
