# 2805 - Gyeongju

owner = KOR
controller = KOR
add_core = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Gyerim"
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = steppestech
hre = no

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1200.1.1 = { road_network = yes }
1356.1.1  = {
	revolt = {
		type = wokou_pirates
		size = 1
	}
	controller = REB
	unrest = 3
	set_province_flag = korea_wokou_rebels
} # Wokou pirates effectively control south Korea
1360.1.1  = {
	revolt = { }
	controller = KOR
	unrest = 0
	clr_province_flag = korea_wokou_rebels
} # Pirates chased
1392.1.1  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1592.4.24 = {
	controller = ODA
} # Japanese invasion
1593.5.18 = {
	controller = JOS
	add_core = ODA
} # The Japanese still retained a small foothold after their first invasion
1597.1.1  = {
	controller = ODA
}
1597.11.1 = {
	controller = JOS
	remove_core = ODA
}
1637.1.1  = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1  = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1  = {
	discovered_by = NED
} # Hendrick Hamel
1680.1.1  = {
	
} # Center of herbal trade in Joseon
1760.1.1  = {
	
}
