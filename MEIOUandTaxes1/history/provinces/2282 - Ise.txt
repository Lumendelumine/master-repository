# 2282 - Ise
# GG/LS - Japanese Civil War

owner = TKI
controller = TKI
culture = kansai
religion = mahayana #shinbutsu
capital = "Suzuka"
trade_goods = linen
hre = no
is_city = yes # citysize = 1500
base_tax = 10
base_production = 10
base_manpower = 6


discovered_by = chinese

1356.1.1 = {
	add_core = TKI
}
1525.1.1 = {
	owner = KTK
	controller = KTK
}
1542.1.1 = { discovered_by = POR }
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1583.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
