# 2437 - Tara

owner = BLU
controller = BLU
culture = bashkir
religion = sunni 
capital = "Ufa"

base_tax = 4
base_production = 4
#base_manpower = 1.0
base_manpower = 2.0
trade_goods = wool
citysize = 3847



discovered_by = steppestech
discovered_by = muslim

hre = no

1356.1.1 = {
	unrest = 3
	add_core = BLU
	add_core = KAZ
	add_core = SIB
	add_core = WHI
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
	remove_core = WHI
	unrest = 0
}
1438.1.1  = {
	owner = KAZ
	controller = KAZ
}
1444.1.1 = {
	remove_core = GOL
	remove_core = WHI
	remove_core = BLU
}

1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1552.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
}
1600.1.1   = {
		culture = russian
	religion = orthodox
	remove_core = KAZ
}
