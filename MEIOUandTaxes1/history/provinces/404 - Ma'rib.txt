#404 - Najran

owner = YEM
controller = YEM
culture = yemeni
religion = sunni
capital = "Najran"
trade_goods = coffee
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
discovered_by = ADA
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = NAJ
}
1524.1.1 = { discovered_by = POR }
1530.1.1 = { owner = HAD controller = HAD add_core = HAD }
1567.1.1 = { unrest = 4 } # Revolt against the Ottomans
1570.1.1 = { unrest = 0 }
1597.9.1 = { unrest = 5 } # Qasimi state, revolt against the Ottomans
1602.1.1 = { unrest = 0 }
1636.1.1 = {
	owner = YEM
	controller = YEM
	remove_core = TUR
}
