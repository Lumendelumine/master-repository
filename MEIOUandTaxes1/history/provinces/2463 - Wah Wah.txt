# 2463 - Kangding

owner = MDO
controller = MDO
culture = tibetan
religion = vajrayana
capital = "Dartsedo" # Kangding in Chinese
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1356.1.1  = {
	add_core = MDO
}
1368.1.1  = {
	add_core = MNG
}
1662.1.1 = {
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
1776.1.1  = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
