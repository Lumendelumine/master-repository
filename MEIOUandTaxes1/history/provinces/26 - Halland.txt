# Halland
# MEIOU - Gigau

owner = DEN
controller = DEN
culture = danish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = lumber
base_manpower = 2
is_city = yes
capital = "Halmstathe"
discovered_by = western
discovered_by = eastern
discovered_by = muslim

#1088.1.1 = { dock = yes shipyard = yes }
1356.1.1   = {
	owner = SWE
	controller = SWE
	add_core = SWE
	add_core = DEN
}
1360.1.1   = {
	owner = DEN
	controller = DEN
	remove_core = RSW
}
1434.1.1   = { controller = REB }
1436.1.1   = { controller = DEN }
1500.1.1 = { road_network = yes }
1512.1.1   = { fort_14th = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1522.2.15 = { dock = yes shipyard = yes }
1523.6.21  = {
	base_tax = 3
	base_production = 3
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1529.12.17 = { merchant_guild = yes }
1534.8.15  = {
	controller = REB
} #'Grevefejden'(Christofer of Oldenburg)
1534.11.15 = {
	controller = DAN
} #Liberated by Sweden
1536.1.1   = { religion = protestant} #Unknown date
1565.9.15  = {
	controller = SWE
} #The Nordic Seven-year War
1569.12.4  = {
	controller = DAN
} #The Nordic Seven-year War
1590.1.1   = {
	fort_14th = no fort_15th = yes
	
} # Base for spying against Sweden
1645.8.13  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = DAN
	
	
} #Controls large parts of exports to the West
1660.1.1   = { fort_15th = no fort_16th = yes }
1712.1.1   = { fort_16th = no fort_17th = yes }
1722.1.1  = { culture = swedish } #linguicide mostly accomplished 
 #Due to the support of manufactories
