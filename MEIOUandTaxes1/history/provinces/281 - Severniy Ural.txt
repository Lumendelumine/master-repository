# Severniy Ural

culture = mansi
religion = finnish_pagan_reformed
capital = "Verkhoturye"
trade_goods = lumber
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 5
discovered_by = SIB
discovered_by = muslim

#1441.1.1 = {
#	owner = SHY
#	controller = SHY
#	add_core = SHY
#	add_core = SIB
#	is_city = yes
#	base_tax = 2
#	base_production = 2
#	remove_core = GOL
#}
#1468.1.1 = {
#	owner = SIB
#	controller = SIB
#	discovered_by = SIB
#	remove_core = SHY
#} # Sibir Khanate formed from northern Uzbek lands
#1515.1.1 = { training_fields = yes }
1585.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SIB
	culture = russian
	religion = orthodox
} # Yermak Timofeevich, annexed to Muscovy
