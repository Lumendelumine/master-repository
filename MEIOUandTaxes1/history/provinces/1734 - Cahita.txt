# 1734 - Cahita
# GG - 22/07/2008

culture = mayo
religion = nahuatl
capital = "Cahita"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 10
native_ferocity = 5
native_hostileness = 6
discovered_by = mesoamerican

1533.1.1 = {
	discovered_by = SPA
}
1641.1.1   = {
	owner = SPA
	controller = SPA
	capital = "El presidio de Pitic"
	citysize = 200
	culture = castillian
	religion = catholic
	set_province_flag = trade_good_set
	trade_goods = maize
	}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
