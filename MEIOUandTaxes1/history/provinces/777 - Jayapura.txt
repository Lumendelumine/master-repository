# 777 - Jayapura

culture = papuan
religion = polynesian_religion
capital = "Jayapura"
trade_goods = crops #fish
hre = no
native_size = 50
native_ferocity = 4
native_hostileness = 9
discovered_by = chinese

1000.1.1   = {
	set_province_flag = papuan_natives
}
1545.1.1 = { discovered_by = SPA }
