# 3532 - Cuauhtitlan

owner = TEP
controller = TEP
add_core = TEP
culture = tepanec
religion = nahuatl
capital = "Cuauhtitlan"


base_tax = 6
base_production = 6
#base_manpower = 2.0
base_manpower = 4.0
citysize = 25000
trade_goods = gold  

hre = no
discovered_by = mesoamerican


1473.1.1   = {
	owner = AZT
	controller = AZT
	citysize = 10000
	base_tax = 6
base_production = 6
#	base_manpower = 1.0
	base_manpower = 2.0
	trade_goods = maize
} # Conquest by Axayacati, sixth tlatoani of Tenochtitlan
1498.1.1   = {
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	citysize = 5000
	add_core = SPA
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 15000
}
1650.1.1   = {
	citysize = 25000
}
1700.1.1   = {
	citysize = 40000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 60000
}
1800.1.1   = {
	citysize = 100000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
