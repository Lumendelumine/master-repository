# No previous file for Baawitigong

culture = ojibwa
religion = totemism
capital = "Baawitigong"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1611.1.1  = {	discovered_by = FRA
		discovered_by = ENG
	    } # �tienne Brul�, Henry Hudson
1668.1.1  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		capital = "Sault Ste. Marie"
		citysize = 100
	    }
1693.1.1  = { add_core = FRA }
1707.5.12 = { discovered_by = GBR }
1763.2.10 = {
		owner = GBR
		controller = GBR
		remove_core = FRA
		citysize = 1200
		religion = protestant
		culture = english
		trade_goods = fur
	    } # Treaty of Paris
1788.2.10 = { add_core = GBR }
1800.1.1  = { citysize = 2590 }
