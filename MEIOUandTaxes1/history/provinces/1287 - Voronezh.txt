# 1287 - Voronezh
# MEIOU-GG - Turko-Mongol mod

owner = BLU
controller = BLU     
culture = crimean
religion = sunni
hre = no
base_tax = 4
base_production = 4
trade_goods = livestock 
base_manpower = 3
is_city = yes
capital = "Voronezh"
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1  = {
	unrest = 3
	add_core = BLU
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
	unrest = 0
}
1502.6.1   = {
	owner = CRI
	controller = CRI
	remove_core = GOL
	add_core = CRI
}
1515.1.1 = { training_fields = yes }
1571.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Conquest of the Khanante by Ivan Grozny
1585.1.1  = { fort_14th = yes } # Founded by Feodor I
1598.1.1  = { unrest = 5 } # "Time of troubles"
1600.1.1  = { religion = orthodox }
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1648.1.1  = { revolt = { type = anti_tax_rebels size = 2 } controller = REB } # Antifeudal insurrection
#1648.1.1  = { culture = ukrainian add_core = UKR }
1649.1.1  = { revolt = {} controller = RUS }
1670.1.1  = { unrest = 8 } # Stepan Razin
1671.1.1  = {
	unrest = 0
	
}
1773.1.1  = { unrest = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { unrest = 0 } # Pugachev is captured
