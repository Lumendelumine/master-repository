# 573 - Kotte

owner = KTH
controller = KTH
culture = sinhala
religion = buddhism
capital = "Kalanpu"
trade_goods = tea
hre = no
base_tax = 8
base_production = 8
base_manpower = 5
is_city = yes
add_core = KTH
discovered_by = muslim
discovered_by = indian

1250.1.1 = { temple = yes }
1505.1.1 = { discovered_by = POR } # Francisco de Almeida
1517.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	capital = "Colombo"
}
1542.1.1 = { add_core = POR fort_16th = yes }
1660.1.1 = {
	owner = NED
	controller = NED
	remove_core = POR
  	capital = "Kandy"
} # Dutch control
1685.1.1  = { 
	add_core = NED
	fort_16th = no
	fort_17th = yes
}
1799.8.1  = { controller = GBR } # Occupied by England
1802.3.25 = {
	owner = GBR
	add_core = GBR
	remove_core = NED
} # Treaty of Amiens
