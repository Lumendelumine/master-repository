# No previous file for Zaghawa

native_size = 25
native_ferocity = 2
native_hostileness = 5
culture = foora
religion = animism
capital = "Kamoi"
base_manpower = 1
trade_goods = crops # salt
hre = no
discovered_by = ALW
discovered_by = YAO
discovered_by = ETH
discovered_by = ADA
discovered_by = DAR
discovered_by = KIT
discovered_by = MKU
discovered_by = DSL
discovered_by = KDF
discovered_by = SLL

1500.1.1 = {
	owner = DAR
	controller = DAR
	add_core = DAR
	discovered_by = SEN
	discovered_by = ETH
	is_city = yes
	trade_goods = wool
}
