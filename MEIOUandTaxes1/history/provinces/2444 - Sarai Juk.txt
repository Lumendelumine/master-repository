# 2444 - Baybaktu

owner = BLU
controller = BLU
culture = tartar
religion = sunni
capital = "Yaitsk"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = NOG
	add_core = BLU
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
	unrest = 0
}
1441.1.1 = {
	owner = NOG
	controller = NOG
	add_core = NOG
	base_tax = 9
base_production = 9
	#base_manpower = 2.5
	base_manpower = 5.0
	set_province_flag = horde_capital
}
1444.1.1 = {
	remove_core = GOL
	remove_core = WHI
	remove_core = BLU
}
1515.1.1 = { training_fields = yes }
#1520.1.1  = {
#	owner = KZH
#	controller = KZH
#} # Qasim Khan conquers Nogai lands
1613.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
    	remove_core = NOG
} # The break up of the Nogai tribe
