# 1244 - Otaheite

owner = TXX
controller = TXX
culture = polynesian
religion = polynesian_religion
capital = "Tahiti"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes

1356.1.1   = {
	add_core = TXX
}
1606.1.1   = {
	discovered_by = SPA
}
1769.4.13  = {
	discovered_by = GBR
}	# Cooks 1st voyage
