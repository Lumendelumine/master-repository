# 336 - Atlas

owner = FEZ
controller = FEZ 
culture = fassi
religion = sunni
capital = "Taourirt"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = wool
discovered_by = TUA
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

1356.1.1 = {
	add_core = TFL
	owner = TLE
	controller = TLE
	add_core = TLE
}
1530.1.1 = {
	owner = TFL
	controller = TFL
	add_core = MOR
	remove_core = TLE
}
1549.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1664.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
	remove_core = MOR
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	remove_core = TFL
}
