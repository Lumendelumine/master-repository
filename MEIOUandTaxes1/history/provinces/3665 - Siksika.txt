# No previous file for Siksika

culture = blackfoot
religion = totemism
capital = "Siksiksa"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 3
native_hostileness = 4

1792.1.1 = { 	discovered_by = GBR
		owner = GBR
		controller = GBR
		citysize = 500
		culture = english
		religion = protestant 
		trade_goods = fur } #Fort Dunvegan
1817.1.1 = { add_core = GBR }
