# 1331 - ayn Al Qusaymah

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = " Bi'r Al-Saba"
trade_goods = livestock
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes

discovered_by = CIR
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
discovered_by = indian

1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1516.1.1   = { add_core = TUR }
1516.11.8  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
#1831.1.1 = {
#	controller = EGY
#}
#1833.6.1 = {
#	owner = EGY
#}
