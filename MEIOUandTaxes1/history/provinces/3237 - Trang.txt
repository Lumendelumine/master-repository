# No previous file for Trang

owner = KED
controller = KED
add_core = MLC
add_core = KED
culture = malayan
religion = buddhism
capital = "Setol"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = austranesian
hre = no

1250.1.1 = { temple = yes }
1500.1.1 = { religion = sunni }
1516.1.1 = { discovered_by = POR } # Godinho de Eredia
1660.1.1 = { fort_14th = yes }
1821.1.1 = {
	owner = SIA
	controller = SIA
	culture = thai
	religion = buddhism
	rename_capital = "Satun" 
	change_province_name = "Satun"
} # Until 1909
