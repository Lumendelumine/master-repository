# 4059 - Sifilke

owner = KNI
controller = KNI
culture = cilician
religion = coptic
capital = "Sifilke"
trade_goods = olive
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = KNI
	add_core = KAR
}
1360.1.1 = {
	controller = KAR
	owner = KAR
	set_province_flag = turkish_name
}
#1453.5.29 = {
#	remove_core = BYZ
#}
1471.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1623.1.1 = {
	controller = REB
} # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1 = {
	controller = TUR
} # Murad tries to quell the corruption
1656.1.1 = {
	culture = turkish
	religion = sunni
}
1658.1.1 = {
	controller = REB
} # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1 = {
	controller = TUR
}
1699.1.1 = {
	controller = REB
} # The Shuff Mountain rebellion 
1700.1.1 = {
	controller = TUR
}
