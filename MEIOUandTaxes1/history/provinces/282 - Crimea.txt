# 285 - Crimea
# MEIOU-GG - Turko-Mongol mod

owner = BLU
controller = BLU
culture = crimean
religion = sunni
capital = "Salaciq"
base_tax = 8
base_production = 8
base_manpower = 7
is_city = yes
trade_goods = salt

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1111.1.1 = { post_system = yes }
1200.1.1 = { marketplace = yes }
1356.1.1 = {
	unrest = 3
	add_core = BLU
	add_core = CRI
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
	unrest = 0
}
1441.1.1 = {
	owner = CRI
	controller = CRI
}
1444.1.1 = {
	remove_core = GOL
	remove_core = WHI
	remove_core = BLU
}
#1475.1.1 = {
#	add_core = TUR
#}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1532.1.1 = {
	capital = "Bakhchisaray"
}
1774.7.21 = {
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
} # Annexed by Catherine II
