# 2986 - Inhassoro

culture = kimwani
religion = sunni
capital = "Inhassoro"
hre = no
native_size = 15
native_ferocity = 6
native_hostileness = 6
trade_goods = crops

1498.1.6   = {
	discovered_by = POR
} #Vasco Da Gama
1550.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
		trade_goods = slaves
}
