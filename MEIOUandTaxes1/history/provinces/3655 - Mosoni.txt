# No previous file for Mosoni

culture = cree
religion = totemism
capital = "Mosoni"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1611.1.1  = { discovered_by = ENG } # Henry Hudson
1672.1.1  = { discovered_by = FRA } # Charles Albanel
1679.1.1  = {	owner = ENG
		controller = ENG
		culture = english
		religion = protestant
		citysize = 100
	    } # Construction of Fort Albany
1700.1.1  = { citysize = 550 trade_goods = fur }
1704.1.1  = { add_core = ENG }
1707.5.12  = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1  = { citysize = 1220 }
1800.1.1  = { citysize = 1850 }
