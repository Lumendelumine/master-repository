# 2851 - Bunifaziu

owner = GEN
controller = GEN
culture = corsican
religion = catholic
capital = "Aiacciu" # Ajaccio
hre = no
base_tax = 3
base_production = 3
trade_goods = livestock
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1088.1.1 = { dock = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = GEN
	add_core = COR
	add_claim = PIS
	add_claim = ARA
}
1399.1.1   = {
	owner = MLO
	controller = MLO
	add_core = MLO
}
1405.1.1   = {
	owner = GEN #Pisa never returned to the island
	revolt = { type = nationalist_rebels size = 1 }
	controller = REB
	remove_core = PIS
}
1406.10.9  = {
	revolt = {}
	controller = GEN
}
1494.1.1   = {
	controller = GEN
	owner = GEN
}  # Pisan Republic
1499.1.1   = {
	owner = GEN
	controller = GEN
	remove_core = MLO
}
1501.1.1   = { remove_core = ARA }
1553.1.1   = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Occupied by the French
1559.1.1   = {
	owner = GEN
	controller = GEN
	remove_core = FRA
} # Treaty of Cateau-Cambrésis
1618.1.1  =  { hre = no }

1729.1.1   = { unrest = 7 } # Revolt against the Genoese
1732.5.1   = { unrest = 3 } # Charles VI sends military assistance & the rebels surrender
1733.11.17 = { unrest = 5 } # Mistrust in the Genoese administration resulted in another Corsican revolt
1735.1.1   = { unrest = 0 controller = REB } # The Corsican assembly declared Corsica independent
1736.4.15  = { owner = COR controller = COR }
1743.1.1   = { owner = GEN controller = GEN } # The Corsicans laid down their arms
1745.1.1   = { controller = REB } # Third Corsican revolt, declared themselves independent in 1746
1753.10.1  = { owner = COR controller = COR }
1755.7.15  = { controller = REB } # Fourth Corsican revolt lead by Pascal Paoli
1768.5.15  = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = GEN
} # Ceded to France, since the Republic of Genoa is unable to restore control
1769.6.13  = { controller = FRA } # France managed to overcome the resistance & Paoli flees
