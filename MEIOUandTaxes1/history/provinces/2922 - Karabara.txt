# 2922 - Karabara

owner = MAL
controller = MAL
culture = tuareg
religion = sunni
capital = "Karabara"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = wool
discovered_by = soudantech
discovered_by = muslim
hre = no

1356.1.1   = {
	add_core = MAL
	add_core = TBK
}
1433.1.1  = {
	owner = TBK
	controller = TBK
}
1438.1.1   = {
	owner = SON
	controller = SON
	add_core = SON
}
1591.4.12  = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SON
} # Battle of Tondibi
1612.1.1   = {
	owner = TBK
	controller = TBK
	remove_core = MOR
} # Moroccans establish the Arma as new independent ruling class
