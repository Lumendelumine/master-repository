# 2941 - Taounat

owner = FEZ
controller = FEZ
culture = rifain
religion = sunni
capital = "Al Hoceima"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = wool
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

1088.1.1 = { dock = yes shipyard = yes }
1356.1.1 = {
	add_core = FEZ
}
1530.1.1 = {
	add_core = MOR
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1664.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
	remove_core = MOR
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	remove_core = TFL
}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
