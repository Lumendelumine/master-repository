# 2649 - Qasr Ibrim

owner = MKU
controller = MKU 
culture = nubian
religion = sunni
capital = "Qasr Ibrim"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = wheat
discovered_by = ALW
discovered_by = MKU
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
hre = no

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = MKU
}
# 1453.1.1 = { controller = REB } # Under control of Awlad Kenz
1510.1.1 = {
	discovered_by = SEN
} # Funj replace Mamluks in control of Suakin
1516.1.1 = {
	add_core = TUR
} # Mamluks fall to Ottomans, Ottomans do not advance up Nile
1524.1.1 = {
	owner = MKU
	controller = MKU
	add_core = MKU
	#add_core = MAM
	remove_core = TUR
}
1540.1.1 = {
	owner = MAM
	controller = MAM
	capital = "Al Dirr"
} #Ottomans occupy Lower Nubia
1802.5.13  = {
	unrest = 8
} # Turkish rule is restored but a few troublesome years follow
1805.1.1 = {
	unrest = 0
	owner = EGY
	controller = EGY
}
1811.6.1   = {
	owner = TUR
	controller = TUR
} # Order is restored
