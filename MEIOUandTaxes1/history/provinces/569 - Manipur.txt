#Province: Manipur
#file name: 569 - Manipur
#MEIOU-FB India 1337+ mod Aug 08

owner = MLB
controller = MLB
culture = meitei
religion = adi_dharam
capital = "Imphal"
trade_goods = rice
hre = no
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = chinese
discovered_by = steppestech

1356.1.1 = { add_core = MLB }
