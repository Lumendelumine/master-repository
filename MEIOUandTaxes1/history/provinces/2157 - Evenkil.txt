# 2157 - Sakhsara

culture = evenki
religion = shamanism
capital = "Sakhsary"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 1

1496.1.1 = {
	discovered_by = SAK
	owner = SAK
	controller = SAK
	add_core = SAK
	is_city = yes
	trade_goods = fur
	culture = yakut
}
1650.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	capital = "Yakutsk"
   	religion = orthodox
   	culture = russian
}
