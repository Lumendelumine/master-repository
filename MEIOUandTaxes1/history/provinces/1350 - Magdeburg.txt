# 1353 - Magdeburg

owner = MAG
controller = MAG
culture = low_saxon
religion = catholic
capital = "Magdeburg"
trade_goods = wheat
hre = yes
base_tax = 10
base_production = 10
base_manpower = 3
is_city = yes
add_core = MAG
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1500.1.1 = { road_network = yes }
1520.1.1 = { temple = yes }
1524.1.1  = { religion = protestant  }
1546.1.1  = { fort_14th = yes }
1648.10.24 = {
	add_core = BRA
} # Peace of Westphalia
1660.1.1  = {  }
1680.6.4   = {
	owner = BRA
	controller = BRA
} # Death of the current administrator, August of Saxe-Weissenfels, as the semi-autonomous Duchy of Magdeburg
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} # Congress of Vienna
