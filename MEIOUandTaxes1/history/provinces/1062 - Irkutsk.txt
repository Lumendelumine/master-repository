# 1062 - Irkutsk

owner = BRT
controller = BRT
culture = buryat
religion = shamanism
capital = "Irkutsk"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3
is_city = yes
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1356.1.1 = {
	add_core = BRT
}
1632.1.1  = { discovered_by = RUS }
1632.9.25 = {
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
	is_city = yes
		trade_goods = fur
	set_province_flag = trade_good_set
}
1657.1.1 = {
	add_core = RUS
}
