# No previous file for Panawahpskek

culture = abenaki
religion = totemism
capital = "Panawahpskek"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 30
native_ferocity = 3
native_hostileness = 4


1497.6.24 = {
	discovered_by = ENG
} # John Cabbot
1604.1.1   = {
	discovered_by = FRA
	owner = FRA
	controller = FRA		
	citysize = 100
	capital = "�le Sainte-Croix"
	religion = catholic
	culture = francien
	trade_goods = fur
	set_province_flag = trade_good_set
}
1622.8.10  = {
	owner = ENG
	controller = ENG
	capital = "Augusta"
	citysize = 500
	religion = protestant #anglican
	culture = english
} # 1622 patent letter
1632.3.29 = {
	is_city = yes
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
} # Treaty of St. Germain-en-Layne, handed back to the French
1654.1.1  = {
	owner = ENG
	controller = ENG
	culture = english
	religion = protestant
} #Captured by New England settlers (again)
1670.1.1 = {
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
} # Treaty, handed back to the French (again, again)
1670.1.1 = { citysize = 1000 } #Capital of French Acadia
1674.1.1  = {
	owner = NED
	controller = NED
	culture = dutch
	religion = reformed
} #To the Dutch
1676.1.1 = {
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
} # Back to the French for the third time.
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
    	remove_core = ENG
}
1750.1.1   = {
	add_core = USA
	culture = american
}
1775.4.19  = {
	spawn_rebels = { type = nationalist_rebels size = 3 win = yes friend = USA }
	unrest = 6
} # Battles of Lexington and Concord
1776.7.4  = {
	revolt = { }
	unrest = 0
	owner = USA
	controller = USA
	religion = protestant
} # Declaration of Independance
1782.9.2   = {
	remove_core = GBR 
} # Acknowledgement of "Thirteen United States", not the Colonies
