# 3535 - Tziuhcoac

owner = TEE
controller = TEE
add_core = TEE
culture = huastec
religion = nahuatl
capital = "Tziuhcoac" 

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 6000
trade_goods = maize 


hre = no

discovered_by = mesoamerican

1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	# capital = "El Taj�n"
	citysize = 2000
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
