# No previous file for Joara

culture = mingwe
religion = totemism
capital = "Joara"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1650.1.1  = {  trade_goods = fur } #Full extent of the Susquehannock & close relatives at start of the Beaver Wars
1671.1.1  = { discovered_by = ENG } # Abraham Wood
1672.1.1  = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1707.5.12 = { discovered_by = GBR }
1750.1.1  = {	owner = GBR
		controller = GBR
		culture = english
		religion = protestant
		citysize = 200 
	    } # British settlers cross the Appalachians
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = { unrest = 0 } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1  = { citysize = 1940 }
