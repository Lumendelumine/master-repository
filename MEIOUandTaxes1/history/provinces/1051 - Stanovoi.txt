#1051 - Olyokma

culture = evenki
religion = shamanism
capital = "Olyokma"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 3

1461.1.1 = {
	discovered_by = SAK
	owner = SAK
	controller = SAK
	add_core = SAK
	is_city = yes
	trade_goods = fur
	culture = yakut
}
1496.1.1 = {
	citysize = 0
	native_size = 50
	native_ferocity = 4.5
	native_hostileness = 9 
	owner = XXX
	controller = XXX
	remove_core = SAK
	culture = evenki
	trade_goods = crops
	base_tax = 1
	base_production = 1
	base_manpower = 1
}
1632.1.1 = { discovered_by = RUS }
1643.1.1 = {
	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
	base_tax = 2
	base_production = 2
	trade_goods = fur
	capital = "Olyokminsk"
}
1655.1.1 = { unrest = 7 } # Yakutsk rebellion
1660.1.1 = { unrest = 0 }
1668.1.1 = {
	add_core = RUS
}
1684.1.1 = { unrest = 4 } # Yakut rebellion
1686.1.1 = { unrest = 0 }
