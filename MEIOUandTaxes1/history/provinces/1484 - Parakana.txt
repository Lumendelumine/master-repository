# 1484 - Jari

culture = tupinamba
religion = pantheism
capital = "Jari"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 6

1661.6.22 = {
	discovered_by = POR
	owner = POR
	controller = POR
	add_core = POR
	culture = portugese
	religion = catholic
	citysize = 500
	trade_goods = cacao
}

1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
