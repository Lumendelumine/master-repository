# 3508 - Tegucigalpa

culture = misumalpan
religion = mesoamerican_religion
capital = "Tegucigalpa"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 4
native_hostileness = 8
discovered_by = mesoamerican

1517.1.1   = {
	discovered_by = SPA
}
1524.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 200
	culture = castillian
	religion = catholic
} # Francisco Hernandez de Cordoba 
1549.1.1   = {
	add_core = SPA
	citysize = 500
}
1600.1.1   = {
	citysize = 1000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1823.7.10  = {
	owner = CAM
	controller = CAM
	remove_core = MEX
}
