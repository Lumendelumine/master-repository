# No previous file for Kanza

culture = kanza
religion = totemism
capital = "Kanza"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 4

1541.1.1 = { discovered_by = SPA } # Francisco V�squez de Coronado
