# 1366 - W�rzburg

owner = WBG
controller = WBG
add_core = WBG
culture = eastfranconian
religion = catholic
base_tax = 7
base_production = 7
trade_goods = wine
base_manpower = 2
is_city = yes
fort_14th = yes
capital = "W�rzburg"
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1647.1.1   = { early_modern_university = yes } # (existed until 1803)
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1814.4.11  = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = WBG
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
