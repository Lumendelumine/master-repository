# No previous file for Ratchaburi

owner = AYU
controller = AYU
add_core = AYU
culture = monic
religion = buddhism
capital = "Ratchaburi"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1500.1.1 = { culture = thai }
1535.1.1 = { discovered_by = POR }
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
