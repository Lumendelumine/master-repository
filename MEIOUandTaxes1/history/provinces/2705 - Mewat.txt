#  - Mewat

owner = ALR
controller = ALR
culture = meo
religion = sunni #converted in the 13th century
capital = "Nuh" #don't need two Alwars
trade_goods = iron
hre = no
base_tax = 4
base_production = 4
#base_manpower = 1.5
base_manpower = 3.0
citysize = 28000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1100.1.1 = { marketplace = yes }
1115.1.1 = { bailiff = yes }
1120.1.1 = { weapons = yes }
1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = ALR
}
1421.1.1  = {
	#owner = MAW
	controller = MAW
}
1428.1.1  = {
	#owner = DLH
	controller = ALR
	#add_core = DLH
}
#1444.1.1 = {
#	add_core = PUN
#}
#1451.4.20 = {
#	remove_core = PUN
#}
1450.1.1  = { citysize = 31000 }
1500.1.1  = { citysize = 28000 }
1526.4.21 = {
	owner = ALR
	controller = ALR
} # Panipat
#1530.1.1 = {
#	owner = MUG
#	add_core = MUG
#	controller = MUG
#	revolt = { type = nationalist_rebels size = 3 }
#} # Mughal Conquest
#1540.1.1 = {
#	owner = BNG
#	controller = BNG
#	add_core = BNG
#} # Delhi falls to Sher Shah Suri
1550.1.1 = { citysize = 33000 }
1553.1.1 = {
	owner = DLH
	controller = DLH
} # Split of Suri Empire
1555.7.23 = {
	owner = MUG
	controller = MUG
	remove_core = BNG
} # Humayun Returns
1556.10.7 = { controller = DLH }	# Hemu
1556.11.5 = {
	controller = MUG
}	# Panipat 2
1600.1.1 = { citysize = 36000 }
1650.1.1 = { citysize = 38000 }
1660.1.1 = { remove_core = ALR }
1690.1.1  = { discovered_by = ENG }
1700.1.1 = { citysize = 46000 }
1707.3.1 = {
	controller = REB
	revolt = { type = pretender_rebels size = 5 leader = "Muhammad Azam Shah" }
} #Pretender
1707.5.12 = { discovered_by = GBR }
1707.6.13 = {
	controller = MUG
	revolt = {  }
} #Pretender beaten
1714.1.1 = {
	owner = ALR
	controller = ALR
	remove_core = MUG
	capital = "Deeg"
} # Jagir granted to the Jats
1733.1.1 = {
	capital = "Bharatpur"
}
1750.1.1 = { citysize = 62000 }
1784.1.1 = {
	controller = GWA
} # Marathas
1785.1.1 = {
	owner = GWA
	capital = "Alwar"
} # Marathas
1803.1.1 = {
	owner = ALR
	controller = ALR
	citysize = 75000
} # British dependency
	   
