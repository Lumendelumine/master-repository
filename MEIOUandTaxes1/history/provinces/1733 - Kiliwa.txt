# 1733 - Kiliwa
# GG - 22/07/2008

culture = hopi
religion = totemism
capital = "Kiliwa"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 25
native_ferocity = 2
native_hostileness = 7
discovered_by = mesoamerican
discovered_by = north_american

1533.1.1 = {
	discovered_by = SPA
}
1697.1.1   = {
	owner = SPA
	controller = SPA
	capital = "Misi�n el Sant�simo Rosario"
	citysize = 100
	culture = mexican
	religion = catholic
	set_province_flag = trade_good_set
	trade_goods = fur
} # Dominican friar Vicente Mora and Francisco Ga
1750.1.1   = {
	add_core = SPA
	citysize = 500
	add_core = MEX
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
