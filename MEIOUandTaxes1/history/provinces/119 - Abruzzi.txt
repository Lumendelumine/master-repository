# 119 - Abruzzi

owner = KNP
controller = KNP
culture = neapolitan 
religion = catholic 
hre = no 
base_tax = 7
base_production = 7        
trade_goods = wine    
base_manpower = 2
is_city = yes        
fort_14th = yes 
capital = "Aquila"
estate = estate_church
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = KNP
	add_core = ANJ
}
1393.1.1  = {
	add_permanent_province_modifier = {
		name = "republic_of_senarica"
		duration = -1
	}
}
1442.1.1  = { add_core = ARA }
1495.2.22 = { controller = FRA } # Charles VIII takes Napoli
1495.7.7  = { controller = KNP } # Charles VIII leaves Italy
 
1502.1.1  = { owner = FRA controller = FRA add_core = FRA } # France and Aragon partitions Naples
1503.6.1  = { owner = ARA controller = ARA add_core = ARA remove_core = FRA remove_core = ANJ} # France forced to withdraw
1504.9.22 = { remove_core = FRA } # The Treaty of Blois in 1504
1516.1.23 = {	owner = SPA
		controller = SPA
		add_core = SPA
	    	remove_core = ARA
	    } # Unification of Spain
1710.1.1  = {  }
1714.3.7  = {	owner = HAB
		controller = HAB
		add_core = HAB
		remove_core = SPA
	    }
1734.6.2  = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3  = {
#	owner = KNP
#	controller = KNP
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
