# 2470 - Muskwa

culture = chilcotin
religion = totemism
capital = "Muskwa"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 4

discovered_by = north_american
1812.1.1   = {
	discovered_by = GBR
} # North West Company and the Hudson's Bay Company
