# 2566 - Soria

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = castillian
religion = catholic 
hre = no
base_tax = 5
base_production = 5
trade_goods = lumber
base_manpower = 6
is_city = yes
capital = "Soria"
estate = estate_nobles
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1  = { 
	add_core = ENR
}
1369.3.23  = { 
	remove_core = ENR
}
1489.1.1 = { medieval_university = yes }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11  = {
	remove_core = CAS
}
1808.6.6   = {
	controller = REB
}
1811.1.1   = {
	controller = SPA
}
1812.10.1  = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
