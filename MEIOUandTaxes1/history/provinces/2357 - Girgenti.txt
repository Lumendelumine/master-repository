# 2357 - Girgenti

owner = SIC
controller = SIC
culture = sicilian 
religion = catholic 
hre = no 
base_tax = 7
base_production = 7
trade_goods = wheat
base_manpower = 3
is_city = yes

capital = "Girgenti"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = ARA
	add_core = SIC
	add_core = KNP
}
1409.1.1  = { owner = ARA controller = ARA }
1500.1.1  = {
	
} 
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = ARA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1706.7.1  = {
	controller = SAV
}
1713.4.11 = {
	owner = SIC
	controller = SIC
	remove_core = SPA
}
1718.8.2  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1734.6.2  = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
1800.1.1  = {
	base_tax = 3
base_production = 3
}
#1815.5.3  = {
#	owner = SIC
#	controller = SIC
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
