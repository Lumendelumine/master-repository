# No previous file for Aondironon

culture = neutral
religion = totemism
capital = "Aondironon"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1615.1.1  = { discovered_by = FRA } # �tienne Br�l�
1629.1.1  = { discovered_by = ENG }
1651.1.1  = { culture = iroquois } #Taken by Iroquois in Beaver Wars.
1701.8.4  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
	    }  #Great Peace of Montreal
1726.1.1  = { add_core = FRA }
1750.1.1  = { citysize = 5900 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.1.1  = { 	culture = english
		religion = protestant 
	} #Loyalist migration after the revolution
1788.2.10 = { add_core = GBR }
1800.1.1  = { citysize = 6350 }
