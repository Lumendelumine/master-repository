# 607 - Indrapura
# LS - Indochina set up
# TM-Smiles indochina-mod for meiou

owner = CHA
controller = CHA
add_core = CHA
culture = cham
religion = hinduism
capital = "Indravapura"
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1356.1.1 = {
	fort_14th = yes
}
1471.4.1 = { owner = DAI controller = DAI add_core = DAI culture = vietnamese religion = mahayana capital = "Cu'a Han" }  # Severe defeat against the Vietnamese

1515.1.1 = { regimental_camp = yes }
1517.2.3 = { bailiff = yes }
1519.4.2 = { wharf = yes }
1522.4.4 = { marketplace = yes }
1527.1.1 = {
	owner = TOK
	controller = TOK
	add_core = TOK
	culture = vietnamese
	religion = mahayana
}
1530.1.1 = {
	owner = ANN
	controller = ANN
	add_core = ANN
	remove_core = TOK
} # Vietnamese control
1535.1.1 = { discovered_by = POR }
1558.1.1 = { owner = ANN controller = ANN add_core = ANN }
1692.1.1 = { unrest = 6 } # Rebellion against the Vietnamese
1693.1.1 = { unrest = 0 }
1769.1.1 = { unrest = 6 } # Tay Son Rebellion
1776.1.1 = {
	unrest = 0
	owner = DAI
	controller = DAI
	add_core = DAI
} # Tay Son Dynasty conquered the Nguyen Lords
1786.1.1 = { unrest = 5 } # Unsuccessful revolt
1787.1.1 = { unrest = 0 }
1802.7.22 = {
	owner = ANN
	controller = ANN
	remove_core = DAI
} # Nguyen Phuc Anh conquered the Tay Son Dynasty
1883.8.25 = { 
	owner = FRA
	controller = FRA
	capital = "Turon"
}
