#263 - sieradzkiye

owner = POL
controller = POL
add_core = POL
culture = polish
religion = catholic
capital = "Sieradz"
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
fort_14th   = yes
trade_goods = linen
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no


1490.1.1   = { unrest = 6 } # Uprising led by Mukha
1492.1.1   = { unrest = 0 }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1   = { controller = REB } # Civil war
1589.1.1   = { controller = PLC } # Coronation of Sigismund III

1596.1.1   = { unrest  = 4 } # Religious struggles, union of Brest
1597.1.1   = { unrest = 0 }
1733.1.1   = { controller = REB } # The war of Polish succession
1735.1.1   = { controller = PLC }
1793.1.23 = {
	controller = PRU
	owner = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Second partition
1807.7.9   = {
	owner = POL
	controller = POL
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = { controller = RUS }
1814.4.11  = { controller = POL }
1815.6.9   = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
