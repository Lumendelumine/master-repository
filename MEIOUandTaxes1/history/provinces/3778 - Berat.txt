# No previous file for Berat

owner = ALB
controller = ALB
culture = albanian
religion = orthodox
capital = "Berat"
trade_goods = wheat
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = ALB
	add_core = ALC
	add_core = SER
	add_core = BYZ
}
#1368.1.1  = {
#	revolt = { }
#	controller = ALC
#	unrest = 0
#} # Pirates chased
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1444.1.1 = {
	add_core = TUR
}
1444.1.1 = {
	remove_core = SER
}
1444.3.2  = {
#	owner = ALB
#	controller = ALB
#	add_core = ALB
	remove_core = ALC
} # Founding of the League of Lezh�
1479.4.25 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1515.2.1 = { training_fields = yes }
1520.1.1  = { religion = sunni } # Predominant religion under Turkish rule
1523.8.16 = { mill = yes }
1555.1.1  = { unrest = 5 } # General discontent against the Janissaries' dominance
1556.1.1  = { unrest = 0  }
1687.1.1  = { unrest = 6 } # Christian counteroffensive against the Ottomans
1690.1.1  = { unrest = 0 }
1788.1.1  = { fort_14th = yes } # Ali Pasha made it a stronghold
1797.1.1  = { controller = REB } # Suliot uprising
