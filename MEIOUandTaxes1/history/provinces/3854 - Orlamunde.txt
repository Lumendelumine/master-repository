# No previous file for Orlam�nde

owner = THU
controller = THU
culture = high_saxon
religion = catholic
capital = "Orlam�nde"
trade_goods = wheat
hre = yes
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
add_core = THU
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1423.1.6   = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = THU
} # Meissen inherited Sachsen - Wittenberg
1485.8.26  = {
	owner = SWR
	controller = SWR
	add_core = SWR
	remove_core = SAX
}
1500.1.1 = { road_network = yes }
1530.1.1   = { religion = protestant }
1547.5.19   = { 
	owner = THU
	controller = THU 
	add_core = THU
}

1572.11.6   = { 
	owner = SWR 
	controller = SWR 
	add_core = SWR 
	remove_core = THU
}

1660.1.1   = {  }
1790.8.1   = { unrest = 5 } # Peasant revolt
1791.1.1   = { unrest = 0 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
