# 2894 - Kindia

culture = dyola
religion = west_african_pagan_reformed
capital = "Kindia"
trade_goods = crops
native_size = 7
native_ferocity = 6
native_hostileness = 7
discovered_by = soudantech
hre = no

1530.1.1 = {
	owner = MNE
	controller = MNE
	add_core = MNE
	is_city = yes
	trade_goods = palm
	}
