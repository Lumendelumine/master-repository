# 1574 - Zeila

owner = ADA
controller = ADA
add_core = ADA
culture = somali # issa
religion = sunni
capital = "Zayla"
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
trade_goods = slaves
hre = no
discovered_by = ALW
discovered_by = MKU
discovered_by = ADA
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1000.1.1 = {
		add_permanent_province_modifier = {
			name = center_of_trade_modifier
			duration = -1
		}
}
1515.2.1 = { training_fields = yes }
1517.8.15 = { discovered_by = POR unrest = 6 } # Portuguese attack Zeila, occupy it, depart
1528.1.1  = { unrest = 4 } # Portuguese attack Zeila
1529.1.1  = { unrest = 0 }
1550.1.1  = { discovered_by = TUR }
1554.1.1 = { unrest = 9 } # Invasion by Oromo causes widespread destruction
1559.1.1 = { unrest = 8 } # Invasion by Galawdewos
1562.1.1 = { unrest = 5 } # Invasion by Oromo
1567.1.1 = { unrest = 5 } # Invasion by Oromo
1568.1.1 = {
	unrest = 0
	owner = HAR
	controller = HAR
}
1577.1.1  = { owner = YEM controller = YEM }
