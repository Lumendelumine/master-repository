# 605 - Prey Nokor
# TM-Smiles indochina-mod for meiou

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Prey Nokor"	#1698 foundation of Gia Dinh near Prey Nokor, 1862 Saigon,
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1356.1.1 = { 
	fort_14th = yes
}
1535.1.1 = { discovered_by = POR }

1698.1.1 = {
	owner = ANN
	controller = ANN
	add_core = ANN
	capital = "Gia Dinh"
} # Vietnamese control
1790.1.1 = {
	fort_14th = no
	fort_18th = yes
}
1862.6.5 = { 
	owner = FRA
	controller = FRA
	capital = "Saigon"
}
