# 660 - Gyalthang

owner = DLI
controller = DLI
culture = baizu
religion = vajrayana
capital = "Tong'an"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
base_manpower = 1
citysize = 37907
add_core = DLI
discovered_by = chinese
discovered_by = steppestech
discovered_by = indian



1200.1.1 = { road_network = yes }
1253.1.1 = {
	owner = YUA
	controller = YUA
}
1274.1.1 = {
	add_core = YUA
} #creation of yunnan_area province
1350.1.1 = {
	owner = DLI		#actually never under Mong Mao kingdom
	controller = DLI
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1383.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1500.1.1 = { citysize = 38204 }
1550.1.1 = { citysize = 39985 }
1600.1.1 = { citysize = 40458 }
1650.1.1 = { citysize = 41860 }
1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
1700.1.1 = { citysize = 42900 }
1750.1.1 = { citysize = 44240 }
1800.1.1 = { citysize = 46700 }
