# 3827 - Marvdasht

owner = ISF
controller = ISF
culture = qashqai
religion = sunni
capital = "Marvdasht"
trade_goods = cotton
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1  = {
	add_core = ISF
	add_core = ORM
	add_core = MUZ
	controller = MUZ
}
1357.1.1   = {
	owner = MUZ
}
1382.5.22  = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = MUZ
}
1444.1.1 = {
	controller = ISF
	owner = ISF
	remove_core = TIM
	add_core = AKK
}
1447.1.1   = {
	controller = QAR
}
1447.3.11  = {
	owner = QAR
	add_core = QAR
	remove_core = TIM
} # Fars and surroundings to Qara Quyunlu
1458.9.1   = { unrest = 7 } # Civil war in Qara Quyunlu
1458.12.1  = { unrest = 0 }
1469.11.1  = {
	controller = AKK
}
1470.1.1   = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1490.1.1   = {
	controller = REB
	revolt = { type = noble_rebels }
} # Civil War
1494.1.1   = {
	controller = AKK
	revolt = { }
} # Civil War
1497.1.1   = {
	controller = REB
	revolt = { type = noble_rebels }
} # Civil War
1501.1.1   = {
	controller = SAM
	revolt = { }
} # Safawid Expansion
1508.1.1   = {
	owner = SAM
	religion = shiite
} # Safawid Expansion
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	remove_core = AKK
} # Safawids "form persia"
1524.1.1  = {
	controller = REB
	revolt = { type = noble_rebels }
} #Quizilbash Civil War
1530.1.1  = {
	controller = PER
	revolt = { }
}
1550.1.1  = {
	
   	
} # Golden age
1587.1.1  = {
	controller = REB
	revolt = { type = noble_rebels }
}
1587.12.11  = {
	controller = PER
	revolt = {  }
}
1710.1.1  = {  }
