# 2126 - Yinculyer

culture = aboriginal
religion = polynesian_religion
trade_goods = crops #grain
capital = "Yinculyer"
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 1
