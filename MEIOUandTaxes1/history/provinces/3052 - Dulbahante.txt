# No previous file for Dulbahante

owner = MRE
controller = MRE
add_core = MRE
culture = somali
religion = sunni
capital = "Dulbahante"
trade_goods = slaves
citysize = 1000
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech

1515.2.1 = { training_fields = yes }
1526.1.1 = { owner = ADA controller = ADA add_core = ADA } #Ahmad Gran secures control over Marehan
1550.1.1 = { discovered_by = TUR }
1555.1.1 = { owner = MRE controller = MRE } #Northern part of province no longer conrolled by ADA
