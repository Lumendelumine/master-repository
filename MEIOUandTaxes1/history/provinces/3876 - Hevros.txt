# No previous file for Hevros

owner = BYZ
controller = BYZ
culture = greek
religion = orthodox
capital = "Adrianopolis"
trade_goods = wool
hre = no
base_tax = 11
base_production = 11
base_manpower = 2
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1300.1.1 = { road_network = yes }
#1354.1.1 = { add_permanent_province_modifier = { name = hussite_war_cost duration = 10000 } } #Byzantine civil war leaves eastern Thrace devastated for a generation
1356.1.1  = {
	add_core = BYZ
	add_claim = OTT
#	add_core = BUL
	add_local_autonomy = 15
}
1361.1.1  = {
	owner = OTT
	controller = OTT
	capital = "Edirne"
	change_province_name = "Rumelia"
	add_local_autonomy = -15
} # Battle of Adrianopole
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1453.5.29 = {
	remove_core = BYZ
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0 }
 # Selimiye mosque
1615.1.1  = { base_tax = 4
base_production = 4 } #The Decentralizing Effect of the Provincial System
1621.1.1  = { unrest = 6 } # Osman II's reforms against the Janissaries
1622.5.20 = { unrest = 7 } # Osman II is murdered
1622.6.1  = { controller = TUR unrest = 0 } # Mustafa I, estimated
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1718.1.1  = { unrest = 3 base_tax = 6
base_production = 6 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1  = { unrest = 0 }
1730.1.1  = {  }
1750.1.1  = { add_core = GRE } # Great fire (1745), earthquake in 1751, city declined
1795.1.1  = { unrest = 6 } # Reforms by Sultan Selim III, tried to replace th Janissary corps
