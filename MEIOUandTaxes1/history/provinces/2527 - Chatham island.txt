# 2527 - Rapa Nui

culture = polynesian
religion = polynesian_religion
capital = "Rapa Nui"
trade_goods = crops # fish
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 10
native_ferocity = 2
native_hostileness = 9

1356.1.1  = {
	add_permanent_province_modifier = { name = remote_island duration = -1 }
}
