# 2996 - Lamu

owner = MLI
controller = MLI
culture = kiunguja
religion = sunni
capital = "Malindi"
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes
trade_goods = ivory


hre = no
discovered_by = ZAN
discovered_by = KIL
discovered_by = QLM
discovered_by = SOF
discovered_by = MLI
discovered_by = MBA
discovered_by = PTA
discovered_by = MOG
discovered_by = muslim
discovered_by = indian

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1356.1.1 = {
	add_core = MLI
}
1488.1.1 = { discovered_by = POR } #P�ro da Covilh� 
1505.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
}
1652.1.1 = {
	discovered_by = OMA
	owner = OMA
	controller = OMA
} #Omanis establish direct control on way to occupy Mombasa
1856.6.1 = {
	owner = ZAN
	controller = ZAN
   	remove_core = OMA
} # Said's will divided his dominions into two separate principalities, with Thuwaini to become the Sultan of Oman and Majid to become the first Sultan of Zanzibar.
