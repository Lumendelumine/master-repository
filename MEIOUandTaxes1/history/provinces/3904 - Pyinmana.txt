# 3904 - Pyinmana

owner = AVA
controller = AVA  
culture = burmese
religion = buddhism
capital = "Pyinmana"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = AVA
	add_core = SST
	add_core = PEG
}
1510.1.16 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = SST
	remove_core = PEG
}
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1530.1.1 = { remove_core = AVA add_core = TAU }
1574.1.1 = {
	owner = AYU
	controller = AYU
}
1575.1.1 = {
	owner = TAU
	controller = TAU
}
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
	remove_core = AVA
}
1885.1.1 = {		
	owner = GBR
	controller = GBR
}
