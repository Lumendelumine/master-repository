# 3357 - �cija + Antequera + Osuna + Estepa + Marchena + Mor�n + Carmona + Archidona + El Arahal + Estepa

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = andalucian # culture = western_andalucian
religion = catholic
hre = no
base_tax = 11
base_production = 11
trade_goods = wheat
base_manpower = 3
capital = "�cija"
is_city = yes
estate = estate_nobles
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1356.1.1   = {
	set_province_flag = spanish_name
	add_core = ENR
	add_core = CAS
	add_permanent_province_modifier = {
		name = "lordship_of_sevilla"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
}
### 1503.1.1  = {  } # The "Casa de la Contrataci�n" is established in Sevilla as the monarchy tries to control American trade through that port.
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille

1523.8.16 = { mill = yes }
1610.1.12  = {  } # Decree for the expulsion of the morisques in Andaluc�a, which is speedily and uneventfully performed
1713.4.11  = { remove_core = CAS }
1808.6.6   = { controller = REB }
1813.12.11 = { controller = SPA }
