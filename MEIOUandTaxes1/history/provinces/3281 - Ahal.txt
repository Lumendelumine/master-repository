# No previous file for Ahal

owner = SDB
controller = SDB
culture = turkmeni
religion = sunni
capital = "Tejen"
trade_goods = cotton
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = mongol_tech
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = SDB
	add_core = TIM
	add_core = KHO
}
1370.1.1   = {
	owner = TIM
	controller = TIM
}
1469.8.27 = {
	owner = KHO
	controller = KHO
	remove_core = TIM
}
1505.1.1 = {
	owner = SHY
	controller = SHY
} # Captured by the Shaybanid horde
1512.1.1  = {
	owner = PER 
	controller = PER
	add_core = PER
	remove_core = SHY
	add_core = KHI
	add_core = BUK
} # Safavids expel Uzbeks from Merv
1677.1.1 = { discovered_by = FRA }
