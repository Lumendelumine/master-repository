# No previous file for Dusamareb

owner = AJU
controller = AJU
add_core = AJU
add_core = HOB

culture = somali
religion = sunni
capital = "Dusamareb"
trade_goods = millet
native_size = 50
native_ferocity = 4.5
native_hostileness = 9 
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
add_local_autonomy = 25
is_city = yes
discovered_by = ETH
discovered_by = ADA
discovered_by = MOG
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
1200.1.1 = { road_network = yes }
1650.1.1 = { owner = HOB controller = HOB remove_core = AJU }
