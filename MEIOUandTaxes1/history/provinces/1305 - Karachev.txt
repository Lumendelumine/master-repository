# 1305 - Karachev

owner = KRC
controller = KRC        
culture = russian
religion = orthodox 
hre = no
base_tax = 5
base_production = 5
trade_goods = wheat
base_manpower = 2
capital = "Karachev"
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = KRC
}
1420.1.1  = {
	owner = LIT
	controller = LIT
	add_core = LIT
} # Estimated
1494.3.21 = {
	owner = MOS
	controller = MOS
	add_core = MOS
}
1503.1.1  = { controller = MOS }
1503.3.21 = {
	owner = MOS
	remove_core = LIT
}
1523.8.16 = { mill = yes }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1567.1.1  = { fort_14th = yes } # Defend the southern borders of Muscovy.
1610.9.27 = { controller = PLC } # Polish-Lithuanian occupation
1612.11.4 = { controller = RUS }
1670.1.1  = { unrest = 8 } # Stepan Razin
1671.1.1  = { unrest = 0 } # Razin is captured
1773.1.1  = { unrest = 5 } # Emelian Pugachev, Cossack insurrection.
1774.9.14 = { unrest = 0 } # Pugachev is captured.
1779.1.1  = { fort_14th = yes } # Almost entirely rebuilt.
 # Became a large trade center.
