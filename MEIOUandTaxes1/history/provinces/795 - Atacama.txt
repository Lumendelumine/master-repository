# 795 - Atacama

culture = muisca
religion = pantheism
capital = "Atacama"
trade_goods = crops
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 2
discovered_by = south_american

1356.1.1 = {
}
1524.1.1   = { discovered_by = SPA }	#FB was 1524
1529.1.1 = {
	owner = QUI
	controller = QUI
	add_core = QUI
	add_core = CZC
	remove_core = INC
}
1530.1.1   = {
	#owner = INC
	#controller = INC
	#add_core = INC
	is_city = yes
	trade_goods = fish
}
1532.9.1   = {
	add_core = SPA
	owner = SPA
	controller = SPA
	culture = castillian
	religion = catholic
	capital = "Esmeraldas"
}
1537.1.1  = { unrest = 8 } # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { unrest = 5 } # Almagro is defeated & executed
1541.1.1  = { unrest = 6 } # Pizzaro is assassinated by supporters of Almagro, his brother assumed control
1548.1.1  = { unrest = 0 } # Gonzalo Pizzaro is also executed & Spain succeeds in reasserting its authority
1750.1.1  = {
	add_core = COL
	culture = colombian
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
