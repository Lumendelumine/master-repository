# 2627 - Machilipatnam

owner = RED
controller = RED
culture = telegu
religion = hinduism
capital = "Machilipatnam"
trade_goods = pepper
hre = no
base_tax = 8
base_production = 8
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
}
1356.1.1  = {
	add_core = RED
	add_core = GOC
}
1428.1.1  = {
	owner = VIJ
	controller = VIJ
	revolt = { }
} # Conquered by Vijayanagar
1443.1.1  = {
	controller = ORI
	owner = ORI
} # Conquered by Gajapatis
1462.1.1  = {
	controller = VIJ
} # Reconquered by Vijayanagar
1464.1.1  = {
	owner = VIJ
} # Reconquered by Vijayanagar
1484.1.1  = {
	controller = ORI
} # Conquered by Gajapatis
1485.1.1  = {
	owner = ORI
} # Conquered by Gajapatis
1498.1.1  = { discovered_by = POR }
1505.1.1  = { discovered_by = POR }
1513.1.1  = {
	controller = VIJ
} # Substantial Vijayanagar expansion
1519.8.1 = {
	owner = VIJ
} # Substantial Vijayanagar expansion
1530.1.1 = {
	owner = GOC
	controller = GOC
	add_core = GOC
}
1565.7.1 = {
	owner = GOC
	controller = GOC
	remove_core = VIJ
} # Conquered by Golconda
1577.1.1  = { add_core = GOC }
1598.1.1  = { 
	owner = POR
	controller = POR
	fort_14th = yes
}
1600.1.1  = { discovered_by = ENG discovered_by = FRA discovered_by = NED }
1610.1.1 = {
	owner = GOC
	controller = GOC
	fort_14th = no
}
#1616.1.1  = {
#	owner = NED
#	controller = NED
#}
1650.1.1  = {
	discovered_by = turkishtech
	#add_core = NED
}
1686.1.1  = {
	owner = NED
	controller = NED
	fort_14th = yes
}
1707.5.12 = { discovered_by = GBR }
1744.1.1  = {
	owner = FRA
	controller = FRA
	fort_14th = no
	fort_17th = yes
}
1759.1.1   = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = FRA
} # Northern Circars to Britain
