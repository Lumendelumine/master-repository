# 442 - Khiva

owner = CHG
controller = CHG
culture = kwarezhmi
religion = sunni
capital = "Khiva"
trade_goods = salt
hre = no
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1111.1.1 = { post_system = yes }
1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = CHG
	add_core = KHI
}
1360.1.1   = {
	owner = KHI
	controller = KHI
}
1379.1.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1469.8.27 = {
	owner = KHI
	controller = KHI
	remove_core = TIM
}
1505.1.1 = {
	owner = SHY
	controller = SHY
} # Captured by the Shaybanid horde
1511.1.1 = {
	owner = KHI 
	controller = KHI
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1677.1.1 = { discovered_by = FRA }
