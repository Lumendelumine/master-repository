# 1050 - Sikhote

owner = MNA
controller = MNA
culture = evenki
religion = shamanism
capital = "Bohori"
trade_goods = lumber
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MNA
}
1515.1.1 = { training_fields = yes }
#1628.1.1 = {
#	owner = MCH
#	controller = MCH
#	add_core = MCH
#} # The Later Jin Khanate
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1643.1.1 = { discovered_by = RUS } # Vasily Poyarkov
1709.1.1 = { discovered_by = SPA }
1858.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
