# 2535 - Ouled Nail

owner = TLE
controller = TLE 
culture = algerian
religion = sunni
capital = "Djelfa"
trade_goods = slaves
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = soudantech

1356.1.1 = {
	add_core = TLE
	add_core = TOG
}
1515.1.1 = { training_fields = yes }
1521.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
}
1543.1.1 = {
	owner = TOG
	controller = TOG
}
1672.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
}
1852.1.1 = {
	owner = FRA
	controller = FRA
}
