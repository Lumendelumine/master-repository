
owner = SER
controller = SER
culture = serbian
religion = catholic
capital = "Cattaro"
trade_goods = fish
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
add_core = SER
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech


1088.1.1 = { dock = yes }
1355.1.1   = {
	owner = MON
	controller = MON
	add_core = MON
	remove_core = SER
} # Collapse of the Serbian empire at the death of Dusan
1420.1.1   = {
	owner = VEN
	controller = VEN
	add_core = VEN
	culture = dalmatian
}
1538.1.1   = {
	controller = TUR
}
1540.10.2  = {
	
	controller = VEN
}
1571.1.1   = {
	controller = TUR
}
1573.3.1   = {
	controller = VEN
}
1600.1.1   = {
	fort_14th = yes
}
1687.1.1   = {
	controller = TUR
}
1699.1.26   = {
	controller = VEN
}
1709.1.1   = {
	
}
1750.1.1   = {
	
}
1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	culture = serbian
} # Treaty of Campo Formio
1805.12.26 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = HAB
} # Treaty of Pressburg
1813.9.20  = { controller = HAB } # Occupied by Austrian forces
1814.4.6   = {
	owner = HAB
	add_core = HAB
	remove_core = FRA
} # Napoleon abdicates
