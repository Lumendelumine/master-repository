# 2820 - Prachinburi

owner = AYU
controller = AYU
culture = khmer
religion = buddhism
capital = "Prachinburi"
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes
trade_goods = rice

discovered_by = chinese
discovered_by = indian
hre = no

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = KHM
	add_core = AYU
}

1530.1.1 = { culture = thai remove_core = KHM }
1767.4.8 = { 
	add_core = SIA
	remove_core = AYU
}
1811.1.1 = { controller = REB } # The Siamese-Cambodian Rebellion
1812.1.1 = { controller = KHM }
1867.1.1 = {
	owner = SIA
	controller = SIA
}
