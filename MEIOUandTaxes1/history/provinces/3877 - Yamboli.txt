# 3877 - Yamboli

owner = THD
controller = THD
culture = pontic
religion = orthodox
capital = "Yamboli"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = fish
marketplace = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no
add_permanent_claim = BYZ

1356.1.1 = {
	add_core = THD
	add_core = VEN
	add_core = TRE
	add_core = GEN
}
1365.1.1 = {
	owner = GEN
	controller = GEN 
	capital = "Cembalo"
}
1461.1.1 = {
	remove_core = TRE
}
1475.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = GEN
	culture = crimean
	religion = sunni
	capital = "Baliklava"
	remove_core = VEN
} # Seized by Gedik Ahmet Pasha
1774.7.21 = {
	owner = CRI
	controller = CRI
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
	capital = "Sevastopol"
	base_tax = 6
	base_manpower = 3
} # Annexed by Catherine II
