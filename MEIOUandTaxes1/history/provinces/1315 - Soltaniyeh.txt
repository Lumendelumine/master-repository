# 1315 - Soltaniyeh
# MEIOU-GG - Mongol mod

owner = CHU
controller = CHU
culture = azerbadjani
religion = sunni #Dei Gratia
capital = "Soltaniyeh"
trade_goods = wool
hre = no
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_core = CHU
	add_core = BLU
	unrest = 4
	add_core = QAR
}
1357.1.1   = {
	owner = BLU
	controller = BLU
	remove_core = CHU
}
1360.1.1   = {
	owner = JAI
	controller = JAI
	remove_core = BLU
}
1384.1.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1408.1.1 = {
	controller = QAR
}
1409.1.1  = {
	owner = QAR
	remove_core = TIM
} # Fars and surroundings to Qara Quyunlu
1444.1.1 = {
	remove_core = JAI
	add_core = AKK
}	
1469.1.1 = {
	controller = AKK
}
1470.1.1 = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1478.1.6   = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Death of Uzun Hasan
1481.1.1   = {
	controller = AKK
	revolt = { }
} # End of civil war
1490.1.1   = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Civil War
1494.1.1   = {
	controller = AKK
	revolt = { }
} # Civil War
1501.1.1  = {
	controller = SAM
}
1508.1.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	religion = shiite
	remove_core = SAM
	remove_core = AKK
} # Safawids "form persia"
1515.1.1 = { training_fields = yes }
1550.1.1 = { fort_14th = yes }
1585.9.22  = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR } # Peace of Istanbul
1604.1.1   = { controller = PER }
1612.11.20 = { owner = PER }
1677.1.1 = { discovered_by = FRA }
1722.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1730.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
1747.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1760.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
