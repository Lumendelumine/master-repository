# 81 - N�rnberg

owner = NUR
controller = NUR
add_core = NUR
capital = "N�rnberg"
trade_goods = steel
culture = bavarian
religion = catholic
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1500.1.1    = { fort_14th = yes }
1501.1.1 = { road_network = yes }
1527.1.1    = {  } # # Philipp der Grossm�tige
1532.1.1    = { religion = protestant }
1620.1.1    = {   fort_14th = yes }
1630.1.1    = { controller = SWE }
1632.1.1    = { controller = NUR }
1685.1.1    = {  } # Reformed refugees find shelter in Kassel (-> Oberneustadt founded)
1806.1.1    = { owner = BAV controller = BAV add_core = BAV } 
1806.7.12   = { hre = no } # The Holy Roman Empire is dissolved
