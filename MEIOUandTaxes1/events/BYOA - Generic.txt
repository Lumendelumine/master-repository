# Generic Bi-Yearly On Action Events


country_event = {

	id = generic_byoa.1

	title = "EVTNAME591051"
	desc = "EVTDESC591051"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "EVTOPTA591051"
		ai_chance = { factor = 100 }
		capital_scope = {
			random_list = {
				40 = { }
				30 = { add_unrest = 0.5 }
				20 = { add_unrest = 1 }
				10 = { add_unrest = 2 }
			}
		}
		random_owned_province = {
			limit = { owner = { ai = yes } }
			owner = { capital_scope = { add_unrest = -5 } }
		}
	}
}

country_event = {

	id = generic_byoa.2

	title = "EVTNAME591052"
	desc = "EVTDESC591052"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "EVTOPTA591052"	#Discontent in capital
		ai_chance = { factor = 100 }
		random_owned_province = {
			random_list = {
				40 = { }
				30 = { add_unrest = 0.5 }
				20 = { add_unrest = 1 }
				10 = { add_unrest = 2 }
			}
		}
	}
}

country_event = {

	id = generic_byoa.3

	title = "EVTNAME591053"
	desc = "EVTDESC591053"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "EVTOPTA591053"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_religious_turmoil"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.4

	title = "EVTNAME591054"
	desc = "EVTDESC591054"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "EVTOPTA591054"
		ai_chance = { factor = 100 }
		random_owned_province = {
			add_province_modifier = {
				name = "yoa_plague"
				duration = 365
			}
		}
	}
}

country_event = {

	id = generic_byoa.5

	title = "EVTNAME591055"
	desc = "EVTDESC591055"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "EVTOPTA591055"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_financial_disaster"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.6

	title = "EVTNAME591056"
	desc = "EVTDESC591056"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_exceptional_year"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.7

	title = "EVTNAME591057"
	desc = "EVTDESC591057"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_monetary_benefits"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.8

	title = "EVTNAME591058"
	desc = "EVTDESC591058"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_production_improvements"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.9

	title = "EVTNAME591059"
	desc = "EVTDESC591059"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.BAD3"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_production_stifled"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.10

	title = "EVTNAME591060"
	desc = "EVTDESC591060"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_successful_bureaucracy"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.11

	title = "EVTNAME591061"
	desc = "EVTDESC591061"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.BAD3"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_corrupt_bureaucracy"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.12

	title = "EVTNAME591062"
	desc = "EVTDESC591062"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_happy_provinces"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.13

	title = "EVTNAME591063"
	desc = "EVTDESC591063"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.BAD3"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_unhappy_merchants"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.14

	title = "EVTNAME591064"
	desc = "EVTDESC591064"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_excellent_vintage"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.15

	title = "EVTNAME591065"
	desc = "EVTDESC591065"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.BAD3"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_poor_uniforms"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.16

	title = "EVTNAME591066"
	desc = "EVTDESC591066"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.BAD3"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_unhappy_provinces"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.17

	title = "EVTNAME591067"
	desc = "EVTDESC591067"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_happy_merchants"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.18

	title = "EVTNAME591068"
	desc = "EVTDESC591068"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.BAD3"
		ai_chance = { factor = 0 }
		add_country_modifier = {
			name = "yoa_poor_vintage"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.19

	title = "EVTNAME591069"
	desc = "EVTDESC591069"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 100 }
		add_country_modifier = {
			name = "yoa_good_uniforms"
			duration = 365
		}
	}
}

country_event = {

	id = generic_byoa.20

	title = "EVTNAME591070"
	desc = "EVTDESC591070"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	trigger = {
		exists = yes
	}

	option = {
		name = "OPT.GOOD2"
		ai_chance = { factor = 100 }
		random_list = {
			25 = { add_stability = -1 }
			50 = {  }
			25 = { add_stability = 1 }
		}
	}
}
