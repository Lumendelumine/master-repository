
# Constitutional Restoration
country_event = {
	id = governments.9479
	title = "EVTNAME9479"
	desc = "EVTDESC9479"
	picture = COURT_eventPicture
	
	trigger = {
		has_country_modifier = separation_of_powers
		government = monarchy
		NOT = {
			government = constitutional_monarchy
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "EVTOPTA9479"
		remove_country_modifier = separation_of_powers
		add_stability = -2
		random_owned_province = {
			limit = {
				is_core = ROOT
				NOT = { has_province_modifier = nobles_organizing }
			}
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				is_core = ROOT
				NOT = { has_province_modifier = nobles_organizing }
			}
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				is_core = ROOT
				NOT = { has_province_modifier = nobles_organizing }
			}
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	
	option = {
		name = "EVTOPTB9479"
		change_government = constitutional_monarchy
		add_stability = 1
	}
}

# The worst excesses stop
country_event = {
	id = governments.9480
	title = "EVTNAME9480"
	desc = "EVTDESC9480"
	picture = CITY_DEVELOPMENT_eventPicture
	
	trigger = {
		NOT = {
			government = revolutionary_republic
		}
		has_country_modifier = cult_of_reason
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "EVTOPTA9480"
		add_stability = 1
		remove_country_modifier = cult_of_reason
	}
}

# Free City status lost (for non Common Sense players)
country_event = {
	id = governments.9900
	title = free_cities.2.t
	desc = free_cities.2.d
	picture = HRE_eventPicture
	
	trigger = {
		NOT = { has_dlc = "Common Sense" }
		num_of_cities = 2
		government = imperial_city
	}
	
	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = free_cities.2.a
		change_government = oligarchic_republic
	}
}
