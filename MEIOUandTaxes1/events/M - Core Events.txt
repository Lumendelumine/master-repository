##########################
#
#0861501 - Core Events
#
##########################
#
# 2011-jun-05	Fuzzbug		new file created woth event 861501
# 2011-jul-14	Fuzzbug		fine tuning of modifiers based on feedback on PI forum
# 2011-jul-27	Fuzzbug		added any_neighbor_province is_core MTTH modifier to 861501
# 2011-dec-25	Gigau		split core gaining process into 2 steps (i.e. split 0861501 into two events)
# 2011-dec-26	Fuzzbug		comments and corrections
# 2012-jan-10	Masakary	added a 3rd event to remove modifier from cored provinces
# 2014-apr-22	Gigau		Ported to MEIOU and Taxes
#
##########################


# regarding_cores.1 : First step in gaining Core - allow well run countries to earn cores much quicker
# (the default in MEIOU is 300 years)
province_event = {
	id = regarding_cores.1
	title = "regarding_cores.1.n"
	desc = "regarding_cores.1.t"
	picture = FLANDERS_eventPicture

	hidden = yes
	
	trigger = {
		NOT = { is_core = owner }
		owner = { stability = 0 }
		controlled_by = owner
		NOT = { has_siege = yes }
		has_construction = core
	}

	mean_time_to_happen = {
		# months = 600	#50 years
		months = 60		#5 years
		### CULTURE ###
		modifier = {
			factor = 0.4 
			has_owner_culture = yes
		}
		modifier = {
			factor = 0.6
			has_owner_accepted_culture = yes
		}
		modifier = {
			factor = 0.8
			NOT = {
				has_owner_culture = yes
				has_owner_accepted_culture = yes
			}
			owner = { culture_group = ROOT }
		}
		
		### RELIGION ###
		modifier = {
			factor = 0.6
			has_owner_religion = yes
		}
		modifier = {
			factor = 0.9
			NOT = { has_owner_religion = yes } 
			owner = { religion_group = ROOT }
			tolerance_to_this = 1
		}
		modifier = {
			factor = 0.9
			NOT = { has_owner_religion = yes } 
			owner = { religion_group = ROOT }
			tolerance_to_this = 2
		}
		modifier = {
			factor = 0.9
			NOT = { has_owner_religion = yes } 
			owner = { religion_group = ROOT }
			tolerance_to_this = 3
		}
		modifier = {
			factor = 0.95
			NOT = { owner = { religion_group = ROOT } }
			tolerance_to_this = 1
		}
		modifier = {
			factor = 0.95
			NOT = { owner = { religion_group = ROOT } }
			tolerance_to_this = 2
		}
		modifier = {
			factor = 0.95
			NOT = { owner = { religion_group = ROOT } }
			tolerance_to_this = 3
		}
		modifier = {
			factor = 1.1
			NOT = { tolerance_to_this = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { tolerance_to_this = -1 }
		}
		modifier = {
			factor = 1.1
			NOT = { tolerance_to_this = -2 }
		}
		modifier = {
			factor = 0.9
			has_owner_religion = yes
			owner = { at_war_with_religious_enemy = yes }
		}
		modifier = {
			factor = 2.0
			religion = catholic
			owner = { is_excommunicated = yes }
			PAP = { check_variable = { which = papal_authority value = 500 } } #A strong pope
		}
		modifier = {
			factor = 1.5 
			religion = catholic
			owner = { is_excommunicated = yes }
			PAP = { NOT = { check_variable = { which = papal_authority value = 500 } } }
			PAP = { check_variable = { which = papal_authority value = 400 } } #An average pope
		}
		modifier = {
			factor = 1.0
			religion = catholic
			owner = { is_excommunicated = yes }
			PAP = { NOT = { check_variable = { which = papal_authority value = 400 } } }
			PAP = { check_variable = { which = papal_authority value = 300 } } #A weak pope
		}
		modifier = {
			factor = 0.85
			religion = catholic
			owner = { is_papal_controller = yes }
			PAP = { check_variable = { which = papal_authority value = 500 } } #A strong pope
		}
		modifier = {
			factor = 0.9
			religion = catholic
			owner = { is_papal_controller = yes }
			PAP = { NOT = { check_variable = { which = papal_authority value = 500 } } }
			PAP = { check_variable = { which = papal_authority value = 400 } } #An average pope
		}
		modifier = {
			factor = 0.95
			religion = catholic
			owner = { is_papal_controller = yes }
			PAP = { NOT = { check_variable = { which = papal_authority value = 400 } } }
			PAP = { check_variable = { which = papal_authority value = 300 } } #A weak pope
		}
		
		### CORES&CLAIMS ###
		modifier = {
			factor = 0.5
			is_claim = owner
		}		
		modifier = {
			factor = 0.8
			any_neighbor_province = {
				owned_by = ROOT
				is_core = ROOT
			}
		}
		
		### COLONIES&OVERSEAS ###
		modifier = {
			factor = 0.5
			is_colony = yes
		}
		modifier = {
			factor = 0.7
			is_owned_by_trade_company = yes
			is_overseas = yes
		}
		modifier = {
			factor = 0.5
			owner = { is_colonial_nation = yes }
			is_overseas = no
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { religion_group = new_world_pagan } }
			religion_group = new_world_pagan
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { religion_group = pagan } }
			religion_group = pagan
			NOT = { religion = baltic_pagan_reformed }
		}
		
		### HRE ###
		modifier = {
			factor = 0.9
			is_part_of_hre = yes
			owner = { capital_scope = { is_part_of_hre = yes } }
		}
		modifier = {
			factor = 0.9
			is_part_of_hre = yes
			owner = { is_emperor = yes }
		}
		modifier = {
			factor = 0.9
			is_part_of_hre = yes
			owner = { is_elector = yes }
		}
		
		
		
		### GLOBAL FACTORS ###
		modifier = {
			factor = 1.2
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0.9
			owner = { stability = 1 }
		}
		modifier = {
			factor = 0.9
			owner = { stability = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { stability = 3 }
		}		
		modifier = {
			factor = 0.9
			owner = { luck = yes }
		}
		modifier = {
			factor = 0.95
			owner = { prestige = 20 }
		}
		modifier = {
			factor = 0.95
			owner = { prestige = 40 }
		}
		modifier = {
			factor = 0.95
			owner = { prestige = 60 }
		}
		modifier = {
			factor = 0.95
			owner = { prestige = 80 }
		}
		modifier = {
			factor = 0.95
			owner = { prestige = 100 }
		}
		modifier = {
			factor = 1.2
			owner = { war_exhaustion = 5 }
		}
		modifier = {
			factor = 1.2
			owner = { war_exhaustion = 10 }
		}
		modifier = {
			factor = 1.2
			owner = { war_exhaustion = 15 }
		}
		modifier = {
			factor = 1.2
			owner = { war_exhaustion = 20 }
		}
		
		### BIG PLAYERS ###
		modifier = {
			factor = 0.5
			owner = { tag = FRA }
			OR = {
				region = france_region
				region = provence_region
				region = languedoc_region
				region = brittany_region
				region = high_countries_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { tag = GBR }
			OR = { region = england_region region = scotland_region region = ireland_region }
		}
		modifier = {
			factor = 0.5
			owner = { tag = SPA }
			OR = {
				region = castille_region
				region = aragon_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { tag = GER }
			OR = {
				region = lower_saxon_circle_region
				region = upper_saxon_circle_region
				region = westphalian_circle_region
				region = upper_rhenish_circle_region
				region = swabian_circle_region
				region = bavarian_circle_region
				region = franconian_circle_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { tag = ITA }
			region = italy_region
		}
		modifier = {
			factor = 0.5
			owner = { tag = LOT }
			OR = {
				region = high_countries_region
				region = belgii_region
				region = low_countries_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { tag = NED }
			OR = {
				region = belgii_region
				region = low_countries_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { tag = TUR }
			OR = {
				region = egypt_region
				near_east_region_trigger = yes
			}
		}
		modifier = {
			factor = 0.5
			owner = { has_country_flag = rum_sultanate_claimed }
			OR = {
				region = coastal_anatolia_region
				region = inland_anatolia_region
				region = east_balkan_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { tag = BYZ }
			OR = {
				region = greece_region
				region = coastal_anatolia_region
				region = inland_anatolia_region
			}
		}
		modifier = {
			factor = 0.5
			owner = { has_country_flag = rus_formed }
			region = russia_region
		}
		modifier = {
			factor = 0.5
			owner = { tag = PLC }
			OR = {
				region = poland_region
				region = ruthenia_region
			}
		}

		
		### PROVINCIAL ADMNISTRATION ###
		modifier = {
			factor = 0.75
			has_province_modifier = "efficient_administration"
		}
		modifier = {
			factor = 0.80
			has_province_modifier = "quite_efficient_administration"
		}
		modifier = {
			factor = 0.85
			has_province_modifier = "rather_efficient_administration"
		}
		modifier = {
			factor = 0.90
			has_province_modifier = "slightly_efficient_administration"
		}
		modifier = {
			factor = 0.95
			has_province_modifier = "average_administration"
		}
		
		### CORING COST MODIFIER ###
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.95
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.9
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.85
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.8
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.75
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.7
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.65
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.6
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.55
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.5
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.45
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.4
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.35
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.3
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.25
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.2
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.15
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.1
					}
				} 
			}
		}
		modifier = {
			factor = 0.95
			owner = { 
				NOT = {
					has_global_modifier_value = {
						which = core_creation
						value = -0.05
					}
				} 
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.05
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.1
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.15
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.2
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.25
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.3
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.35
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.4
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.45
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.5
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.55
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.6
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.65
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.7
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.75
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.8
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.85
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.9
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 0.95
				}
			}
		}
		modifier = {
			factor = 1.05
			owner = { 
				has_global_modifier_value = {
					which = core_creation
					value = 1.0
				}
			}
		}
		
		### LOCAL AUTONOMY ###
		modifier = {
			factor = 1.15
			local_autonomy = 90
		}
		modifier = {
			factor = 1.10
			local_autonomy = 80
		}
		modifier = {
			factor = 1.05
			local_autonomy = 70
		}
		modifier = {
			factor = 1.05
			local_autonomy = 60
		}	
		modifier = {
			factor = 0.95
			NOT = { local_autonomy = 50 } 
		}		
		modifier = {
			factor = 0.95
			NOT = { local_autonomy = 40 } 
		}
		modifier = {
			factor = 0.90
			NOT = { local_autonomy = 30 } 
		}
		modifier = {
			factor = 0.85
			NOT = { local_autonomy = 20 } 
		}
		modifier = {
			factor = 0.80
			NOT = { local_autonomy = 10 } 
		}
		
		### NATIONALISM ###
		modifier = {
			factor = 1.3
			nationalism = 4
		}
		modifier = {
			factor = 1.3
			nationalism = 8
		}
		modifier = {
			factor = 1.3
			nationalism = 12
		}
		modifier = {
			factor = 1.3
			nationalism = 16
		}
		modifier = {
			factor = 1.3
			nationalism = 20
		}
		modifier = {
			factor = 1.3
			nationalism = 24
		}
		modifier = {
			factor = 1.3
			nationalism = 28
		}
	}

	option = {
		name = "OPT.EXCELLENT"
		trigger = {
			NOT = { has_province_modifier = "gaining_control" }
		}
		province_event = { id = regarding_cores.2 days = 0 }
	}
	
	option = {
		name = "OPT.EXCELLENT"
		trigger = {
			has_province_modifier = "gaining_control"
		}
		province_event = { id = regarding_cores.3 days = 0 }
	}
}

# regarding_cores.2 : Gaining control notification
province_event = {
	id = regarding_cores.2
	title = "regarding_cores.2.n"
	desc = "regarding_cores.2.t"
	picture = FLANDERS_eventPicture

	is_triggered_only = yes

	option = {
		name = "OPT.EXCELLENT"
		add_province_modifier = {
			name = "gaining_control"
			duration = -1
		}
	}
}

# regarding_cores.3 : Core gained notification
province_event = {
	id = regarding_cores.3
	title = "regarding_cores.3.n"
	desc = "regarding_cores.3.t"
	picture = FLANDERS_eventPicture

	is_triggered_only = yes

	immediate = { #prevent abuse
		add_core = owner
	}
	
	option = {
		name = "OPT.EXCELLENT"
		tooltip = { add_core = owner } #prevent abuse
		reduce_local_autonomy_times5_effect = yes
		if = {
			limit = { owner = { has_country_flag = rising_nation ai = yes } }
			add_local_autonomy = -25
		}
		remove_province_modifier = "gaining_control"
	}
}

# regarding_cores.4 : Losing cores
country_event = {
	id = regarding_cores.4
	title = "regarding_cores.4.n"
	desc = "regarding_cores.4.t"
	picture = REVOLUTION_eventPicture

	trigger = {
		NOT = { prestige = -90 }
	}

	mean_time_to_happen = {
		months = 200
	}

	option = {
		name = "OPT.YIELD"
		every_country = {
			every_owned_province = {
				limit = {
					NOT = { owned_by = ROOT }
					NOT = { culture = ROOT }
					is_core = ROOT
				}
				remove_core = ROOT
			}
		}
		add_prestige = 90
	}
}
