namespace = DO_cot_development

country_event = { ###This Event Adds Development to Coastal Centers of Trade
	id = DO_cot_development.001
	title = DO_cot_development.001.t
	desc = DO_cot_development.001.d
	picture = HARBOUR_eventPicture
	hidden = yes
	
	trigger = {	any_owned_province = {
					OR = {
						has_province_modifier = minor_inland_center_of_trade_modifier
						has_province_modifier = minor_center_of_trade_modifier
						has_province_modifier = center_of_trade_modifier  
						has_province_modifier = inland_center_of_trade_modifier 
						has_province_modifier = natural_harbour 
						has_province_modifier = mediterranean_harbour }
				}
				NOT = { has_country_flag = show_development }
	}
	mean_time_to_happen = {    
		days = 7300
					
############# Positive Modifiers ###############		
		
		
		modifier = {
			factor = 0.75
			government = republic 
		}
		
		modifier = {
			factor = 0.5
			has_active_policy = promote_our_centers_of_trade
		}
		
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.05
					}
			}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.10
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.15
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.20
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.25
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.30
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.35
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.40
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.45
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.50
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.55
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.60
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.65
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.70
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.75
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.80
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.85
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.90
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.95
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.00
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.10
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.20
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.30
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.40
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.50
					}
				}
		}
		
######## Negative Modifiers ########		
		
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.05
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.1
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.15
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.2
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.25
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.3
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.35
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.4
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.45
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.5
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.55
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.6
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.65
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.7
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.75
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.8
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.85
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.9
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.95
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.1
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.2
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.3
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.4
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.5
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.6
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.7
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.8
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.9
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 2
				}
		}
	}	
			
	option = {					
		name = "DO_cot_development.001.a"
		every_owned_province = {
			if = {	
				limit = { has_province_modifier = mediterranean_harbour }
				random_list = {
						2 = { add_base_manpower = 1 }
						4 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						92 = { }
					}
			}	
			
			if = {
				limit = { has_province_modifier = center_of_trade_modifier }
					random_list = {
						5 = { add_base_manpower = 1 }
						10 = { add_base_tax = 1 }
						5 = { add_base_production = 1 }
						80 = { }
					}
			}
			if = {
				limit = { has_province_modifier = inland_center_of_trade_modifier }
					random_list = {
						5 = { add_base_manpower = 1 }
						10 = { add_base_tax = 1 }
						5 = { add_base_production = 1 }
						80 = { }
					}
			}
			if = {
				limit = { has_province_modifier = minor_inland_center_of_trade_modifier }
					random_list = {
						3 = { add_base_manpower = 1 }
						7 = { add_base_tax = 1 }
						3 = { add_base_production = 1 }
						87 = { }
					}
			}
			if = {
				limit = { has_province_modifier = natural_harbour }
					random_list = {
						3 = { add_base_manpower = 1 }
						7 = { add_base_tax = 1 }
						3 = { add_base_production = 1 }
						87 = { }
					}
			}
			if = {
				limit = { has_province_modifier = minor_center_of_trade_modifier }
					random_list = {
						3 = { add_base_manpower = 1 }
						7 = { add_base_tax = 1 }
						3 = { add_base_production = 1 }
						87 = { }
					}
			}
			if = {
				limit = {
					any_neighbor_province = {
						has_province_modifier = center_of_trade_modifier
						owned_by = ROOT}  
				}	
				set_province_flag = neighbor_select
				random_list = {
					2 = { add_base_manpower = 1 }
					4 = { add_base_tax = 1 }
					2 = { add_base_production = 1 }
					92 = {}
				}
			}
			if = { 
				limit = {
					owner = { has_active_policy = promote_our_centers_of_trade }
				}
				if = {
					limit = {
						any_neighbor_province = {
							has_province_modifier = minor_inland_center_of_trade_modifier
							owned_by = ROOT
						}
					} 
					random_list = {
						2 = { add_base_manpower = 1 }
						4 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						92 = {}
					}
				}
				if = {
					limit = {
						any_neighbor_province = {
							has_province_modifier = minor_center_of_trade_modifier
							owned_by = ROOT
						}  
					}	
					random_list = {
						2 = { add_base_manpower = 1 }
						3 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						93 = {}
					}
				}
				if = {
					limit = {
						any_neighbor_province = {
							has_province_modifier = natural_harbour 
							owned_by = ROOT
						}  
					}	
					random_list = {
						2 = { add_base_manpower = 1 }
						3 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						93 = {}
					}
				}
			}
		}
		every_owned_province = { clr_province_flag = neighbor_select }
	}
}

country_event = { ###This Event Adds Development to Coastal Centers of Trade
	id = DO_cot_development.002
	title = DO_cot_development.002.t
	desc = DO_cot_development.002.d
	picture = HARBOUR_eventPicture
	hidden = no
	
	trigger = {	any_owned_province = {
					OR = {
						has_province_modifier = minor_inland_center_of_trade_modifier
						has_province_modifier = minor_center_of_trade_modifier
						has_province_modifier = center_of_trade_modifier  
						has_province_modifier = inland_center_of_trade_modifier 
						has_province_modifier = natural_harbour }
				}
				has_country_flag = show_development
	}
	mean_time_to_happen = {    
		days = 7300
					
############# Positive Modifiers ###############		
		
		
		modifier = {
			factor = 0.5
			has_active_policy = promote_our_centers_of_trade
		}
		
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.05
					}
			}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.10
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.15
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.20
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.25
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.30
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.35
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.40
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.45
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.50
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.55
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.60
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.65
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.70
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.75
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.80
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.85
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.90
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -0.95
					}
				}
		}
		modifier = {
			factor = 0.95
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.00
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.10
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.20
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.30
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.40
					}
				}
		}
		modifier = {
			factor = 0.90
			NOT = {
					has_global_modifier_value = {
						which = development_cost
						value = -1.50
					}
				}
		}
		
######## Negative Modifiers ########		
		
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.05
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.1
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.15
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.2
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.25
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.3
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.35
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.4
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.45
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.5
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.55
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.6
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.65
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.7
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.75
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.8
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.85
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.9
				}
		}
		modifier = {
			factor = 1.05
				has_global_modifier_value = {
					which = development_cost
					value = 0.95
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.1
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.2
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.3
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.4
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.5
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.6
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.7
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.8
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 1.9
				}
		}
		modifier = {
			factor = 1.10
				has_global_modifier_value = {
					which = development_cost
					value = 2
				}
		}
	}	
			
	option = {					
		name = "DO_cot_development.002.a"
		every_owned_province = {
			if = {	
				limit = { has_province_modifier = mediterranean_harbour }
				random_list = {
						2 = { add_base_manpower = 1 }
						4 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						92 = { }
					}
			}	
			
			if = {
				limit = { has_province_modifier = center_of_trade_modifier }
					random_list = {
						5 = { add_base_manpower = 1 }
						10 = { add_base_tax = 1 }
						5 = { add_base_production = 1 }
						80 = { }
					}
			}
			if = {
				limit = { has_province_modifier = inland_center_of_trade_modifier }
					random_list = {
						5 = { add_base_manpower = 1 }
						10 = { add_base_tax = 1 }
						5 = { add_base_production = 1 }
						80 = { }
					}
			}
			if = {
				limit = { has_province_modifier = minor_inland_center_of_trade_modifier }
					random_list = {
						3 = { add_base_manpower = 1 }
						7 = { add_base_tax = 1 }
						3 = { add_base_production = 1 }
						87 = { }
					}
			}
			if = {
				limit = { has_province_modifier = natural_harbour }
					random_list = {
						3 = { add_base_manpower = 1 }
						7 = { add_base_tax = 1 }
						3 = { add_base_production = 1 }
						87 = { }
					}
			}
			if = {
				limit = { has_province_modifier = minor_center_of_trade_modifier }
					random_list = {
						3 = { add_base_manpower = 1 }
						7 = { add_base_tax = 1 }
						3 = { add_base_production = 1 }
						87 = { }
					}
			}
			if = {
				limit = {
					any_neighbor_province = {
						has_province_modifier = center_of_trade_modifier
						owned_by = ROOT}  
				}	
				set_province_flag = neighbor_select
				random_list = {
					2 = { add_base_manpower = 1 }
					4 = { add_base_tax = 1 }
					2 = { add_base_production = 1 }
					92 = {}
				}
			}
			if = { 
				limit = {
					owner = { has_active_policy = promote_our_centers_of_trade }
				}
				if = {
					limit = {
						any_neighbor_province = {
							has_province_modifier = minor_inland_center_of_trade_modifier
							owned_by = ROOT
						}
					} 
					random_list = {
						2 = { add_base_manpower = 1 }
						4 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						92 = {}
					}
				}
				if = {
					limit = {
						any_neighbor_province = {
							has_province_modifier = minor_center_of_trade_modifier
							owned_by = ROOT
						}  
					}	
					random_list = {
						2 = { add_base_manpower = 1 }
						3 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						93 = {}
					}
				}
				if = {
					limit = {
						any_neighbor_province = {
							has_province_modifier = natural_harbour 
							owned_by = ROOT
						}  
					}	
					random_list = {
						2 = { add_base_manpower = 1 }
						3 = { add_base_tax = 1 }
						2 = { add_base_production = 1 }
						93 = {}
					}
				}
			}
		}
		every_owned_province = { clr_province_flag = neighbor_select }
	}
}


#every_owned_province = {
#			if = {
#				limit = {
#					owner = { has_active_policy = promote_our_centers_of_trade }
#					any_neighbor_province = {
#						any_neighbor_province = {
#							has_province_modifier = center_of_trade_modifier
#							owned_by = ROOT  
#							NOT = { has_province_flag = neighbor_select 
#							has_province_modifier = center_of_trade_modifier 
#							has_province_modifier = inland_center_of_trade_modifier } } } } 
#							random_list = {
#								2 = { add_base_manpower = 1 }
#								3 = { add_base_tax = 1 }
#								2 = { add_base_production = 1 }
#								93 = {}
#							}
#						
#					
#			}
#		}