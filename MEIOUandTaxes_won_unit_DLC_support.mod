name="M&T Wealth of Nations Western Ships DLC Support v2.0"
path="mod/MEIOUandTaxes_won_unit_DLC_support"
picture="MEIOUandTaxesWoN.jpg"
supported_version="1.15"
dependencies={
	"MEIOU and Taxes 1.23"
}
tags={
	"MEIOU and Taxes"
}
